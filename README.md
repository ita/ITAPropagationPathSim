## ITAPropagationPathSim

ITAPropagationPathSim is a C++ library to simulate acoustic propagation using the principle of geometrical acoustics.
It includes two simulation approaches:
1) Interaction with geometric objects (such as buildings in urban areas)
  - Considering reflection and diffraction
  - Assuming straight path propagation
2) Propagation through stratified atmosphere (inhomogeneous and moving medium)
  - Considering the effects of refraction and advection
  - Neglecting the presence of geometric objects besides a flat ground

This library is is part of [ITAGeometricalAcousics](https://git.rwth-aachen.de/ita/ITAGeometricalAcousics), a collection of C++ libraries for geometrical acoustics simulation.


### License

Copyright 2015-2024 Institute of Technical Acoustics, RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


### Quick build guide

It is recommended to clone and follow the build guide of the parent project [ITAGeometricalAcoustics](https://git.rwth-aachen.de/ita/ITAGeometricalAcoustics), which includes this project as a submodule.
