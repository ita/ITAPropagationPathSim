/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/ModelBase.h>
#include <ITAGeo/Scene.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAPropagationPathSim/UrbanEngine/UrbanDiffraction.h>
#include <ITAPropagationPathSim/UrbanEngine/UrbanImageSource.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

int main( int iNumInArgs, char* ppcInArgs[] )
{
	string sLocalFileBaseName;
	if( iNumInArgs > 1 )
		sLocalFileBaseName = string( ppcInArgs[1] );
	else
		sLocalFileBaseName = "SimpleBuildingsUrban";
	string sExtension = "skp";

	string sSKPFilePathInput = sLocalFileBaseName + "." + sExtension;
	VistaFileSystemFile oFile( sSKPFilePathInput );
	assert( oFile.Exists( ) );
	string sSKPFilePathOutput_Vis = oFile.GetParentDirectory( ) + "/" + sLocalFileBaseName + "_Vis" + "." + sExtension;

	// Source
	auto pSource     = std::make_shared<CEmitter>( );
	pSource->sName   = "My Diffraction Source";
	pSource->qOrient = VistaQuaternion( 0, -1, 0, 0 );
	pSource->vPos.SetValues( 1.7f, 0.66f, 0.11f );
	// pSource->vPos.SetValues(0.6f, 1.32f, 0.11f);

	cout << pSource->ToString( ) << endl;

	// Receiver
	auto pReceiver     = std::make_shared<CSensor>( );
	pReceiver->sName   = "My Diffraction Receiver";
	pReceiver->qOrient = VistaQuaternion( 0, 1, 0, 0 );
	pReceiver->vPos.SetValues( -2.2f, 0.33f, 0.22f );

	cout << pReceiver->ToString( ) << endl;

	// Urban model
	auto pUrbanModel = make_shared<ITAGeo::Urban::CModel>( );

	// Load urban model
	try
	{
		cout << "Loading model from file path '" << sSKPFilePathInput << "'" << endl;
		if( pUrbanModel->Load( oFile.GetName( ) ) )
			cout << "Successfully loaded file '" << oFile.GetLocalName( ) << "' as urban model" << endl;
		else
			ITA_EXCEPT1( INVALID_PARAMETER, "Could not load geometry from file " + oFile.GetLocalName( ) );
	}
	catch( const ITAException& e )
	{
		cerr << "[ ERROR ] " << e << endl;
		return 255;
	}

	// Sketchup model for saving results in sketchup file
	auto pSUModel = make_shared<SketchUp::CModel>( );

	// Load SU model
	try
	{
		cout << "Loading model from file path '" << sSKPFilePathInput << "'" << endl;
		if( pSUModel->Load( oFile.GetName( ) ) )
			cout << "Successfully loaded file '" << oFile.GetLocalName( ) << "' as su model" << endl;
		else
			ITA_EXCEPT1( INVALID_PARAMETER, "Could not load geometry from file " + oFile.GetLocalName( ) );
	}
	catch( const ITAException& e )
	{
		cerr << "[ ERROR ] " << e << endl;
		return 255;
	}

	// Urban Diffraction engine
	ITAPropagationPathSim::UrbanEngine::CDiffractionPath oUrbanDiffraction( *pUrbanModel, DiffractionOrder::DIFFRACTION_ORDER_3 );

	oUrbanDiffraction.ConstructDiffractionTree( pSource.get( ) );

	CPropagationPathList oPathListAll, oPathListVisible;
	oUrbanDiffraction.ConstructDiffractionPaths( pReceiver, oPathListAll );

	// Get the visible sound paths
	pUrbanModel->FilterVisiblePaths( oPathListAll, oPathListVisible );

	// Add paths to visualization
	for( auto oPath: oPathListVisible )
	{
		string sOrderSize = to_string( oPath.size( ) - 2 );
		pSUModel->AddPropagationPathVisualization( oPath, "DiffractionPath_vis_order_" + sOrderSize );
	}
	for( auto oPath: oPathListAll )
	{
		pSUModel->AddPropagationPathVisualization( oPath, "DiffractionPath_all" );
	}

	// Add emitter and sensor to visualization
	pSUModel->AddEmitterVisualization( *pSource, "Source A" );
	pSUModel->AddSensorVisualization( *pReceiver, "Receiver A" );

	// Store model
	pSUModel->Store( sLocalFileBaseName + "_vis." + sExtension );

	return 0;
}