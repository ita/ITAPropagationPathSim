#include "test_definitions.h"

#include <ITAGeo/Model.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>

ITAGeo::CPropagationPathList runSimulation( const SSimulationParameters parameters )
{
	auto pModel = std::make_shared<ITAGeo::CModel>( );

	auto pMaterialManager    = std::make_shared<ITAGeo::Material::CMaterialManager>( );
	auto pDirectivityManager = std::make_shared<ITAGeo::Directivity::CDirectivityManager>( );
	auto pResourceManager    = std::make_shared<ITAGeo::CResourceManager>( pMaterialManager, pDirectivityManager );

	// Config
	ITAPropagationPathSim::CombinedModel::CPathEngine::CSimulationConfig oSimConfig; // Auto-default
	ITAPropagationPathSim::CombinedModel::CPathEngine::CAbortionCriteria oSimAbort;  // Auto-default

	oSimAbort.iMaxDiffractionOrder = parameters.diffraction_order;
	oSimAbort.iMaxReflectionOrder  = parameters.reflection_order;
	oSimAbort.iMaxCombinedOrder    = parameters.diffraction_order + parameters.reflection_order;

	oSimConfig.iNumberIterationApexCalculation = 10;
	oSimConfig.bFilterIntersectedPaths         = true;

	pModel->SetMaterialManager( pMaterialManager );

	REQUIRE( pModel->Load( parameters.file_path ) );

	ITAPropagationPathSim::CombinedModel::CPathEngine oPathEngine;
	oPathEngine.SetSimulationConfiguration( oSimConfig );
	oPathEngine.SetAbortionCriteria( oSimAbort );

	oPathEngine.InitializePathEnvironment( pModel->GetOpenMesh( ) );

	auto source_position   = parameters.source_position;
	auto receiver_position = parameters.receiver_position;

	oPathEngine.SetEntities( std::make_shared<ITAGeo::CEmitter>( source_position ), std::make_shared<ITAGeo::CSensor>( receiver_position ) );

	ITAGeo::CPropagationPathList oPathList;
	oPathEngine.ConstructPropagationPaths( oPathList );

	return oPathList;
}

TEST_CASE( "ITAPropagationPathSim::IEM/direct-sound", "[ITAPropagationPathSim][IEM][Required]" )
{
	const std::vector<std::string> test_mesh_files {
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
		"FreeField.skp",
#endif
		"FreeField.dae", "FreeField.obj"
	};

	for( auto&& test_mesh_file: test_mesh_files )
	{
		DYNAMIC_SECTION( "Testing Mesh " << test_mesh_file )
		{
			SSimulationParameters parameters;
			parameters.file_path         = std::string( ITAPROPSIM_TEST_RESOURCE_DIR ) + "/" + test_mesh_file;
			parameters.source_position   = generateRandomVec3( );
			parameters.receiver_position = generateRandomVec3( );

			auto oPathList = runSimulation( parameters );

			REQUIRE( oPathList.size( ) == 1 );

			const auto direct_path = oPathList.at( 0 );

			checkDirectSound( direct_path, parameters );
		}
	}
}

TEST_CASE( "ITAPropagationPathSim::IEM/ground-reflection", "[ITAPropagationPathSim][IEM][Required]" )
{
	const std::vector<std::string> test_mesh_files {
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
		"Ground.skp",
#endif
		"Ground.dae", "Ground.obj"
	};

	for( auto&& test_mesh_file: test_mesh_files )
	{
		DYNAMIC_SECTION( "Testing Mesh " << test_mesh_file )
		{
			SSimulationParameters parameters;
			parameters.file_path         = std::string( ITAPROPSIM_TEST_RESOURCE_DIR ) + "/" + test_mesh_file;
			parameters.source_position   = generateRandomVec3AboveGround( );
			parameters.receiver_position = generateRandomVec3AboveGround( );

			INFO( "Source position: " << parameters.source_position );
			INFO( "Receiver position: " << parameters.receiver_position );

			auto oPathList = runSimulation( parameters );

			REQUIRE( oPathList.size( ) == 2 );

			const auto direct_path     = oPathList.at( 0 );
			const auto reflection_path = oPathList.at( 1 );

			checkDirectSound( direct_path, parameters );
			checkGroundReflection( reflection_path, parameters );
		}
	}
}

TEST_CASE( "ITAPropagationPathSim::IEM/wedge-diffraction", "[ITAPropagationPathSim][IEM][Required]" )
{
	const std::vector<std::string> test_mesh_files {
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
		"Wedge.skp",
#endif
		"Wedge.dae", "Wedge.obj", "WedgeDualMaterial.dae"
	};

	for( auto&& test_mesh_file: test_mesh_files )
	{
		DYNAMIC_SECTION( "Testing Mesh " << test_mesh_file )
		{
			SSimulationParameters parameters;
			parameters.file_path         = std::string( ITAPROPSIM_TEST_RESOURCE_DIR ) + "/" + test_mesh_file;
			parameters.source_position   = generateRandomVec3Wedge( Type::source );
			parameters.receiver_position = generateRandomVec3Wedge( Type::receiver );

			INFO( "Source position: " << parameters.source_position );
			INFO( "Receiver position: " << parameters.receiver_position );

			auto oPathList = runSimulation( parameters );

			REQUIRE( oPathList.size( ) == 1 );

			const auto diffracted_path = oPathList.at( 0 );

			checkDiffraction( diffracted_path, parameters );
		}
	}
}