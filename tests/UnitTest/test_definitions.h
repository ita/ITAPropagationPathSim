#ifndef INCLUDE_WATCHER_ITA_UNIT_TEST_DEFINITIONS
#define INCLUDE_WATCHER_ITA_UNIT_TEST_DEFINITIONS

#include <ITAException.h>
#include <ITAGeo/Utils.h>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_translate_exception.hpp>
#include <catch2/generators/catch_generators_all.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>
#include <fakeit.hpp>
#include <limits>

constexpr int MAX_REPS = 5;

enum class Type
{
	source,
	receiver
};

CATCH_TRANSLATE_EXCEPTION( const ITAException& ex )
{
	return ex.ToString( );
}

#define GENERATE_RANDOM( type, reps ) GENERATE_COPY( take( reps, random( std::numeric_limits<type>::min( ), std::numeric_limits<type>::max( ) ) ) )

inline VistaVector3D generateRandomVec3( bool repetitions = true )
{
	VistaVector3D vec;
	vec[0] = GENERATE_RANDOM( float, repetitions ? MAX_REPS : 1 );
	vec[1] = GENERATE_RANDOM( float, 1 );
	vec[2] = GENERATE_RANDOM( float, 1 );

	return vec;
}

inline VistaVector3D generateRandomVec3AboveGround( bool repetitions = true )
{
	VistaVector3D vec;
	vec[0] = GENERATE_COPY( take( repetitions ? MAX_REPS : 1, random( -50.f, 50.f ) ) );
	vec[1] = GENERATE_COPY( take( 1, random( 0.f, 50.f ) ) );
	vec[2] = GENERATE_COPY( take( 1, random( -50.f, 50.f ) ) );

	return vec;
}


inline VistaVector3D generateRandomVec3Wedge( Type type, bool repetitions = true )
{
	VistaVector3D vec;
	if( type == Type::source )
	{
		vec[0] = GENERATE_COPY( take( repetitions ? MAX_REPS : 1, random( -50.f, 50.f ) ) );
		vec[1] = GENERATE_COPY( take( 1, random( -30.f, -1.f ) ) );
		vec[2] = GENERATE_COPY( take( 1, random( -50.f, -15.f ) ) );
	}
	else
	{
		vec[0] = GENERATE_COPY( take( repetitions ? MAX_REPS : 1, random( -50.f, 50.f ) ) );
		vec[1] = GENERATE_COPY( take( 1, random( -30.f, -1.f ) ) );
		vec[2] = GENERATE_COPY( take( 1, random( 15.f, 50.f ) ) );
	}

	return vec;
}

struct SSimulationParameters
{
	std::string file_path;
	VistaVector3D source_position;
	VistaVector3D receiver_position;
	int reflection_order  = 1;
	int diffraction_order = 1;
};

inline void checkDirectSound( const ITAGeo::CPropagationPath direct_path, const SSimulationParameters parameters )
{
	REQUIRE( direct_path.size( ) == 2 );

	REQUIRE( direct_path.at( 0 )->iAnchorType == ITAGeo::CPropagationAnchor::ACOUSTIC_EMITTER );
	REQUIRE( direct_path.at( 1 )->iAnchorType == ITAGeo::CPropagationAnchor::ACOUSTIC_SENSOR );

	REQUIRE( direct_path.at( 0 )->v3InteractionPoint == parameters.source_position );
	REQUIRE( direct_path.at( 1 )->v3InteractionPoint == parameters.receiver_position );
}

inline void checkGroundReflection( const ITAGeo::CPropagationPath reflection_path, const SSimulationParameters parameters )
{
	REQUIRE( reflection_path.size( ) == 3 );

	REQUIRE( reflection_path.at( 0 )->iAnchorType == ITAGeo::CPropagationAnchor::ACOUSTIC_EMITTER );
	REQUIRE( reflection_path.at( 1 )->iAnchorType == ITAGeo::CPropagationAnchor::SPECULAR_REFLECTION );
	REQUIRE( reflection_path.at( 2 )->iAnchorType == ITAGeo::CPropagationAnchor::ACOUSTIC_SENSOR );

	REQUIRE( reflection_path.at( 0 )->v3InteractionPoint == parameters.source_position );
	REQUIRE( reflection_path.at( 2 )->v3InteractionPoint == parameters.receiver_position );

	auto mirror_image = parameters.source_position;
	mirror_image[1]   = -mirror_image[1]; // flip "height"

	const auto sound_path_direction = parameters.receiver_position - mirror_image;

	const float t = -mirror_image[1] / sound_path_direction[1];

	const auto interaction_point = mirror_image + t * sound_path_direction;

	for( int i = 0; i < 3; ++i )
	{
		INFO( "Current i: " << i );
		REQUIRE_THAT( reflection_path.at( 1 )->v3InteractionPoint[i], Catch::Matchers::WithinAbs( interaction_point[i], 1e-4 ) ); // 0.1mm accuracy
	}
}

inline void checkDiffraction( const ITAGeo::CPropagationPath diffracted_path, const SSimulationParameters parameters )
{
	REQUIRE( diffracted_path.size( ) == 3 );

	REQUIRE( diffracted_path.at( 0 )->iAnchorType == ITAGeo::CPropagationAnchor::ACOUSTIC_EMITTER );
	REQUIRE( diffracted_path.at( 1 )->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_OUTER_APEX );
	REQUIRE( diffracted_path.at( 2 )->iAnchorType == ITAGeo::CPropagationAnchor::ACOUSTIC_SENSOR );

	REQUIRE( diffracted_path.at( 0 )->v3InteractionPoint == parameters.source_position );
	REQUIRE( diffracted_path.at( 2 )->v3InteractionPoint == parameters.receiver_position );

	// Check that the ESEA is fulfilled. Ref Tsingos 2001 "Modeling ..."
	auto source_2_interaction   = parameters.source_position - diffracted_path.at( 1 )->v3InteractionPoint;
	auto receiver_2_interaction = parameters.receiver_position - diffracted_path.at( 1 )->v3InteractionPoint;

	source_2_interaction.Normalize( );
	receiver_2_interaction.Normalize( );

	const auto esea_lhs = source_2_interaction.Dot( VistaVector3D( 1, 0, 0 ) );
	const auto esea_rhs = receiver_2_interaction.Dot( VistaVector3D( -1, 0, 0 ) );

	const float pi = 3.14159265358979323846264338327950288419716939937510582097;

	INFO( "Difference = " << ( esea_lhs - esea_rhs ) * 180 / pi << "deg" );

	REQUIRE_THAT( esea_lhs, Catch::Matchers::WithinAbs( esea_rhs, 1 * pi / 180 ) ); // 1 deg accuracy
}

#endif