/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Runs the Kessel loudspeaker setup for diffraction assessment
 *
 * See: http://www.iet.ntnu.no/~svensson/Benchmarks/Benchmark_Kessel1.html
 *
 */

#include <ITAException.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/Scene.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/ConvexDiffraction/Engine.h>
#include <ITAStopWatch.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <assert.h>
#include <fstream>
#include <iostream>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

int main( int, char** )
{
	string sFileBaseName = "KesselLoudspeakerCabinet";
	string sSKPFilePath  = sFileBaseName + ".skp";
	VistaFileSystemFile oFile( sSKPFilePath );
	assert( oFile.Exists( ) );

	auto pSource     = make_shared<CEmitter>( );
	pSource->sName   = "Loudspeaker Source";
	pSource->qOrient = VistaQuaternion( 0, -1, 0, 0 ); // irrelevant here, but looking at receiver
	pSource->vPos.SetValues( 0.0f, 0.0f, 0.0f + ITAConstants::EPS_F_L );

	auto pReceiver     = make_shared<CSensor>( );
	pReceiver->sName   = "Receiver";
	pReceiver->qOrient = VistaQuaternion( 0, 1, 0, 0 ); // irrelevant here, but looking at loudspeaker
	pReceiver->vPos.SetValues( 0.0f, 0.0f, 1.0f );

	CScene oGeoScene;
	oGeoScene.vpSoundSensors.push_back( pReceiver );
	oGeoScene.vpSoundEmitters.push_back( pSource );
	SketchUp::CModel oGeoModel;
	oGeoScene.pGeoModel = &oGeoModel;

	try
	{
		if( oGeoModel.Load( oFile.GetName( ) ) )
			cout << "Successfully loaded file '" << oFile.GetLocalName( ) << "' as geo model" << endl;
	}
	catch( const ITAException& e )
	{
		cerr << e << endl;
	}

	oGeoModel.TriangulateMeshes( );

	if( oGeoModel.IsWaterproof( ) == false )
		cerr << "[ WARNING ] Model is not waterproof. Trying top level mesh anyway." << endl;


	ConvexDiffraction::CEngine* pDiffractionEngine = ConvexDiffraction::CEngine::Create( oGeoModel.GetTopLevelMesh( ), pSource, pReceiver );
	pDiffractionEngine->SetDefaultAbortionCriteria( );
	pDiffractionEngine->oAbortionCriteria.iDiffractionPreparationOrder = 4;
	pDiffractionEngine->oAbortionCriteria.iNumMaxDiffractions          = 4;

	cout << pDiffractionEngine->oAbortionCriteria << endl;


	CPropagationPathList voPaths;
	pDiffractionEngine->CalculateDiffractionPaths( voPaths );

	int iMaxPropLength = -1;
	for( auto oPath: voPaths )
	{
		cout << oPath << endl;
		if( iMaxPropLength < (int)oPath.size( ) )
			iMaxPropLength = (int)oPath.size( );
	}

	if( voPaths.empty( ) )
		cerr << "[ WARNING ] No valid propagation paths found" << endl;
	else
	{
		cout << "Calculated " << voPaths.size( ) << " diffraction paths at maximum order of " << iMaxPropLength << endl;

		Utils::JSON::Export( voPaths, sFileBaseName + ".json" );
		cout << "Stored propagation paths to '" << sFileBaseName << ".json'" << endl;
	}

	Halfedge::CMeshModel oDiffractionVisualizationMesh;
	oDiffractionVisualizationMesh.SetName( "ITA_Vis_AcousticDiffractionGradient" );
	pDiffractionEngine->GetCombinedDiffractionGradientVisualization( &oDiffractionVisualizationMesh );
	oGeoModel.AddVisualizationMesh( &oDiffractionVisualizationMesh, "CombinedDiffractionGradient" );


	for( auto oPath: voPaths )
		oGeoModel.AddPropagationPathVisualization( oPath, "DiffractionContour" );

	oGeoModel.Store( sFileBaseName + "_Vis.skp" );

	/* When ready ... @todo move to PropPathModels
	CITADiffractionFilter oDiffractionFilter;
	oDiffractionFilter.Prepare( voPaths );
	int iFilterLengthSamples;
	if( oDiffractionFilter.GetFilterLengthSamples( iFilterLengthSamples ) == true )
	{
	ITASampleBuffer sfDiffractionIR( iFilterLengthSamples );
	oDiffractionFilter.Calculate( sfDiffractionIR );
	cout << "Diffraction impulse response calcuated." << endl;
	cout << sfDiffractionIR << endl;
	}
	else
	{
	cout << "No filter available." << endl;
	}
	*/

	delete pDiffractionEngine;

	return 0;
}
