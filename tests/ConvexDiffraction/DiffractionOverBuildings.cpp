/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/ModelBase.h>
#include <ITAGeo/Scene.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/ConvexDiffraction/Engine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

//! Diffraction over buildings test
/*
 * Runs the Diffraction tests over different building geometries with roof shapes
 *
 */
int main( int iNumInArgs, char* ppcInArgs[] )
{
	string sLocalFileBaseName;
	if( iNumInArgs > 1 )
		sLocalFileBaseName = string( ppcInArgs[1] );
	else
		sLocalFileBaseName = "Building_SaddleRoof";
	string sExtension = "skp";

	string sSKPFilePathInput = sLocalFileBaseName + "." + sExtension;
	VistaFileSystemFile oFile( sSKPFilePathInput );
	assert( oFile.Exists( ) );
	string sSKPFilePathOutput_Vis = oFile.GetParentDirectory( ) + "/" + sLocalFileBaseName + "_Vis" + "." + sExtension;

	auto pSource     = std::make_shared<ITAGeo::CEmitter>( );
	pSource->sName   = "My Diffraction Source";
	pSource->qOrient = VistaQuaternion( 0, -1, 0, 0 );
	pSource->vPos.SetValues( 1.7f, 0.66f, 0.11f );

	cout << pSource->ToString( ) << endl;

	auto pReceiver     = std::make_shared<CSensor>( );
	pReceiver->sName   = "My Diffraction Receiver";
	pReceiver->qOrient = VistaQuaternion( 0, 1, 0, 0 );
	pReceiver->vPos.SetValues( -2.2f, 0.33f, 0.22f );

	cout << pReceiver->ToString( ) << endl;

	CScene oGeoScene;
	oGeoScene.vpSoundSensors.push_back( pReceiver );
	oGeoScene.vpSoundEmitters.push_back( pSource );
	SketchUp::CModel* pGeoModel = new SketchUp::CModel( );
	oGeoScene.pGeoModel         = pGeoModel;

	try
	{
		cout << "Loading model from file path '" << sSKPFilePathInput << "'" << endl;
		if( oGeoScene.pGeoModel->Load( oFile.GetName( ) ) )
			cout << "Successfully loaded file '" << oFile.GetLocalName( ) << "' as geo model" << endl;
		else
			ITA_EXCEPT1( INVALID_PARAMETER, "Could not load geometry from file " + oFile.GetLocalName( ) );
	}
	catch( const ITAException& e )
	{
		cerr << "[ ERROR ] " << e << endl;
		return 255;
	}

	VistaVector3D v3Min, v3Max;
	pGeoModel->GetTopLevelMesh( )->GetBoundingBoxAxisAligned( v3Min, v3Max );
	cout << "Axes-aligned bounding box: " << v3Min << " < " << v3Max << endl;

	bool bSourceInsideBuilding = pGeoModel->GetTopLevelMesh( )->GetInsideBoundingBoxAxisAligned( pSource->vPos );
	if( bSourceInsideBuilding )
	{
		cerr << "[ ERROR ] Sound source inside building" << endl;
		return 255;
	}
	else
		cout << "[ OK ] Source outside building" << endl;

	bool bReceiverInsideBuilding = pGeoModel->GetTopLevelMesh( )->GetInsideBoundingBoxAxisAligned( pReceiver->vPos );
	if( bReceiverInsideBuilding )
	{
		cerr << "[ ERROR ] Receiver inside building" << endl;
		return 255;
	}
	else
		cout << "[ OK ] Receiver outside building" << endl;

	pGeoModel->TriangulateMeshes( );

	if( pGeoModel->IsWaterproof( ) == false )
		cerr << "[ WARNING ] Model is not waterproof. Trying top level mesh anyway." << endl;


	// Diffraction calc

	ConvexDiffraction::CEngine* pConvexDiffractionEngine = ConvexDiffraction::CEngine::Create( pGeoModel->GetTopLevelMesh( ), pSource, pReceiver );
	pConvexDiffractionEngine->SetDefaultAbortionCriteria( );
	pConvexDiffractionEngine->oAbortionCriteria.iDiffractionPreparationOrder = 3;
	pConvexDiffractionEngine->oAbortionCriteria.iNumMaxDiffractions          = 3;

	cout << pConvexDiffractionEngine->oAbortionCriteria << endl;

	CPropagationPathList voPaths;
	ITAStopWatch sw;
	sw.start( );
	pConvexDiffractionEngine->CalculateDiffractionPaths( voPaths ); // magic happens here
	cout << "Calculation time: " << timeToString( sw.stop( ) ) << endl;

	Utils::JSON::Export( voPaths, sLocalFileBaseName + ".json" );
	cout << "Exported propagation paths to '" << sLocalFileBaseName << ".json'" << endl;


	// Visualization preps

	Halfedge::CMeshModel oDiffractionVisualizationMeshCombined;
	oDiffractionVisualizationMeshCombined.SetName( "DiffractionGradientCombined" );
	Halfedge::CMeshModel oDiffractionVisualizationMeshSource;
	oDiffractionVisualizationMeshSource.SetName( "DiffractionGradientSource" );
	Halfedge::CMeshModel oDiffractionVisualizationMeshReceiver;
	oDiffractionVisualizationMeshReceiver.SetName( "DiffractionGradientReceiver" );
	pConvexDiffractionEngine->GetCombinedDiffractionGradientVisualization( &oDiffractionVisualizationMeshCombined );
	pConvexDiffractionEngine->GetFromSourceDiffractionGradientVisualization( &oDiffractionVisualizationMeshSource );
	pConvexDiffractionEngine->GetFromTargetDiffractionGradientVisualization( &oDiffractionVisualizationMeshReceiver );

	delete pConvexDiffractionEngine;


	pGeoModel->AddVisualizationMesh( &oDiffractionVisualizationMeshCombined );
	pGeoModel->AddVisualizationMesh( &oDiffractionVisualizationMeshSource );
	pGeoModel->AddVisualizationMesh( &oDiffractionVisualizationMeshReceiver );


	// Add direct sound path
	CPropagationPath oDirectPath;
	oDirectPath.push_back( pSource );
	oDirectPath.push_back( pReceiver );
	pGeoModel->AddPropagationPathVisualization( oDirectPath, "ITA_Visuals_DirectSound" );

	// Diffraction paths
	std::vector<CPropagationPath>::const_iterator cit = voPaths.begin( );
	int n                                             = 1;
	while( cit != voPaths.end( ) )
		pGeoModel->AddPropagationPathVisualization( *cit++, "ITA_Visuals_DiffractionPath_" + IntToString( n++ ) );

	oGeoScene.pGeoModel->Store( sSKPFilePathOutput_Vis );
	delete oGeoScene.pGeoModel;

	/* When ready ...
	CITADiffractionFilter oDiffractionFilter;
	oDiffractionFilter.Prepare( voPaths );
	int iFilterLengthSamples;
	if( oDiffractionFilter.GetFilterLengthSamples( iFilterLengthSamples ) == true )
	{
	    ITASampleBuffer sfDiffractionIR( iFilterLengthSamples );
	    oDiffractionFilter.Calculate( sfDiffractionIR );
	    cout << "Diffraction impulse response calcuated." << endl;
	    cout << sfDiffractionIR << endl;
	}
	else
	{
	    cout << "No filter available." << endl;
	}
	*/

	return 0;
}
