/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests the image (source) model algorithm.
 *
 */

#include <ITABaseDefinitions.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Urban/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/UrbanEngine/UrbanImageSource.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <cassert>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

int main( int iNumInArgs, char *pcInArgs[] )
{
	string sSUFolder   = "SketchUpFiles/";
	string sJsonFolder = "JsonFiles/";

	string sInFile = "SimpleUrbanEnvironment";
	if( iNumInArgs > 1 )
		sInFile = string( pcInArgs[1] );

	auto pMaterialDirectory = std::make_shared<Material::CMaterialManager>( "./" );

	SketchUp::CModel oGeoModel;
	ITAGeo::Urban::CModel oUrbanModel;

	oUrbanModel.SetMaterialManager( pMaterialDirectory );


	if( oUrbanModel.Load( sSUFolder + sInFile + ".skp" ) )
	{
		cout << "Succesffully loaded '" << sInFile << "'" << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}

	// SketchUp geo model for adding later calculated paths to sketchUp file
	oGeoModel.Load( sSUFolder + sInFile + ".skp" );

	// Set emitter and sensor
	auto pEmitter = make_shared<CEmitter>( VistaVector3D( 5.1f, 2.2f, 1.7f ) );
	// auto pSensor = make_shared<CSensor>( VistaVector3D( 3.0f, 3.3f, 1.3f ) );
	auto pSensor = make_shared<CSensor>( VistaVector3D( -3.0f, -3.3f, 2.3f ) );

	cout << "Emitter: " << pEmitter->vPos << endl;
	cout << "Sensor: " << pSensor->vPos << endl;

	// Set the mirror image engine for the urban environment
	ITAPropagationPathSim::UrbanEngine::MirrorImage::CSource oUrbanEngine( oUrbanModel, Order::ORDER_2 );
	oUrbanEngine.ConstructImages( pEmitter );

	// Set the reflection sound paths
	CPropagationPathList oPathListAll, oPathListVisible;
	oUrbanEngine.ConstructReflectionPathsOutside( pSensor, oPathListAll );

	// Get the visible sound paths
	oUrbanModel.FilterVisiblePaths( oPathListAll, oPathListVisible );


	// Add paths to SketchUp Visualisation
	string layerName = "All_Refl_Paths_Number_Reflection_" + to_string( oPathListAll.GetNumPaths( ) );
	for( const auto &oPath: oPathListAll )
	{
		oGeoModel.AddPropagationPathVisualization( oPath, layerName );
	}

	layerName = "Visible_Refl_Paths_Number_Reflection_" + to_string( oPathListVisible.GetNumPaths( ) );
	for( const auto &oPath: oPathListVisible )
	{
		oGeoModel.AddPropagationPathVisualization( oPath, layerName );
	}

	// Add emitter and sensor to visualization
	oGeoModel.AddEmitterVisualization( *pEmitter, "Emitter A" );
	oGeoModel.AddSensorVisualization( *pSensor, "Sensor A" );

	// Store model
	oGeoModel.Store( sSUFolder + "SimpleUrbanImageModelTest_" + sInFile + ".skp" );

	Utils::JSON::Export( oPathListVisible, sJsonFolder + "SimpleUrbanImageModelTest_" + sInFile + ".json" );

	return 0;
}
