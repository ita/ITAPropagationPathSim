/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests the image (source) model algorithm.
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAPropagationPathSim/MirrorImage/Engine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <cassert>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

int main( int iNumInArgs, char* pcInArgs[] )
{
	string sSubFolder = "SketchUpFiles/";

	// string sInFile = "NonSimpleRoom - Outside.skp";
	string sInFile = "SimpleRoom.skp";
	if( iNumInArgs > 1 )
		sInFile = string( pcInArgs[1] );

	auto pMaterialDirectory = std::make_shared<Material::CMaterialManager>( "./" );

	SketchUp::CModel oGeoModel;
	oGeoModel.SetMaterialManager( pMaterialDirectory );
	if( oGeoModel.Load( sSubFolder + sInFile ) )
	{
		cout << "Succesffully loaded '" << sInFile << "'" << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}

	// oGeoModel.TriangulateMeshes();

	// auto pSensor = make_shared< CSensor >(VistaVector3D(-3.0f, -3.3f, 2.3f));
	auto pSensor  = make_shared<CSensor>( VistaVector3D( 3.0f, 3.3f, 1.3f ) );
	auto pEmitter = make_shared<CEmitter>( VistaVector3D( 2.1f, 2.2f, 1.7f ) );

	cout << "Emitter: " << pEmitter->vPos << endl;
	cout << "Sensor: " << pSensor->vPos << endl;

	VistaVector3D v3BBMin, v3BBMax;
	oGeoModel.GetTopLevelMesh( )->GetBoundingBoxAxisAligned( v3BBMin, v3BBMax );
	cout << "Bounding box lower corner: " << v3BBMin << endl;
	cout << "Bounding box upper corner: " << v3BBMax << endl;

	if( oGeoModel.GetTopLevelMesh( )->GetInsideBoundingBoxAxisAligned( pEmitter->v3InteractionPoint ) )
		cout << "Emitter inside axis-aligned bounding box" << endl;
	else
		cout << "Emitter outside axis-aligned bounding box!" << endl;

	if( oGeoModel.GetTopLevelMesh( )->GetInsideBoundingBoxAxisAligned( pSensor->v3InteractionPoint ) )
		cout << "Sensor inside axis-aligned bounding box" << endl;
	else
		cout << "Sensor outside axis-aligned bounding box!" << endl;

	VistaVector3D v3SBCentroid;
	float fSBRadius;
	oGeoModel.GetTopLevelMesh( )->GetBoundingSphere( v3SBCentroid, fSBRadius );
	cout << "Bounding sphere centroid: " << v3SBCentroid << endl;
	cout << "Bounding sphere radius: " << fSBRadius << endl;

	if( oGeoModel.GetTopLevelMesh( )->GetInsideBoundingSphere( pEmitter->v3InteractionPoint ) )
		cout << "Emitter inside bounding sphere" << endl;
	else
		cout << "Emitter outside bounding sphere!" << endl;

	if( oGeoModel.GetTopLevelMesh( )->GetInsideBoundingSphere( pSensor->v3InteractionPoint ) )
		cout << "Sensor inside bounding sphere" << endl;
	else
		cout << "Sensor outside bounding sphere!" << endl;

	oGeoModel.GetTopLevelMesh( )->GetInsideBoundingBoxAxisAligned( pEmitter->v3InteractionPoint );

	const ITAGeo::Halfedge::CMeshModel& oModelRef( *( oGeoModel.GetTopLevelMesh( ) ) );
	MirrorImage::CEngine oISEngine( oModelRef, ITAGeo::ORDER_3 );
	oISEngine.ConstructImages( pEmitter );

	cout << "Simple image model engine constructed " << oISEngine.GetNumberOfImages( ) << " images" << endl;
	cout << "Number of images: " << oISEngine.GetNumberOfImages( ) << endl;

#if 0
	CPropagationPathList lImagesViz;
	oISEngine.GetImageVisualizationPaths( lImagesViz );
	for( auto p : lImagesViz )
		oGeoModel.AddPropagationPathVisualization( p, "MIM_AllImages" );
#endif

#if 0

	CPropagationPathList lImagesHedgehogViz;
	oISEngine.GetImagesHedgehogVisualization( lImagesHedgehogViz, pSensor );
	for( auto p : lImagesHedgehogViz )
		oGeoModel.AddPropagationPathVisualization( p, "MIM_ImagesHedgehog" );

#endif

#if 0

	int iMaxOrder = ITAGeo::ORDER_2;
	for( int n = 0; n < iMaxOrder; n++ )
	{
		CPropagationPathList lPropagationPaths;
		oISEngine.GetAudiblePaths( pSensor, lPropagationPaths, n );

		cout << "Found " << lPropagationPaths.size() << " audible images for order " << n << endl;

		for( auto path : lPropagationPaths )
		{
			assert( path.size() > 1 && path.size() <= n + 2 );
			oGeoModel.AddPropagationPathVisualization( path, "MIM_Paths_EO" + std::to_string( n ) );
		}
	}

#endif

#if 0

	CPropagationPathList lDirectPath;
	oISEngine.GetAudiblePathsForOrder( pSensor, lDirectPath, ITAGeo::ORDER_0 );

	assert( lDirectPath.size() <= 1 );
	for( auto path : lDirectPath )
	{
		assert( path.size() == 2 );
		oGeoModel.AddPropagationPathVisualization( path, "MIM_Path_Direct" );
	}

#endif

#if 0

	CPropagationPathList lISFirstOrder;
	oISEngine.GetAudiblePathsForOrder( pSensor, lISFirstOrder, ITAGeo::ORDER_1 );

	for( auto path : lISFirstOrder )
	{
		assert( path.size() == ITAGeo::ORDER_1 + 2 );
		oGeoModel.AddPropagationPathVisualization( path, "MIM_Path_ORDER_1" );
	}

#endif

#if 0

	CPropagationPathList lISSecondOrder;
	oISEngine.GetAudiblePathsForOrder( pSensor, lISSecondOrder, ITAGeo::ORDER_2 );

	for( auto path : lISSecondOrder )
	{
		assert( path.size() == ITAGeo::ORDER_2 + 2 );
		oGeoModel.AddPropagationPathVisualization( path, "MIM_Path_ORDER_2" );
	}

#endif


#if 1
	CPropagationPathList pathList;

	ITAStopWatch sw;
	sw.start( );
	if( oGeoModel.GetTopLevelMesh( )->GetInsideMeshJordanMethodAxisAligned( pSensor->vPos ) )
		oISEngine.ConstructReflectionPathsInsideForOrder( pSensor, pathList, ITAGeo::ORDER_0 );
	else
		oISEngine.ConstructReflectionPathsOutsideForOrder( pSensor, pathList, ITAGeo::ORDER_0 );

	cout << "Calculation time direct path: " << timeToString( sw.stop( ) ) << endl;

	string layerName = "Refl_Path_ORDER_0_NUMBER_" + to_string( pathList.GetNumPaths( ) );
	for( auto path: pathList )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerName );
	}

	pathList.clear( );

	sw.start( );
	if( oGeoModel.GetTopLevelMesh( )->GetInsideMeshJordanMethodAxisAligned( pSensor->vPos ) )
		oISEngine.ConstructReflectionPathsInsideForOrder( pSensor, pathList, ITAGeo::ORDER_1 );
	else
		oISEngine.ConstructReflectionPathsOutsideForOrder( pSensor, pathList, ITAGeo::ORDER_1 );
	cout << "Calculation time reflection paths 1st order: " << timeToString( sw.stop( ) ) << endl;

	layerName = "Refl_Path_ORDER_1_NUMBER_" + to_string( pathList.GetNumPaths( ) );
	for( auto path: pathList )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerName );
	}

	pathList.clear( );
	sw.start( );

	if( oGeoModel.GetTopLevelMesh( )->GetInsideMeshJordanMethodAxisAligned( pSensor->vPos ) )
		oISEngine.ConstructReflectionPathsInsideForOrder( pSensor, pathList, ITAGeo::ORDER_2 );
	else
		oISEngine.ConstructReflectionPathsOutsideForOrder( pSensor, pathList, ITAGeo::ORDER_2 );
	cout << "Calculation time reflection paths 2nd order: " << timeToString( sw.stop( ) ) << endl;

	layerName = "Refl_Path_ORDER_2_NUMBER_" + to_string( pathList.GetNumPaths( ) );
	for( auto path: pathList )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerName );
	}

	pathList.clear( );
	sw.start( );
	if( oGeoModel.GetTopLevelMesh( )->GetInsideMeshJordanMethodAxisAligned( pSensor->vPos ) )
		oISEngine.ConstructReflectionPathsInsideForOrder( pSensor, pathList, ITAGeo::ORDER_3 );
	else
		oISEngine.ConstructReflectionPathsOutsideForOrder( pSensor, pathList, ITAGeo::ORDER_3 );
	cout << "Calculation time reflection paths 3rd order: " << timeToString( sw.stop( ) ) << endl;

	layerName = "Refl_Path_ORDER_3_NUMBER_" + to_string( pathList.GetNumPaths( ) );
	for( auto path: pathList )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerName );
	}
#endif


#if 1
	CPropagationPathList pathListAll;

	sw.start( );
	oISEngine.ConstructReflectionPaths( pSensor, pathListAll );
	cout << "Calculation time all reflection paths without face culling: " << timeToString( sw.stop( ) ) << endl;

	string layerNameNoCulling = "All_Paths_NoFaceCulling_NUMBER_" + to_string( pathListAll.GetNumPaths( ) );
	for( auto path: pathListAll )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerNameNoCulling );
	}
	CPropagationPathList pathListVisible;

	sw.start( );
	oGeoModel.GetTopLevelMesh( )->FilterVisiblePaths( pathListAll, pathListVisible );
	cout << "Calculation time filtering visible reflection paths without face culling: " << timeToString( sw.stop( ) ) << endl;
	layerNameNoCulling = "Visible_Paths_NoFaceCulling_NUMBER_" + to_string( pathListVisible.GetNumPaths( ) );

	for( auto path: pathListVisible )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerNameNoCulling );
	}

	pathListAll.clear( );

	sw.start( );
	if( oGeoModel.GetTopLevelMesh( )->GetInsideMeshJordanMethodAxisAligned( pSensor->vPos ) )
		oISEngine.ConstructReflectionPathsInside( pSensor, pathListAll );
	else
		oISEngine.ConstructReflectionPathsOutside( pSensor, pathListAll );

	cout << "Calculation time all reflection paths with face culling: " << timeToString( sw.stop( ) ) << endl;

	layerNameNoCulling = "All_Paths_FaceCulling_NUMBER_" + to_string( pathListAll.GetNumPaths( ) );
	for( auto path: pathListAll )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerNameNoCulling );
	}

	sw.start( );
	oGeoModel.GetTopLevelMesh( )->FilterVisiblePaths( pathListAll, pathListVisible );
	cout << "Calculation time filtering visible reflection paths with face culling: " << timeToString( sw.stop( ) ) << endl;
	layerNameNoCulling = "Visible_Paths_FaceCulling_NUMBER_" + to_string( pathListVisible.GetNumPaths( ) );

	for( auto path: pathListVisible )
	{
		oGeoModel.AddPropagationPathVisualization( path, layerNameNoCulling );
	}

#endif

	oGeoModel.Store( sSubFolder + "SimpleImageModelTest_" + sInFile );

	return 0;
}
