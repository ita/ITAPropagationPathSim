/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Car pass-by with a receiver placed in a building canyon, car
 * comes out of the acoust shadow from the right building row and
 * proceedes into the acoustic shadow of the left building row.
 *
 * Positional sampling is at 30 Hz, car speed result is then 50 km/h.
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;


int main( int, char** )
{
	cout << " - - - - -  INTERNOISE 2019 Stienen Vorl�nder  - - - - - " << endl << endl;

	string sBaseFolder = "D:\\Users\\stienen\\Sciebo\\ITA\\Publikationen\\2019 INTERNOISE Madrid\\Auralization";
	string sName       = "stienen2910auralization";


	// Geo input

	auto pMeshModelList = make_shared<ITAGeo::Halfedge::CMeshModelList>( );

	string sInFile = "stienen2910auralization_StreetCanyon_simplified.skp";
	if( pMeshModelList->Load( sBaseFolder + "\\Geometry\\" + sInFile ) )
	{
		cout << "Successfully loaded geometry mesh from input file '" << sInFile << "'" << endl << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}


	// Scene

	shared_ptr<CSensor> pSensor = make_shared<CSensor>( VistaVector3D( 50.0f, 0.0f, 1.7f ) );
	pSensor->sName              = "UrbanReceiver";

	VistaVector3D v3VehicleStartPoint( 0.0f, -50.0f, 1.0f );
	shared_ptr<CEmitter> pEmitter = make_shared<CEmitter>( v3VehicleStartPoint );
	pEmitter->sName               = "UrbanCar";
	float fVehicleSpeedKMH        = 50.0f;
	float fVehicleSpeed           = fVehicleSpeedKMH / 3.6f;
	VistaVector3D v3MovementDirection( 0.0f, 1.0f, 0.0f );
	float fTravelDistance = 100.0f; // m

	cout << "Vehicle will move at speed " << fVehicleSpeed << " m/s for the distance of " << fTravelDistance << " m." << endl;


	// Configuration

	float fFrameRate = 30.0f;

	const bool bOnlyNeighbouredEdgeDiffraction    = false;
	const bool bDiffractionOnlyIntoShadowedEdges  = true;  //
	const bool bFilterNotVisiblePathsBetweenEdges = false; // Intersection test between edges(expensive)
	const bool bFilterNotVisiblePointToEdge       = false; // Intersection test between emitter/sensor and edges
	const bool bFilterNotVisiblePaths             = true;  // Intersection test of calculated sub paths
	const float fIntersectionTestResolution       = 0.001f;
	const int iNumIterations                      = 10; //!< Number of iterations for the calculation of the aperture points

	const float fMaxAccumulatedDiffractionAngle = -2.0f * ITAConstants::PI_F; // zero or below -> disabled

	const float fLevelDropThreshhold = -90.f - 30.f - 11.f;    // 90 dB Power level of Diesel; 30 dB masking; 11 dB initial power level drop
	const float fReflectionPenalty   = -10.0f * log10( 0.8f ); // Level reduction of a reflection with reflection factor rho = 0.8 from ISO9613-2
	const float fDiffractionPenalty =
	    2.5f; // Minimum level reduction of a diffracted signal into the shadow region for a low frequency (Taken from BA Filbert Figure 16. UTD)

	const int iMaxReflectionOrder  = 2;
	const int iMaxDiffractionOrder = 1;
	const int iMaxCombinedOrder    = 3;

	cout << "Propagation path algorithm configuration:" << endl;
	cout << "\tReflection order  : " << iMaxReflectionOrder << endl;
	cout << "\tDiffraction order : " << iMaxDiffractionOrder << endl;
	cout << "\tMaximum order     : " << iMaxCombinedOrder << endl;

	cout << endl;


	CombinedModel::CPathEngine::CSimulationConfig oSimConf;
	oSimConf.bFilterNotNeighbouredEdges           = bOnlyNeighbouredEdgeDiffraction;
	oSimConf.bFilterEdgeToEdgeIntersectedPaths    = bFilterNotVisiblePathsBetweenEdges;
	oSimConf.bFilterEmitterToEdgeIntersectedPaths = bFilterNotVisiblePointToEdge;
	oSimConf.bFilterSensorToEdgeIntersectedPaths  = bFilterNotVisiblePointToEdge;
	oSimConf.bFilterIlluminatedRegionDiffraction  = bDiffractionOnlyIntoShadowedEdges;
	oSimConf.iNumberIterationApexCalculation      = iNumIterations;
	oSimConf.fIntersectionTestResolution          = fIntersectionTestResolution;
	oSimConf.bFilterIntersectedPaths              = bFilterNotVisiblePaths;

	CombinedModel::CPathEngine::CAbortionCriteria oAbort;
	oAbort.fDiffractionPenalty        = fDiffractionPenalty;
	oAbort.fAccumulatedAngleThreshold = fMaxAccumulatedDiffractionAngle;
	oAbort.fDynamicRange              = fLevelDropThreshhold;
	oAbort.fReflectionPenalty         = fReflectionPenalty;
	oAbort.iMaxCombinedOrder          = iMaxCombinedOrder;
	oAbort.iMaxDiffractionOrder       = iMaxDiffractionOrder;
	oAbort.iMaxReflectionOrder        = iMaxReflectionOrder;


	// Simulation

	ITAStopWatch sw;
	unsigned int iNumFrames = (unsigned int)floor( fFrameRate * fTravelDistance / fVehicleSpeed );
	for( unsigned int n = 0; n < iNumFrames; n++ )
	{
		char cFrameID[] = "0000";
		sprintf_s( cFrameID, "%04d", n + 1 );

		cout << " * Calculating frame '" << cFrameID << "' ( " << n + 1 << " / " << iNumFrames << " )" << endl;
		sw.start( );

		// @todo: fix bugs for path engine reuse on source/receiver movement
		auto pPathEngine = make_shared<CombinedModel::CPathEngine>( );
		pPathEngine->SetSimulationConfiguration( oSimConf );
		pPathEngine->SetAbortionCriteria( oAbort );
		pPathEngine->InitializePathEnvironment( pMeshModelList );
		// Path finding
		pPathEngine->SetEntities( pEmitter, pSensor );
		CPropagationPathList oPathList;
		pPathEngine->ConstructPropagationPaths( oPathList );
		cout << "\t -> found " << oPathList.size( ) << " visible paths" << endl;

		// Propagation path list export
		string sPropPathFilePath = sName + "_paths_f" + cFrameID + ".json";
		Utils::JSON::Export( oPathList, sBaseFolder + "\\Simulation\\" + sPropPathFilePath );
		cout << "\tExported propagation path list to '" << sPropPathFilePath << "'" << endl;

		if( n == 0 )
		{
			// Export visualization
			SketchUp::CModel oGeoModel_Viz;
			try
			{
				oGeoModel_Viz.Load( sBaseFolder + "\\Geometry\\" + sInFile );
			}
			catch( ITAException& e )
			{
				cerr << "\tCould not load '" << sInFile << "' for visualization export: " << e << endl;
				return 255;
			}

			oGeoModel_Viz.AddEmitterVisualization( *pEmitter, pEmitter->sName );
			oGeoModel_Viz.AddSensorVisualization( *pSensor, pSensor->sName );


			size_t numberOfPaths[iMaxDiffractionOrder + 1][iMaxReflectionOrder + 1] = { 0 };
			for( auto& oPath: oPathList )
			{
				long iNumReflections  = (long)oPath.GetNumReflections( );
				long iNumDiffractions = (long)oPath.GetNumDiffractions( );

				numberOfPaths[iNumDiffractions][iNumReflections]++;
			}

			for( auto& oPath: oPathList )
			{
				long iNumReflections  = (long)oPath.GetNumReflections( );
				long iNumDiffractions = (long)oPath.GetNumDiffractions( );

				string sPathName = "Refl_Order_" + to_string( iNumReflections ) + "_Diffr_Order_" + to_string( iNumDiffractions ) + "_Path_Amount_" +
				                   to_string( numberOfPaths[iNumDiffractions][iNumReflections] );
				oGeoModel_Viz.AddPropagationPathVisualization( oPath, sPathName );
			}

			string sVizFilePath = sName + "_paths_f" + cFrameID + ".skp";
			oGeoModel_Viz.Store( sBaseFolder + "\\Visualization\\" + sVizFilePath );
			cout << "\tExported visualization file list to '" << sVizFilePath << "'" << endl;
		}

		auto fRuntime = sw.stop( );
		cout << "\tFinished simulation in " << timeToString( fRuntime ) << endl;

		cout << endl;
	}

	return 0;
}
