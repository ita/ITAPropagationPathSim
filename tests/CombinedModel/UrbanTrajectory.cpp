/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Example generating propagation paths along a trajectory in
 * an urban setting.
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <VistaTools/VistaFileSystemDirectory.h>
#include <cassert>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;


int main( int, char** )
{
	std::string sInFile          = "UrbanSetting.skp";
	std::string sOutputDirectory = "UrbanTrajectory";

	auto pMeshModelList = make_shared<Halfedge::CMeshModelList>( );

	if( pMeshModelList->Load( sInFile ) )
	{
		cout << "Successfully loaded '" << sInFile << "'" << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}

	VistaFileSystemDirectory d( sOutputDirectory );
	try
	{
		if( !d.Exists( ) )
			d.CreateWithParentDirectories( );
	}
	catch( ... )
	{
		cerr << "Could not create target folder " << sOutputDirectory << ", aborting." << endl;
		return 255;
	}


	// Scene

	auto pReceiver = make_shared<CSensor>( VistaVector3D( 15.0f, 0.0f, 1.7f ) );
	auto pVehicle  = make_shared<CEmitter>( VistaVector3D( 0.0f, -46.5f, 1.0f ) );
	VistaVector3D v3Direction( 0.0f, 1.0f, 0.0f ); // Y-direction (ISO / SketchUp coordinate system, "sideways")


	// Configuration

	cout << "Initializing simulation engine" << endl;

	CombinedModel::CPathEngine::CSimulationConfig oConf;
	oConf.bFilterIlluminatedRegionDiffraction = true;
	oConf.bFilterIntersectedPaths             = true;

	CombinedModel::CPathEngine::CAbortionCriteria oAbort;
	oAbort.iMaxDiffractionOrder = 1;
	oAbort.iMaxReflectionOrder  = 1;

	auto pPathEngine = make_shared<CombinedModel::CPathEngine>( );

	pPathEngine->SetSimulationConfiguration( oConf );
	pPathEngine->SetAbortionCriteria( oAbort );

	pPathEngine->InitializePathEnvironment( pMeshModelList );

	cout << "Placing entities" << endl;

	// We're moving the emitter, so it's more efficient to construct images from the receiver (set as origin )
	pPathEngine->SetEntities( pReceiver, pVehicle );


	// Simulation

	// Calculate how many time frames to calculate, and how far to move the source between each timeframe
	int iBlockSize         = 128;
	float fSampleRate      = 44100;
	float fSpeed           = 30.0f * 1000.0f / ( 60.0f * 60.0f ); // Speed in m/s
	float fDistance        = 100.0f;
	float fSimulationTime  = fDistance / fSpeed;                                  // Time the car takes to move the simulated distance
	int iSimulationSamples = (int)ceil( fSampleRate * fSimulationTime );          // total number of samples throughout the simulation
	int iNumSteps          = (int)ceil( iSimulationSamples / (float)iBlockSize ); // total number of blocks/ updated in the simulation
	float fIncrement       = fDistance / (float)iNumSteps;                        // how much to increment y each time (+1 to account for starting pos)

	// Simulate propagation paths for each sample of the trajectory
	ITAStopWatch sw;
	for( int i = 0; i <= iNumSteps; i++ )
	{
		sw.start( );

		cout << "Simulating frame " << std::setw( 5 ) << i + 1 << " of " << iNumSteps << endl;

		if( i > 0 )
		{
			pVehicle->vPos += v3Direction * fIncrement;
			pPathEngine->UpdateTargetEntity( pVehicle );
		}

		CPropagationPathList oPathList;
		pPathEngine->ConstructPropagationPaths( oPathList );
		cout << "Found " << to_string( oPathList.size( ) ) << " audible paths." << endl;

		stringstream ss;
		int n0_characters = (int)floor( log10( iNumSteps ) ) + 1;
		ss << sOutputDirectory << "/"
		   << "UrbanTrajectory_pps_" << setfill( '0' ) << setw( n0_characters ) << i << ".json";

		Utils::JSON::Export( oPathList, ss.str( ).c_str( ) );

		cout << "Done in " << timeToString( sw.stop( ) ) << " seconds." << endl;

		// Every now and then, export a visualization model
		if( i == 0 || i == (int)iNumSteps / 20 || i == iNumSteps - 1 )
		{
			SketchUp::CModel oGeoModel_Viz;
			if( !oGeoModel_Viz.Load( sInFile ) )
			{
				cerr << "Could not load " << sInFile << endl;
				return 255;
			}

			oGeoModel_Viz.AddEmitterVisualization( *pVehicle, "Vehicle" );
			oGeoModel_Viz.AddSensorVisualization( *pReceiver, "Receiver" );

			cout << "The path with the longest length is " << oPathList.GetMaxLength( ) << " m." << endl;

			// Group propagation paths by order of reflection and diffraction (maybe we'll change this and add all paths for a handful of spatial steps for a nice
			// animation)
			vector<vector<int> > vviNumberOfPaths;
			vviNumberOfPaths.resize( oAbort.iMaxDiffractionOrder + 1 );
			for( auto& n: vviNumberOfPaths )
				n.resize( oAbort.iMaxReflectionOrder + 1 );

			for( auto& oPath: oPathList )
			{
				long iNumReflections  = (long)oPath.GetNumReflections( );
				long iNumDiffractions = (long)oPath.GetNumDiffractions( );

				vviNumberOfPaths[iNumDiffractions][iNumReflections]++;
			}

			for( auto& oPath: oPathList )
			{
				long iNumReflections  = (long)oPath.GetNumReflections( );
				long iNumDiffractions = (long)oPath.GetNumDiffractions( );

				string sPathName = "Refl_Order_" + to_string( iNumReflections ) + "_Diffr_Order_" + to_string( iNumDiffractions ) + "_Path_Amount_" +
				                   to_string( vviNumberOfPaths[iNumDiffractions][iNumReflections] );
				oGeoModel_Viz.AddPropagationPathVisualization( oPath, sPathName );
			}

			stringstream ss_viz;
			ss_viz << sOutputDirectory << "/"
			       << "UrbanTrajectory_pps_" << setw( 5 ) << setfill( '0' ) << i + 1 << "_viz.skp";

			string sVizFilePath = ss_viz.str( ).c_str( );
			try
			{
				oGeoModel_Viz.Store( sVizFilePath );
				cout << "Exported visualization file to " << sVizFilePath << endl;
			}
			catch( ... )
			{
				cerr << "Failed to export visualization file to " << sVizFilePath << endl;
			}

			cout << endl;
		}
	}

	return 0;
}
