/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests the image (source) model algorithm.
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <cassert>
#include <nlohmann/json.hpp>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

int main( int iNumInArgs, char* pcInArgs[] )
{
	// string sSubFolder = "SketchUpFiles/";
	string sSubFolder = string( ITAPROPPATHSIM_COMBINEDMODELTEST_DIRECTORY ) + "/SketchUpFiles/";

	string sInFile = "ThreeBuildings.skp";
	if( iNumInArgs > 1 )
		sInFile = string( pcInArgs[1] );


	// Load geo model(for later saving of the propagation paths)
	SketchUp::CModel oGeoModel;

	if( oGeoModel.Load( sSubFolder + sInFile ) )
	{
		cout << "Successfully loaded '" << sInFile << "'" << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}

	// Load mesh model list
	auto pMeshModelList = make_shared<ITAGeo::Halfedge::CMeshModelList>( );

	if( pMeshModelList->Load( sSubFolder + sInFile ) )
	{
		cout << "Successfully loaded '" << sInFile << "'" << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}


	// Configuration of the engine
	const bool bOnlyNeighbouredEdgeDiffraction    = false;
	const bool bDiffractionOnlyIntoShadowedEdges  = false; //
	const bool bFilterNotVisiblePathsBetweenEdges = false; // Intersection test between edges(expensive)
	const bool bFilterNotVisiblePointToEdge       = true;  // Intersection test between emitter/sensor and edges
	const bool bFilterNotVisiblePaths             = true;  // Intersection test of calculated sub paths
	const float fIntersectionTestResolution       = 0.001f;
	const int iNumIterations                      = 4; //!< Number of iterations for the calculation of the aperture points

	const float fMaxAccumulatedDiffractionAngle = -ITAConstants::PI_F;

	const float fLevelDropThreshhold = -90.f - 30.f - 11.f;     // 90 dB Power level of Diesel; 30 dB masking; 11 dB initial power level drop
	const float fReflectionPenalty   = -10.0f * log10f( 0.8f ); // Level reduction of a reflection with reflection factor rho = 0.8 from ISO9613-2
	const float fDiffractionPenalty =
	    2.5f; // Minimum level reduction of a diffracted signal into the shadow region for a low frequency (Taken from BA Filbert Figure 16. UTD)

	// Multiple simulations
#if 1
	shared_ptr<CEmitter> pSource;
	shared_ptr<CSensor> pReceiver;
	for( int iSource = 1; iSource <= 1; iSource++ )
	{
		switch( iSource )
		{
			case 1:
				pSource = make_shared<CEmitter>( VistaVector3D( -1.0f, 20.f, 1.0f ) ); // S_1 Car_1
				break;
			case 2:
				pSource = make_shared<CEmitter>( VistaVector3D( -1.0f, 0.0f, 1.0f ) ); // S_2 Car_2
				break;
			case 3:
				pSource = make_shared<CEmitter>( VistaVector3D( -1.0f, -20.f, 1.0f ) ); // S_3 Car_3
				break;
			case 4:
				pSource = make_shared<CEmitter>( VistaVector3D( -30.0f, 0.0f, 1.7f ) ); // S_4 Human_Backyard
				break;
		}
		for( int iReceiver = 2; iReceiver <= 3; iReceiver++ )
		{
			switch( iReceiver )
			{
				case 1:
					pReceiver = make_shared<CSensor>( VistaVector3D( 50.0f, 5.0f, 1.7f ) ); // R_1 Human_street far
					break;
				case 2:
					pReceiver = make_shared<CSensor>( VistaVector3D( -30.0f, 0.0f, 1.7f ) ); // R_2 Human_Backyard
					break;
				case 3:
					pReceiver = make_shared<CSensor>( VistaVector3D( 10.0f, 5.0f, 1.7f ) ); // R_3 Human_street middle
					break;
				case 4:
					pReceiver = make_shared<CSensor>( VistaVector3D( 5.0f, 5.0f, 1.7f ) ); // R_4 Human_street near
					break;
			}


			for( int iOrder = 0; iOrder <= 4; iOrder++ )
			{
				cout << endl << "Source: " << iSource << "; Receiver: " << iReceiver << "; Order: " << iOrder << endl;


				// Variables for processing times
				double d1, d2, d3, d4;


				const int iMaxDiffractionOrder = iOrder;
				const int iMaxReflectionOrder  = iOrder;
				const int iMaxCombinedOrder    = iOrder;

				CombinedModel::CPathEngine::CSimulationConfig oSimConf;
				oSimConf.bFilterNotNeighbouredEdges           = bOnlyNeighbouredEdgeDiffraction;
				oSimConf.bFilterEdgeToEdgeIntersectedPaths    = bFilterNotVisiblePathsBetweenEdges;
				oSimConf.bFilterEmitterToEdgeIntersectedPaths = bFilterNotVisiblePointToEdge;
				oSimConf.bFilterSensorToEdgeIntersectedPaths  = bFilterNotVisiblePointToEdge;
				oSimConf.bFilterIlluminatedRegionDiffraction  = bDiffractionOnlyIntoShadowedEdges;
				oSimConf.iNumberIterationApexCalculation      = iNumIterations;
				oSimConf.fIntersectionTestResolution          = fIntersectionTestResolution;
				oSimConf.bFilterIntersectedPaths              = bFilterNotVisiblePaths;

				CombinedModel::CPathEngine::CAbortionCriteria oAbort;
				oAbort.fDiffractionPenalty        = fDiffractionPenalty;
				oAbort.fAccumulatedAngleThreshold = fMaxAccumulatedDiffractionAngle;
				oAbort.fDynamicRange              = fLevelDropThreshhold;
				oAbort.fReflectionPenalty         = fReflectionPenalty;
				oAbort.iMaxCombinedOrder          = iMaxCombinedOrder;
				oAbort.iMaxDiffractionOrder       = iMaxDiffractionOrder;
				oAbort.iMaxReflectionOrder        = iMaxReflectionOrder;


				ITAStopWatch sw;
				sw.start( );

				auto pPathEngine = make_shared<CombinedModel::CPathEngine>( );
				cout << "Calculation time initialization path engine: " << timeToString( sw.stop( ) ) << endl;

				sw.start( );

				pPathEngine->SetSimulationConfiguration( oSimConf );
				pPathEngine->SetAbortionCriteria( oAbort );
				cout << "Calculation time configuring filter: " << timeToString( sw.stop( ) ) << endl;

				sw.start( );

				pPathEngine->InitializePathEnvironment( pMeshModelList );
				pPathEngine->SetEntities( pSource, pReceiver ); // Builds tree and image edges
				d1 = sw.stop( );
				cout << "Calculation time pre-processing of path environment: " << timeToString( d1 ) << endl;

				size_t numNodes = pPathEngine->GetNumberPropagationPathCandidates( );

				sw.start( );

				pPathEngine->UpdateTargetEntity( pReceiver );
				d3 = sw.stop( );
				cout << "Calculation time applying target / sensor: " << timeToString( d3 ) << endl;

				sw.start( );

				pPathEngine->UpdateOriginEntity( pSource );
				d2 = sw.stop( );
				cout << "Calculation time applying origin / emitter: " << timeToString( d2 ) << endl;

				CPropagationPathList oPathListAll;

				sw.start( );

				pPathEngine->ConstructPropagationPaths( oPathListAll );
				// pPathEngine->ConstructPropagationPathsWithStopWatch(oPathListAll);
				d4 = sw.stop( );
				cout << "Calculation time propagation path creation: " << timeToString( d4 ) << endl;

				size_t numPaths = oPathListAll.size( );
				cout << "Nodes: " << to_string( numNodes ) << endl;
				cout << "Visible paths: " << to_string( numPaths ) << endl;


				// Add emitter and sensor to visualization
				// oGeoModel.AddEmitterVisualization(*pSource, "Emitter A");
				// oGeoModel.AddSensorVisualization(*pSensor, "Sensor A");


				nlohmann::json jnRoot;
				jnRoot["source"]   = iSource;
				jnRoot["receiver"] = iReceiver;
				jnRoot["order"]    = iOrder;
				jnRoot["numNodes"] = numNodes;
				jnRoot["numPaths"] = numPaths;

				nlohmann::json jnProcTimes;
				jnProcTimes["preproc"]      = d1;
				jnProcTimes["source"]       = d2;
				jnProcTimes["emitter"]      = d3;
				jnProcTimes["pathsFinding"] = d4;
				jnRoot["ProcessingTimes"]   = jnProcTimes;

				std::string ss;
				ss = jnRoot.dump( );

				std::ofstream out( sSubFolder + "S" + IntToString( iSource ) + "R" + IntToString( iReceiver ) + "O" + IntToString( iOrder ) + ".json" );
				out << ss.c_str( );


				out.close( );
			}
		}
	}


	return 0;
#endif

	// Single simulation
#if 0

	const int iMaxDiffractionOrder = 4;
	const int iMaxReflectionOrder = 4;
	const int iMaxCombinedOrder = 1; //iMaxDiffractionOrder + iMaxReflectionOrder;

	ITAStopWatch sw; sw.start();

	auto pPathEngine = make_shared<CombinedModel::CPathEngine>();
	cout << "Calculation time initialization path engine: " << timeToString(sw.stop()) << endl;

	sw.start();

	pPathEngine->Configure(bOnlyNeighbouredEdgeDiffraction, bDiffractionOnlyIntoShadowRegion, bFilterNotVisiblePathsBetweenEdges, bFilterNotVisiblePointToEdge, bFilterNotVisiblePointToEdge, bFilterNotVisiblePaths, iNumIterations, fIntersectionTestResolution);
	pPathEngine->SetAbortionCriteria(iMaxDiffractionOrder, iMaxReflectionOrder, iMaxCombinedOrder, fLevelDropThreshhold, fReflectionPenalty, fDiffractionPenalty, fMaxAccumulatedDiffractionAngle);
	cout << "Calculation time configuring filter: " << timeToString(sw.stop()) << endl;

	sw.start();

	pPathEngine->InitializePathEnvironment(pMeshModelList);
	cout << "Calculation time initialization path environment: " << timeToString(sw.stop()) << endl;

	sw.start();

	pPathEngine->ApplyEmitter(pEmitter);
	cout << "Calculation time applying emitter: " << timeToString(sw.stop()) << endl;

	cout << "Number propagation path candidates after source insertion: " << pPathEngine->GetNumberPropagationPathCandidates() << endl;

	sw.start();

	pPathEngine->ApplySensor(pSensor);
	cout << "Calculation time applying sensor: " << timeToString(sw.stop()) << endl;

	CPropagationPathList oPathList;
	cout << "Calculation time applying sensor: " << timeToString(sw.stop()) << endl;

	sw.start();

	pPathEngine->ConstructPropagationPaths(oPathList);
	//pPathEngine->ConstructPropagationPathsWithStopWatch(oPathListAll);
	cout << "Calculation time propagation path creation: " << timeToString(sw.stop()) << endl;

	cout << "Visible paths: " << to_string(oPathList.size()) << endl;


	//Add emitter and sensor to visualization
	oGeoModel.AddEmitterVisualization(*pEmitter, "Emitter A");
	oGeoModel.AddSensorVisualization(*pSensor, "Sensor A");

	cout << "The path with the longest length is " << oPathList.GetMaxLength() << " m." << endl;

	size_t numberOfPaths[iMaxDiffractionOrder + 1][iMaxReflectionOrder + 1] = { 0 };
	for (auto& oPath : oPathList)
	{
		long iNumReflections = (long)oPath.GetNumReflections();
		long iNumDiffractions = (long)oPath.GetNumDiffractions();

		numberOfPaths[ iNumDiffractions ][ iNumReflections ]++;
			}

	for( auto& oPath : oPathList )
	{
		long iNumReflections = ( long ) oPath.GetNumReflections();
		long iNumDiffractions = ( long ) oPath.GetNumDiffractions();


		string sPathName = "Refl_Order_" + to_string( iNumReflections ) + "_Diffr_Order_" + to_string( iNumDiffractions ) + "_Path_Amount_" + to_string( numberOfPaths[ iNumDiffractions ][ iNumReflections ] );
		oGeoModel.AddPropagationPathVisualization( oPath, sPathName );
	}

	oPathList.Store( "CombinedModelTest_" + sInFile + ".json" );

	oGeoModel.Store( sSubFolder + "CombinedModelTest_" + sInFile );

	return 0;

#endif
}
