/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * GRAS scene 5
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>


using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;


int main( int, char** )
{
	cout << " - - - - -  GRAS scene 5.1  - - - - - " << endl << endl;


	// Geo input

	auto pMeshModelList = make_shared<ITAGeo::Halfedge::CMeshModelList>( );

	string sInFile = "GRAS_scene5_geometry.skp";
	if( pMeshModelList->Load( sInFile ) )
	{
		cout << "Successfully loaded geometry mesh from input file '" << sInFile << "'" << endl << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
		return 255;
	}

	// Scenes

	struct CGRASScene
	{
		inline CGRASScene( const std::string& sName, shared_ptr<CEmitter> pEmitter, shared_ptr<CSensor> pSensor )
		    : sName( sName )
		    , pEmitter( pEmitter )
		    , pSensor( pSensor ) {};

		shared_ptr<CSensor> pSensor;
		shared_ptr<CEmitter> pEmitter;
		string sName;
	};

	auto pMP01   = make_shared<CSensor>( VistaVector3D( 8.512f, 2.985f, 1.235f ) );
	pMP01->sName = "MP01_GRAS40AF";
	auto pMP02   = make_shared<CSensor>( VistaVector3D( 8.512f, 2.985f, 0.006f ) );
	pMP02->sName = "MP02_GRAS40AF";
	auto pMP03   = make_shared<CSensor>( VistaVector3D( 8.512f, 2.985f, 2.0f ) );
	pMP03->sName = "MP03_GRAS40AF";
	auto pMP04   = make_shared<CSensor>( VistaVector3D( 8.512f, 2.985f, 3.0f ) );
	pMP04->sName = "MP04_GRAS40AF";

	auto pLS01     = make_shared<CEmitter>( VistaVector3D( 2.487f, 2.985f, 1.235f ) );
	pLS01->sName   = "LS01_Genelec8020c";
	auto pLS02     = make_shared<CEmitter>( VistaVector3D( 2.487f, 2.985f, 2.0f ) );
	pLS02->sName   = "LS02_Genelec8020c";
	auto pLS03     = make_shared<CEmitter>( VistaVector3D( 2.487f, 2.985f, 3.0f ) );
	pLS03->sName   = "LS03_Genelec8020c";
	auto pLS04     = make_shared<CEmitter>( VistaVector3D( 2.487f, 2.985f, 0.135f ) );
	pLS04->qOrient = VistaQuaternion( ); // @todo apply transform of theta = 34.6 degree around Y direction
	pLS04->sName   = "LS04_Genelec8020c";


	std::vector<CGRASScene> vGRAS_5_1_Scenes = {
		CGRASScene( "GRAS_5_1_LS01_MP01", pLS01, pMP01 ), CGRASScene( "GRAS_5_1_LS02_MP01", pLS02, pMP01 ), CGRASScene( "GRAS_5_1_LS03_MP01", pLS03, pMP01 ),
		CGRASScene( "GRAS_5_1_LS04_MP01", pLS04, pMP01 ), CGRASScene( "GRAS_5_1_LS01_MP02", pLS01, pMP02 ), CGRASScene( "GRAS_5_1_LS02_MP02", pLS02, pMP02 ),
		CGRASScene( "GRAS_5_1_LS03_MP02", pLS03, pMP02 ), CGRASScene( "GRAS_5_1_LS04_MP02", pLS04, pMP02 ), CGRASScene( "GRAS_5_1_LS01_MP03", pLS01, pMP03 ),
		CGRASScene( "GRAS_5_1_LS02_MP03", pLS02, pMP03 ), CGRASScene( "GRAS_5_1_LS03_MP03", pLS03, pMP03 ), CGRASScene( "GRAS_5_1_LS04_MP03", pLS04, pMP03 ),
		CGRASScene( "GRAS_5_1_LS01_MP04", pLS01, pMP04 ), CGRASScene( "GRAS_5_1_LS02_MP04", pLS02, pMP04 ), CGRASScene( "GRAS_5_1_LS03_MP04", pLS03, pMP04 ),
		CGRASScene( "GRAS_5_1_LS04_MP04", pLS04, pMP04 ),
	};


	// Configuration

	const bool bOnlyNeighbouredEdgeDiffraction    = false;
	const bool bDiffractionOnlyIntoShadowedEdges  = false; //
	const bool bFilterNotVisiblePathsBetweenEdges = false; // Intersection test between edges(expensive)
	const bool bFilterNotVisiblePointToEdge       = false; // Intersection test between emitter/sensor and edges
	const bool bFilterNotVisiblePaths             = true;  // Intersection test of calculated sub paths
	const float fIntersectionTestResolution       = 0.001f;
	const int iNumIterations                      = 10; //!< Number of iterations for the calculation of the aperture points

	const float fMaxAccumulatedDiffractionAngle = -2.0f * ITAConstants::PI_F; // zero or below -> disabled

	const float fLevelDropThreshhold = -90.f - 30.f - 11.f;    // 90 dB Power level of Diesel; 30 dB masking; 11 dB initial power level drop
	const float fReflectionPenalty   = -10.0f * log10( 0.8f ); // Level reduction of a reflection with reflection factor rho = 0.8 from ISO9613-2
	const float fDiffractionPenalty =
	    2.5f; // Minimum level reduction of a diffracted signal into the shadow region for a low frequency (Taken from BA Filbert Figure 16. UTD)

	const int iMaxDiffractionOrder = 2;
	const int iMaxReflectionOrder  = 2;
	const int iMaxCombinedOrder    = 4;

	cout << "Propagation path algorithm configuration:" << endl;
	cout << "\tReflection order  : " << iMaxReflectionOrder << endl;
	cout << "\tDiffraction order : " << iMaxReflectionOrder << endl;
	cout << "\tMaximum order     : " << iMaxCombinedOrder << endl;

	cout << endl;

	CombinedModel::CPathEngine::CSimulationConfig oSimConf;
	oSimConf.bFilterNotNeighbouredEdges           = bOnlyNeighbouredEdgeDiffraction;
	oSimConf.bFilterEdgeToEdgeIntersectedPaths    = bFilterNotVisiblePathsBetweenEdges;
	oSimConf.bFilterEmitterToEdgeIntersectedPaths = bFilterNotVisiblePointToEdge;
	oSimConf.bFilterSensorToEdgeIntersectedPaths  = bFilterNotVisiblePointToEdge;
	oSimConf.bFilterIlluminatedRegionDiffraction  = bDiffractionOnlyIntoShadowedEdges;
	oSimConf.iNumberIterationApexCalculation      = iNumIterations;
	oSimConf.fIntersectionTestResolution          = fIntersectionTestResolution;
	oSimConf.bFilterIntersectedPaths              = bFilterNotVisiblePaths;

	CombinedModel::CPathEngine::CAbortionCriteria oAbort;
	oAbort.fDiffractionPenalty        = fDiffractionPenalty;
	oAbort.fAccumulatedAngleThreshold = fMaxAccumulatedDiffractionAngle;
	oAbort.fDynamicRange              = fLevelDropThreshhold;
	oAbort.fReflectionPenalty         = fReflectionPenalty;
	oAbort.iMaxCombinedOrder          = iMaxCombinedOrder;
	oAbort.iMaxDiffractionOrder       = iMaxDiffractionOrder;
	oAbort.iMaxReflectionOrder        = iMaxReflectionOrder;

	// Simulation

	ITAStopWatch sw;
	int i = 1;
	for( auto& scene: vGRAS_5_1_Scenes )
	{
		cout << "* Starting " << scene.sName << " (" << i++ << "/" << vGRAS_5_1_Scenes.size( ) << ")" << endl;
		sw.start( );

		// Path finding
		auto pPathEngine = make_shared<CombinedModel::CPathEngine>( );
		pPathEngine->SetSimulationConfiguration( oSimConf );
		pPathEngine->SetAbortionCriteria( oAbort );
		pPathEngine->InitializePathEnvironment( pMeshModelList );
		pPathEngine->SetEntities( scene.pEmitter, scene.pSensor );
		CPropagationPathList oPathList;
		pPathEngine->ConstructPropagationPaths( oPathList );
		cout << "\t -> found " << oPathList.size( ) << " visible paths" << endl;

		// Propagation path list export
		string sPropPathFilePath = scene.sName + "_propagation_paths.json";
		Utils::JSON::Export( oPathList, sPropPathFilePath );
		cout << "\tExported propagation path list to '" << sPropPathFilePath << "'" << endl;

		// Export visualization
		SketchUp::CModel oGeoModel_Viz;
		try
		{
			oGeoModel_Viz.Load( sInFile );
		}
		catch( ITAException& e )
		{
			cerr << "\tCould not load '" << sInFile << "' for visualization export: " << e << endl;
			return 255;
		}

		oGeoModel_Viz.AddEmitterVisualization( *scene.pEmitter, scene.pEmitter->sName );
		oGeoModel_Viz.AddSensorVisualization( *scene.pSensor, scene.pSensor->sName );

		size_t numberOfPaths[iMaxDiffractionOrder + 1][iMaxReflectionOrder + 1] = { 0 };
		for( auto& oPath: oPathList )
		{
			long iNumReflections  = (long)oPath.GetNumReflections( );
			long iNumDiffractions = (long)oPath.GetNumDiffractions( );

			numberOfPaths[iNumDiffractions][iNumReflections]++;
		}

		for( auto& oPath: oPathList )
		{
			long iNumReflections  = (long)oPath.GetNumReflections( );
			long iNumDiffractions = (long)oPath.GetNumDiffractions( );

			string sPathName = "Refl_Order_" + to_string( iNumReflections ) + "_Diffr_Order_" + to_string( iNumDiffractions ) + "_Path_Amount_" +
			                   to_string( numberOfPaths[iNumDiffractions][iNumReflections] );
			oGeoModel_Viz.AddPropagationPathVisualization( oPath, sPathName );
		}

		string sVizFilePath = scene.sName + "_visualization.skp";
		oGeoModel_Viz.Store( sVizFilePath );
		cout << "\tExported visualization file list to '" << sVizFilePath << "'" << endl;

		auto fRuntime = sw.stop( );
		cout << "\tFinished simulation in " << timeToString( fRuntime ) << endl;

		cout << endl;
	}

	return 0;
}
