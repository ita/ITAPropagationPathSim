function ODESolverTest()

%% Define Atmospheres

inhomAtmos = itaAtmosphere;

%Settings
%(These are the standard settings, itaAtmosphere is already initialized with these values)
inhomAtmos.windProfile = WindProfile.Log; %Enum: Zero, Const, Log
inhomAtmos.tempProfile = TempProfile.ISA; %Enum: Const, ISA

inhomAtmos.z0 = 0.1;                 %Surface Roughness for Log Wind Profile [m]
inhomAtmos.kappa = 0.4;              %Karman constant for Log Wind Profile []
inhomAtmos.vStar = 0.6;              %Friction velocity for Log Wind Profile [m/s]

inhomAtmos.vDirConst = [1 0 0];      %Normal in wind direction []

inhomAtmos.R = 1.4;                  %Air Gas Constant, used to calculate c
inhomAtmos.gamma = 402.8/1.4;        %Ratio of Specific Heats, used to calculate c

homAtmos = inhomAtmos;
homAtmos.windProfile = WindProfile.Zero;
homAtmos.tempProfile = TempProfile.Const;
homAtmos.TConst = 293.15;
homAtmos.constStaticPressure = 101325;

%% Define Ray Distribution
theta = 135;
% theta = 90;
phi = 0;
rayDist = RayDistribution(Distribution.ModifiedGaussian, theta, phi);

%% Define Ray Tracer
art = itaART;
art.rayDistribution = rayDist;

art.source = [0 0 1000];        %Can also be a single itaCoordinates
art.receiver = [0 0 0];         %Can also be a single itaCoordinates

art.dt = 0.01;                   %Integration variable [s]
art.tMax = 10;                   %Maximum time of tracing [s]

art.bAdaptiveDt = false;
art.bCopyPeriodicRefl = false;
art.bUseSlowness = true;
art.bUseWaitbar = false;

%% Run Tests
art.atmosphere = homAtmos;
cppFileSuffix = 'Homogeneous';
plotTitle = 'ODE-Solver - Homogeneous';
runMatlabVsCppRayTest(art, ['ODESolverTest_Euler_' cppFileSuffix '.json'],...
    ['ODESolverTest_RungeKutta_' cppFileSuffix '.json'], plotTitle, true);

art.atmosphere = inhomAtmos;
cppFileSuffix = 'Inhomogeneous';
plotTitle = 'ODE-Solver - Inhomogeneous';
runMatlabVsCppRayTest(art, ['ODESolverTest_Euler_' cppFileSuffix '.json'],...
    ['ODESolverTest_RungeKutta_' cppFileSuffix '.json'], plotTitle, true);
