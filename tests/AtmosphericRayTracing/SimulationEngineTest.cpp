/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests the image (source) model algorithm.
 *
 */

#include <ITAStopWatch.h>
// #include <ITAStringUtils.h>

#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/ODESolver/ODESolver.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/RayGrid.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Utils/RayToPropagationPath.h>

// #include <cassert>

#include <math.h>
#include <stdio.h>
#include <vector>

using namespace std;
using namespace ITAGeo;
using namespace ITAGeo::Utils;
using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::Simulation;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::Utils;

void runTest( const CStratifiedAtmosphere& atmosphere, const double& sourceAltitude, const VistaVector3D& rayDirection, const string& fileSuffix )
{
	double tMax = 15;
	auto engine = Simulation::CEngine( std::make_shared<CAbortAtMaxTime>( tMax ) );
	double dt   = 0.01;

	engine.settings.adaptiveIntegration.bActive             = true;
	engine.settings.adaptiveIntegration.dMaxError           = 0.015;
	engine.settings.adaptiveIntegration.dUncriticalError    = 0.005;
	engine.settings.adaptiveIntegration.iMaxAdaptationLevel = 32; // 32;


	engine.settings.dIntegrationTimeStep = dt;
	VistaVector3D sourcePosition         = VistaVector3D( 0, 0, sourceAltitude );

	cout << fileSuffix << ":" << endl;
	cout << "Starting Simulation-Engine..." << endl;
	engine.settings.solverMethod                   = Simulation::EULER;
	std::vector<std::shared_ptr<CRay>> resultEuler = engine.Run( atmosphere, sourcePosition, { rayDirection } );
	engine.settings.solverMethod                   = Simulation::RUNGE_KUTTA;
	std::vector<std::shared_ptr<CRay>> resultRunge = engine.Run( atmosphere, sourcePosition, { rayDirection } );
	cout << "Starting export..." << endl;

	CPropagationPath eulerRay = ToPropagationPath( *resultEuler[0] );
	JSON::Export( eulerRay, "SimulationEngineTest_Euler_" + fileSuffix + ".json" );
	CPropagationPath rungeRay = ToPropagationPath( *resultRunge[0] );
	JSON::Export( rungeRay, "SimulationEngineTest_RungeKutta_" + fileSuffix + ".json" );
	cout << "Finished" << endl << endl;
}

void TestHomogeneousAtmosphere( const double& sourceAltitude, const VistaVector3D& rayDirection, const string& fileSuffix )
{
	auto humidProfile          = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile           = std::make_shared<TemperatureProfiles::CRoom>( );
	auto windProfile           = std::make_shared<WindProfiles::CZero>( );
	auto homogeneousAtmosphere = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );

	runTest( homogeneousAtmosphere, sourceAltitude, rayDirection, fileSuffix );
}

void TestInhomogeneousAtmosphere( const double& sourceAltitude, const VistaVector3D& rayDirection, const string& fileSuffix )
{
	auto humidProfile            = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile             = std::make_shared<TemperatureProfiles::CISA>( );
	auto windProfile             = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
	auto inhomogeneousAtmosphere = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );

	runTest( inhomogeneousAtmosphere, sourceAltitude, rayDirection, fileSuffix );
}

void TestTraceMultipleRays( bool bUseMultiThreading )
{
	auto humidProfile = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile  = std::make_shared<TemperatureProfiles::CISA>( );
	auto windProfile  = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
	auto atmosphere   = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );

	auto engine                     = Simulation::CEngine( );
	engine.settings.bMultiThreading = bUseMultiThreading;
	VistaVector3D sourcePosition    = VistaVector3D( 0, 0, 1000 );

	auto baseRayGrid = CEquiangularRayDistribution( sourcePosition, 8, 2 );
	// auto baseRayGrid = CEquiangularRayDistribution(sourcePosition, 13, 24);
	ITAStopWatch sw;
	for( int idx = 0; idx < 100; idx++ )
	{
		auto rayGrid = baseRayGrid.CopyWithNewRays( );
		sw.start( );
		engine.Run( atmosphere, rayGrid.UniqueRays( ) );
		sw.stop( );
	}
	if( bUseMultiThreading )
		cout << "Benchmark with OpenMP:" << endl;
	else
		cout << "Benchmark without OpenMP:" << endl;
	cout << sw << endl << endl;
}


int main( int iNumInArgs, char* pcInArgs[] )
{
	// Sequential
	TestTraceMultipleRays( false );

	// Multithreading
	TestTraceMultipleRays( true );

	double sourceAltitude = 1000;
	auto rayDirection     = VistaVector3D( 1, 0, -1 ).GetNormalized( );
	TestHomogeneousAtmosphere( sourceAltitude, rayDirection, "HomogeneousDiagonal" );

	sourceAltitude = 1000;
	rayDirection   = VistaVector3D( 1, 0, -1 ).GetNormalized( );
	TestInhomogeneousAtmosphere( sourceAltitude, rayDirection, "InhomogeneousDiagonal" );

	sourceAltitude = 1000;
	rayDirection   = VistaVector3D( 0, 0, -1 ).GetNormalized( );
	TestInhomogeneousAtmosphere( sourceAltitude, rayDirection, "InhomogeneousDownward" );

	sourceAltitude = 50;
	rayDirection   = VistaVector3D( 1, 0, 0 ).GetNormalized( );
	TestInhomogeneousAtmosphere( sourceAltitude, rayDirection, "Multi-Reflection" );

	sourceAltitude     = 50;
	const double theta = 101;
	rayDirection       = VistaVector3D( -sin( theta / 180.0 * M_PI ), 0, cos( theta / 180.0 * M_PI ) );
	TestInhomogeneousAtmosphere( sourceAltitude, rayDirection, "Upwind" );
}
