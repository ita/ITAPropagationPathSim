function SimulationEngineTest()

%% Define Atmospheres

inhomAtmos = itaAtmosphere;

%Settings
%(These are the standard settings, itaAtmosphere is already initialized with these values)
inhomAtmos.windProfile = WindProfile.Log; %Enum: Zero, Const, Log
inhomAtmos.tempProfile = TempProfile.ISA; %Enum: Const, ISA

inhomAtmos.z0 = 0.1;                 %Surface Roughness for Log Wind Profile [m]
inhomAtmos.kappa = 0.4;              %Karman constant for Log Wind Profile []
inhomAtmos.vStar = 0.6;              %Friction velocity for Log Wind Profile [m/s]

inhomAtmos.vDirConst = [1 0 0];      %Normal in wind direction []

inhomAtmos.R = 1.4;                  %Air Gas Constant, used to calculate c
inhomAtmos.gamma = 402.8/1.4;        %Ratio of Specific Heats, used to calculate c

homAtmos = inhomAtmos;
homAtmos.windProfile = WindProfile.Zero;
homAtmos.tempProfile = TempProfile.Const;
homAtmos.TConst = 293.15;
homAtmos.constStaticPressure = 101325;

%% Define Ray Distribution
% theta = 135;
% theta = 90;
% theta = 180;
% phi = 0;
% rayDist = RayDistribution(Distribution.ModifiedGaussian, theta, phi);

%% Define Ray Tracer
art = itaART;
% art.rayDistribution = rayDist;

art.source = [0 0 50];        %Can also be a single itaCoordinates
art.receiver = [0 0 0];         %Can also be a single itaCoordinates

art.dt = 0.01;                   %Integration variable [s]
art.tMax = 15;                   %Maximum time of tracing [s]

art.bAdaptiveDt = true;
art.maxErrorV = 0.015;
art.uncriticalErrorV = 0.005;
%art: maximum level for dt adaptation = 32

art.bCopyPeriodicRefl = true;
art.bUseSlowness = true;
art.bUseWaitbar = false;

%% Run Tests
art.atmosphere = homAtmos;
art.source = [0 0 1000];
theta = 135; art.rayDistribution = RayDistribution(Distribution.ModifiedGaussian, theta, 0);
cppFileSuffix = 'HomogeneousDiagonal';
runSingleTest(art, cppFileSuffix)

art.atmosphere = inhomAtmos;
art.source = [0 0 1000];
theta = 135; art.rayDistribution = RayDistribution(Distribution.ModifiedGaussian, theta, 0);
cppFileSuffix = 'InhomogeneousDiagonal';
runSingleTest(art, cppFileSuffix);

art.atmosphere = inhomAtmos;
art.source = [0 0 1000];
theta = 180; art.rayDistribution = RayDistribution(Distribution.ModifiedGaussian, theta, 0);
cppFileSuffix = 'InhomogeneousDownward';
runSingleTest(art, cppFileSuffix);

art.atmosphere = inhomAtmos;
art.source = [0 0 50];
theta = 90; art.rayDistribution = RayDistribution(Distribution.ModifiedGaussian, theta, 0);
cppFileSuffix = 'Multi-Reflection';
plotTitle = 'Downwind - Multi-Reflection';
runSingleTest(art, cppFileSuffix, plotTitle);

art.atmosphere = inhomAtmos;
art.source = [0 0 50];
theta = 101; phi = 180; art.rayDistribution = RayDistribution(Distribution.ModifiedGaussian, theta, phi);
cppFileSuffix = 'Upwind';
plotTitle = 'Upwind';
runSingleTest(art, cppFileSuffix, plotTitle);

function runSingleTest(art, cppFileSuffix, plotTitle)
if nargin == 2; plotTitle = cppFileSuffix; end
runMatlabVsCppRayTest(art, ['SimulationEngineTest_Euler_' cppFileSuffix '.json'],...
    ['SimulationEngineTest_RungeKutta_' cppFileSuffix '.json'], plotTitle, true);
