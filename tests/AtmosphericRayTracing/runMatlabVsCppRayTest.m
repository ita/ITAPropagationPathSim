function runMatlabVsCppRayTest(art, cppEulerFile, cppRungeKuttaFile, plotTitleBase, doPlotRays)

if nargin == 4
    doPlotRays = false;
end

%% Ray Tracing
art.bUseRungeKutta = false;
artRes = art.traceRays;
matlabEulerRay = artRes.rays;

art.bUseRungeKutta = true;
artRes = art.traceRays;
matlabRungeRay = artRes.rays;

%% Load C++ Results
cppEulerRay = itaAtmosRay.load(cppEulerFile);
cppRungeRay = itaAtmosRay.load(cppRungeKuttaFile);

%% Filtering
cppEulerPoints = cppEulerRay.interpolateToRay(matlabEulerRay, 'nearest');
cppRungePoints = cppRungeRay.interpolateToRay(matlabRungeRay, 'nearest');

%% Error Calculation
rMatlabEuler = matlabEulerRay.applyGroundReflection();
deltaREuler = cppEulerPoints - rMatlabEuler;
deltaREulerAbs = deltaREuler.r;

rMatlabRunge = matlabRungeRay.applyGroundReflection();
deltaRRunge = cppRungePoints - rMatlabRunge;
deltaRRungeAbs = deltaRRunge.r;

%% Relative Error
pathLengthEuler = [1 matlabEulerRay.pathLength(2:matlabEulerRay.numPoints)]';
pathLengthRunge = [1 matlabRungeRay.pathLength(2:matlabRungeRay.numPoints)]';
relDeltaREuler = deltaREulerAbs ./ pathLengthEuler * 100;
relDeltaRRunge = deltaRRungeAbs ./ pathLengthRunge * 100;

%% Plot rays
if doPlotRays
    plotRays(matlabEulerRay, matlabRungeRay, cppEulerRay, cppRungeRay)
    title(plotTitleBase)
end

%% Plot distance
t = art.timeVec;
plotDistance(matlabEulerRay.t, deltaREulerAbs, matlabRungeRay.t, deltaRRungeAbs, plotTitleBase);
plotRelativeDistance(matlabEulerRay.t, relDeltaREuler, matlabRungeRay.t, relDeltaRRunge, plotTitleBase)

function plotDistance(tEuler, deltaREulerAbs, tRunge, deltaRRungeAbs, titleStr)
figure
plot(tEuler, deltaREulerAbs)
hold on
plot(tRunge, deltaRRungeAbs)
title([titleStr ' - C++ vs Matlab'])
xlabel('t [s]')
ylabel('d [m]')
legend({'Euler method', 'Runge-Kutta method'}, 'location', 'northwest');
grid on

function plotRelativeDistance(tEuler, relDeltaREuler, tRunge, relDeltaRRunge, titleStr)

figure
plot(tEuler, relDeltaREuler)
hold on
plot(tRunge, relDeltaRRunge)
title([titleStr ' - C++ vs Matlab'])
xlabel('t [s]')
ylabel('\eta d [%]')
legend({'Euler method', 'Runge-Kutta method'}, 'location', 'northwest');
grid on

function plotRays(matlabEulerRay, matlabRungeRay, cppEulerRay, cppRungeRay)
f = figure;
baseLineWidth = 1.5;

matlabEulerRay.pr(f, {'linestyle', 'color', 'linewidth'}, {'-', [0 0.6 0], baseLineWidth*1.25});
hold on
matlabRungeRay.pr(f, {'linestyle', 'color', 'linewidth'}, {'-.',[0 0 0.6], baseLineWidth});
cppEulerRay.pr(f, {'linestyle', 'color', 'linewidth'}, {':',[1.0 0.6 0], baseLineWidth*1.5});
cppRungeRay.pr(f, {'linestyle', 'color', 'linewidth'}, {'--', [0.6 0 0], baseLineWidth*1.25});
legend({'Matlab - Euler', 'Matlab - Runge-Kutta', 'C++ - Euler', 'C++ - Runge-Kutta'}, 'location', 'northwest');
grid on
view(0,0)