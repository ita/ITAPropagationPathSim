cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

add_subdirectory ("EigenraySearch")

# ######################################################################################################################

add_executable (ODESolverTest ODESolverTest.cpp)
target_link_libraries (ODESolverTest PUBLIC ITAPropagationPathSim::ITAPropagationPathSim)

set_property (TARGET ODESolverTest PROPERTY FOLDER "Tests/ITAPropagationPathSim/AtmosphericRayTracing")
set_property (TARGET ODESolverTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ODESolverTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (SimulationEngineTest SimulationEngineTest.cpp)
target_link_libraries (SimulationEngineTest PUBLIC ITAPropagationPathSim::ITAPropagationPathSim)

set_property (TARGET SimulationEngineTest PROPERTY FOLDER "Tests/ITAPropagationPathSim/AtmosphericRayTracing")
set_property (TARGET SimulationEngineTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS SimulationEngineTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
