/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests the image (source) model algorithm.
 *
 */

// #include <ITAStopWatch.h>
// #include <ITAStringUtils.h>

#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/ODESolver/ODESolver.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Utils/RayToPropagationPath.h>

// #include <cassert>

#include <stdio.h>
#include <vector>

using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::Utils;


void runTest( const CStratifiedAtmosphere& atmosphere, const string& fileSuffix )
{
	double tMax = 10;
	double dt   = 0.01;
	double rz   = 1000;
	auto n      = VistaVector3D( 1, 0, -1 ).GetNormalized( );

	double c        = atmosphere.SpeedOfSound( rz );
	VistaVector3D v = atmosphere.WindVector( rz );

	auto r = VistaVector3D( 0, 0, rz );
	auto s = n / ( c + n.Dot( v ) );

	auto rayEuler = CRay( r, n );
	auto rayRunge = CRay( r, n );

	auto rRunge = r;
	auto sRunge = s;
	double time = 0.0;

	cout << fileSuffix << ":" << endl;
	cout << "Start solving ODE..." << endl;
	for( double t = 0.0; t < tMax - dt / 2; t += dt )
	{
		time += dt;
		ODESolver::Euler( r, s, dt, atmosphere );
		rayEuler.Append( r, s, time );

		ODESolver::RungeKutta( rRunge, sRunge, dt, atmosphere );
		rayRunge.Append( rRunge, sRunge, time );
	}
	cout << "Starting export..." << endl;

	ITAGeo::Utils::JSON::Export( ToPropagationPath( rayEuler ), "ODESolverTest_Euler_" + fileSuffix + ".json" );
	ITAGeo::Utils::JSON::Export( ToPropagationPath( rayRunge ), "ODESolverTest_RungeKutta_" + fileSuffix + ".json" );
	cout << "Finished" << endl << endl;
}


int main( int iNumInArgs, char* pcInArgs[] )
{
	auto humidProfile          = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile           = std::make_shared<TemperatureProfiles::CRoom>( );
	auto windProfile           = std::make_shared<WindProfiles::CZero>( );
	auto homogeneousAtmosphere = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );

	auto humidProfile2           = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile2            = std::make_shared<TemperatureProfiles::CISA>( );
	auto windProfile2            = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
	auto inhomogeneousAtmosphere = CStratifiedAtmosphere( tempProfile2, windProfile2, humidProfile2 );

	runTest( homogeneousAtmosphere, "Homogeneous" );
	runTest( inhomogeneousAtmosphere, "Inhomogeneous" );
}
