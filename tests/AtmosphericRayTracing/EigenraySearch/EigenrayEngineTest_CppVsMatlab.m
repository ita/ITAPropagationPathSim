function EigenrayEngineTest_CppVsMatlab()

%% Define Atmospheres

inhomAtmos = itaAtmosphere;

%Settings
%(These are the standard settings, itaAtmosphere is already initialized with these values)
inhomAtmos.windProfile = WindProfile.Log; %Enum: Zero, Const, Log
inhomAtmos.tempProfile = TempProfile.ISA; %Enum: Const, ISA

inhomAtmos.z0 = 0.1;                 %Surface Roughness for Log Wind Profile [m]
inhomAtmos.kappa = 0.4;              %Karman constant for Log Wind Profile []
inhomAtmos.vStar = 0.6;              %Friction velocity for Log Wind Profile [m/s]

inhomAtmos.vDirConst = [1 0 0];      %Normal in wind direction []

inhomAtmos.R = 1.4;                  %Air Gas Constant, used to calculate c
inhomAtmos.gamma = 402.8/1.4;        %Ratio of Specific Heats, used to calculate c

homAtmos = inhomAtmos;
homAtmos.windProfile = WindProfile.Zero;
homAtmos.tempProfile = TempProfile.Const;
homAtmos.TConst = 293.15;
homAtmos.constStaticPressure = 101325;

%% Define Ray Tracer
art = itaART;

art.source = [0 0 50];        %Can also be a single itaCoordinates
art.receiver = [0 0 1.8];         %Can also be a single itaCoordinates

art.dt = 0.1;                   %Integration variable [s]
art.tMax = 15;                   %Maximum time of tracing [s]

art.bAdaptiveDt = true;
art.maxErrorV = 0.015;
art.uncriticalErrorV = 0.005;
%art: maximum level for dt adaptation = 32

art.bCopyPeriodicRefl = true;
art.bUseSlowness = true;
art.bUseWaitbar = false;

%% Eigenray search properties

art.bLimitEigenraySearchDirections = false; %If true, directions in which an eigenray is looked for will be limited

art.maxReceiverRadius = 1;                  %Maximum receiver radius used to calculate receiverRadius
art.maxSourceReceiverAngle = 1;             %Maximum allowed angle between direct connection and eigenray used to calculate receiverRadius
art.maxEigenraySearchIterations = 30;       %Abort criterion for eigenray search: maximum number of adaptations.
art.minEigenraySearchAngle = 0.001;         %Abort criterion for eigenray search: minimum angle between rays.

art.rayZoomThresh = 1.0;                    %Threshold used for improved ray zooming. If this is undershot, the algorithm does not make a decision.
art.angleForGeomSpreading = 0.01;           %Angle between rays used to calculate the geometrical spreading

art.maxReflOrder = 1;

%% Test Matlab vs Cpp
art.atmosphere = homAtmos;
runTestReceiverNearGroundSourceElevation(art, 'Homogeneous');

art.atmosphere = inhomAtmos;
runTestReceiverNearGroundSourceElevation(art, 'Inhomogeneous');


function runTestReceiverNearGroundSourceElevation(art, atmosphereSuffix)
sourceReceiverDistance = 2000;
phi = 0;
for sourceRecElevation = -90:45:90
    art.source.x = sourceReceiverDistance*sind(sourceRecElevation) + art.receiver.x;
    art.source.y = art.receiver.y;
    art.source.z = sourceReceiverDistance*cosd(sourceRecElevation) + art.receiver.z;
    
    jsonFolder = fullfile( 'output', fileparts(mfilename('fullpath')) );
    cppFilename = ['ReceiverNearGround_' atmosphereSuffix '_SourceElevation' num2str(sourceRecElevation) '_SourceAzimuth' num2str(phi) '.json'];
    runMatlabVsCppEigenrayTest(art, fullfile(jsonFolder, cppFilename), fileparts(cppFilename), true);
end
