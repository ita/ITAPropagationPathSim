function runMatlabVsCppEigenrayTest(art, cppFile, plotTitle, doPlotRays)

if nargin == 3
    doPlotRays = false;
end

%% Load C++ Results
cppRays = itaAtmosRay.load(cppFile);

%% Ray Tracing
matlabRays = art.findEigenrays;

%% Number of rays
if numel(matlabRays) ~= numel(cppRays)
    warning('Number of eigenrays of Matlab and C++ simulation does not match')
end

%% Plot rays
if doPlotRays
    plotRays(art, matlabRays, cppRays)
    title(plotTitle)
end

%% Plot distance
plotAbsoluteAndRelativeDistance(matlabRays, cppRays, plotTitle)


 function plotAbsoluteAndRelativeDistance(matlabRays, cppRays, titleStr)
%% Init
nRays = min(numel(matlabRays), numel(cppRays));
axAbsoluteDist = axes(figure);
axRelativeDist = axes(figure);
hold(axAbsoluteDist, 'on')
hold(axRelativeDist, 'on')

for idx=1:nRays
    %% Error Calculation
    cppPoints = cppRays(idx).interpolateToRay(matlabRays(idx), 'linear');
    matlabPoints = matlabRays(idx).applyGroundReflection();
    
    deltaR = cppPoints - matlabPoints;
    deltaRAbs = deltaR.r;
    
    pathLength = [1 matlabRays(idx).pathLength(2:matlabRays(idx).numPoints)]';
    relDeltaR = deltaRAbs ./ pathLength * 100;
    
    plot(axAbsoluteDist, matlabRays(idx).t, deltaRAbs)    
    plot(axRelativeDist, matlabRays(idx).t, relDeltaR)
end

%%
title(axAbsoluteDist, [titleStr ' - C++ vs Matlab'])
xlabel(axAbsoluteDist, 't [s]')
ylabel(axAbsoluteDist, 'd [m]')
legend(axAbsoluteDist, {'Direct path', '1st-order reflection'}, 'location', 'northwest');
grid(axAbsoluteDist, 'on')

title(axRelativeDist, [titleStr ' - C++ vs Matlab'])
xlabel(axRelativeDist, 't [s]')
ylabel(axRelativeDist, '\eta d [%]')
legend(axRelativeDist, {'Direct path', '1st-order reflection'}, 'location', 'northwest');
grid(axRelativeDist, 'on')


function plotRays(art, matlabRays, cppRays)
    
nRays = min(numel(matlabRays), numel(cppRays));
f = figure;
baseLineWidth = 1.5;

for idx=1:nRays
    matlabRays(idx).pr(f, {'linestyle', 'linewidth'}, {'-',  baseLineWidth*1.25});
    hold on
    cppRays(idx).pr(f, {'linestyle', 'linewidth'}, {'--', baseLineWidth*1.5});
end
% matlabRays.pr(f, {'linestyle', 'color', 'linewidth'}, {'-', [0 0.6 0], baseLineWidth*1.25});
% matlabRungeRay.pr(f, {'linestyle', 'color', 'linewidth'}, {'-.',[0 0 0.6], baseLineWidth});
% cppRays.pr(f, {'linestyle', 'color', 'linewidth'}, {':',[1.0 0.6 0], baseLineWidth*1.5});
% cppRungeRay.pr(f, {'linestyle', 'color', 'linewidth'}, {'--', [0.6 0 0], baseLineWidth*1.25});

plot3(art.source.x, art.source.y, art.source.z, ' ok', 'markerfacecolor', 'b')
plot3(art.receiver.x, art.receiver.y, art.receiver.z, ' ok', 'markerfacecolor', [0 0.6 0])

legend({'Direct path - Matlab', 'Direct path - Cpp', '1st-order - Matlab', '1st-order - Cpp'}, 'location', 'northwest');
grid on
view(0,0)