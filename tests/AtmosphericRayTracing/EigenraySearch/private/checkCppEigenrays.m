function checkCppEigenrays(cppFile, plotTitle)

%% Load C++ Results
if exist('AtmosphericRay', 'class')
    cppRays = AtmosphericRay.load(cppFile);
else
    cppRays = itaAtmosRay.load(cppFile);
end
if nargin == 1
    [~, plotTitle, ~] = fileparts(cppFile);
end

%% Plot rays
receiverPos = [0 0 1.8];
nRays = numel(cppRays);
f = figure;
ax = axes(f);
hold(ax, 'on');

legendEntries = cell(size(cppRays));
for idx=1:nRays
    receiverDistance = norm(cppRays(idx).r.cart(end,:) - receiverPos);
    reflOrder = cppRays(idx).numReflections;
    legendEntries{idx} = ['order: ' num2str(reflOrder) ', dist: ' num2str(receiverDistance) 'm'];
    if isa(cppRays, 'AtmosphericRay')
        cppRays(idx).plot([], ax, 'linestyle', '--', 'linewidth', 2.25);
    else
        cppRays(idx).pr(f, {'linestyle', 'linewidth'}, {'--', 2.25});
    end
end

plot3(cppRays(1).r0.x, cppRays(1).r0.y, cppRays(1).r0.z, ' ok', 'markerfacecolor', 'b')
plot3(receiverPos(1), receiverPos(2), receiverPos(3), ' ok', 'markerfacecolor', [0 0.6 0])

legend(legendEntries, 'location', 'northwest');
grid on
view(0,0)
title(plotTitle)