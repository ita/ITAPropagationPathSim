/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests the image (source) model algorithm.
 *
 */

// #include <ITAStopWatch.h>
// #include <ITAStringUtils.h>

#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/AdaptiveRayGrid.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;
using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch;

void CheckNumberOfRays( const std::string& type, const int expectedNumber, const int realNumber )
{
	cout << type << ": ";
	if( realNumber == expectedNumber )
		cout << "OK: Expected and found " << realNumber << " rays." << endl;
	else
		cout << "ERROR: Expected " << expectedNumber << " but found " << realNumber << " rays." << endl;
}

void CheckAdaptedRayGrid( const CAdaptiveRayGrid& adaptiveRayGrid, int nExpectedRays, int nExpectedUniqueRays, int nExpectedNewRays )
{
	cout << "Checking adapted ray grid..." << endl;
	cout << "Theta angles: ";
	for( const double& theta: adaptiveRayGrid.ThetaDeg( ) )
		cout << theta << " ";
	cout << endl;

	cout << "Phi angles: ";
	for( const double& phi: adaptiveRayGrid.PhiDeg( ) )
		cout << phi << " ";
	cout << endl;

	const int nRays       = adaptiveRayGrid.NTheta( ) * adaptiveRayGrid.NPhi( );
	const int nUniqueRays = adaptiveRayGrid.UniqueRays( ).size( );
	const int nNewRays    = adaptiveRayGrid.NewRaysOfLastAdaptation( ).size( );

	CheckNumberOfRays( "All rays", nExpectedRays, nRays );
	CheckNumberOfRays( "Unique rays", nExpectedUniqueRays, nUniqueRays );
	CheckNumberOfRays( "New rays", nExpectedNewRays, nNewRays );
	cout << endl;
}

void Test2DGrid( )
{
	auto sourcePos                      = VistaVector3D( 0, 0, 1000 );
	CEquiangularRayDistribution rayGrid = CEquiangularRayDistribution( sourcePos, 7, 12 );
	const int nTheta                    = rayGrid.NTheta( );
	const int nPhi                      = rayGrid.NPhi( );

	CAdaptiveRayGrid adaptiveRayGrid;
	CAdaptiveRayGrid nonCircularGrid;
	CAdaptiveRayGrid nonCircularGridWithDuplicates;
	std::shared_ptr<CRay> ray;

	//--- 1) RAY IN MIDDLE ---
	//------------------------
	// a) no duplicates
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 2, 2 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 25, 25, 16 );

	// b) with duplicates
	nonCircularGridWithDuplicates = CAdaptiveRayGrid( rayGrid );
	ray                           = rayGrid.At( 1, 2 );
	nonCircularGridWithDuplicates.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( nonCircularGridWithDuplicates, 25, 21, 14 );


	//--- 2) AT BOUNDARY, CIRCULAR ---
	//--------------------------------
	// a) at lower phi-"boundary"
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 3, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 25, 25, 16 );

	// b) at upper phi-"boundary"
	nonCircularGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 2, 11 );
	nonCircularGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( nonCircularGrid, 25, 25, 16 );


	//--- 3) AT BOUNDARY, NON-CIRCULAR ---
	//------------------------------------
	// a) At lower theta/phi boundary ---
	adaptiveRayGrid = nonCircularGrid; // 5x5
	ray             = adaptiveRayGrid.At( 0, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 9, 9, 5 );

	// b) At upper theta/phi boundary ---
	adaptiveRayGrid = nonCircularGrid; // 5x5
	ray             = adaptiveRayGrid.At( 4, 4 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 9, 9, 5 );

	// c) At northpole ---
	adaptiveRayGrid = nonCircularGridWithDuplicates; // 5x5
	ray             = adaptiveRayGrid.At( 0, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 27, 19, 13 );

	// d) At lower phi boundary, next to northpole ---
	adaptiveRayGrid = nonCircularGridWithDuplicates; // 5x5
	ray             = adaptiveRayGrid.At( 1, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 15, 13, 8 );


	//--- 4) AT POLES ---
	//-------------------
	// a) Northpole
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 0, 5 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 3 * ( 2 * nPhi ), 2 * ( 2 * nPhi ) + 1, ( 2 + 1 ) * nPhi );

	// b) Southpole
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 6, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 3 * ( 2 * nPhi ), 2 * ( 2 * nPhi ) + 1, ( 2 + 1 ) * nPhi );
}

void Test1DThetaGrid( )
{
	auto sourcePos   = VistaVector3D( 0, 0, 1000 );
	auto rayGrid     = CRayGrid( sourcePos, { 0, 30, 60, 90, 120, 150, 180 }, { 0 } );
	const int nTheta = rayGrid.NTheta( );
	const int nPhi   = rayGrid.NPhi( );

	CAdaptiveRayGrid adaptiveRayGrid;
	CAdaptiveRayGrid nonCircularGrid;
	CAdaptiveRayGrid nonCircularGridIncludingNorthPole;
	std::shared_ptr<CRay> ray;

	//--- 1) RAY IN MIDDLE ---
	//------------------------
	// a) no duplicates
	nonCircularGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 2, 0 );
	nonCircularGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( nonCircularGrid, 5, 5, 2 );

	// b) with duplicates
	nonCircularGridIncludingNorthPole = CAdaptiveRayGrid( rayGrid );
	ray                               = rayGrid.At( 1, 0 );
	nonCircularGridIncludingNorthPole.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( nonCircularGridIncludingNorthPole, 5, 5, 2 );


	//--- 3) AT BOUNDARY, NON-CIRCULAR ---
	//------------------------------------
	// a) At lower theta/phi boundary ---
	adaptiveRayGrid = nonCircularGrid; // 5x1
	ray             = adaptiveRayGrid.At( 0, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 3, 3, 1 );

	// b) At upper theta/phi boundary ---
	adaptiveRayGrid = nonCircularGrid; // 5x1
	ray             = adaptiveRayGrid.At( 4, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 3, 3, 1 );

	// d) At lower phi boundary, next to northpole ---
	adaptiveRayGrid = nonCircularGridIncludingNorthPole; // 5x1
	ray             = adaptiveRayGrid.At( 1, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 5, 5, 2 );


	//--- 4) AT POLES ---
	//-------------------
	// a) Northpole
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( 0, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 3, 3, 1 );

	// b) Southpole
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	ray             = rayGrid.At( nTheta - 1, 0 );
	adaptiveRayGrid.ZoomIntoRay( ray );
	CheckAdaptedRayGrid( adaptiveRayGrid, 3, 3, 1 );
}

// void Test1DPhiGrid()
//{
//    auto sourcePos = VistaVector3D(0, 0, 1000);
//    std::vector<double> phiVec = { 0, 90, 180, 270 };
//    std::vector<double> thetaVec = { 0 };
//    CRayGrid poleRayGrid1D = CRayGrid(sourcePos, thetaVec, phiVec, true);
//    thetaVec = { 90 };
//    CRayGrid rayGrid1D = CRayGrid(sourcePos, thetaVec, phiVec, false);
//}

void TestAdvancedRayZooming( )
{
	const auto sourcePos                = VistaVector3D( 0, 0, 1000 );
	const auto receiverPos              = VistaVector3D( 500, 0, 900 );
	CEquiangularRayDistribution rayGrid = CEquiangularRayDistribution( sourcePos, 7, 12 );
	const float dEnd                    = 1000.0;
	const double tEnd                   = 10.0;
	const int nPoints                   = 10;
	for( auto ray: rayGrid.UniqueRays( ) )
	{
		for( int idxPoint = 1; idxPoint < nPoints; idxPoint++ )
		{
			ray->Append( rayGrid.SourcePosition( ) + ray->InitialDirection( ) * dEnd * idxPoint / nPoints, ray->InitialDirection( ), tEnd * idxPoint / nPoints );
			ray->UpdateMinimumReceiverDistance( receiverPos );
		}
		ray->FinalizeMinimumReceiverDistances( );
	}

	CAdaptiveRayGrid adaptiveRayGrid;
	std::shared_ptr<CRay> pRay;

	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	pRay            = rayGrid.At( 3, 0 );
	adaptiveRayGrid.ZoomIntoRay( pRay, receiverPos, 0.1 );
	CheckAdaptedRayGrid( adaptiveRayGrid, 15, 15, 9 );
}

void TestSpecificRayZooming( )
{
	const auto sourcePos                = VistaVector3D( 0, 0, 1000 );
	CEquiangularRayDistribution rayGrid = CEquiangularRayDistribution( sourcePos, 7, 12 );
	const int nTheta                    = rayGrid.NTheta( );
	CAdaptiveRayGrid adaptiveRayGrid;
	std::shared_ptr<CRay> pRay;

	// Default
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	pRay            = rayGrid.At( 1, 0 );
	adaptiveRayGrid.ZoomIntoRay( pRay, 0.01, 0.01 );
	CheckAdaptedRayGrid( adaptiveRayGrid, 9, 9, 8 );

	// Northpole
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	pRay            = rayGrid.At( 0, 0 );
	adaptiveRayGrid.ZoomIntoRay( pRay, 0.01, 0.01 );
	CheckAdaptedRayGrid( adaptiveRayGrid, 6, 4, 3 );

	// Southpole
	adaptiveRayGrid = CAdaptiveRayGrid( rayGrid );
	pRay            = rayGrid.At( nTheta - 1, 0 );
	adaptiveRayGrid.ZoomIntoRay( pRay, 0.01, 0.01 );
	CheckAdaptedRayGrid( adaptiveRayGrid, 6, 4, 3 );

	// Next to pole
	adaptiveRayGrid = CAdaptiveRayGrid( CRayGrid( sourcePos, { 0.01 }, { 5, 10, 15 } ) );
	pRay            = adaptiveRayGrid.At( 0, 0 );
	adaptiveRayGrid.ZoomIntoRay( pRay, 0.01, 0.01 );
	CheckAdaptedRayGrid( adaptiveRayGrid, 9, 7, 6 );
}


void WavefrontSurfaceTest( const CRayGrid& rayGrid, double referenceValue = -1 )
{
	if( referenceValue == -1 )
		referenceValue = M_PI / 180.0 * ( rayGrid.PhiDeg( ).back( ) - rayGrid.PhiDeg( ).front( ) ) *
		                 ( std::cos( rayGrid.ThetaDeg( ).front( ) * M_PI / 180 ) - std::cos( rayGrid.ThetaDeg( ).back( ) * M_PI / 180 ) );

	const float dRef  = 1.0;
	const float dEnd  = 2000.0;
	const double tRef = 1.0;
	const double tEnd = 10.0;
	for( auto ray: rayGrid.UniqueRays( ) )
	{
		ray->Append( rayGrid.SourcePosition( ) + ray->InitialDirection( ) * dRef, ray->InitialDirection( ), tRef );
		ray->Append( rayGrid.SourcePosition( ) + ray->InitialDirection( ) * dEnd, ray->InitialDirection( ), tEnd );
	}

	cout << "Reference value: " << referenceValue << " m^2: " << endl;
	cout << "Relative errors..." << endl;
	cout << "WavefrontSurfaceReference(): " << ( rayGrid.WavefrontSurfaceReference( ) - referenceValue ) * 100 / referenceValue << " %" << endl;
	cout << "WavefrontSurface() at 1m: " << ( rayGrid.WavefrontSurface( tRef ) - referenceValue ) * 100 / referenceValue << " %" << endl;
	cout << "WavefrontSurface() at 2000m / 2000^2: " << ( rayGrid.WavefrontSurface( tEnd ) / dEnd / dEnd - referenceValue ) * 100 / referenceValue << " %" << endl;
	cout << "-----" << endl;
}
void WavefrontSurfaceTest( const double thetaMiddle, const double phiMiddle, const double deltaAngle )
{
	std::vector<double> thetaVec = { thetaMiddle - deltaAngle, thetaMiddle, thetaMiddle + deltaAngle };
	std::vector<double> phiVec   = { phiMiddle - deltaAngle, phiMiddle, phiMiddle + deltaAngle };
	cout << "Main direction (theta/phi): " << thetaMiddle << ", " << phiMiddle << endl;
	cout << "Delta angle: " << deltaAngle << " deg" << endl;
	if( thetaVec.back( ) > 180 )
		thetaVec.pop_back( );
	if( thetaVec.front( ) < 0 )
		thetaVec.erase( thetaVec.begin( ) );

	auto sourcePos = VistaVector3D( 0, 0, 1000 );
	auto rayGrid   = CRayGrid( sourcePos, thetaVec, phiVec );

	WavefrontSurfaceTest( rayGrid );
}

void TestWavefrontSurfaces( )
{
	cout << "Testing multiple wavefront calculations:" << endl << endl;

	cout << "Full sphere:" << endl;
	WavefrontSurfaceTest( CEquiangularRayDistribution( VistaVector3D( 0, 0, 1000 ), 181, 360 ), 4 * M_PI );
	cout << endl;

	cout << "Resolution test:" << endl;
	WavefrontSurfaceTest( 90, 0, 5 );
	WavefrontSurfaceTest( 90, 0, 1 );
	WavefrontSurfaceTest( 90, 0, 0.01 );
	cout << endl;

	cout << "Main direction = north/south pole:" << endl;
	WavefrontSurfaceTest( 0, 0, 0.01 );
	WavefrontSurfaceTest( 180, 0, 0.01 );
	cout << endl;

	cout << "Main direction next to north/south pole:" << endl;
	WavefrontSurfaceTest( 5, 0, 0.01 );
	WavefrontSurfaceTest( 175, 0, 0.01 );
	cout << endl;
}


int main( int iNumInArgs, char* pcInArgs[] )
{
	// Test2DGrid();
	// Test1DThetaGrid();

	TestAdvancedRayZooming( );
	// TestSpecificRayZooming();

	// TestWavefrontSurfaces();
}
