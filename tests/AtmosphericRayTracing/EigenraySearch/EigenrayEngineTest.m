function EigenrayEngineTest()

jsonFolder = fullfile( fileparts(mfilename('fullpath')), 'output' );

%% Special case azimuth > 324�
cppFilename = 'SourceReceiverAzimuthAbove324Deg.json';
checkCppEigenrays( fullfile(jsonFolder, cppFilename) );


%% Check C++ data only
checkResultsReceiverNearGroundSourceElevation(jsonFolder, 'Homogeneous');
checkResultsReceiverNearGroundSourceElevation(jsonFolder, 'Inhomogeneous');
checkResultsReceiverNearGroundSourceAzimuth(jsonFolder, 'Inhomogeneous');


function checkResultsReceiverNearGroundSourceElevation(jsonFolder, atmosphereSuffix)
sourceAzimuth = 0;
for sourceElevation = -90:45:90
    cppFilename = ['ReceiverNearGround_' atmosphereSuffix '_SourceElevation' num2str(sourceElevation) '_SourceAzimuth' num2str(sourceAzimuth) '.json'];
    checkCppEigenrays( fullfile(jsonFolder, cppFilename) );
end

function checkResultsReceiverNearGroundSourceAzimuth(jsonFolder, atmosphereSuffix)
sourceElevation = 30;
for sourceAzimuth = 0:10:350
    cppFilename = ['ReceiverNearGround_' atmosphereSuffix '_SourceElevation' num2str(sourceElevation) '_SourceAzimuth' num2str(sourceAzimuth) '.json'];
    checkCppEigenrays( fullfile(jsonFolder, cppFilename) );
end
