/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *
 *
 */

// #include <ITAStopWatch.h>
// #include <ITAStringUtils.h>

#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenrayEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenraySettings.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTSettings.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Utils/RayToPropagationPath.h>


// #include <cassert>

#include <ITAFileSystemUtils.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <vector>

using namespace std;
using namespace ITAGeo;
using namespace ITAGeo::Utils;
using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::Utils;

void runTest( const CStratifiedAtmosphere& atmosphere, const VistaVector3D& sourcePosition, const VistaVector3D& receiverPosition, const string& filename )
{
	EigenraySearch::CEngine engine;
	engine.eigenraySettings.rayAdaptation.abort.maxNAdaptations       = 30;
	engine.eigenraySettings.rayAdaptation.abort.minAngleResolutionDeg = 0.001;

	engine.eigenraySettings.rayAdaptation.accuracy.maxReceiverRadius        = 1;
	engine.eigenraySettings.rayAdaptation.accuracy.maxSourceReceiverAngle   = 1;
	engine.eigenraySettings.rayAdaptation.accuracy.maxAngleForGeomSpreading = 0.01;

	engine.eigenraySettings.rayAdaptation.advancedRayZooming.bActive   = true;
	engine.eigenraySettings.rayAdaptation.advancedRayZooming.threshold = 1.0;

	engine.eigenraySettings.rayTracing.bAbortOnReceiverDistIncrease = true;
	engine.eigenraySettings.rayTracing.maxReflectionOrder           = 1;
	engine.eigenraySettings.rayTracing.maxTime                      = 15;


	cout << filename << ":" << endl;
	cout << "Starting Simulation-Engine..." << endl;
	std::vector<std::shared_ptr<CRay>> eigenrays = engine.Run( atmosphere, sourcePosition, receiverPosition );

	cout << "Starting export..." << endl;
	CPropagationPathList raysAsPropPathList = ToPropagationPath( eigenrays );

	makeDirectory( "output" );
	std::string filepath = combinePath( "output", filename + ".json" );
	JSON::Export( raysAsPropPathList, filepath );
	cout << "Finished" << endl << endl;
}

void runTestReceiverNearGround( const CStratifiedAtmosphere& atmosphere, const int sourceElevationDeg, const int sourceAzimuthnDeg, const string& atmosphereSuffix )
{
	VistaVector3D receiverPosition      = VistaVector3D( 0, 0, 1.8 );
	const double sourceReceiverDistance = 2000;
	const double sourceX =
	    sourceReceiverDistance * std::sin( sourceElevationDeg * M_PI / 180.0 ) * std::cos( sourceAzimuthnDeg * M_PI / 180.0 ) + receiverPosition[Vista::X];
	const double sourceY =
	    sourceReceiverDistance * std::sin( sourceElevationDeg * M_PI / 180.0 ) * std::sin( sourceAzimuthnDeg * M_PI / 180.0 ) + receiverPosition[Vista::Y];
	const double sourceZ               = sourceReceiverDistance * std::cos( sourceElevationDeg * M_PI / 180.0 ) + receiverPosition[Vista::Z];
	const VistaVector3D sourcePosition = VistaVector3D( sourceX, sourceY, sourceZ );

	const string filename =
	    "ReceiverNearGround_" + atmosphereSuffix + "_SourceElevation" + to_string( sourceElevationDeg ) + "_SourceAzimuth" + to_string( sourceAzimuthnDeg );

	runTest( atmosphere, sourcePosition, receiverPosition, filename );
}
void TestReceiverNearGroundSourceElevation( const CStratifiedAtmosphere& atmosphere, const string& atmosphereSuffix )
{
	const int phi = 0;
	for( int sourceElevation = -90; sourceElevation <= 90; sourceElevation += 45 )
		runTestReceiverNearGround( atmosphere, sourceElevation, phi, atmosphereSuffix );
}

void TestReceiverNearGroundSourceAzimuth( const CStratifiedAtmosphere& atmosphere, const string& atmosphereSuffix )
{
	const int theta = 30;
	for( int sourceAzimuth = 0; sourceAzimuth < 360; sourceAzimuth += 10 )
		runTestReceiverNearGround( atmosphere, theta, sourceAzimuth, atmosphereSuffix );
}


void TestSourceAtGround( const CStratifiedAtmosphere& atmosphere, const string& atmosphereSuffix )
{
	VistaVector3D sourcePosition   = VistaVector3D( 0, 0, 0 );
	VistaVector3D receiverPosition = VistaVector3D( 6500, 0, 1.8 );

	const string filename = "SourceAtGround" + atmosphereSuffix;
	// runTest(atmosphere, sourcePosition, receiverPosition, filename);

	EigenraySearch::CEngine engine;
	engine.eigenraySettings.rayAdaptation.abort.maxNAdaptations       = 30;
	engine.eigenraySettings.rayAdaptation.abort.minAngleResolutionDeg = 0.0001;

	engine.eigenraySettings.rayAdaptation.accuracy.maxReceiverRadius        = 1;
	engine.eigenraySettings.rayAdaptation.accuracy.maxSourceReceiverAngle   = 1;
	engine.eigenraySettings.rayAdaptation.accuracy.maxAngleForGeomSpreading = 0.01;

	engine.eigenraySettings.rayAdaptation.advancedRayZooming.bActive = false;
	// engine.eigenraySettings.rayAdaptation.advancedRayZooming.threshold = 1.0;

	engine.eigenraySettings.rayTracing.bAbortOnReceiverDistIncrease = true;
	engine.eigenraySettings.rayTracing.maxReflectionOrder           = 1;
	engine.eigenraySettings.rayTracing.maxTime                      = 30;


	cout << filename << ":" << endl;
	cout << "Starting Simulation-Engine..." << endl;
	std::vector<std::shared_ptr<CRay>> eigenrays = engine.Run( atmosphere, sourcePosition, receiverPosition );
}

CStratifiedAtmosphere GetHomogeneousAtmosphere( )
{
	auto humidProfile = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile  = std::make_shared<TemperatureProfiles::CRoom>( );
	auto windProfile  = std::make_shared<WindProfiles::CZero>( );
	return CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );
}

CStratifiedAtmosphere GetInhomogeneousAtmosphere( const VistaVector3D& v3WindDir = VistaVector3D( 1, 0, 0 ) )
{
	auto humidProfile = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile  = std::make_shared<TemperatureProfiles::CISA>( );
	auto windProfile  = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, v3WindDir );
	return CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );
}

void TestSourceReceiverAzimuthAbove324Deg( )
{
	const CStratifiedAtmosphere atmosphere = GetInhomogeneousAtmosphere( );
	const VistaVector3D receiverPosition   = VistaVector3D( 0, 0, 1.8 );
	const VistaVector3D sourcePosition     = VistaVector3D( -1000, 200, 600 );
	const string filename                  = "SourceReceiverAzimuthAbove324Deg";

	runTest( atmosphere, sourcePosition, receiverPosition, filename );
}


void TestSpreadingLoss( const EigenraySearch::CEngine& engine, const VistaVector3D& sourcePosition, const VistaVector3D& receiverPosition,
                        const CStratifiedAtmosphere& atmosphere, const double& dMaxExpectedSL = 0.005 )
{
	const double dDistanceLaw         = 1.0 / ( sourcePosition - receiverPosition ).GetLength( );
	const double dDistanceLawVertical = 1.0 / std::abs( sourcePosition[Vista::Z] - receiverPosition[Vista::Z] );

	std::vector<std::shared_ptr<CRay>> eigenrays = engine.Run( atmosphere, sourcePosition, receiverPosition );
	const double dRelDistanceLawDev              = std::abs( 1.0 - eigenrays[0]->SpreadingLoss( ) / dDistanceLaw ) * 100;
	const double dRelDistanceLawVerticalDev      = std::abs( 1.0 - eigenrays[0]->SpreadingLoss( ) / dDistanceLawVertical ) * 100;
	const double dMaxRelDev                      = 1.0; //%

	std::cout << "Spreadingloss test: " << std::endl
	          << "Source: " << sourcePosition << std::endl
	          << "Receiver: " << receiverPosition << std::endl
	          << "Spreading loss: " << eigenrays[0]->SpreadingLoss( ) << std::endl
	          << "Relative deviation from distance law: " << std::endl
	          << "1) Full distance: " << dRelDistanceLawDev << "%" << std::endl
	          << "2) Vertical distance: " << dRelDistanceLawVerticalDev << "%" << std::endl;
	if( std::isinf( eigenrays[0]->SpreadingLoss( ) ) )
		std::cout << "ERROR in spreading loss calculation: Inf value found." << std::endl;
	else if( eigenrays[0]->SpreadingLoss( ) >= dMaxExpectedSL )
		std::cout << "ERROR in spreading loss calculation: Value >= " << dMaxExpectedSL << " found." << std::endl;
	else if( dRelDistanceLawDev >= dMaxRelDev )
		std::cout << "ERROR in spreading loss calculation: Value deviates " << dRelDistanceLawDev << "% from distance law." << std::endl;
	else
		std::cout << "Seems OK" << std::endl;
	std::cout << std::endl;
}
void TestSpecialSpreadingLossCases( ) // In these case, there was a bug with the spreading loss. Used this test to Debug this scenario.
{
	// Introduction:
	// It seems that in some cases, where the initial eigenray direction is close to a pole,
	// a numerical cancellation happened during calculation of wavefront surface at the receiver: S = 0 => loss = inf
	// EigenraySearch::CAdaptiveWorker::CalculateEigenraySpreadingLoss() was changed so that for eigenrays close to the pole, a lower azimuth resolution is used to avoid
	// this problem

	const CStratifiedAtmosphere atmosphere = GetInhomogeneousAtmosphere( );
	EigenraySearch::CEngine engine;
	engine.eigenraySettings.rayTracing.maxReflectionOrder = 0;

	// Case 1
	VistaVector3D receiverPosition = VistaVector3D( -17.5, 100, 1 );
	VistaVector3D sourcePosition   = VistaVector3D( -50.97, 100, 1020.7 );
	TestSpreadingLoss( engine, sourcePosition, receiverPosition, atmosphere );

	// Case 2 - Eigenray directly at pole
	sourcePosition   = VistaVector3D( -20.3577849403681, 100, 1022.15620755078 );
	receiverPosition = VistaVector3D( 17.5, 100, 1 );
	TestSpreadingLoss( engine, sourcePosition, receiverPosition, atmosphere );

	// Case 3
	sourcePosition                                                          = VistaVector3D( 17.5, 100, 1.8 );
	receiverPosition                                                        = VistaVector3D( 56.15, 100, 1.0266e+3 );
	engine.eigenraySettings.rayAdaptation.accuracy.maxAngleForGeomSpreading = 0.01;
	engine.eigenraySettings.rayAdaptation.accuracy.maxReceiverRadius        = 0.25;
	engine.eigenraySettings.rayAdaptation.abort.maxNAdaptations             = 50;
	engine.eigenraySettings.rayAdaptation.abort.minAngleResolutionDeg       = 0.00001;
	engine.eigenraySettings.rayTracing.maxTime                              = 50;
	TestSpreadingLoss( engine, sourcePosition, receiverPosition, GetInhomogeneousAtmosphere( VistaVector3D( -1, 0, 0 ) ) );
}

void TestReceiverInShadowZone( )
{
	const std::string filename = "ReceiverInShadowZone";
	const auto atmosphere      = GetInhomogeneousAtmosphere( );

	EigenraySearch::CEngine engine;
	engine.eigenraySettings.rayAdaptation.abort.maxNAdaptations       = 30;
	engine.eigenraySettings.rayAdaptation.abort.minAngleResolutionDeg = 0.001;

	VistaVector3D receiverPosition      = VistaVector3D( 0, 0, 1.8 );
	const double sourceReceiverDistance = 2000;
	const int sourceElevationDeg        = 88;
	const int sourceAzimuthDeg          = 0;
	const double sourceX =
	    sourceReceiverDistance * std::sin( sourceElevationDeg * M_PI / 180.0 ) * std::cos( sourceAzimuthDeg * M_PI / 180.0 ) + receiverPosition[Vista::X];
	const double sourceY =
	    sourceReceiverDistance * std::sin( sourceElevationDeg * M_PI / 180.0 ) * std::sin( sourceAzimuthDeg * M_PI / 180.0 ) + receiverPosition[Vista::Y];
	const double sourceZ               = sourceReceiverDistance * std::cos( sourceElevationDeg * M_PI / 180.0 ) + receiverPosition[Vista::Z];
	const VistaVector3D sourcePosition = VistaVector3D( sourceX, sourceY, sourceZ );


	cout << filename << ":" << endl;
	cout << "Starting Simulation-Engine..." << endl;
	std::vector<std::shared_ptr<CRay>> eigenrays = engine.Run( atmosphere, sourcePosition, receiverPosition );

	cout << "Starting export..." << endl;
	CPropagationPathList raysAsPropPathList = ToPropagationPath( eigenrays );

	makeDirectory( "output" );
	std::string filepath = combinePath( "output", filename + ".json" );
	JSON::Export( raysAsPropPathList, filepath );
	cout << "Finished" << endl << endl;
}


int main( int iNumInArgs, char* pcInArgs[] )
{
	// Disable multi-threading for debugging purposes
	omp_set_num_threads( 1 );

	TestSpecialSpreadingLossCases( );

	TestSourceReceiverAzimuthAbove324Deg( );

	const CStratifiedAtmosphere homAtmosphere   = GetHomogeneousAtmosphere( );
	const CStratifiedAtmosphere inhomAtmosphere = GetInhomogeneousAtmosphere( );

	TestSourceAtGround( inhomAtmosphere, "Inhomogeneous" );

	TestReceiverNearGroundSourceElevation( homAtmosphere, "Homogeneous" );
	TestReceiverNearGroundSourceElevation( inhomAtmosphere, "Inhomogeneous" );
	TestReceiverNearGroundSourceAzimuth( inhomAtmosphere, "Inhomogeneous" );

	TestReceiverInShadowZone( );
}
