/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_PROPAGATIONPATHSIM_ART_SIMULATION_ENGINE
#define IW_ITA_PROPAGATIONPATHSIM_ART_SIMULATION_ENGINE

#include <ITAPropagationPathSim/Definitions.h>

// Vista includes
#include <VistaBase/VistaVector3D.h>

// ITA includes
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTSettings.h>

// STD
#include <memory>
#include <set>
#include <vector>

namespace ITAPropagationPathSim
{
	namespace AtmosphericRayTracing
	{
		namespace Simulation
		{
			class ITA_PROPAGATION_PATH_SIM_API CEngine
			{
			public:
				std::shared_ptr<IExternalWatcher> pExternalWatcher; //!< Reference to externally defined abort criterion.
				Simulation::Settings settings;

			public:
				CEngine( bool bSuppressWarnings = false );
				CEngine( std::shared_ptr<IExternalWatcher> pWatcher, bool bSuppressWarnings = false );

			public:
				// Initializes rays with given source position and initial direction, traces and returns them
				std::vector<std::shared_ptr<CRay>> Run( const ITAGeo::CStratifiedAtmosphere& atmosphere, const VistaVector3D& m_v3SourcePosition,
				                                        const std::vector<VistaVector3D>& v3RayDirections ) const;
				// Traces given rays and returns true if all rays were inside valid bounds of atmosphere
				bool Run( const ITAGeo::CStratifiedAtmosphere& atmosphere, const std::set<std::shared_ptr<CRay>>& rays ) const;

			private:
				// Traces rays and returns true if all rays were inside valid bounds of atmosphere
				bool TraceRays( const ITAGeo::CStratifiedAtmosphere& atmosphere, const std::vector<std::shared_ptr<CRay>>& rays ) const;

				bool m_bSuppressWarnings;              // If set to true, suppresses out of atmosphere bounds warning
				bool m_bOutOfAtmosphereBounds = false; // Indicates whether last Run() call used data above the specified limit of the atmosphere
			};
		} // namespace Simulation
	} // namespace AtmosphericRayTracing
} // namespace ITAPropagationPathSim

#endif // IW_ITA_PROPAGATIONPATHSIM_ART_SIMULATION_ENGINE