#include <ITAPropagationPathSim\CombinedModel\DiffractionLocator.h>
#include <ITAPropagationPathSim\CombinedModel\ImageConstructor.h>
#include <ITAPropagationPathSim\CombinedModel\PropagationEngine.h>
#include <ITAPropagationPathSim\CombinedModel\ReflectionLocator.h>
#include <ITAStopWatch.h> //TODO:Delete, just for testing
#include <ITAStringUtils.h>

// OpenMesh includes
#include <OpenMesh/Core/Utils/PropertyManager.hh>

using namespace ITAGeo;

// Typedefs
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;


ITAPropagationPathSim::CombinedModel::CPathEngine::CPathEngine( ) : m_pProgressHandler( nullptr )
{
	m_oAbort.SetDefaults( );
	m_oConfig.SetDefaults( );
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::SetSimulationConfiguration( const CSimulationConfig& oConfig )
{
	m_oConfig = oConfig;

	// Recreate the visibility map, if mesh is initialized
	if( m_pMesh != nullptr )
	{
		m_mvpShapeVisibilityMap.clear( );
		CreateVisibilityMap( );
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::SetAbortionCriteria( const CAbortionCriteria& oAbort )
{
	m_oAbort = oAbort;
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::SetProgressCallbackHandler( ITABase::IProgressHandler* pHandler )
{
	m_pProgressHandler = pHandler;
}

float ITAPropagationPathSim::CombinedModel::CPathEngine::GetMaxPropagationRange( ) const
{
	if( GetMaxPropagationRangeValid( ) )
		return powf( 10.f, m_oAbort.fDynamicRange / 20 );
	else
		return 0.0f;
}

bool ITAPropagationPathSim::CombinedModel::CPathEngine::GetMaxPropagationRangeValid( ) const
{
	return m_oAbort.fDynamicRange > 0.0f;
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::InitializePathEnvironment( std::unique_ptr<CITAMesh> pMesh )
{
	if( m_pProgressHandler )
		m_pProgressHandler->SetSection( "Initializing path environment" );

	// swap input mesh with current mesh. Once this function runs out of scope the input pMesh is destroyed and with it the old mesh (that we swapped).
	m_pMesh.swap( pMesh );

	m_pMesh->request_face_normals( );
	m_pMesh->update_face_normals( );

	if( m_oConfig.bExportRuntimeStatistics )
		m_swConstructShapes.start( );

	ConstructPropagationShapes( );

	if( m_oConfig.bExportRuntimeStatistics )
		m_swConstructShapes.stop( );

	CreateVisibilityMap( );
#ifdef _DEBUG
	std::cout << "Visibility Map constructed. Number of Shapes: " << m_vpPropagationShapes.size( ) << "                              " << std::endl;
#endif
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::SetEntities( std::shared_ptr<ITAGeo::CPropagationAnchor> pOriginEntity,
                                                                     std::shared_ptr<ITAGeo::CPropagationAnchor> pTargetEntity )
{
	if( !pOriginEntity )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Origin entity empty" );

	if( !pTargetEntity )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Target entity empty" );

	// Create propagation tree

	if( m_pProgressHandler )
		m_pProgressHandler->SetSection( "Configuring origin and target entities" );

	UpdateTargetEntity( pTargetEntity );

	UpdateOriginEntity( pOriginEntity ); // Creates propagation shape list!
}

//! @detail NMK: Pleaaase just rewrite this function and all the called function. CreatePropagationTree and CreatePropagationList are just ???
//! I think, as long as we have usefull PropagationCandiates at the end it should be fine
//! I tried to change the PropCandiate creation by doing it shape by shape. This is slower (?) and I think I am doing more but I hoped to remove
//! the large child vector which is not needed anymore. This function is where the simulation happens
void ITAPropagationPathSim::CombinedModel::CPathEngine::UpdateOriginEntity( std::shared_ptr<CPropagationAnchor> pOriginEntity )
{
	// NMK: For performance reasons I changed this function to include all the work needed to find propagation paths. Previously, the functions
	// were applied to all shapes at once. This lead to an gigantic memory consumption because the shape class and functions are not build for this
	// Therefor, as quick fix, I serialised the work. We no do each function only on one shape. First Tree for one shape, then images, then image
	// edges, and so forth. We also remove all the unnecessary paths that we do not need anymore. This frees up Gigabytes of memory.

	if( !m_pTargetEntity )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Target entity has not been set yet, please do before updating origin" );

	m_pOriginEntity = pOriginEntity;

	// set illumination by target variable of all shapes
	SetShapeIlluminationByTargetEntity( );

	if( m_pProgressHandler )
		m_pProgressHandler->SetItem( "Find Propagation Paths" );

	m_vpPropagationTree.clear( );
	m_vpPropagationCandidates.clear( );
	int i = 0;
	// saving all valid paths in a temporary vector
	std::vector<std::shared_ptr<CPropagationShape>> vpFinalPaths;
	for( const std::shared_ptr<CPropagationShape>& pPropagationShape: m_vpPropagationShapes )
	{
		if( m_pProgressHandler )
			m_pProgressHandler->PushProgressUpdate( i++ / float( m_vpPropagationShapes.size( ) ) * 100.0f );

		// Conversion from Visibility Map to Tree
		CreatePropagationTree( m_pOriginEntity, pPropagationShape );

		if( m_oConfig.bExportRuntimeStatistics )
			m_swImageConstruction.start( );

		ImageConstruction::ConstructImages( m_pOriginEntity->v3InteractionPoint, m_vpPropagationTree );

		ImageConstruction::ConstructImageEdges( m_vpPropagationTree );

		if( m_oConfig.bExportRuntimeStatistics )
			m_swImageConstruction.stop( );

		// Conversion from Tree to Lists
		CreatePropagationLists( );

		// we dont need the tree anymore as all paths were converted to propagationCandidates
		m_vpPropagationTree.clear( );


		/// remove Propagation Candidates that do not lead to a valid path, tradeoff memory vs. performance ----------------------------------------------
		// the following part is pulled from the ConstructPropagationPaths function
		Diffraction::ConstructAperturePoints( m_pOriginEntity, m_pTargetEntity, (int)m_oConfig.iNumberIterationApexCalculation, m_vpPropagationCandidates,
		                                      m_vpPropagationCandidates, m_pProgressHandler );

		ImageConstruction::ConstructImageApertures( m_vpPropagationCandidates, m_vpPropagationCandidates );

		Reflection::ConstructPointsOfReflection( m_pTargetEntity, m_vpPropagationCandidates, m_vpPropagationCandidates );

		/// Delete invalid Paths based on abortion/config criterions ---------------------------------------------------------------------------------------------
		if( m_oAbort.fDynamicRange > 0.0f )
			PerceptionalCulling( m_vpPropagationCandidates, m_vpPropagationCandidates );

		if( m_oAbort.fAccumulatedAngleThreshold > 0 )
			Diffraction::AccumulatedAngleCulling( m_oAbort.fAccumulatedAngleThreshold, m_pOriginEntity, m_pTargetEntity, m_vpPropagationCandidates,
			                                      m_vpPropagationCandidates );


		if( m_oConfig.bFilterIntersectedPaths )
			FilterVisiblePaths( m_vpPropagationCandidates, m_vpPropagationCandidates );


		vpFinalPaths.insert( vpFinalPaths.end( ), m_vpPropagationCandidates.begin( ), m_vpPropagationCandidates.end( ) );
		m_vpPropagationCandidates.clear( );
		m_vpPropagationCandidates.shrink_to_fit( );
	}
	m_vpPropagationCandidates = vpFinalPaths;
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::UpdateTargetEntity( std::shared_ptr<CPropagationAnchor> pTargetEntity )
{
	m_pTargetEntity = pTargetEntity;
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::ApplyEmitter( std::shared_ptr<ITAGeo::CEmitter> pEmitter )
{
	ITA_EXCEPT_NOT_IMPLEMENTED; // not sure if this should be used .... better use update methods

	// if( m_pProgressHandler )
	//	m_pProgressHandler->SetSection( "Applying emitter algorithms" );

	//// Set emitter
	// m_pOriginEntity = pEmitter;

	//// Create propagation tree
	// CreatePropagationTree( pEmitter );

	// PushStatus( "Constructing specluar images of emitter", 0.0f );
	// ImageConstruction::ConstructImages( pEmitter->v3InteractionPoint, m_vpPropagationTree );

	//// Construct image edges
	// PushStatus( "Constructing edge images of emitter", 0.0f );
	// ImageConstruction::ConstructImageEdges( m_vpPropagationTree );

	/* Following not working accurately
	// Calculate accumulated angle and angle culling for tree (without knowing the exactly position)
	if( m_oConfig.bAccumulatedAngleThreshold > 0 )
	Diffraction::AccumulatedAngleCulling(m_oConfig.bAccumulatedAngleThreshold, m_pEmitter,m_vpPropagationTree, m_vpPropagationTree);
	*/
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::ApplySensor( std::shared_ptr<ITAGeo::CSensor> pSensor )
{
	ITA_EXCEPT_NOT_IMPLEMENTED; // not sure if this should be used .... better use update methods

	m_pTargetEntity = pSensor;

	if( !m_pTargetEntity )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Sensor empty" );

	if( m_pProgressHandler )
		m_pProgressHandler->SetSection( "Applying sensor algorithms" );

	CreatePropagationLists( );
}

size_t ITAPropagationPathSim::CombinedModel::CPathEngine::GetNumberPropagationPathCandidates( )
{
	if( m_vpPropagationCandidates.size( ) > 0 )
		return m_vpPropagationCandidates.size( );
	else
	{
		size_t iNumber = 0;
		std::vector<CPropagationShapeShared> pCandidates;

		pCandidates.insert( pCandidates.end( ), m_vpPropagationTree.begin( ), m_vpPropagationTree.end( ) );

		for( int i = 0; i < pCandidates.size( ); i++ )
		{
			auto pCandidate = pCandidates[i];

			if( pCandidate == nullptr )
				continue;

			iNumber++;

			pCandidates.insert( pCandidates.end( ), pCandidate->vpChildren.begin( ), pCandidate->vpChildren.end( ) );
		}

		return iNumber;
	}
}

std::vector<ITABase::CStatistics> ITAPropagationPathSim::CombinedModel::CPathEngine::GetRuntimeStatistics( ) const
{
	std::vector<ITABase::CStatistics> voStats;
	voStats.push_back( m_swConstructShapes.GetStatistics( "ConstructShapes" ) );
	voStats.push_back( m_swVisibilityMap.GetStatistics( "VisibilityMap" ) );
	voStats.push_back( m_swPropagationTreeConstruction.GetStatistics( "PropagationTreeConstruction" ) );
	voStats.push_back( m_swImageConstruction.GetStatistics( "ImageConstruction" ) );
	voStats.push_back( m_swPropgationPathsConstruction.GetStatistics( "PropgationPathsConstruction" ) );
	return voStats;
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::ConstructPropagationPaths( ITAGeo::CPropagationPathList& oPaths )
{
	ConvertShapeListsToPropagationPaths( oPaths );

	if( m_oConfig.bExportRuntimeStatistics )
		m_swPropgationPathsConstruction.stop( );
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::PerceptionalCulling( const std::vector<CPropagationShapeShared> vpAllPathsIn,
                                                                             std::vector<CPropagationShapeShared>& vpAudiblePathsOut )
{
	vpAudiblePathsOut.clear( );

	bool bIsPerceived;

	// Length of current path divided into two parts. The first length is the path length from the emitter to the first edge
	// and the second length is the path length from the first edge to the sensor
	float fLength1, fLength2;

	// Accumulated penalty in dB for reflections and diffractions
	float fAccumulatedPenalty;
	for( auto vpPropagationList: vpAllPathsIn )
	{
		if( vpPropagationList == nullptr )
			continue;

		auto pCurrentShape = vpPropagationList;

		fLength1 = ( m_pOriginEntity->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );
		fLength2 = -1.f;

		if( pCurrentShape->iShapeType == CPropagationShape::FACE )
		{
			fAccumulatedPenalty = m_oAbort.fReflectionPenalty;
		}
		else
		{
			fAccumulatedPenalty = m_oAbort.fDiffractionPenalty;
		}

		while( pCurrentShape->pChild != nullptr )
		{
			auto pLastShape = pCurrentShape;

			pCurrentShape = pCurrentShape->pChild;

			if( pCurrentShape->iShapeType == CPropagationShape::FACE )
			{
				fAccumulatedPenalty += m_oAbort.fReflectionPenalty;

				if( fLength2 < -Vista::Epsilon )
				{
					fLength1 += ( *pLastShape->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );
				}
				else
				{
					fLength2 += ( *pLastShape->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );
				}
			}
			else
			{
				fAccumulatedPenalty += m_oAbort.fDiffractionPenalty;

				if( fLength2 < -Vista::Epsilon )
				{
					fLength1 += ( *pLastShape->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );

					fLength2 = 0.0f;
				}
				else
				{
					fLength2 += ( *pLastShape->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );
				}
			}
		}

		// Add length between last shape and sensor
		if( fLength2 < -Vista::Epsilon )
		{
			fLength1 += ( m_pTargetEntity->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );
		}
		else
		{
			fLength2 += ( m_pTargetEntity->v3InteractionPoint - *pCurrentShape->v3InteractionPoint ).GetLength( );
		}


		// Penalty plus level drop of spherical source
		float fTotalLevelDrop = fAccumulatedPenalty + 20 * log10f( fLength1 );

		// Added level drop of line source after first edge
		if( fLength2 > Vista::Epsilon )
			fTotalLevelDrop += 10 * log10f( fLength2 * ( fLength1 + fLength2 ) / fLength1 );

		// Compare total level drop with threshold
		bIsPerceived = ( m_oAbort.fDynamicRange - fTotalLevelDrop ) > 0.0f;

		// Add propagation path if it is in the audible range
		if( bIsPerceived )
			vpAudiblePathsOut.push_back( vpPropagationList );
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::FilterVisiblePaths( const std::vector<CPropagationShapeShared> vpAllPathsIn,
                                                                            std::vector<CPropagationShapeShared>& vpVisiblePathsOut )
{
	vpVisiblePathsOut.clear( );

	bool bIsVisible;

	for( auto& vpPropagationList: vpAllPathsIn )
	{
		if( vpPropagationList == nullptr )
			continue;

		// First, check for intersections between emitter and first aperture/reflection point
		if( IsPathVisible( m_pOriginEntity->v3InteractionPoint, *vpPropagationList->v3InteractionPoint ) )
		{
			bIsVisible = true;

			auto pShape = vpPropagationList;
			while( pShape->pChild != nullptr )
			{
				if( IsPathVisible( *pShape->v3InteractionPoint, *pShape->pChild->v3InteractionPoint ) )
				{
					pShape = pShape->pChild;
				}
				else
				{
					bIsVisible = false;
					break; // Break the while loop
				}
			}

			// If the path is not already invalid, also check for intersections between sensor and last point
			if( bIsVisible )
				if( IsPathVisible( m_pTargetEntity->v3InteractionPoint, *pShape->v3InteractionPoint ) )
					vpVisiblePathsOut.push_back( vpPropagationList );
		}
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::ConvertShapeListsToPropagationPaths( ITAGeo::CPropagationPathList& oPathsOut )
{
	oPathsOut.clear( );

	// If the intersection filter member variable is set, only allow not intersected paths
	if( ( !m_oConfig.bFilterIntersectedPaths ) || IsPathVisible( m_pOriginEntity->v3InteractionPoint, m_pTargetEntity->v3InteractionPoint ) )
	{
		// Add direct path
		CPropagationPath oDirectPath;
		oDirectPath.push_back( m_pOriginEntity );
		oDirectPath.push_back( m_pTargetEntity );
		oPathsOut.push_back( oDirectPath );
	}

	for( auto& pShapeStart: m_vpPropagationCandidates )
	{
		if( pShapeStart == nullptr )
			continue;

		// Create path
		CPropagationPath oPath;

		// The path always starts with the emitter
		oPath.push_back( m_pOriginEntity );

		CPropagationShapeShared pShape = pShapeStart;
		while( pShape != nullptr )
		{
			switch( pShape->iShapeType )
			{
				case CPropagationShape::FACE:
				{
					CPropagationFaceShared pFace = std::static_pointer_cast<CPropagationFace>( pShape );

					// Create specular reflection anchor
					std::shared_ptr<CSpecularReflection> pReflectionAnchor = std::make_shared<CSpecularReflection>( );
					pReflectionAnchor->v3InteractionPoint                  = *pFace->v3InteractionPoint;
					pReflectionAnchor->v3FaceNormal                        = pFace->pPlane->GetNormVector( );
					pReflectionAnchor->iPolygonID                          = (int)pFace->GetPolygonID( );
					pReflectionAnchor->pMaterial                           = pFace->pMaterial;

					oPath.push_back( pReflectionAnchor );
					break;
				}
				case CPropagationShape::EDGE:
				{
					CPropagationEdgeShared pEdge = std::static_pointer_cast<CPropagationEdge>( pShape );

					std::shared_ptr<CITADiffractionWedgeApertureBase> pDiffractionAnchor;

					if( pEdge->IsOuterWedge( ) )
						pDiffractionAnchor = std::make_shared<CITADiffractionOuterWedgeAperture>( );
					else
						pDiffractionAnchor = std::make_shared<CITADiffractionInnerWedgeAperture>( );

					pDiffractionAnchor->v3InteractionPoint        = *pEdge->v3InteractionPoint;
					pDiffractionAnchor->v3MainWedgeFaceNormal     = *pEdge->v3MainFaceNormal;
					pDiffractionAnchor->v3OppositeWedgeFaceNormal = *pEdge->v3OppositeFaceNormal;
					pDiffractionAnchor->v3VertextStart            = *pEdge->v3StartVertex;
					pDiffractionAnchor->v3VertextEnd              = *pEdge->v3EndVertex;
					pDiffractionAnchor->iMainWedgeFaceID          = pEdge->hMainFace.idx( );
					pDiffractionAnchor->iOppositeWedgeFaceID      = pEdge->hOppositeFace.idx( );

					oPath.push_back( pDiffractionAnchor );

					break;
				}
			}

			pShape = pShape->pChild;
		}

		// The path always ends with the sensor
		oPath.push_back( m_pTargetEntity );

		// Add current path to path list
		oPathsOut.push_back( oPath );
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::CreatePropagationLists( )
{
	int i = 0;
	for( auto& pPropagationShape: m_vpPropagationTree )
	{
		// PushStatus( "Filling propagation tree", i++ / float( m_vpPropagationTree.size( ) ) * 100.0f );
		RecursiveAddShapesToPropagationLists( pPropagationShape );
	}
}

//! @details NMK: This function basically just takes every shape and adds all the visible shapes from the visibility mapp as its children. RECURSIVELY?!! There some
//! abortion criterion which is why not the full vector (not all visible shapes) are added as children but yeah. This leads to a tree which explodes exponentially in
//! width.
void ITAPropagationPathSim::CombinedModel::CPathEngine::CreatePropagationTree( std::shared_ptr<CPropagationAnchor> pTrunk,
                                                                               const std::shared_ptr<CPropagationShape>& pPropagationShape )
{
	if( !pTrunk )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Can't create propagation tree without an emitter" );

	// Set initial values for number of diffractions/reflections
	// if( m_pProgressHandler )
	//	m_pProgressHandler->SetItem( "Creating propagation tree" );

	if( m_oConfig.bExportRuntimeStatistics )
		m_swPropagationTreeConstruction.start( );

	// If m_pMaxCombinedOrder order is zero, no paths are added
	if( m_oAbort.iMaxCombinedOrder <= 0 )
	{
		if( m_oConfig.bExportRuntimeStatistics )
			m_swPropagationTreeConstruction.stop( );
		return;
	}


	//// Initialize the accumulated penalty [dB]
	// float fAccumulatedPenalty = 0.0f;

	//// Initialize the distance until the first edge and the distance after the first edge as well as the level drop on the first edge
	// float fFirstDistance = 0.0f, fSecondDistance = 0.0f;
	// float fLevelDropAtFirstEdge = -1.0f; // Set to minus one so that it is clearly seen, that the first edge is not reached yet(will be overwritten on the first edge)

	// int i = 0;
	// for( const auto& pPropagationShape: m_vpPropagationShapes )
	//{
	// if( m_pProgressHandler )
	//	m_pProgressHandler->PushProgressUpdate( i++ / float( m_vpPropagationShapes.size( ) ) * 100.0f );

	// Initialize the accumulated penalty [dB]
	float fAccumulatedPenalty = 0.0f;

	// Initialize the distance until the first edge and the distance after the first edge as well as the level drop on the first edge
	float fFirstDistance = 0.0f, fSecondDistance = 0.0f;
	float fLevelDropAtFirstEdge = -1.0f; // Set to minus one so that it is clearly seen, that the first edge is not reached yet(will be overwritten on the first edge)

	switch( pPropagationShape->iShapeType )
	{
		case CPropagationShape::FACE:
		{
			// Add penalty [dB]
			fAccumulatedPenalty += m_oAbort.fReflectionPenalty;

			// Only add reflection paths if iMaxReflections is at least one
			if( m_oAbort.iMaxReflectionOrder >= 1 )
			{
				CPropagationFaceShared pFaceCopy = std::make_shared<CPropagationFace>( );
				pFaceCopy->CopyFrom( *std::static_pointer_cast<CPropagationFace>( pPropagationShape ) );

				// Set minimum distance of face to emitter
				pFaceCopy->SetMinimumDistance( pTrunk->v3InteractionPoint );

				// Boolean for illuminable faces
				bool bCanIlluminated = true;

				// Check for audibility if a max level drop threshold is given
				if( m_oAbort.fDynamicRange > 0.0f )
				{
					// Calculate propagation level drop (without the reflection and diffraction penalty)
					float fLevelDrop = pFaceCopy->CalculateLevelDrop( fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

					if( m_oAbort.fDynamicRange - fLevelDrop - fAccumulatedPenalty < 0.0f )
						bCanIlluminated = false;
				}

				if( bCanIlluminated )
					bCanIlluminated = CanPointIlluminateFace( pTrunk->v3InteractionPoint, pFaceCopy->hFace );

				if( bCanIlluminated )
				{
					// Add face copy to propagation tree
					m_vpPropagationTree.push_back( pFaceCopy );

					// Add further propagation shapes if higher order than one is allowed
					if( m_oAbort.iMaxCombinedOrder > 1 )
					{
						for( const auto& pPropagationShapeChild: m_mvpShapeVisibilityMap[pPropagationShape->GetIdentifier( )] )
						{
							CPropagationShapeShared vpShapeChildCopy;
							RecursiveAddChildrenToTree( pPropagationShapeChild, vpShapeChildCopy, 1, 0, 1, fAccumulatedPenalty, fFirstDistance, fSecondDistance,
							                            fLevelDropAtFirstEdge );

							if( vpShapeChildCopy != nullptr )
							{
								vpShapeChildCopy->pParent = pFaceCopy;
								pFaceCopy->vpChildren.push_back( vpShapeChildCopy );
							}
						}
					}
				}
			}
			break;
		}
		case CPropagationShape::EDGE:
		{
			// Add penalty [dB]
			fAccumulatedPenalty += m_oAbort.fDiffractionPenalty;

			// Only add reflection paths if iMaxDiffractions is at least one
			if( m_oAbort.iMaxDiffractionOrder >= 1 )
			{
				CPropagationEdgeShared pEdgeCopy = std::make_shared<CPropagationEdge>( );
				pEdgeCopy->CopyFrom( *std::static_pointer_cast<CPropagationEdge>( pPropagationShape ) );

				// Set minimum distance of edge to emitter
				pEdgeCopy->SetMinimumDistance( pTrunk->v3InteractionPoint );


				// Boolean for illuminable edges
				bool bCanIlluminated = true;

				// Check for audibility if a max level drop threshold is given
				if( m_oAbort.fDynamicRange > 0.0f )
				{
					// Calculate propagation level drop (without the reflection and diffraction penalty)
					float fLevelDrop = pEdgeCopy->CalculateLevelDrop( fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

					if( m_oAbort.fDynamicRange - fLevelDrop - fAccumulatedPenalty < 0.0f )
						bCanIlluminated = false;
				}

				if( bCanIlluminated )
					bCanIlluminated = CanPointIlluminateEdge( pTrunk->v3InteractionPoint, pEdgeCopy, m_oConfig.bFilterEmitterToEdgeIntersectedPaths );

				if( bCanIlluminated )
				{
					// Add edge copy to propagation tree
					m_vpPropagationTree.push_back( pEdgeCopy );

					// Add further propagation shapes if higher order than one is allowed
					if( m_oAbort.iMaxCombinedOrder > 1 )
					{
						for( const auto& pPropagationShapeChild: m_mvpShapeVisibilityMap[pPropagationShape->GetIdentifier( )] )
						{
							CPropagationShapeShared vpShapeChildCopy;
							RecursiveAddChildrenToTree( pPropagationShapeChild, vpShapeChildCopy, 0, 1, 1, fAccumulatedPenalty, fFirstDistance, fSecondDistance,
							                            fLevelDropAtFirstEdge );

							if( vpShapeChildCopy != nullptr )
							{
								vpShapeChildCopy->pParent = pEdgeCopy;
								pEdgeCopy->vpChildren.push_back( vpShapeChildCopy );
							}
						}
					}
				}
			}
			break;
		}
	}
	//}

	if( m_oConfig.bExportRuntimeStatistics )
		m_swPropagationTreeConstruction.stop( );
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::RecursiveAddChildrenToTree( const CPropagationShapeShared& pPropagationShapeChildIn,
                                                                                    CPropagationShapeShared& vpShapeChildCopyOut, int iReflectionOrder,
                                                                                    int iDiffractionOrder, int iCombinedOrder, float fAccumulatedPenalty,
                                                                                    float fFirstDistance, float fSecondDistance, float fLevelDropAtFirstEdge )
{
	switch( pPropagationShapeChildIn->iShapeType )
	{
		case CPropagationShape::FACE:
		{
			// Add penalty [dB]
			fAccumulatedPenalty += m_oAbort.fReflectionPenalty;

			// Only add child if max reflection order is not reached yet
			if( iReflectionOrder < m_oAbort.iMaxReflectionOrder )
			{
				// Cast shape as face and copy shape
				CPropagationFaceShared pFaceChildCopy = std::make_shared<CPropagationFace>( );
				pFaceChildCopy->CopyFrom( *std::static_pointer_cast<CPropagationFace>( pPropagationShapeChildIn ) );

				// Set output parameter
				vpShapeChildCopyOut = pFaceChildCopy;

				bool bCanIlluminated = true;

				// Check for audibility if a max level drop threshold is given
				if( m_oAbort.fDynamicRange > 0.0f )
				{
					// Calculate propagation level drop (without the reflection and diffraction penalty)
					float fLevelDrop = pFaceChildCopy->CalculateLevelDrop( fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

					if( m_oAbort.fDynamicRange - fLevelDrop - fAccumulatedPenalty < 0.0f )
						bCanIlluminated = false;
				}

				// Add further children, if max order is not reached yet
				if( bCanIlluminated && iCombinedOrder + 1 < m_oAbort.iMaxCombinedOrder )
				{
					for( const auto& pShapeChildChild: m_mvpShapeVisibilityMap[pPropagationShapeChildIn->GetIdentifier( )] )
					{
						CPropagationShapeShared vpShapeChildChildCopy;
						RecursiveAddChildrenToTree( pShapeChildChild, vpShapeChildChildCopy, iReflectionOrder + 1, iDiffractionOrder, iCombinedOrder + 1,
						                            fAccumulatedPenalty, fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

						if( vpShapeChildChildCopy != nullptr )
						{
							vpShapeChildChildCopy->pParent = vpShapeChildCopyOut;
							vpShapeChildCopyOut->vpChildren.push_back( vpShapeChildChildCopy );
						}
					}
				}
			}
			break;
		}
		case CPropagationShape::EDGE:
		{
			// Add penalty [dB]
			fAccumulatedPenalty += m_oAbort.fDiffractionPenalty;

			// Only add child if max diffraction order is not reached yet
			if( iDiffractionOrder < m_oAbort.iMaxDiffractionOrder )
			{
				// Cast shape as edge and copy shape
				CPropagationEdgeShared pEdgeChildCopy = std::make_shared<CPropagationEdge>( );
				pEdgeChildCopy->CopyFrom( *std::static_pointer_cast<CPropagationEdge>( pPropagationShapeChildIn ) );

				// Set output parameter
				vpShapeChildCopyOut = pEdgeChildCopy;

				bool bCanIlluminated = true;

				// Check for audibility if a max level drop threshold is given
				if( m_oAbort.fDynamicRange > 0.0f )
				{
					// Calculate propagation level drop (without the reflection and diffraction penalty)
					float fLevelDrop = pEdgeChildCopy->CalculateLevelDrop( fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

					if( m_oAbort.fDynamicRange - fLevelDrop - fAccumulatedPenalty < 0.0f )
						bCanIlluminated = false;
				}

				// Add further children, if max order is not reached yet
				if( bCanIlluminated && iCombinedOrder + 1 < m_oAbort.iMaxCombinedOrder )
				{
					for( const auto& pShapeChildChild: m_mvpShapeVisibilityMap[pPropagationShapeChildIn->GetIdentifier( )] )
					{
						CPropagationShapeShared vpShapeChildChildCopy;
						RecursiveAddChildrenToTree( pShapeChildChild, vpShapeChildChildCopy, iReflectionOrder, iDiffractionOrder + 1, iCombinedOrder + 1,
						                            fAccumulatedPenalty, fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

						if( vpShapeChildChildCopy != nullptr )
						{
							vpShapeChildChildCopy->pParent = vpShapeChildCopyOut;
							vpShapeChildCopyOut->vpChildren.push_back( vpShapeChildChildCopy );
						}
					}
				}
			}
			break;
		}
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::RecursiveAddShapesToPropagationLists( std::shared_ptr<CPropagationShape>& pPropagationShapeIn )
{
	// NMK: so first of all I dont understand half of the copies that are done here. Second, we are adding every shape of a possible path to the
	// propagationCandidates??? And we add them multiple times if I understood that correctly? We repeat everything we just did for a node for all
	// its children. But why would I do everything for the parent(s) again that I just did??? That makes no sense to me.
	// E.g. we have a path A->B->C->D. Then we start with A and A has no parent so for all childs. In this case only B. Then for B we check parent
	// which is A. We do some weird copying and add him to the propCandidates. I think we are copying to create an individual node for each children??
	// Anyways, A has no more parents so we go to the children of B which is only C. Then we repeat that??? parent of C is B. B is added to PropCand.
	// Parent of B is A, therefor A is added AGAIN(????) to PropCandidates. Im lost.
	// Edit: I think the idea of this function is to convert the tree to a list. We also take every possible length of a path and add it as candidate.
	// I dont know what monster came up with this structure, first visiblity map, then a tree, then a list, which all do the same but yeah.

	if( pPropagationShapeIn == nullptr )
		return;

	assert( m_pTargetEntity );

	// In this if case, pPropagationShapeIn is eventually tested for illuminability for the sensor
	// and its branches are followed up until the first branch(illuminable by the emitter)
	if( *pPropagationShapeIn->pIsIlluminableByTargetEntity != Tristate::False )
	{
		assert( *pPropagationShapeIn->pIsIlluminableByTargetEntity != Tristate::Undefined ); // NMK: this should never happen afaik

		CPropagationShapeShared pShape;

		switch( pPropagationShapeIn->iShapeType )
		{
			case CPropagationShape::FACE:
			{
				auto pFace = std::make_shared<CPropagationFace>( );
				pFace->CopyFrom( *std::static_pointer_cast<CPropagationFace>( pPropagationShapeIn ) );

				// If the face has no valid image source, do not follow up its branch and return immediately
				if( pFace->pHasValidImageSource == false )
					return;

				pShape = pFace;

				break;
			}
			case CPropagationShape::EDGE:
			{
				auto pEdge = std::make_shared<CPropagationEdge>( );
				pEdge->CopyFrom( *std::static_pointer_cast<CPropagationEdge>( pPropagationShapeIn ) );

				// If the edge has no valid image edge, do not follow up its branch and return immediately
				if( pEdge->pHasValidImageEdge.has_value( ) && pEdge->pHasValidImageEdge.value( ) == false )
					return;

				pShape = pEdge;

				break;
			}
		}

		if( pShape != nullptr )
		{
			// Add parents up until the start point is reached
			while( CPropagationShapeShared pParent = pShape->pParent.lock( ) )
			{
				switch( pParent->iShapeType )
				{
					// NMK: why are we copying here??? EDIT: Copy because we "want" unique lists, with unique nodes
					case CPropagationShape::FACE:
					{
						CPropagationFaceShared pFaceParent = std::make_shared<CPropagationFace>( );
						pFaceParent->CopyFrom( *std::static_pointer_cast<CPropagationFace>( pParent ) );
						pParent = pFaceParent;
						break;
					}
					case CPropagationShape::EDGE:
					{
						CPropagationEdgeShared pEdgeParent = std::make_shared<CPropagationEdge>( );
						pEdgeParent->CopyFrom( *std::static_pointer_cast<CPropagationEdge>( pParent ) );
						pParent = pEdgeParent;
						break;
					}
				}

				// Set parent and child member variables to each other
				pShape->pParent = pParent;
				pParent->pChild = pShape;

				// Set copied parent to new shape for next iteration
				pShape = pParent;
			}

			// Add shape to list
			m_vpPropagationCandidates.push_back( pShape );

			// NMK: children no longer needed. Therefor throw them away.
			pShape->vpChildren.clear( ); // shape is top most node
			pShape->vpChildren.shrink_to_fit( );
			while( pShape->pChild != nullptr )
			{
				pShape = pShape->pChild;
				pShape->vpChildren.clear( );
				pShape->vpChildren.shrink_to_fit( );
			}
		}
	}

	// Add further shapes to propagation lists
	for( auto& pChild: pPropagationShapeIn->vpChildren )
		RecursiveAddShapesToPropagationLists( pChild );
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::CreateVisibilityMap( )
{
	if( m_pProgressHandler )
		m_pProgressHandler->SetItem( "Creating visibility map" );

	if( m_oConfig.bExportRuntimeStatistics )
		m_swVisibilityMap.start( );

	int k         = 0;
	int iNumSteps = (int)std::ceil( m_vpPropagationShapes.size( ) * ( m_vpPropagationShapes.size( ) - 1 ) / 2 ); // n * (n-1) / 2

	// NMK: creating visibility map, testing if shape to shape visibility in both directions, if that is the case add both shapes into visiblity map.
	// Removed strange copies from previous implementation. IMO not necessarcy EXCEPT if the fMinimumDistance is important later down the line. But that
	// doesnt seem to be the case. Looks like it is only used in calculatLevelDrop functin (not used here) and here for comparison against maxPropRange
	// We always test if path from source to receiver works AND from receiver to source. IDK if this makes sense for all shapes. Edge to Face and
	// Face to Edge maybe(?), because they would use different functions but not for face to face.

	for( size_t i = 0; i < m_vpPropagationShapes.size( ); i++ )
	{
		for( size_t j = i + 1; j < m_vpPropagationShapes.size( ); j++ )
		{
			if( m_pProgressHandler )
				m_pProgressHandler->PushProgressUpdate( k++ / float( iNumSteps ) * 100.0f );

			const std::shared_ptr<CPropagationShape> pShapeFrom = m_vpPropagationShapes.at( i );
			const std::shared_ptr<CPropagationShape> pShapeTo   = m_vpPropagationShapes.at( j );
			bool bCanIlluminate                                 = false;

			if( pShapeFrom->iShapeType == CPropagationShape::FACE )
			{
				auto pFaceFrom = std::static_pointer_cast<CPropagationFace>( pShapeFrom );

				// Face to face
				if( pShapeTo->iShapeType == CPropagationShape::FACE )
				{
					auto pFaceTo = std::static_pointer_cast<CPropagationFace>( pShapeTo );

					// Set minimum distance and test by means of the maximum propagation range for spherical propagation
					if( GetMaxPropagationRangeValid( ) )
					{
						pFaceTo->SetMinimumDistance( *pShapeFrom );
						if( pFaceTo->fMinimumDistance > GetMaxPropagationRange( ) )
							continue;
					}

					// If the faces are the same, they are not able to create valid paths
					if( pFaceFrom->hFace == pFaceTo->hFace )
						continue;

					// Start illumination test for both directions
					CanFaceIlluminateFace( bCanIlluminate, pFaceTo, pFaceFrom );
					if( bCanIlluminate )
						CanFaceIlluminateFace( bCanIlluminate, pFaceFrom, pFaceTo );
				}
				// face to edge
				else if( pShapeTo->iShapeType == CPropagationShape::EDGE )
				{
					auto pEdgeTo = std::static_pointer_cast<CPropagationEdge>( pShapeTo );

					// Set minimum distance and test by means of the maximum propagation range for spherical propagation
					if( GetMaxPropagationRangeValid( ) )
					{
						pEdgeTo->SetMinimumDistance( *pShapeFrom );
						if( pEdgeTo->fMinimumDistance > GetMaxPropagationRange( ) )
							continue;
					}

					// If the faces are the same, they are not able to create valid paths
					if( pFaceFrom->hFace == pEdgeTo->hMainFace || pFaceFrom->hFace == pEdgeTo->hOppositeFace )
						continue;

					// Start illumination test for both directions
					CanEdgeIlluminateFace( bCanIlluminate, pFaceFrom, pEdgeTo );
					if( bCanIlluminate )
						CanFaceIlluminateEdge( bCanIlluminate, pFaceFrom, pEdgeTo );
				}
			}
			else if( pShapeFrom->iShapeType == CPropagationShape::EDGE )
			{
				auto pEdgeFrom = std::static_pointer_cast<CPropagationEdge>( pShapeFrom );

				// edge to face
				if( pShapeTo->iShapeType == CPropagationShape::FACE )
				{
					auto pFaceTo = std::static_pointer_cast<CPropagationFace>( pShapeTo );

					// Set minimum distance and test by means of the maximum propagation range for spherical propagation
					if( GetMaxPropagationRangeValid( ) )
					{
						pFaceTo->SetMinimumDistance( *pShapeFrom );
						if( pFaceTo->fMinimumDistance > GetMaxPropagationRange( ) )
							continue;
					}

					// If the faces are the same, they are not able to create valid paths
					if( pFaceTo->hFace == pEdgeFrom->hMainFace || pFaceTo->hFace == pEdgeFrom->hOppositeFace )
						continue;

					// Start illumination test for both directions
					CanEdgeIlluminateFace( bCanIlluminate, pFaceTo, pEdgeFrom );
					if( bCanIlluminate )
						CanFaceIlluminateEdge( bCanIlluminate, pFaceTo, pEdgeFrom );
				}
				// edge to edge
				else if( pShapeTo->iShapeType == CPropagationShape::EDGE )
				{
					auto pEdgeTo = std::static_pointer_cast<CPropagationEdge>( pShapeTo );

					// Set minimum distance and test by means of the maximum propagation range for spherical propagation
					if( GetMaxPropagationRangeValid( ) )
					{
						pEdgeTo->SetMinimumDistance( *pShapeFrom );
						if( pEdgeTo->fMinimumDistance > GetMaxPropagationRange( ) )
							continue;
					}

					// If only neighboured edges are allowed, verify neighboorhood
					if( m_oConfig.bFilterNotNeighbouredEdges )
					{
						// If the edges are neighboured, at least one of their faces are equal
						if( pEdgeTo->hMainFace != pEdgeFrom->hMainFace && pEdgeTo->hMainFace != pEdgeFrom->hOppositeFace &&
						    pEdgeTo->hOppositeFace != pEdgeFrom->hMainFace && pEdgeTo->hOppositeFace != pEdgeFrom->hOppositeFace )
							continue;
					}

					// Start illumination test for both directions
					CanEdgeIlluminateEdge( bCanIlluminate, pEdgeFrom, pEdgeTo );
					if( bCanIlluminate )
						CanEdgeIlluminateEdge( bCanIlluminate, pEdgeFrom, pEdgeTo, m_oConfig.bFilterEdgeToEdgeIntersectedPaths );
				}
			}

			// Store if shape-to-shape visible
			if( bCanIlluminate )
			{
				m_mvpShapeVisibilityMap[pShapeFrom->GetIdentifier( )].push_back( pShapeTo );
				m_mvpShapeVisibilityMap[pShapeTo->GetIdentifier( )].push_back( pShapeFrom );
			}
		}
	}


	if( m_oConfig.bExportRuntimeStatistics )
		m_swVisibilityMap.stop( );
}

bool ITAPropagationPathSim::CombinedModel::CPathEngine::CanPointIlluminateFace( const VistaVector3D& v3Point, CITAMesh::FaceHandle hFace )
{
	return ITAGeoUtils::CanFaceBeIlluminated( *m_pMesh, hFace, v3Point );
}

bool ITAPropagationPathSim::CombinedModel::CPathEngine::CanPointIlluminateEdge( const VistaVector3D& v3Point, CPropagationEdgeShared& pPropagationEdge,
                                                                                const bool& bTestIntersection /* = false */ )
{
	if( !CanPointIlluminateFace( v3Point, pPropagationEdge->hMainFace ) && !CanPointIlluminateFace( v3Point, pPropagationEdge->hOppositeFace ) )
	{
		return false;
	}
	if( bTestIntersection )
	{
		// Resolution (100 �m) - NMK: comment outdated. 100um would take for ever
		float fResolution             = 10000 * ITAConstants::EPS_F_L;
		VistaVector3D v3EdgeDirection = *pPropagationEdge->v3EndVertex - *pPropagationEdge->v3StartVertex;
		float fRelativeStepSize       = fResolution / ( v3EdgeDirection.GetLength( ) );
		float fRelativePosition       = fRelativeStepSize;

		while( fRelativePosition < 1 )
		{
			// If a visible path is found, the edge can be illuminated by the point
			if( IsPathVisible( v3Point, *pPropagationEdge->v3StartVertex + fRelativePosition * v3EdgeDirection ) )
				return true;
			else // Search further points on the edge
				fRelativePosition += fRelativeStepSize;
		}

		// No Visible path found
		return false;
	}
	else
	{
		return true;
	}
}

bool ITAPropagationPathSim::CombinedModel::CPathEngine::IsPathVisible( const VistaVector3D& v3StartPoint, const VistaVector3D& v3EndPoint )
{
	// Search for intersections between two neighbored anchors and repeat it for all neighbored anchors of the path

	// Iterate intersection search over all faces of current mesh model
	auto faceIter = m_pMesh->faces_begin( );
	while( faceIter != m_pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *faceIter++ );

		// If one of the path sections crosses one of the faces, the whole path won't be visible
		if( ITAGeoUtils::IsLineIntersectingFace( v3StartPoint, v3EndPoint, m_pMesh.get( ), hFace ) == EIntersecting::BETWEEN )
			return false;
	}

	// No intersections occur
	return true;
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::CanEdgeIlluminateFace( bool& bCanEdgeIlluminateFaceOut, CPropagationFaceShared& pPropagationFace,
                                                                               CPropagationEdgeShared& pPropagationEdge )
{
	float eps = ITAConstants::EPS_F_L;

	bCanEdgeIlluminateFaceOut = ITAGeoUtils::CanFaceBeIlluminated( *m_pMesh, pPropagationFace->hFace,
	                                                               ( 1 - eps ) * ( *pPropagationEdge->v3StartVertex ) + ( eps ) * ( *pPropagationEdge->v3EndVertex ) );
	bCanEdgeIlluminateFaceOut |= ITAGeoUtils::CanFaceBeIlluminated( *m_pMesh, pPropagationFace->hFace,
	                                                                ( 1 - eps ) * ( *pPropagationEdge->v3EndVertex ) + ( eps ) * ( *pPropagationEdge->v3StartVertex ) );
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::CanFaceIlluminateEdge( bool& bCanFaceIlluminateEdgeOut, CPropagationFaceShared& pPropagationFace,
                                                                               CPropagationEdgeShared& pPropagationEdge )
{
	// Iterate over all vertices of the face. If at least one vertex is in the illuminable range of the edge, return true
	bool bMainFaceIlluminated, bOppositeFaceIlluminated;

	for( auto& pVertex: pPropagationFace->vv3Vertices )
	{
		bMainFaceIlluminated     = ITAGeoUtils::CanFaceBeIlluminated( *m_pMesh, pPropagationEdge->hMainFace, *pVertex );
		bOppositeFaceIlluminated = ITAGeoUtils::CanFaceBeIlluminated( *m_pMesh, pPropagationEdge->hOppositeFace, *pVertex );

		// Check whether wedge angle is >pi or <pi; TODO: export in own function
		bool bIsOuterAngle = pPropagationEdge->v3MainFaceNormal->Cross( *pPropagationEdge->v3OppositeFaceNormal )
		                         .Dot( *pPropagationEdge->v3EndVertex - *pPropagationEdge->v3StartVertex ) > 0;


		if( m_oConfig.bFilterIlluminatedRegionDiffraction )
			if( bIsOuterAngle )
				bCanFaceIlluminateEdgeOut = bMainFaceIlluminated != bOppositeFaceIlluminated; // A XOR B, only valid, if at least one point can lie in the shadow region
			else
				bCanFaceIlluminateEdgeOut = false; // Never illuminable
		else if( bIsOuterAngle )
			bCanFaceIlluminateEdgeOut = bMainFaceIlluminated || bOppositeFaceIlluminated; // A OR B, also valid, if face can only lie in the illuminated region
		else
			bCanFaceIlluminateEdgeOut = bMainFaceIlluminated && bOppositeFaceIlluminated; // A AND B, only valid if face lie comnplete in the illuminated region

		if( bCanFaceIlluminateEdgeOut )
			return;
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::CanFaceIlluminateFace( bool& bCanFaceIlluminateFaceOut, CPropagationFaceShared& pPropagationFaceStart,
                                                                               CPropagationFaceShared& pPropagationFaceEnd )
{
	// Iterate over all vertices of the face. If at least one vertex is in the illuminable range of the edge, return true
	for( auto& pVertex: pPropagationFaceStart->vv3Vertices )
	{
		if( ITAGeoUtils::CanFaceBeIlluminated( *m_pMesh, pPropagationFaceEnd->hFace, *pVertex ) )
		{
			bCanFaceIlluminateFaceOut = true;
			return;
		}
	}
	return; // bCanFaceIlluminateFace out is false by default
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::CanEdgeIlluminateEdge( bool& bCanEdgeIlluminateEdgeOut, CPropagationEdgeShared& pPropagationEdgeStart,
                                                                               CPropagationEdgeShared& pPropagationEdgeEnd, const bool& bTestIntersection /* = false */ )
{
	bool bMainFaceIlluminated, bOppositeFaceIlluminated;

	float eps = ITAConstants::EPS_F_L;
	// Illumination test at from vertex position


	// similar to canEdgeIlluminateFace function, can we hit face from either start or end vertex
	bMainFaceIlluminated = ITAGeoUtils::CanFaceBeIlluminated(
	    *m_pMesh, pPropagationEdgeEnd->hMainFace, ( 1 - eps ) * ( *pPropagationEdgeStart->v3StartVertex ) + ( eps ) * ( *pPropagationEdgeStart->v3EndVertex ) );
	bMainFaceIlluminated |= ITAGeoUtils::CanFaceBeIlluminated(
	    *m_pMesh, pPropagationEdgeEnd->hMainFace, ( 1 - eps ) * ( *pPropagationEdgeStart->v3EndVertex ) + ( eps ) * ( *pPropagationEdgeStart->v3StartVertex ) );

	bOppositeFaceIlluminated = ITAGeoUtils::CanFaceBeIlluminated(
	    *m_pMesh, pPropagationEdgeEnd->hOppositeFace, ( 1 - eps ) * ( *pPropagationEdgeStart->v3StartVertex ) + ( eps ) * ( *pPropagationEdgeStart->v3EndVertex ) );
	bOppositeFaceIlluminated |= ITAGeoUtils::CanFaceBeIlluminated(
	    *m_pMesh, pPropagationEdgeEnd->hOppositeFace, ( 1 - eps ) * ( *pPropagationEdgeStart->v3EndVertex ) + ( eps ) * ( *pPropagationEdgeStart->v3StartVertex ) );

	// Check whether wedge angle is >pi or <pi; TODO: export in own function
	bool bIsOuterAngle = pPropagationEdgeEnd->v3MainFaceNormal->Cross( *pPropagationEdgeEnd->v3OppositeFaceNormal )
	                         .Dot( *pPropagationEdgeEnd->v3EndVertex - *pPropagationEdgeEnd->v3StartVertex ) > 0;


	if( m_oConfig.bFilterIlluminatedRegionDiffraction )
		if( bIsOuterAngle )
			bCanEdgeIlluminateEdgeOut = bMainFaceIlluminated != bOppositeFaceIlluminated; // A XOR B, only valid, if at least one point can lie in the shadow region
		else
			bCanEdgeIlluminateEdgeOut = false; // Never illuminable
	else if( bIsOuterAngle )
		bCanEdgeIlluminateEdgeOut = bMainFaceIlluminated || bOppositeFaceIlluminated; // A OR B, also valid, if face can only lie in the illuminated region
	else
		bCanEdgeIlluminateEdgeOut = bMainFaceIlluminated && bOppositeFaceIlluminated; // A AND B, only valid if face lie comnplete in the illuminated region


	// If boolean set, also test for intersections(
	if( bCanEdgeIlluminateEdgeOut && bTestIntersection )
	{
		// Set output parameter to false
		bCanEdgeIlluminateEdgeOut = false;

		// Resolution (100 �m) - NMK: changed to 0.1 for performance reasons.
		float fResolution             = 10000 * ITAConstants::EPS_F_L;
		VistaVector3D v3EdgeDirection = *pPropagationEdgeStart->v3EndVertex - *pPropagationEdgeStart->v3StartVertex;
		float fRelativeStepSize       = fResolution / ( v3EdgeDirection.GetLength( ) );
		float fRelativePosition       = fRelativeStepSize;

		while( fRelativePosition < 1 )
		{
			if( CanPointIlluminateEdge( *pPropagationEdgeStart->v3StartVertex + fRelativePosition * v3EdgeDirection, pPropagationEdgeEnd, bTestIntersection ) )
			{
				bCanEdgeIlluminateEdgeOut = true; // if at least one position doesn't contain a intersection with other faces
				break;
			}
			else
			{
				fRelativePosition += fRelativeStepSize;
			}
		}
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::ConstructPropagationShapes( )
{
	m_vpPropagationShapes.clear( );

	auto mMaterialManager = OpenMesh::getOrMakeProperty<CITAMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *m_pMesh, "Acoustics::Material::Face" );

	// Iterate over all faces of mesh
	CITAMesh::ConstFaceIter cf_it = m_pMesh->faces_begin( );
	int i                         = 0;
	while( cf_it != m_pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *cf_it++ );

		if( m_pProgressHandler )
			m_pProgressHandler->PushProgressUpdate( "Constructing face propagation items", i++ / float( m_pMesh->n_faces( ) ) * 100.0f );

		CPropagationFaceShared pPropagationFace = std::make_shared<CPropagationFace>( );

		pPropagationFace->iShapeType                   = CPropagationShape::FACE;
		pPropagationFace->pIsIlluminableByTargetEntity = std::make_shared<Tristate>( Tristate::Undefined );
		pPropagationFace->hFace                        = hFace;
		pPropagationFace->hShape                       = hFace;
		pPropagationFace->pMaterial                    = mMaterialManager[hFace].GetMaterial( );

		// Get vertices
		// Iterate over all vertices of current face
		CITAMesh::ConstFaceVertexIter cfv_it = m_pMesh->cfv_begin( hFace );
		while( cfv_it != m_pMesh->cfv_end( hFace ) )
		{
			CITAMesh::VertexHandle hVertex( *cfv_it++ );

			pPropagationFace->vv3Vertices.push_back( std::make_shared<VistaVector3D>( m_pMesh->point( hVertex ).data( ) ) );
		}

		// Construct boundary sphere
		pPropagationFace->SetBoundarySphere( );

		// Construct plane
		pPropagationFace->pPlane = std::make_shared<VistaPlane>( );
		pPropagationFace->pPlane->SetOrigin( *pPropagationFace->vv3Vertices[0] );                               // Set first vertex as origin
		pPropagationFace->pPlane->SetNormVector( VistaVector3D( m_pMesh->calc_face_normal( hFace ).data( ) ) ); // Set face normal as plane normal

		m_vpPropagationShapes.push_back( pPropagationFace );
	}

	// Iterate over all edges of mesh
	CITAMesh::ConstEdgeIter cf_et = m_pMesh->edges_begin( );
	int j                         = 0;
	while( cf_et != m_pMesh->edges_end( ) )
	{
		CITAMesh::EdgeHandle hEdge( *cf_et++ );

		if( m_pProgressHandler )
			m_pProgressHandler->PushProgressUpdate( "Constructing edge propagation items", j++ / float( m_pMesh->n_edges( ) ) * 100.0f );

		// Halfedge representation of edge
		auto hHalfedge = m_pMesh->halfedge_handle( hEdge, 0 );

		// Main and opposite  face
		auto hMainFace     = m_pMesh->face_handle( hHalfedge );
		auto hOppositeFace = m_pMesh->opposite_face_handle( hHalfedge );

		// Ignore edge if only one face is connected with halfedge. One example is the ground surface
		if( !hMainFace.is_valid( ) || !hOppositeFace.is_valid( ) )
			continue;

		// Main and opposite  face normals
		auto v3MainFaceNormal     = m_pMesh->calc_face_normal( hMainFace );
		auto v3OppositeFaceNormal = m_pMesh->calc_face_normal( hOppositeFace );

		// Ignore edge if normals of the two aligned faces are the same. In this case, the edge is not really an edge
		if( ( v3MainFaceNormal - v3OppositeFaceNormal ).length( ) < ITAConstants::EPS_F_L )
			continue;

		CPropagationEdgeShared pPropagationEdge = std::make_shared<CPropagationEdge>( );

		pPropagationEdge->iShapeType                   = CPropagationShape::EDGE;
		pPropagationEdge->pIsIlluminableByTargetEntity = std::make_shared<Tristate>( Tristate::Undefined );
		pPropagationEdge->hEdge                        = hEdge;
		pPropagationEdge->hHalfedge                    = hHalfedge;
		pPropagationEdge->hShape                       = hHalfedge;

		// Set vertices
		auto hFromVertex                = m_pMesh->from_vertex_handle( pPropagationEdge->hHalfedge );
		auto hToVertex                  = m_pMesh->to_vertex_handle( pPropagationEdge->hHalfedge );
		pPropagationEdge->v3StartVertex = std::make_shared<VistaVector3D>( m_pMesh->point( hFromVertex ).data( ) );
		pPropagationEdge->v3EndVertex   = std::make_shared<VistaVector3D>( m_pMesh->point( hToVertex ).data( ) );
		pPropagationEdge->vv3Vertices.push_back( pPropagationEdge->v3StartVertex ); // start and end vertices are added to genereal vertices property
		pPropagationEdge->vv3Vertices.push_back( pPropagationEdge->v3EndVertex );

		// Set boundary sphere
		pPropagationEdge->SetBoundarySphere( );

		// Set face normals, if possible
		pPropagationEdge->hMainFace        = m_pMesh->face_handle( pPropagationEdge->hHalfedge );
		pPropagationEdge->v3MainFaceNormal = std::make_shared<VistaVector3D>( m_pMesh->calc_face_normal( pPropagationEdge->hMainFace ).data( ) );

		pPropagationEdge->hOppositeFace = m_pMesh->opposite_face_handle( pPropagationEdge->hHalfedge );
		if( !pPropagationEdge->hOppositeFace.is_valid( ) )
			continue;
		else
			pPropagationEdge->v3OppositeFaceNormal = std::make_shared<VistaVector3D>( m_pMesh->calc_face_normal( pPropagationEdge->hOppositeFace ).data( ) );

		m_vpPropagationShapes.push_back( pPropagationEdge );
	}
}

void ITAPropagationPathSim::CombinedModel::CPathEngine::PushStatus( std::string s, float f )
{
	if( m_pProgressHandler )
		m_pProgressHandler->PushProgressUpdate( s, f );
}


void ITAPropagationPathSim::CombinedModel::CPathEngine::SetShapeIlluminationByTargetEntity( )
{
	for( std::shared_ptr<CPropagationShape> pShape: m_vpPropagationShapes )
	{
		if( pShape->iShapeType == CPropagationShape::FACE )
		{
			std::shared_ptr<CPropagationFace> pFace = std::static_pointer_cast<CPropagationFace>( pShape );

			if( CanPointIlluminateFace( m_pTargetEntity->v3InteractionPoint, pFace->hFace ) )
				*pFace->pIsIlluminableByTargetEntity = Tristate::True;
			else
				*pFace->pIsIlluminableByTargetEntity = Tristate::False;
		}
		else if( pShape->iShapeType == CPropagationShape::EDGE )
		{
			std::shared_ptr<CPropagationEdge> pEdge = std::static_pointer_cast<CPropagationEdge>( pShape );

			if( CanPointIlluminateEdge( m_pTargetEntity->v3InteractionPoint, pEdge, m_oConfig.bFilterSensorToEdgeIntersectedPaths ) )
				*pEdge->pIsIlluminableByTargetEntity = Tristate::True;
			else
				*pEdge->pIsIlluminableByTargetEntity = Tristate::False;
		}
	}

	return;
}