#include <ITAPropagationPathSim/CombinedModel/PropagationShapes.h>


using namespace ITAPropagationPathSim::CombinedModel;


void CPropagationShape::CopyFrom( const CPropagationShape& oPropagationShapeIn )
{
	iShapeType = oPropagationShapeIn.iShapeType;

	vv3Vertices = oPropagationShapeIn.vv3Vertices; // vec of pointer
	pParent     = oPropagationShapeIn.pParent;     // weakptr
	pChild      = oPropagationShapeIn.pChild;      // pointer
	vpChildren  = oPropagationShapeIn.vpChildren;  // vec of pointer

	v3Barycenter     = oPropagationShapeIn.v3Barycenter; // pointer
	fRadius          = oPropagationShapeIn.fRadius;
	fMinimumDistance = oPropagationShapeIn.fMinimumDistance;

	hShape = oPropagationShapeIn.hShape;

	pIsIlluminableByTargetEntity = oPropagationShapeIn.pIsIlluminableByTargetEntity; // pointer
}

void CPropagationShape::GetBoundingBoxAxesAligned( VistaVector3D& v3Min, VistaVector3D& v3Max ) const
{
	v3Min = *vv3Vertices[0];
	v3Max = v3Min;

	for( auto& v3Vertex: vv3Vertices )
	{
		v3Min.SetValues( fminf( v3Min[0], ( *v3Vertex )[0] ), fminf( v3Min[1], ( *v3Vertex )[1] ), fminf( v3Min[2], ( *v3Vertex )[2] ) );
		v3Max.SetValues( fmaxf( v3Max[0], ( *v3Vertex )[0] ), fmaxf( v3Max[1], ( *v3Vertex )[1] ), fmaxf( v3Max[2], ( *v3Vertex )[2] ) );
	}
}

bool CPropagationShape::SetBoundarySphere( )
{
	// the shape must have at least one vertex
	if( vv3Vertices.size( ) < 1 )
		return false;

	// Set barycenter
	v3Barycenter = std::make_shared<VistaVector3D>( 0.0, 0.0, 0.0, 1.0 );
	for( auto& v3Vertex: vv3Vertices )
		*v3Barycenter += *v3Vertex;

	*v3Barycenter /= float( vv3Vertices.size( ) );

	// Set the radius of the sphere(longest distance between vertex and shape)
	fRadius = 0.0f;
	for( auto& v3Vertex: vv3Vertices )
		fRadius = fmaxf( fRadius, ( *v3Barycenter - *v3Vertex ).GetLength( ) );

	return true;
}

bool CPropagationShape::SetMinimumDistance( const CPropagationShape& oPropagationShape )
{
	if( v3Barycenter == nullptr || oPropagationShape.v3Barycenter == nullptr )
		return false;

	// Calculate the distance between two spheres
	fMinimumDistance = CalculateMinimumDistance( *oPropagationShape.v3Barycenter ) - oPropagationShape.fRadius;

	// Negative distances are not possible
	fMinimumDistance = fmaxf( fMinimumDistance, 0.0f );


	return true;
}

int CPropagationShape::GetPolygonID( ) const
{
	if( hShape.is_valid( ) )
		return hShape.idx( );
	else
		return -1;
}

bool CPropagationShape::SetMinimumDistance( const VistaVector3D& v3Point )
{
	if( v3Barycenter == nullptr )
		return false;

	fMinimumDistance = CalculateMinimumDistance( v3Point );

	return true;
}

float CPropagationShape::CalculateMinimumDistance( const VistaVector3D& v3Point )
{
	// Calculate the distance between two spheres
	float fMinimumDistance = ( *v3Barycenter - v3Point ).GetLength( ) - fRadius;

	// Negative distances are not possible
	fMinimumDistance = fmaxf( fMinimumDistance, 0.0f );
	return fMinimumDistance;
}

float CPropagationShape::CalculateLevelDrop( float& fFirstDistance, float& fSecondDistance, const float& fLevelDropAtFirstEdge )
{
	float fLevelDrop = 0;
	if( fLevelDropAtFirstEdge <= 0.0f )
	{
		fFirstDistance += fMinimumDistance;

		fLevelDrop = fmaxf( 0.0f, 20 * log10f( fFirstDistance ) );
	}
	else
	{
		fSecondDistance += fMinimumDistance;

		float fAdditionalLevelDrop = 10 * log10f( fSecondDistance * ( fSecondDistance + fFirstDistance ) / fFirstDistance );
		fLevelDrop                 = fLevelDropAtFirstEdge + fmaxf( 0.0f, fAdditionalLevelDrop );
	}


	return fLevelDrop;
}

size_t CPropagationShape::GetIdentifier( ) const
{
	std::hash<std::string> hashString;
	std::hash<EShapeType> hashType;
	std::hash<OpenMesh::BaseHandle> hashShape;

	// identifier is deprecated because all shapes belong to the same model and therefor same name. Hardcode random string.
	size_t identifier = hashString( "model" );
	identifier ^= hashType( iShapeType ) + 0x9e3779b9 + ( identifier << 6 ) + ( identifier >> 2 ); // Magic number:	2 ^ 32 / ((1 + sqrt(5)) / 2) = 0x9e3779b9
	identifier ^= hashShape( hShape ) + 0x9e3779b9 + ( identifier << 6 ) + ( identifier >> 2 );
	return identifier;
}


void CPropagationFace::CopyFrom( const CPropagationFace& oPropagationFaceIn )
{
	CPropagationShape::CopyFrom( oPropagationFaceIn );

	hFace                  = oPropagationFaceIn.hFace;
	pPlane                 = oPropagationFaceIn.pPlane;                 // ptr
	v3ImageSource          = oPropagationFaceIn.v3ImageSource;          // ptr
	v3ImageEdgeSourceStart = oPropagationFaceIn.v3ImageEdgeSourceStart; // ptr
	v3ImageEdgeSourceEnd   = oPropagationFaceIn.v3ImageEdgeSourceEnd;   // ptr
	pHasValidImageSource   = oPropagationFaceIn.pHasValidImageSource;   // ptr
	pMaterial              = oPropagationFaceIn.pMaterial;              // ptr
}

bool CPropagationFace::SetMinimumDistance( const CPropagationShape& oPropagationShape )
{
	if( v3Barycenter == nullptr || oPropagationShape.v3Barycenter == nullptr )
		return false;

	// Calculate the distance between two spheres
	fMinimumDistance = CalculateMinimumDistance( *oPropagationShape.v3Barycenter ) - oPropagationShape.fRadius;

	return true;
}

bool CPropagationFace::SetMinimumDistance( const VistaVector3D& v3Point )
{
	if( v3Barycenter == nullptr )
		return false;

	fMinimumDistance = CalculateMinimumDistance( v3Point );

	return true;
}

float CPropagationFace::CalculateMinimumDistance( const VistaVector3D& v3Point )
{
	// Distance to plane
	float fPlaneDistance = fabs( pPlane->CalcDistance( v3Point ) );

	// Distance to sphere
	float fSphereDistance = CPropagationShape::CalculateMinimumDistance( v3Point );

	// Maximum of both is used
	return std::max( fPlaneDistance, fSphereDistance );
}


void CPropagationEdge::CopyFrom( const CPropagationEdge& oPropagationEdgeIn )
{
	CPropagationShape::CopyFrom( oPropagationEdgeIn );

	hHalfedge                = oPropagationEdgeIn.hHalfedge;
	hEdge                    = oPropagationEdgeIn.hEdge;
	hMainFace                = oPropagationEdgeIn.hMainFace;
	hOppositeFace            = oPropagationEdgeIn.hOppositeFace;
	v3StartVertex            = oPropagationEdgeIn.v3StartVertex;            // ptr
	v3EndVertex              = oPropagationEdgeIn.v3EndVertex;              // ptr
	v3MainFaceNormal         = oPropagationEdgeIn.v3MainFaceNormal;         // ptr
	v3OppositeFaceNormal     = oPropagationEdgeIn.v3OppositeFaceNormal;     // ptr
	v3ImageEdgeReceiverStart = oPropagationEdgeIn.v3ImageEdgeReceiverStart; // ptr
	v3ImageEdgeReceiverEnd   = oPropagationEdgeIn.v3ImageEdgeReceiverEnd;   // ptr
	pHasValidImageEdge       = oPropagationEdgeIn.pHasValidImageEdge;       // ptr
}

bool CPropagationEdge::SetMinimumDistance( const CPropagationShape& oPropagationShape )
{
	if( v3Barycenter == nullptr || oPropagationShape.v3Barycenter == nullptr )
		return false;

	// Calculate the distance between two spheres
	fMinimumDistance = CalculateMinimumDistance( *oPropagationShape.v3Barycenter ) - oPropagationShape.fRadius;

	// Negative distances are not possible
	fMinimumDistance = fmaxf( fMinimumDistance, 0.0f );


	return true;
}

bool CPropagationEdge::SetMinimumDistance( const VistaVector3D& v3Point )
{
	if( v3Barycenter == nullptr )
		return false;

	fMinimumDistance = CalculateMinimumDistance( v3Point );

	return true;
}

float CPropagationEdge::CalculateMinimumDistance( const VistaVector3D& v3Point )
{
	// Distance to projection on line defined by edge
	VistaPlane plane1, plane2;
	plane1.SetOrigin( *v3StartVertex );
	plane2.SetOrigin( *v3StartVertex );
	plane1.SetNormVector( *v3MainFaceNormal );
	plane2.SetNormVector( *v3OppositeFaceNormal );

	// Project point on first plane. Thereafter, project projected point on second plane
	VistaVector3D v3Projection = plane1.CalcNearestPointOnPlane( v3Point );
	v3Projection               = plane2.CalcNearestPointOnPlane( v3Projection );

	float fLineDistance = ( v3Point - v3Projection ).GetLength( );

	// Distance to sphere
	float fSphereDistance = CPropagationShape::CalculateMinimumDistance( v3Point );

	// Maximum of both is used
	return std::max( fLineDistance, fSphereDistance );
}

float CPropagationEdge::CalculateLevelDrop( float& fFirstDistance, float& fSecondDistance, float& fLevelDropAtFirstEdge )
{
	float fLevelDrop = 0;

	fLevelDrop = CPropagationShape::CalculateLevelDrop( fFirstDistance, fSecondDistance, fLevelDropAtFirstEdge );

	// This is the first edge
	if( fLevelDropAtFirstEdge < 0.0f )
	{
		fLevelDropAtFirstEdge = fLevelDrop;
	}

	return fLevelDrop;
}

bool ITAPropagationPathSim::CombinedModel::CPropagationEdge::IsOuterWedge( ) const
{
	VistaVector3D v3ApertureDirection = *v3EndVertex - *v3StartVertex;
	bool bSameDirection               = v3MainFaceNormal->Cross( *v3OppositeFaceNormal ).Dot( v3ApertureDirection ) > 0.0f;

	// Per convention, outer wedges defined in a right-handed manner result in a positive
	// wedge aperture direction ( main x opposite = aperture dir)

	return bSameDirection;
}
