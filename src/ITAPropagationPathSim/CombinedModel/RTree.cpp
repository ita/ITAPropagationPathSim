#include <ITAPropagationPathSim/CombinedModel/RTree.h>

using namespace ITAGeo;
using namespace ITAPropagationPathSim::CombinedModel;


//===RTree::CBuildings class=======================================================================================

// ---Constructor & Destructor-------------------------------------------------------------------------------------

CShapesTree::CShapesTree( ) {}

CShapesTree::~CShapesTree( ) {}

// ---Create functions----------------------------------------------------------------------------------------------

void CShapesTree::Create( const std::vector<CPropagationShapeShared>& vpShapesIn )
{
	// Set buildings vector
	m_vpShapes = vpShapesIn;

	// Set vertex VistaVectors
	SetVertices( );

	// Create branches if number of shapes is more than one
	if( m_vpShapes.size( ) > 1 )
		CreateBranches( );
}

void CShapesTree::CreateBranches( )
{
	// Create 8 vectors of shape-vectors (for each region one vector)
	std::vector<std::vector<CPropagationShapeShared>> vvpBranchShapes;
	vvpBranchShapes.resize( 8 );

	// Points, where branches are sub-divided into. Mid points between max and min.
	float fSubDivideX = ( m_v3TreeMin[0] + m_v3TreeMax[0] ) / 2.0f;
	float fSubDivideY = ( m_v3TreeMin[1] + m_v3TreeMax[1] ) / 2.0f;
	float fSubDivideZ = ( m_v3TreeMin[2] + m_v3TreeMax[2] ) / 2.0f;

	// Add building to branchbuildings vector according to its location
	for( auto& pShape: m_vpShapes )
	{
		VistaVector3D v3Min, v3Max;
		pShape->GetBoundingBoxAxesAligned( v3Min, v3Max );

		if( v3Min[2] < fSubDivideZ )
		{
			if( v3Min[1] < fSubDivideY )
			{
				if( v3Min[0] < fSubDivideX )
				{
					vvpBranchShapes[0].push_back( pShape );
				}
				else
				{
					vvpBranchShapes[1].push_back( pShape );
				}
			}
			else
			{
				if( v3Min[0] < fSubDivideX )
				{
					vvpBranchShapes[2].push_back( pShape );
				}
				else
				{
					vvpBranchShapes[3].push_back( pShape );
				}
			}
		}
		else
		{
			if( v3Min[1] < fSubDivideY )
			{
				if( v3Min[0] < fSubDivideX )
				{
					vvpBranchShapes[4].push_back( pShape );
				}
				else
				{
					vvpBranchShapes[5].push_back( pShape );
				}
			}
			else
			{
				if( v3Min[0] < fSubDivideX )
				{
					vvpBranchShapes[6].push_back( pShape );
				}
				else
				{
					vvpBranchShapes[7].push_back( pShape );
				}
			}
		}
	}

	// For each subdivided building vector, create a branch. Create no further branch, if only one vvpBranchShapes vector exists
	if( vvpBranchShapes.size( ) > 1 )
		for( auto& vpShapesCurrentBranch: vvpBranchShapes )
		{
			if( vpShapesCurrentBranch.size( ) > 0 )
			{
				auto pBranch = std::make_shared<CShapesTree>( );
				pBranch->Create( vpShapesCurrentBranch );

				m_vpBranches.push_back( pBranch );
			}
		}
}

void CShapesTree::SetVertices( )
{
	// Create start min and max
	m_vpShapes[0]->GetBoundingBoxAxesAligned( m_v3TreeMin, m_v3TreeMax );

	// Update min and max
	for( auto& pShape: m_vpShapes )
	{
		VistaVector3D v3Min, v3Max;

		pShape->GetBoundingBoxAxesAligned( v3Min, v3Max );

		for( int i = 0; i < 3; i++ )
		{
			m_v3TreeMin[i] = std::min( m_v3TreeMin[i], v3Min[i] );
			m_v3TreeMax[i] = std::max( m_v3TreeMax[i], v3Max[i] );
		}
	}
}

// ---Test function----------------------------------------------------------------------------------------------

bool CShapesTree::CanLineSegmentIntersect( const VistaVector3D& oLineStart, const VistaVector3D& oLineEnd, const CPropagationShapeShared& pShape ) const
{
	// Compute intersection test with bounding box. if if intersectes, further test lower branches for intersections
	return RecursiveIntersectionTest( oLineStart, oLineEnd, *this, pShape );
}
bool CShapesTree::CanLineSegmentIntersect( const VistaVector3D& oLineStart, const VistaVector3D& oLineEnd ) const
{
	// Compute intersection test with bounding box. if if intersectes, further test lower branches for intersections
	return RecursiveIntersectionTest( oLineStart, oLineEnd, *this );
}

bool CShapesTree::RecursiveIntersectionTest( const VistaVector3D& oLineStart, const VistaVector3D& oLineEnd, const CShapesTree& oTree,
                                             const CPropagationShapeShared& pShape ) const
{
	if( !oTree.ContainsShape( pShape ) )
		return false;

	VistaVector3D v3Origin = oLineStart;
	VistaVector3D v3Dir    = ( oLineEnd - oLineStart ).GetNormalized( );

	// Iersection test with the bounding box. Only if line intersects the box, test its branch
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection

	float tmin = ( oTree.m_v3TreeMin[0] - v3Origin[0] ) / v3Dir[0];
	float tmax = ( oTree.m_v3TreeMax[0] - v3Origin[0] ) / v3Dir[0];

	if( tmin > tmax )
		std::swap( tmin, tmax );

	float tymin = ( oTree.m_v3TreeMin[1] - v3Origin[1] ) / v3Dir[1];
	float tymax = ( oTree.m_v3TreeMax[1] - v3Origin[1] ) / v3Dir[1];

	if( tymin > tymax )
		std::swap( tymin, tymax );

	if( ( tmin > tymax ) || ( tymin > tmax ) )
		return false;

	if( tymin > tmin )
		tmin = tymin;

	if( tymax < tmax )
		tmax = tymax;

	float tzmin = ( oTree.m_v3TreeMin[2] - v3Origin[2] ) / v3Dir[2];
	float tzmax = ( oTree.m_v3TreeMax[2] - v3Origin[2] ) / v3Dir[2];

	if( tzmin > tzmax )
		std::swap( tzmin, tzmax );

	if( ( tmin > tzmax ) || ( tzmin > tmax ) )
		return false;

	if( tzmin > tmin )
		tmin = tzmin;

	if( tzmax < tmax )
		tmax = tzmax;


	// Intersection test with children

	bool bIntersectsChild = false;
	for( auto& pChild: oTree.m_vpBranches )
	{
		bIntersectsChild |= RecursiveIntersectionTest( oLineStart, oLineEnd, *pChild, pShape );
	}

	return bIntersectsChild;
}

bool CShapesTree::RecursiveIntersectionTest( const VistaVector3D& oLineStart, const VistaVector3D& oLineEnd, const CShapesTree& oTree ) const
{
	VistaVector3D v3Origin = oLineStart;
	VistaVector3D v3Dir    = ( oLineEnd - oLineStart ).GetNormalized( );

	// Iersection test with the bounding box. Only if line intersects the box, test its branch
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection

	float tmin = ( oTree.m_v3TreeMin[0] - v3Origin[0] ) / v3Dir[0];
	float tmax = ( oTree.m_v3TreeMax[0] - v3Origin[0] ) / v3Dir[0];

	if( tmin > tmax )
		std::swap( tmin, tmax );

	float tymin = ( oTree.m_v3TreeMin[1] - v3Origin[1] ) / v3Dir[1];
	float tymax = ( oTree.m_v3TreeMax[1] - v3Origin[1] ) / v3Dir[1];

	if( tymin > tymax )
		std::swap( tymin, tymax );

	if( ( tmin > tymax ) || ( tymin > tmax ) )
		return false;

	if( tymin > tmin )
		tmin = tymin;

	if( tymax < tmax )
		tmax = tymax;

	float tzmin = ( oTree.m_v3TreeMin[2] - v3Origin[2] ) / v3Dir[2];
	float tzmax = ( oTree.m_v3TreeMax[2] - v3Origin[2] ) / v3Dir[2];

	if( tzmin > tzmax )
		std::swap( tzmin, tzmax );

	if( ( tmin > tzmax ) || ( tzmin > tmax ) )
		return false;

	if( tzmin > tmin )
		tmin = tzmin;

	if( tzmax < tmax )
		tmax = tzmax;


	// Intersection test with children

	bool bIntersectsChild = false;
	for( auto& pChild: oTree.m_vpBranches )
	{
		bIntersectsChild |= RecursiveIntersectionTest( oLineStart, oLineEnd, *pChild );
	}

	return bIntersectsChild;
}
bool ITAPropagationPathSim::CombinedModel::CShapesTree::ContainsShape( const CPropagationShapeShared& pShapeIn ) const
{
	for( auto& pTreeShape: m_vpShapes )
	{
		if( pTreeShape->GetIdentifier( ) == pShapeIn->GetIdentifier( ) )
			return true;
	}


	return false;
}
