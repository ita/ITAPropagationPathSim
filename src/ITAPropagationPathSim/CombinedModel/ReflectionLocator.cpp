#include <ITAPropagationPathSim\CombinedModel\ReflectionLocator.h>

bool ITAPropagationPathSim::CombinedModel::Reflection::ConstructPointsOfReflection( std::shared_ptr<const ITAGeo::CPropagationAnchor> pSensor,
                                                                                    const std::vector<CPropagationShapeShared> pPropagationListsIn,
                                                                                    std::vector<CPropagationShapeShared>& pPropagationListsOut )
{
	pPropagationListsOut.clear( );

	for( auto& pStartShape: pPropagationListsIn )
	{
		// Empty lists are ignored
		if( pStartShape == nullptr )
			continue;

		bool bValidReflectionPoints = true;

		// First, go to the last shape
		CPropagationShapeShared pShape = pStartShape;
		while( pShape->pChild != nullptr )
			pShape = pShape->pChild;

		// Set the interaction point of the sensor as the starting receiver
		VistaVector3D v3LastInteraction = pSensor->v3InteractionPoint;

		// Go through all shapes until the first shape is reached
		while( pShape != nullptr )
		{
			CPropagationFaceShared pFace;
			if( pShape->iShapeType == CPropagationShape::FACE )
			{
				pFace = std::static_pointer_cast<CPropagationFace>( pShape );
				// Calculate the points of intersection
				if( ITAGeoUtils::IsPointInFrontOfPlane( *pFace->pPlane, v3LastInteraction ) )
				{
					// If the face doesn't contain a valid face, the whole path will be invalid
					if( pFace->pHasValidImageSource == false )
					{
						bValidReflectionPoints = false;
						break;
					}

					// Calculate the point of intersection.
					// The point is directly saved as v3LastInteraction,
					// because it will be also used in the next iteration
					bool bValid = pFace->pPlane->CalcIntersection( VistaLineSegment( pFace->v3ImageSource, v3LastInteraction ), v3LastInteraction );

					if( bValid && ITAGeoUtils::IsPointInFace( v3LastInteraction, pFace->vv3Vertices ) )
					{
						pFace->v3InteractionPoint = std::make_unique<VistaVector3D>( v3LastInteraction );
					}
					else // Invalid path because the current interaction point does not lie on the face
					{
						bValidReflectionPoints = false;

						break;
					}
				}
				else // Invalid path because the last intersection is not in front of face
				{
					bValidReflectionPoints = false;

					break;
				}
			}
			else // Everytime an edge occurs, replace the receiver with the aperture point(interaction point) of the edge
			{
				v3LastInteraction = *pShape->v3InteractionPoint;
			}

			pShape = pShape->pParent.lock( );
		}

		// If no invalid point occurs, add shapes to output list
		if( bValidReflectionPoints )
			pPropagationListsOut.push_back( pStartShape );
	}

	return true;
}
