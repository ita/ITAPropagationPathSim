#include <ITAPropagationPathSim/CombinedModel/ImageConstructor.h>

using namespace ITAPropagationPathSim::CombinedModel;

namespace ITAPropagationPathSim
{
	namespace CombinedModel
	{
		namespace ImageConstruction
		{
			bool RecursiveConstructImageSources( CPropagationFaceShared pPropagationFace );

			bool ConstructImages( const VistaVector3D& v3Pos, std::vector<CPropagationShapeShared>& vpPropagationTree )
			{
				for( auto& pStartShape: vpPropagationTree )
				{
					if( pStartShape->iShapeType == CPropagationShape::FACE )
					{
						auto pStartFace = std::static_pointer_cast<CPropagationFace>( pStartShape );

						// Initialize image point and set it
						ITAGeoUtils::MirrorPointOverPlane( v3Pos, *pStartFace->pPlane, pStartFace->v3ImageSource );

						// First image point is valid
						pStartFace->pHasValidImageSource = true;

						// Add further images
						for( auto& pShapeChild: pStartFace->vpChildren )
						{
							assert( pShapeChild );
							if( pShapeChild->iShapeType == CPropagationShape::FACE )
							{
								pStartFace = std::static_pointer_cast<CPropagationFace>( pShapeChild );
								if( RecursiveConstructImageSources( pStartFace ) )
									pStartFace->pHasValidImageSource = true;
								else
									pShapeChild = nullptr;
							}
						}
					}
				}

				return true;
			}

			// Recursive construct of image sources (only visible locally)
			bool RecursiveConstructImageSources( CPropagationFaceShared pPropagationFace )
			{
				auto v3ParentImageSource = std::static_pointer_cast<CPropagationFace>( pPropagationFace->pParent.lock( ) )->v3ImageSource;

				// If the previous image source lies beyond the plane, a valid path can not be built
				if( !ITAGeoUtils::IsPointInFrontOfPlane( *pPropagationFace->pPlane, v3ParentImageSource ) )
					return false;

				// Initialize image source and set it
				ITAGeoUtils::MirrorPointOverPlane( v3ParentImageSource, *pPropagationFace->pPlane, pPropagationFace->v3ImageSource );

				// Add further images
				for( auto& pShapeChild: pPropagationFace->vpChildren )
				{
					if( pShapeChild->iShapeType == CPropagationShape::FACE )
					{
						auto pPropagationFaceChild = std::static_pointer_cast<CPropagationFace>( pShapeChild );

						if( RecursiveConstructImageSources( pPropagationFaceChild ) )
							pPropagationFaceChild->pHasValidImageSource = true;
						else
							pShapeChild = nullptr;
					}
				}

				return true;
			}

			bool ConstructImages( const VistaVector3D& v3SourceIn, std::vector<CPropagationFaceShared>& pFacesOut )
			{
				VistaVector3D v3ImageSource = v3SourceIn;
				for( auto& pFace: pFacesOut )
				{
					if( ITAGeoUtils::IsPointInFrontOfPlane( *pFace->pPlane, v3ImageSource ) )
					{
						ITAGeoUtils::MirrorPointOverPlane( v3ImageSource, *pFace->pPlane, v3ImageSource );
						pFace->v3ImageSource        = v3ImageSource;
						pFace->pHasValidImageSource = true;
					}
					else // Image source is not illuminable and thus, the whole path is invalid
					{
						pFace->pHasValidImageSource = false;
						return false;
					}
				}

				// No invalid image source found
				return true;
			}

			bool ConstructImageApertures( const std::vector<CPropagationShapeShared> vpPropagationListsIn, std::vector<CPropagationShapeShared>& vpPropagationListsOut )
			{
				vpPropagationListsOut.clear( );

				for( auto& pStartShape: vpPropagationListsIn )
				{
					bool bValidImageApertures      = true;
					CPropagationShapeShared pShape = pStartShape; // Start point
					while( pShape != nullptr )
					{
						while( pShape != nullptr && pShape->iShapeType != CPropagationShape::EDGE )
						{
							pShape = pShape->pChild;
						}

						// No edge found, go to next start shape
						if( pShape == nullptr )
							continue;

						// Set secondary source to the aperture point of the edge
						VistaVector3D v3SecondarySource = *pShape->v3InteractionPoint;

						// Set pShape to the child of the edge
						pShape = pShape->pChild;

						// Vector of faces between two edges
						std::vector<CPropagationFaceShared> vpFaces;
						while( pShape != nullptr && pShape->iShapeType == CPropagationShape::FACE )
						{
							vpFaces.push_back( std::static_pointer_cast<CPropagationFace>( pShape ) );
							pShape = pShape->pChild;
						}

						// Construct image sources between two edges
						if( !ConstructImages( v3SecondarySource, vpFaces ) )
						{
							// If no valid image source could be created, ignore the propagation candidate and set its pointer to zero
							bValidImageApertures = false;
							break; // break the while loop
						}
					}

					if( bValidImageApertures )
						vpPropagationListsOut.push_back( pStartShape );
				}

				return true;
			}

			bool RecursiveConstructImageEdge( std::shared_ptr<const CPropagationFace> pParentFace, std::shared_ptr<CPropagationEdge>& pPropagationEdge )
			{
				// Only one image must be in front of plane
				bool bValidImageEdge = ITAGeoUtils::IsPointInFrontOfPlane( *pParentFace->pPlane, *pPropagationEdge->v3ImageEdgeReceiverStart );
				bValidImageEdge |= ITAGeoUtils::IsPointInFrontOfPlane( *pParentFace->pPlane, *pPropagationEdge->v3ImageEdgeReceiverStart );

				// If both images are not valid the whole image edge is invalid
				if( !bValidImageEdge )
					return false;


				ITAGeoUtils::MirrorPointOverPlane( *( pPropagationEdge->v3ImageEdgeReceiverStart.get( ) ), *( pParentFace->pPlane.get( ) ),
				                                   *( pPropagationEdge->v3ImageEdgeReceiverStart.get( ) ) );
				ITAGeoUtils::MirrorPointOverPlane( *( pPropagationEdge->v3ImageEdgeReceiverEnd.get( ) ), *( pParentFace->pPlane.get( ) ),
				                                   *( pPropagationEdge->v3ImageEdgeReceiverEnd.get( ) ) );

				// Recursive construction until no further faces are found
				if( !pParentFace->pParent.expired( ) && pParentFace->pParent.lock( )->iShapeType == CPropagationShape::FACE )
				{
					auto pGrandParentFace = std::static_pointer_cast<CPropagationFace>( pPropagationEdge->pParent.lock( ) );
					bValidImageEdge       = RecursiveConstructImageEdge( pGrandParentFace, pPropagationEdge );
				}

				return bValidImageEdge;
			}

			bool RecursiveConstructImageEdges( CPropagationShapeShared& pPropagationShape )
			{
				// NMK: I added hasValidImage... to true or false in some places. Done to convert pointer to bools. I think the old implementation
				// was buggy/bad practices. Now works with bools but maybe (?) consumes more memory, because later in PropEngin.RecursiveAddShapesToPropagationLists
				// paths are dealt with that should be disregarded early on? But w/e
				if( pPropagationShape == nullptr )
					return false;

				// Create image edge receiver in case of an edge and image edge source of an face followed by at least one edge
				// Due to the structure of the propagation tree, image edge sources of each mirrored edge is saved in the corresponding following
				// propagation faces and the image edge receiver are saved in the corresponding edge followed up by a number of faces
				if( pPropagationShape->iShapeType == CPropagationShape::EDGE )
				{
					auto pPropagationEdge = std::static_pointer_cast<CPropagationEdge>( pPropagationShape );

					// Create image vertices only for edges with faces as parents
					if( !pPropagationEdge->pParent.expired( ) && pPropagationEdge->pParent.lock( )->iShapeType == CPropagationShape::FACE )
					{
						pPropagationEdge->v3ImageEdgeReceiverStart = std::make_shared<VistaVector3D>( *pPropagationEdge->v3StartVertex );
						pPropagationEdge->v3ImageEdgeReceiverEnd   = std::make_shared<VistaVector3D>( *pPropagationEdge->v3EndVertex );
						pPropagationEdge->pHasValidImageEdge       = true;

						auto pParentFace = std::static_pointer_cast<CPropagationFace>( pPropagationEdge->pParent.lock( ) );
						while( true )
						{
							// Only one image must be in front of plane
							bool bValidImageEdge = ITAGeoUtils::IsPointInFrontOfPlane( *pParentFace->pPlane, *pPropagationEdge->v3ImageEdgeReceiverStart );
							bValidImageEdge |= ITAGeoUtils::IsPointInFrontOfPlane( *pParentFace->pPlane, *pPropagationEdge->v3ImageEdgeReceiverEnd );

							// If both images are not valid the whole image edge is invalid
							if( !bValidImageEdge )
							{
								pPropagationEdge->pHasValidImageEdge = false;

								break;
							}

							ITAGeoUtils::MirrorPointOverPlane( *pPropagationEdge->v3StartVertex, *pParentFace->pPlane, *pPropagationEdge->v3ImageEdgeReceiverStart );
							ITAGeoUtils::MirrorPointOverPlane( *pPropagationEdge->v3EndVertex, *pParentFace->pPlane, *pPropagationEdge->v3ImageEdgeReceiverEnd );

							if( !pParentFace->pParent.expired( ) && pParentFace->pParent.lock( )->iShapeType == CPropagationShape::FACE )
							{
								pParentFace = std::static_pointer_cast<CPropagationFace>( pParentFace->pParent.lock( ) );
							}
							else
							{
								break;
							}
						}
					}
				}
				else if( pPropagationShape->iShapeType == CPropagationShape::FACE )
				{
					auto pPropagationFace = std::static_pointer_cast<CPropagationFace>( pPropagationShape );

					// Create image vertices only for edges with faces as parents
					if( !pPropagationFace->pParent.expired( ) )
					{
						pPropagationFace->pHasValidImageSource = true; // NMK: this correct?

						if( pPropagationFace->pParent.lock( )->iShapeType == CPropagationShape::FACE )
						{
							auto pParentFace = std::static_pointer_cast<CPropagationFace>( pPropagationFace->pParent.lock( ) );

							if( pParentFace->v3ImageEdgeSourceStart != VISTA_VECTOR3D_FLOAT_MAXIMUM )
							{
								// Only one image must be in front of plane
								bool bValidImageEdge = ITAGeoUtils::IsPointInFrontOfPlane( *pPropagationFace->pPlane, pParentFace->v3ImageEdgeSourceStart );
								bValidImageEdge |= ITAGeoUtils::IsPointInFrontOfPlane( *pPropagationFace->pPlane, pParentFace->v3ImageEdgeSourceEnd );

								// Set image edge source if valid
								if( bValidImageEdge )
								{
									ITAGeoUtils::MirrorPointOverPlane( pParentFace->v3ImageEdgeSourceStart, *pPropagationFace->pPlane,
									                                   pPropagationFace->v3ImageEdgeSourceStart );
									ITAGeoUtils::MirrorPointOverPlane( pParentFace->v3ImageEdgeSourceEnd, *pPropagationFace->pPlane,
									                                   pPropagationFace->v3ImageEdgeSourceEnd );
								}
								else
								{
									pPropagationFace->pHasValidImageSource = false; // NMK: this correct?
								}
							}
						}
						else if( pPropagationFace->pParent.lock( )->iShapeType == CPropagationShape::EDGE )
						{
							auto pParentEdge                = std::static_pointer_cast<CPropagationEdge>( pPropagationFace->pParent.lock( ) );
							pParentEdge->pHasValidImageEdge = true; // NMK: this correct?
							// Calculate image edge source.
							// A PointInFrontOfPlane test is not needed, because a backfaceculling with vertices and plane is already done.

							ITAGeoUtils::MirrorPointOverPlane( *pParentEdge->v3StartVertex, *pPropagationFace->pPlane, pPropagationFace->v3ImageEdgeSourceStart );
							ITAGeoUtils::MirrorPointOverPlane( *pParentEdge->v3EndVertex, *pPropagationFace->pPlane, pPropagationFace->v3ImageEdgeSourceEnd );
						}
					}
				}
				// Go one level deeper
				for( auto& pShapeChild: pPropagationShape->vpChildren )
					RecursiveConstructImageEdges( pShapeChild );


				return true;
			}

			bool ConstructImageEdges( std::vector<CPropagationShapeShared>& vpPropagationTree )
			{
				// First element of tree has never an image edge, so only construct them for its children
				for( auto& pShape: vpPropagationTree )
					if( pShape != nullptr )
						for( auto& pShapeChild: pShape->vpChildren )
							if( pShapeChild != nullptr )
								RecursiveConstructImageEdges( pShapeChild );

				return true;
			}
		} // namespace ImageConstruction
	} // namespace CombinedModel
} // namespace ITAPropagationPathSim
