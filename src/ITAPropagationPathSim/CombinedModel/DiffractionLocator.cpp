﻿#include <ITAPropagationPathSim/CombinedModel/DiffractionLocator.h>

using namespace std;
using namespace ITAGeo;

bool RecursiveAngleCulling( const ITAPropagationPathSim::CombinedModel::CPropagationShapeShared pShapeIn,
                            ITAPropagationPathSim::CombinedModel::CPropagationShapeShared& pShapeOut, float fAccumulatedAngle, const float fAngleThreshold,
                            VistaVector3D& v3Source, ITAPropagationPathSim::CombinedModel::CPropagationEdgeShared pDiffractionEdge = nullptr )
{
	if( pShapeIn->iShapeType == ITAPropagationPathSim::CombinedModel::CPropagationShape::EDGE )
	{
		auto pEdge = static_pointer_cast<ITAPropagationPathSim::CombinedModel::CPropagationEdge>( pShapeIn );

		VistaVector3D v3Diffraction, v3Target;


		if( pDiffractionEdge == nullptr )
			pDiffractionEdge = pEdge;
		else
		{
			if( !pDiffractionEdge->pParent.expired( ) )
			{
				switch( pEdge->pParent.lock( )->iShapeType )
				{
					case ITAPropagationPathSim::CombinedModel::CPropagationShape::FACE:
					{
						auto pParentFace = static_pointer_cast<ITAPropagationPathSim::CombinedModel::CPropagationFace>( pEdge->pParent.lock( ) );

						if( pParentFace->v3ImageEdgeSourceStart != VISTA_VECTOR3D_FLOAT_MAXIMUM )
						{
							// Mid point of the image of the previous edge
							v3Source = 0.5 * ( pParentFace->v3ImageEdgeSourceStart + pParentFace->v3ImageEdgeSourceEnd );
						}
						else // use image source
						{
							v3Source = pParentFace->v3ImageSource;
						}
						break;
					}
					case ITAPropagationPathSim::CombinedModel::CPropagationShape::EDGE:
					{
						auto pParentEdge = static_pointer_cast<ITAPropagationPathSim::CombinedModel::CPropagationEdge>( pEdge->pParent.lock( ) );

						// Mid point of the previous edge
						v3Source = 0.5 * ( *pParentEdge->v3StartVertex + *pParentEdge->v3EndVertex );

						break;
					}
				}
			}

			// Set expected point of diffraction to the mid of the middle edge, where the path is diffracted
			v3Diffraction = 0.5 * ( *pDiffractionEdge->v3StartVertex + *pDiffractionEdge->v3EndVertex );

			// Set expected receiving point to the mid of the last edge
			if( pEdge->v3ImageEdgeReceiverStart == nullptr )
				v3Target = 0.5 * ( *pEdge->v3StartVertex + *pEdge->v3EndVertex );
			else
				v3Target = 0.5 * ( *pEdge->v3ImageEdgeReceiverStart + *pEdge->v3ImageEdgeReceiverEnd );

			// Calculated how much the propagation path is diffracted
			float fAngle;
			ITAGeoUtils::CalculateDiffractionAngle( v3Source, v3Diffraction, v3Target, fAngle );

			// Add angle to the accumulated angle
			fAccumulatedAngle += fAngle;

			// Compare accumulated angle with threshold
			if( fAccumulatedAngle >= fAngleThreshold )
				return false;
		}
	}

	vector<ITAPropagationPathSim::CombinedModel::CPropagationShapeShared> vpChildren;
	for( auto pChild: pShapeIn->vpChildren )
	{
		if( RecursiveAngleCulling( pChild, pChild, fAccumulatedAngle, fAngleThreshold, v3Source, pDiffractionEdge ) )
			vpChildren.push_back( pChild );
	}

	pShapeOut->vpChildren = vpChildren;

	return true;
}

bool ITAPropagationPathSim::CombinedModel::Diffraction::ConstructAperturePoints( shared_ptr<const CPropagationAnchor> pEmitter,
                                                                                 shared_ptr<const CPropagationAnchor> pSensor, const int iNumberIterations,
                                                                                 const vector<CPropagationShapeShared> vpPropagationListsIn,
                                                                                 vector<CPropagationShapeShared>& pPropagationListsOut,
                                                                                 ITABase::IProgressHandler* pProgressHandler /* = nullptr */ )
{
	pPropagationListsOut.clear( );

	size_t N = vpPropagationListsIn.size( );
	for( size_t n = 0; n < N; n++ )
	{
		auto& pPropagationStartPoint( vpPropagationListsIn[n] );

		// if( pProgressHandler )
		//	pProgressHandler->PushProgressUpdate( n / float( N ) * 100.0f );

		if( pPropagationStartPoint == nullptr )
			continue;

		// Boolean for valid apex points
		bool bValidAperturePoints = true;

		// The diffraction points are calculated by solving a inhomogeneous coordinate system.

		// E_in and E_out are direction vectors of the edges.
		// Because of possibly occuring reflections during the paths, E_in can contain mirrored versions of the direction edge directions.
		// VertexDiff contains the vista vectors of the difference between the FromVertices of two edges
		vector<shared_ptr<VistaVector3D>> vpE_in, vpE_out;
		vector<unique_ptr<VistaVector3D>> vpStartVertexDiff;

		// RelativeApexPosition contains the aperture position relative to the edge from- and to-vertices.
		// vfE_square contains the squared length of the edge
		vector<float> vfRelativeApexPosition, vfE_square;

		// The propagation paths always starts at the position of the emitter
		shared_ptr<VistaVector3D> v3LastStartVertex = make_shared<VistaVector3D>( pEmitter->v3InteractionPoint );

		// The propagation paths always ends at the location of the sensor
		// Because of possible faces after the last edge, a local copy of the location is needed and will eventually mirrored in a later step
		VistaVector3D v3SensorPosition = pSensor->v3InteractionPoint;

		CPropagationShapeShared pShape = pPropagationStartPoint;
		CPropagationEdgeShared pEdge;
		while( pShape != nullptr )
		{
			// Diffraction can only occur on edges
			if( pShape->iShapeType != CPropagationShape::EDGE )
			{
				// If the shape is a face and is the last shape of the current list, break the while loop
				// The face is needed for the calculation of the image receiver
				if( pShape->pChild == nullptr )
					break;

				pShape = pShape->pChild;
			}
			else // pShape is an edge
			{
				// If pEdge is the first edge and the parent face contains no valid image source, the whole path is not able to be valid
				if( pEdge == nullptr )
				{
					if( !pShape->pParent.expired( ) && pShape->pParent.lock( )->iShapeType == CPropagationShape::FACE )
					{
						auto pFace = static_pointer_cast<CPropagationFace>( pShape->pParent.lock( ) );
						if( pFace->pHasValidImageSource == false )
							continue;
					}
				}

				pEdge = static_pointer_cast<CPropagationEdge>( pShape );

				// The start value for the aperture position is located on the mid of the edge
				vfRelativeApexPosition.push_back( 0.5 );

				// The edge direction for the outgoing part is the difference of the end points of the edge
				vpE_out.push_back( make_shared<VistaVector3D>( *pEdge->v3EndVertex - *pEdge->v3StartVertex ) );

				// Get the squared length. The length of an edge is always the same whether it was mirrored or not
				vfE_square.push_back( vpE_out.back( )->GetLengthSquared( ) );

				// The first shape has no parent. If the parent is a face, the edge must be mirrored(already done in a previous step)
				if( !pEdge->pParent.expired( ) && pEdge->pParent.lock( )->iShapeType == CPropagationShape::FACE )
				{
					vpE_in.push_back( make_shared<VistaVector3D>( *pEdge->v3ImageEdgeReceiverEnd - *pEdge->v3ImageEdgeReceiverStart ) );

					vpStartVertexDiff.push_back( make_unique<VistaVector3D>( *v3LastStartVertex - *pEdge->v3ImageEdgeReceiverStart ) );
				}
				else
				{
					// The input edge direction does not have to be mirrored and is therefore the same direction as the output edge direction
					vpE_in.push_back( vpE_out.back( ) );

					vpStartVertexDiff.push_back( make_unique<VistaVector3D>( *v3LastStartVertex - *pEdge->v3StartVertex ) );
				}

				// Set the current from vertex as the next "last" one
				v3LastStartVertex = pEdge->v3StartVertex;

				// Set the next shape to the child of the current shape
				pShape = pShape->pChild;
			}
		}

		// Old implementation(image edge source used instead of image receiver):
		// If the last shape is not an edge, the image receiver is needed for the calculation of the aperture points
		// while (pShape != nullptr && pShape->iShapeType == CPropagationShape::FACE)
		//{
		//	auto pPlane = static_pointer_cast<CPropagationFace>(pShape)->pPlane;

		//	// Check for a possible visibility
		//	if (ITAGeoUtils::IsPointInFrontOfPlane(*pPlane, v3SensorPosition))
		//	{
		//		//Mirror the (imaged) sensor until an edge is reached
		//		ITAGeoUtils::MirrorPointOverPlane(v3SensorPosition, *static_pointer_cast<CPropagationFace>(pShape)->pPlane, v3SensorPosition);

		//		pShape = pShape->pParent.lock();
		//	}
		//	else //If the (imaged) sensor is not visible, the whole path is invalid
		//	{
		//		bValidAperturePoints = false;
		//		break;
		//	}
		//}
		//
		////In the last step, the path candidate could be declared as invalid.
		////If it is invalid, go to the next path candidate
		// if (bValidAperturePoints == false)
		//	continue;


		// Add path candidate to list and go to the next path candidate, if no edge was found
		if( pEdge == nullptr )
		{
			pPropagationListsOut.push_back( pPropagationStartPoint );
			continue;
		}

		// If the last shape is not an edge, the image edge source of the last edge is needed for the calculation of the aperture points
		if( pShape != nullptr && pShape->iShapeType == CPropagationShape::FACE )
		{
			auto pFace = static_pointer_cast<CPropagationFace>( pShape );

			// If the image edge source is empty, there is no valid image edge and thus, the path is invalid
			if( pFace->v3ImageEdgeSourceStart == VISTA_VECTOR3D_FLOAT_MAXIMUM )
				continue;

			v3LastStartVertex = std::make_shared<VistaVector3D>( pFace->v3ImageEdgeSourceStart ); // NMK: have to make shared cuz algo on sharedptr
			vpE_out.back( )   = make_shared<VistaVector3D>( pFace->v3ImageEdgeSourceEnd - pFace->v3ImageEdgeSourceStart );
		}

		// Add the receiver to the differences vector
		vpStartVertexDiff.push_back( make_unique<VistaVector3D>( *v3LastStartVertex - v3SensorPosition ) );

		for( size_t iCurrentIteration = 0; iCurrentIteration < iNumberIterations; iCurrentIteration++ )
		{
			// Length of direction vector between two adjacent points in the shortest path.
			vector<float> vfDirectionVectorLength;

			// Get the length of the shortest paths between two adjecent aperture points
			// The length is determined by the relative aperture position of the last iteration

			// The first direction vector(between the source and the first (imaged) edge) does not contain a correction vector for the source
			vfDirectionVectorLength.push_back( ( *vpStartVertexDiff[0] - vfRelativeApexPosition[0] * *vpE_in[0] ).GetLength( ) );

			for( int i = 1; i < vfRelativeApexPosition.size( ); i++ )
				vfDirectionVectorLength.push_back(
				    ( *vpStartVertexDiff[i] - ( vfRelativeApexPosition[i] * *vpE_in[i] ) + ( vfRelativeApexPosition[i - 1] * *vpE_out[i - 1] ) ).GetLength( ) );

			// The last direction vector(between the last edge and the (imaged) receiver) does not contain a correction vector for the receiver
			vfDirectionVectorLength.push_back( ( *vpStartVertexDiff.back( ) + vfRelativeApexPosition.back( ) * *vpE_out.back( ) ).GetLength( ) );

			// The terms can be written in tridiagonal matrix form as following:
			//  b[i-1] *  vfRelativeApexPosition[i-1] + a[i] *  vfRelativeApexPosition[i] + b[i] *  vfRelativeApexPosition[i+1] = c[i]
			// with b[-1] *  vfRelativeApexPosition[-1] = 0 and b[n-1] *  vfRelativeApexPosition[n] =0 and with n, the number of diffractions
			// ┌                                                               ┐ ┌                            ┐ ┌       ┐
			// | a[0]   b[0]   0      0      0  ... 0      0      0      0     | | vfRelativeApexPosition[0]  | | c[0]  |
			// | b[0]   a[1]   b[1]   0      0  ... 0      0      0      0     | | vfRelativeApexPosition[1]  | | c[1]  |
			// | 0      b[1]   a[2]   b[2]   0  ... 0      0      0      0     | | vfRelativeApexPosition[2]  | | c[2]  |
			// | ...    ...    ...    ...    ...    ...    ...    ...    ...   |*| ...                        |=| ...   |
			// | 0      0      0      0      0  ... 0      b[n-3] a[n-2] b[n-2]| | vfRelativeApexPosition[n-2]| | c[n-2]|
			// | 0      0      0      0      0  ... 0      0      b[n-2] a[n-1]| | vfRelativeApexPosition[n-1]| | c[n-1]|
			// └                                                               ┘ └                            ┘ └       ┘
			vector<float> a, b, c;
			for( int i = 0; i < vfE_square.size( ); i++ )
			{
				a.push_back( vfE_square[i] * ( 1 / vfDirectionVectorLength[i] + 1 / vfDirectionVectorLength[i + 1] ) );
				c.push_back( vpStartVertexDiff[i]->Dot( *vpE_in[i] ) / vfDirectionVectorLength[i] -
				             vpStartVertexDiff[i + 1]->Dot( *vpE_out[i] ) / vfDirectionVectorLength[i + 1] );
			}

			for( int i = 0; i < vfE_square.size( ) - 1; i++ )
				b.push_back( -vpE_out[i]->Dot( *vpE_in[i + 1] ) / vfDirectionVectorLength[i + 1] );

			// To get vfRelativeApexPosition, the Gaussian-elimination is used to delete
			// the term  b[i-1] * vfRelativeApexPosition[i-1] in each row, so that
			// ┌                                                 ┐ ┌                            ┐ ┌       ┐
			// | 1      e[0]   0      0  ... 0      0      0     | | vfRelativeApexPosition[0]  | | d[0]  |
			// | 0      1      e[1]   0  ... 0      0      0     | | vfRelativeApexPosition[1]  | | d[1]  |
			// | ...    ...    ...    ...    ...    ...    ...   |*| ...                        |=| ...   |
			// | 0      0      0      0  ... 0      1      e[n-2]| | vfRelativeApexPosition[n-2]| | d[n-2]|
			// | 0      0      0      0  ... 0      0      1     | | vfRelativeApexPosition[n-1]| | d[n-1]|
			// └                                                 ┘ └                            ┘ └       ┘
			vector<float> d, e;

			// The first row remains the same
			d.push_back( c.front( ) / a.front( ) );

			if( a.size( ) > 1 )
				e.push_back( b.front( ) / a.front( ) );

			for( int i = 1; i < a.size( ) - 1; i++ )
				e.push_back( b[i] / ( a[i] - b[i - 1] * e[i - 1] ) );

			for( int i = 1; i < a.size( ); i++ )
				d.push_back( ( c[i] - b[i - 1] * d[i - 1] ) / ( a[i] - b[i - 1] * e[i - 1] ) );

			// Solve the equation system beginning at the last row
			vfRelativeApexPosition.back( ) = d.back( );
			for( int i = vfRelativeApexPosition.size( ) - 2; i >= 0; i-- )
				vfRelativeApexPosition[i] = ( d[i] - e[i] * vfRelativeApexPosition[i + 1] );
		}

		// After the last iteration check vfRelativeApexPosition for valid values [0;1]
		for( auto& fRelApexPos: vfRelativeApexPosition )
		{
			// If one apex point does not lie on the edge, the whole path is invalid
			if( !( fRelApexPos < 1 && fRelApexPos > 0 ) )
			{
				bValidAperturePoints = false;

				break;
			}
		}

		// Set the aperture points of the propagation candidate starting with the last edge
		while( bValidAperturePoints != false && pEdge != nullptr )
		{
			// Set the interaction point to the aperture point location
			// v3InteractionPoint = from_vertex + vfRelativeApexPosition * (to_vertex - from_vertex)
			VistaVector3D v3InteractionPoint =
			    ( 1 - vfRelativeApexPosition.back( ) ) * ( *pEdge->v3StartVertex ) + vfRelativeApexPosition.back( ) * ( *pEdge->v3EndVertex );

			// Interaction point must lie in valid range of parent and child of edge
			if( !pEdge->pParent.expired( ) )
			{
				if( pEdge->pParent.lock( )->iShapeType == CPropagationShape::FACE )
				{
					auto pParentFace = static_pointer_cast<CPropagationFace>( pEdge->pParent.lock( ) );

					bValidAperturePoints = ITAGeoUtils::IsPointInFrontOfPlane( *pParentFace->pPlane, v3InteractionPoint );
				}
				else
				{
					auto pParentEdge = static_pointer_cast<CPropagationEdge>( pEdge->pParent.lock( ) );

					// Illumination test at from vertex position
					VistaPlane oMainPlane, oOppositePlane;
					oMainPlane.SetOrigin( *pParentEdge->v3StartVertex );
					oOppositePlane.SetOrigin( *pParentEdge->v3StartVertex );
					oMainPlane.SetNormVector( *pParentEdge->v3MainFaceNormal );
					oOppositePlane.SetNormVector( *pParentEdge->v3OppositeFaceNormal );

					bool bMainFaceIlluminated     = ITAGeoUtils::IsPointInFrontOfPlane( oMainPlane, v3InteractionPoint );
					bool bOppositeFaceIlluminated = ITAGeoUtils::IsPointInFrontOfPlane( oOppositePlane, v3InteractionPoint );

					// Check whether wedge angle is >pi or <pi; TODO: export in own function
					bool bIsOuterAngle =
					    pParentEdge->v3MainFaceNormal->Cross( *pParentEdge->v3OppositeFaceNormal ).Dot( *pParentEdge->v3EndVertex - *pParentEdge->v3StartVertex ) > 0;

					if( bIsOuterAngle )
						bValidAperturePoints = bMainFaceIlluminated || bOppositeFaceIlluminated;
					else
						bValidAperturePoints = bMainFaceIlluminated && bOppositeFaceIlluminated;
				}
			}
			if( pEdge->pChild != nullptr )
			{
				if( pEdge->pChild->iShapeType == CPropagationShape::FACE )
				{
					auto pChildFace = static_pointer_cast<CPropagationFace>( pEdge->pChild );

					bValidAperturePoints = ITAGeoUtils::IsPointInFrontOfPlane( *pChildFace->pPlane, v3InteractionPoint );
				}
				else
				{
					auto pChildEdge = static_pointer_cast<CPropagationEdge>( pEdge->pChild );

					// Illumination test at from vertex position
					VistaPlane oMainPlane, oOppositePlane;
					oMainPlane.SetOrigin( *pChildEdge->v3StartVertex );
					oOppositePlane.SetOrigin( *pChildEdge->v3StartVertex );
					oMainPlane.SetNormVector( *pChildEdge->v3MainFaceNormal );
					oOppositePlane.SetNormVector( *pChildEdge->v3OppositeFaceNormal );
					bool bMainFaceIlluminated     = ITAGeoUtils::IsPointInFrontOfPlane( oMainPlane, v3InteractionPoint );
					bool bOppositeFaceIlluminated = ITAGeoUtils::IsPointInFrontOfPlane( oOppositePlane, v3InteractionPoint );

					// Check whether wedge angle is >pi or <pi; TODO: export in own function
					bool bIsOuterAngle =
					    pChildEdge->v3MainFaceNormal->Cross( *pChildEdge->v3OppositeFaceNormal ).Dot( *pChildEdge->v3EndVertex - *pChildEdge->v3StartVertex ) > 0;

					if( bIsOuterAngle )
						bValidAperturePoints = bMainFaceIlluminated || bOppositeFaceIlluminated;
					else
						bValidAperturePoints = bMainFaceIlluminated && bOppositeFaceIlluminated;
				}
			}

			// Set the interaction point
			pEdge->v3InteractionPoint = make_unique<VistaVector3D>( v3InteractionPoint );

			// Pop the last element
			vfRelativeApexPosition.pop_back( );

			// Get the previous shape
			pShape = pEdge->pParent.lock( );

			// Loop until the next edge is found or no further shape are before the current one
			while( pShape != nullptr && pShape->iShapeType != CPropagationShape::EDGE )
			{
				pShape = pShape->pParent.lock( );
			}

			// Break if no further edge are found
			if( pShape == nullptr )
				break;

			// If an edge is found set pEdge
			pEdge = static_pointer_cast<CPropagationEdge>( pShape );
		}

		if( bValidAperturePoints )
			pPropagationListsOut.push_back( pPropagationStartPoint );
	}

	return true;
}

bool ITAPropagationPathSim::CombinedModel::Diffraction::AccumulatedAngleCulling( const float fAngleThreshold, shared_ptr<const CPropagationAnchor> pEmitter,
                                                                                 const vector<CPropagationShapeShared> pPropagationTreeIn,
                                                                                 vector<CPropagationShapeShared>& pPropagationTreeOut )
{
	pPropagationTreeOut.clear( );

	VistaVector3D v3Source = pEmitter->v3InteractionPoint;

	CPropagationEdgeShared pDiffractionEdge;


	for( auto pStartShape: pPropagationTreeIn )
	{
		float fAccumulatedAngle = 0.0f;

		// If the shape is an edge, set it to the first edge where the propagation path will be diffracted
		if( pStartShape->iShapeType == CPropagationShape::EDGE )
		{
			auto pEdge = static_pointer_cast<CPropagationEdge>( pStartShape );

			pDiffractionEdge = pEdge;
		}

		vector<CPropagationShapeShared> vpChildren;
		for( auto pChild: pStartShape->vpChildren )
		{
			if( pChild == nullptr )
				continue;
			else if( RecursiveAngleCulling( pChild, pChild, fAccumulatedAngle, fAngleThreshold, v3Source, pDiffractionEdge ) )
				vpChildren.push_back( pChild );
		}

		pStartShape->vpChildren = vpChildren;
		pPropagationTreeOut.push_back( pStartShape );
	}

	return true;
}

bool ITAPropagationPathSim::CombinedModel::Diffraction::AccumulatedAngleCulling( const float fAngleThreshold, shared_ptr<const CPropagationAnchor> pEmitter,
                                                                                 shared_ptr<const CPropagationAnchor> pSensor,
                                                                                 const vector<CPropagationShapeShared> pPropagationListsIn,
                                                                                 vector<CPropagationShapeShared>& pPropagationListsOut )
{
	pPropagationListsOut.clear( );

	VistaVector3D v3Source, v3AperturePoint, v3Target;


	for( auto pStartShape: pPropagationListsIn )
	{
		float fAccumulatedAngle = 0.0f;
		bool bValidPath         = true;

		CPropagationShapeShared pCurrentShape = pStartShape;
		CPropagationShapeShared pLastShape;

		while( pCurrentShape != nullptr )
		{
			if( pCurrentShape->iShapeType == CPropagationShape::EDGE )
			{
				// Get current source of diffracted sub path
				if( pLastShape == nullptr )
				{
					v3Source = pEmitter->v3InteractionPoint;
				}
				else
				{
					v3Source = *pLastShape->v3InteractionPoint;
				}

				// Get current aperture point
				v3AperturePoint = *pCurrentShape->v3InteractionPoint;

				// Get Target of current diffraction
				if( pCurrentShape->pChild == nullptr )
				{
					v3Target = pSensor->v3InteractionPoint;
				}
				else
				{
					v3Target = *pCurrentShape->pChild->v3InteractionPoint;
				}

				// Calculate angle
				float fAngle;
				ITAGeoUtils::CalculateDiffractionAngle( v3Source, v3AperturePoint, v3Target, fAngle );
				fAccumulatedAngle += fAngle;

				// Check if accumulated angle is bigger than threshold
				if( fAccumulatedAngle > fAngleThreshold )
				{
					bValidPath = false;
					break;
				}
			}

			pLastShape    = pCurrentShape;
			pCurrentShape = pCurrentShape->pChild;
		}

		// Path is valid if the accumulated angle is not exceeding the threshold
		if( bValidPath )
		{
			pPropagationListsOut.push_back( pStartShape );
		}
	}

	return true;
}
