#include <ITAPropagationPathSim/ConvexDiffraction/EngineImpl.h>

// ITA includes
#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Utils.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>

// Vista includes
#include <VistaBase/VistaQuaternion.h>
#include <VistaMath/VistaGeometries.h>

// STL includes
#include <assert.h>
#include <list>
#include <stdio.h>

using namespace ITAGeo;

#define ITAMESH_MACRO CITAMesh* pMesh = m_pGeoModel->GetMesh( );


//! Face property structure for diffraction algorithms
struct CITADiffractionFace
{
	int iFromSourceDiffractionOrder;
	int iFromReceiverDiffractionOrder;

	CITADiffractionFace( ) { iFromSourceDiffractionOrder = iFromReceiverDiffractionOrder = DIFFRACTION_ORDER_INF; };
};
OpenMesh::FPropHandleT<CITADiffractionFace> tDiffractionFaceProp;

struct CITADiffractionCrawlingParameter
{
	bool bOutOfRange;
	double dValidIncidenceAngleRad;

	inline CITADiffractionCrawlingParameter( )
	    : bOutOfRange( false )
	    , dValidIncidenceAngleRad( 0 ) {

	    };

	inline CITADiffractionCrawlingParameter( const CITADiffractionCrawlingParameter& rhs )
	{
		bOutOfRange             = rhs.bOutOfRange;
		dValidIncidenceAngleRad = rhs.dValidIncidenceAngleRad;
	};

	inline CITADiffractionCrawlingParameter( const CITADiffractionCrawlingParameter* rhs )
	{
		bOutOfRange             = rhs->bOutOfRange;
		dValidIncidenceAngleRad = rhs->dValidIncidenceAngleRad;
	};
};

//! Edge property structure for diffraction algorithms
struct CITADiffractionEdge
{
	int iMinimalDiffractionOrderSourceViewpoint;   //!< Minimal edge diffraction order from source viewpoint
	int iMinimalDiffractionOrderReceiverViewpoint; //!< Minimal edge diffraction order from receiver viewpoint
	int iCombinedMinimalDiffractionOrder;          //!< Minimal edge diffraction order, @sa EDiffractionOrder

	bool bCoplanar; //!< For special treatment if faces of this edge are coplanar
	std::vector<bool> vbDiffractionIteration;
	double dAnglePreviousHalfedgeRad; //!< Defines the valid start vertex angle that is required to build a valid path with previous halfedge
	std::map<CITAMesh::HalfedgeHandle, CITADiffractionCrawlingParameter> mCrawlingHalfedges;

	//! Default constructor initializes all members
	inline CITADiffractionEdge( )
	{
		bCoplanar                                 = false;
		iCombinedMinimalDiffractionOrder          = DIFFRACTION_ORDER_INF;
		iMinimalDiffractionOrderSourceViewpoint   = DIFFRACTION_ORDER_INF;
		iMinimalDiffractionOrderReceiverViewpoint = DIFFRACTION_ORDER_INF;
		dAnglePreviousHalfedgeRad                 = 0;
	};
};

OpenMesh::HPropHandleT<CITADiffractionEdge> tDiffractionEdgeProp;


// -- ENGINE --

CEngineImpl::CEngineImpl( Halfedge::CMeshModel* pModel, std::shared_ptr<CPropagationAnchor> pSource, std::shared_ptr<CPropagationAnchor> pTarget )
    : m_pGeoModel( pModel )
    , m_pSourceAnchor( pSource )
    , m_pTargetAnchor( pTarget )
{
	if( m_pGeoModel == nullptr )
		ITA_EXCEPT1( INVALID_PARAMETER, "Mesh model pointer is NULL" );

	CITAMesh* pMesh = m_pGeoModel->GetMesh( );

	pMesh->add_property( tDiffractionFaceProp );
	pMesh->add_property( tDiffractionEdgeProp );
}

CEngineImpl::~CEngineImpl( )
{
	delete m_pGeoModel;
}

bool CEngineImpl::CalculateDiffractionPaths( CPropagationPathList& lPaths )
{
	/*
	Diffraction calculation follows the principle of finding paths of ascending complexity in a symmetrical
	manner.
	Line-of-sight validation of source and receiver is calculated and, if present, pushed to
	the final propagation path list as first element (step #1).
	Then, diffraction contours are determined to lock on to a deterministic starting point (step #2),
	which follow in the prop path list, if present.
	From the marked diffraction edges, those with exclusive direct line-of-sight to source or listener are
	validated and appended, if they represent a valid second order diffraction path.
	For a non-convex mesh, also specular reflections are tested using a first order image source representation.
	This is done by checking all visible diffraction edges' aperture points with any given polygon.
	*/

	lPaths.clear( );

	if( !m_pGeoModel->IsWaterproofMesh( ) )
	{
		std::cerr << "Model is not waterproof, can not calculate diffraction paths." << std::endl;
		return false;
	}

	if( m_pGeoModel->IsValidTriangleMesh( ) == false )
	{
		std::cout << "Model is not a valid triangle representation, triangulate to convert." << std::endl;
		return false;
	}

	if( !oAbortionCriteria.Validate( ) )
	{
		std::cout << "Warning: your abortion criteria are invalid, no guarantees given for result." << std::endl;
	}

	/*
	Determine two-folded halfedge to opposite halfedge angular ranges
	that are valid for crawling diffraction paths
	*/
	PrepareDiffractionCrawlingParameters( );

	/*
	Check and mark coplanar faces at it's adjacent halfedges
	*/
	MarkCoplanarDiffractionEdges( );

	/*
	Mark illuminated areas (faces) from source and receiver viewpoint
	*/
	bool bOverlappingIlluminationAreas = MarkIlluminatedFaces( );
	if( bOverlappingIlluminationAreas == false )
		std::cout << "Illuminated areas from source and receiver viewpoint do not overlap. No refraction into illuminated area possible." << std::endl;
	else
		std::cout << "Found overlapping illumination areas, refraction into illuminated area will occur." << std::endl;

	/*
	Extended recursive diffraction contour search is performed to find areas (faces)
	that are reachable by the entities over given abortion criteria at maximum search order
	*/
	bool bEntireMeshCovered = RecursiveDiffractionGradientSearch( DIFFRACTION_ORDER_1 );
	if( bEntireMeshCovered == false )
	{
		std::cout << "Not all mesh faces could be covered by given abortion restriction." << std::endl;
	}

	// -- pre-calculations done --

	/*
	Now do a lookup on the mesh with the marked faces and determine all possible
	propagation paths by forward rotation chain approach.
	*/
	std::vector<CITAGeoPropagationPathTree> voPropagationCandidates;
	DerivePropagationPathTreeFromIlluminationArea( voPropagationCandidates );

	std::cout << "Found " << voPropagationCandidates.size( ) << " diffraction path candidate trees." << std::endl;

	FilterValidPropagationPaths( voPropagationCandidates, lPaths );

	std::cout << "Found " << lPaths.size( ) << " valid diffraction paths." << std::endl;

	return true;
}

void CEngineImpl::DerivePropagationPathTreeFromIlluminationArea( std::vector<CITAGeoPropagationPathTree>& vAllCandidateTrees )
{
	ITAMESH_MACRO;

	// Iterate over all faces of mesh and generate candidate trees from illuminated areas (source)
	CITAMesh::FaceIter f_it = pMesh->faces_begin( );
	while( f_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );

		// Derivation begins and illuminated faces (from source)
		int iFromSourceOrder = pMesh->property( tDiffractionFaceProp, hFace ).iFromSourceDiffractionOrder;
		if( iFromSourceOrder != DIFFRACTION_ORDER_0 )
			continue;

		// If not reachable by receiver (precalculated by diffraction order gradient), skip.
		int iFromReceiverOrder = pMesh->property( tDiffractionFaceProp, hFace ).iFromReceiverDiffractionOrder;
		if( iFromReceiverOrder == DIFFRACTION_ORDER_INF )
			continue;

		// If this face's combined diffraction order of both entities is too high, this path
		// exceeds the requested complexity.
		int iCombinedOrder = iFromSourceOrder + iFromReceiverOrder;
		if( iCombinedOrder > oAbortionCriteria.iNumMaxDiffractions )
			continue;

		/*
		 * We have found a face that is reachable within given abortion criterium and starts at requested source illumination area,
		 * but we only know that there must be at least one propagation path beginning at this face.
		 * The candidate's tree origin will start here, but the final path(s) are yet to be determined.
		 */

		CITAGeoPropagationPathTree oCandidateTree;
		oCandidateTree.iTrunkFaceCombinedContourOrder = iCombinedOrder;
		oCandidateTree.hTrunkFace                     = hFace;


		// Construct trees from halfedges on this face
		CITAMesh::FaceHalfedgeIter fh_it = pMesh->fh_begin( hFace );
		while( fh_it != pMesh->fh_end( hFace ) )
		{
			CITAMesh::HalfedgeHandle hNextHalfedge( *fh_it++ );

			// A diffraction path can not start at edge of coplanar faces
			if( pMesh->property( tDiffractionEdgeProp, hNextHalfedge ).bCoplanar )
				continue;

			std::vector<CITAMesh::HalfedgeHandle> vhTrunkSeed;
			vhTrunkSeed.push_back( hNextHalfedge );
			RecursiveFollowGradientBranchesToTarget( vhTrunkSeed, oCandidateTree.vvhCandidates );
		}
		vAllCandidateTrees.push_back( oCandidateTree );
	}
}

void CEngineImpl::RecursiveFollowGradientBranchesToTarget( const std::vector<CITAMesh::HalfedgeHandle>& vhTrunkToThisBranchChain,
                                                           std::vector<std::vector<CITAMesh::HalfedgeHandle> >& vvhBranchesList ) const
{
	ITAMESH_MACRO;

	CITAMesh::HalfedgeHandle hForkingHalfedge = vhTrunkToThisBranchChain.back( );
	CITAMesh::HalfedgeHandle hTargetHalfedge  = pMesh->opposite_halfedge_handle( hForkingHalfedge );
	CITAMesh::FaceHandle hTargetFace          = pMesh->face_handle( hTargetHalfedge );

	int iFromTargetToBranchFaceDiffractionOrder = pMesh->property( tDiffractionFaceProp, hTargetFace ).iFromReceiverDiffractionOrder;

	if( iFromTargetToBranchFaceDiffractionOrder == DIFFRACTION_ORDER_INF )
		return; // Not reachable, dismiss.

	int iCurrentDiffractionOrderDepth = int( vhTrunkToThisBranchChain.size( ) );
	if( iCurrentDiffractionOrderDepth > oAbortionCriteria.iNumMaxDiffractions )
		return; // Not reacheable, either. Dismiss.

	if( iFromTargetToBranchFaceDiffractionOrder == DIFFRACTION_ORDER_0 )
	{
		// From here to target possible. If not an edge between two coplanar faces,
		// add this candidate, but continue for more candidates!
		if( pMesh->property( tDiffractionEdgeProp, hForkingHalfedge ).bCoplanar == false )
			vvhBranchesList.push_back( vhTrunkToThisBranchChain );
	}

	CITAMesh::FaceHalfedgeIter fh_it = pMesh->fh_begin( hTargetFace );
	while( fh_it != pMesh->fh_end( hTargetFace ) )
	{
		CITAMesh::HalfedgeHandle hNextHalfedge( *fh_it++ );

		// No backscattering to the branch's source face
		if( hForkingHalfedge == pMesh->opposite_halfedge_handle( hNextHalfedge ) )
			continue;

		CITAMesh::FaceHandle hNextOppositeFace = pMesh->face_handle( pMesh->opposite_halfedge_handle( hNextHalfedge ) );

		int iNextFromReceiverOrder = pMesh->property( tDiffractionFaceProp, hNextOppositeFace ).iFromReceiverDiffractionOrder;

		if( iNextFromReceiverOrder <= iFromTargetToBranchFaceDiffractionOrder && iNextFromReceiverOrder != DIFFRACTION_ORDER_INF )
		{
			// Gradient direction towards lower or equal order ... use current face order as basis
			std::vector<CITAMesh::HalfedgeHandle> vhNewBranchChain( vhTrunkToThisBranchChain );
			vhNewBranchChain.push_back( hNextHalfedge );
			assert( iNextFromReceiverOrder >= DIFFRACTION_ORDER_0 );
			RecursiveFollowGradientBranchesToTarget( vhNewBranchChain, vvhBranchesList );
		}
	}
}

bool CEngineImpl::ValidateAbortionAngleRanges( std::vector<CITAMesh::HalfedgeHandle>& oCandidatePath ) const
{
	// Iterate over candidates and check revers angle range
	for( size_t n = 0; n < oCandidatePath.size( ); n++ )
	{
		if( !ReverseAngleRangeTest( oCandidatePath, m_pSourceAnchor->v3InteractionPoint ) )
			return false;
	}
	return true;
}

bool CEngineImpl::ReverseAngleRangeTest( const std::vector<CITAMesh::HalfedgeHandle>& vhPath, const VistaVector3D& /* vFromPos */ ) const
{
	// Reverse iteration
	for( size_t n = vhPath.size( ) - 1; n > 0; n-- )
	{
		CITAMesh::HalfedgeHandle hFrom( vhPath[n] );
		CITAMesh::HalfedgeHandle hTo( vhPath[n - 1] );
		double dAngle = GetValidIncidenceAngleRad( hFrom, hTo );
		if( dAngle < 0 )
			return false;
	}

	return true;
}

double CEngineImpl::GetValidIncidenceAngleRad( CITAMesh::HalfedgeHandle hSourceEdge, CITAMesh::HalfedgeHandle hTargetEdge ) const
{
	ITAMESH_MACRO;

	const VistaVector3D v3SourceEdgeStartVertex( CITAMesh::Point( pMesh->point( pMesh->from_vertex_handle( hSourceEdge ) ) ).data( ) );
	const VistaVector3D v3SourceEdgeEndVertex( CITAMesh::Point( pMesh->point( pMesh->to_vertex_handle( hSourceEdge ) ) ).data( ) );
	const VistaVector3D v3TargetEdgeStartVertex( CITAMesh::Point( pMesh->point( pMesh->from_vertex_handle( hTargetEdge ) ) ).data( ) );
	const VistaVector3D v3TargetEdgeEndVertex( CITAMesh::Point( pMesh->point( pMesh->to_vertex_handle( hTargetEdge ) ) ).data( ) );

	const VistaVector3D v3SourceEdgeDir( ( v3SourceEdgeEndVertex - v3SourceEdgeStartVertex ).GetNormalized( ) );
	const VistaVector3D v3TargetEdgeDir( ( v3TargetEdgeEndVertex - v3TargetEdgeStartVertex ).GetNormalized( ) );

	return acos( v3SourceEdgeDir.Dot( v3TargetEdgeDir ) );
}

void CEngineImpl::PrepareDiffractionCrawlingParameters( )
{
	ITAMESH_MACRO;

	// Iterate over faces to determine inter-incidence angle range for all diffraction wedges on a face.
	// This operations are not depending on source and receiver location
	CITAMesh::FaceIter f_it = pMesh->faces_sbegin( );
	while( f_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );
		assert( pMesh->is_valid_handle( hFace ) );

		// Face Halfedges
		CITAMesh::FaceHalfedgeCCWIter fh_ccw_it = pMesh->cfh_ccwbegin( hFace );
		while( fh_ccw_it != pMesh->cfh_ccwend( hFace ) )
		{
			CITAMesh::HalfedgeHandle hHalfedgeStart( *fh_ccw_it++ );
			std::map<CITAMesh::HalfedgeHandle, CITADiffractionCrawlingParameter>& mConnections(
			    pMesh->property( tDiffractionEdgeProp, hHalfedgeStart ).mCrawlingHalfedges );

			// Connect to other halfedges on face
			CITAMesh::FaceHalfedgeCCWIter fh_ccw_it2 = pMesh->cfh_ccwbegin( hFace );
			while( fh_ccw_it2 != pMesh->cfh_ccwend( hFace ) )
			{
				CITAMesh::HalfedgeHandle hHalfedgeTarget( *fh_ccw_it2++ );

				if( hHalfedgeStart == hHalfedgeTarget )
					continue;

				std::map<CITAMesh::HalfedgeHandle, CITADiffractionCrawlingParameter>::iterator it = mConnections.find( hHalfedgeTarget );
				assert( it == mConnections.end( ) );

				CITADiffractionCrawlingParameter oCrawlingParams;
				oCrawlingParams.dValidIncidenceAngleRad = GetValidIncidenceAngleRad( hHalfedgeStart, hHalfedgeTarget );
				oCrawlingParams.bOutOfRange             = oCrawlingParams.dValidIncidenceAngleRad < 0 ? true : false;

				mConnections.insert( std::pair<CITAMesh::HalfedgeHandle, CITADiffractionCrawlingParameter>( hHalfedgeTarget, oCrawlingParams ) );
			}
		}
	}
}

void CEngineImpl::MarkCoplanarDiffractionEdges( )
{
	ITAMESH_MACRO;

	CITAMesh::HalfedgeIter he_it = pMesh->halfedges_begin( );
	while( he_it != pMesh->halfedges_end( ) )
	{
		CITAMesh::HalfedgeHandle hCurrentHalfedge( *he_it++ );
		CITAMesh::FaceHandle hSourceFace = pMesh->face_handle( hCurrentHalfedge );
		CITAMesh::FaceHandle hTargetFace = pMesh->face_handle( pMesh->opposite_halfedge_handle( hCurrentHalfedge ) );

		VistaVector3D v3SourceFaceNormal( pMesh->calc_face_normal( hSourceFace ).data( ) );
		VistaVector3D v3TargeteFaceNormal( pMesh->calc_face_normal( hTargetFace ).data( ) );

		if( v3SourceFaceNormal.Dot( v3TargeteFaceNormal ) > 1 - Vista::Epsilon )
			pMesh->property( tDiffractionEdgeProp, hCurrentHalfedge ).bCoplanar = true;
	}
}

bool CEngineImpl::MarkIlluminatedFaces( )
{
	ITAMESH_MACRO;

	bool bFirstOrderDiffractionFound = false;

	// Iterate over faces to find illuminated faces
	CITAMesh::FaceIter f_it = pMesh->faces_sbegin( );
	while( f_it != pMesh->faces_end( ) )
	{
		// Main face
		CITAMesh::FaceHandle hFace( *f_it++ );
		assert( pMesh->is_valid_handle( hFace ) );
		VistaVector3D v3FaceNormal( pMesh->calc_face_normal( hFace ).data( ) );

		CITAMesh::ConstFaceVertexIter cfv_it = pMesh->cfv_begin( hFace );
		CITAMesh::VertexHandle hFirstVertex( *cfv_it );
		assert( pMesh->is_valid_handle( hFirstVertex ) );
		VistaVector3D v3FirstVertex( pMesh->point( hFirstVertex ).data( ) );
		VistaPlane oPlane; // Careful, use in this way only! (create, set origin, set normalized normal vector)
		oPlane.SetOrigin( v3FirstVertex );
		oPlane.SetNormVector( v3FaceNormal );

		// Source
		double dSourceDistance         = oPlane.CalcDistance( m_pSourceAnchor->v3InteractionPoint );
		bool bSourceFacePlaneVisiblity = ( dSourceDistance + Vista::Epsilon > 0.0f );
		if( bSourceFacePlaneVisiblity )
			pMesh->property( tDiffractionFaceProp, hFace ).iFromSourceDiffractionOrder = DIFFRACTION_ORDER_0;

		// Target
		double dReceiverDistance         = oPlane.CalcDistance( m_pTargetAnchor->v3InteractionPoint );
		bool bReceiverFacePlaneVisiblity = ( dReceiverDistance + Vista::Epsilon > 0.0f );
		if( bReceiverFacePlaneVisiblity )
			pMesh->property( tDiffractionFaceProp, hFace ).iFromReceiverDiffractionOrder = DIFFRACTION_ORDER_0;

		if( bFirstOrderDiffractionFound == false )
			if( bSourceFacePlaneVisiblity && bReceiverFacePlaneVisiblity )
				bFirstOrderDiffractionFound = true;
	}

	return bFirstOrderDiffractionFound;
}

bool CEngineImpl::RecursiveDiffractionGradientSearch( const int iCurrentRecursionIterationOrder )
{
	ITAMESH_MACRO;

	const int iPreviousIterationOrder = iCurrentRecursionIterationOrder - 1;
	assert( iPreviousIterationOrder != DIFFRACTION_ORDER_INF );

	bool bWholeMeshCovered = true;

	// Iterate over faces
	CITAMesh::FaceIter f_it = pMesh->faces_sbegin( );
	while( f_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );
		assert( pMesh->is_valid_handle( hFace ) );

		// Source
		const int iFromSourceDiffractionOrder = pMesh->property( tDiffractionFaceProp, hFace ).iFromSourceDiffractionOrder;
		bool bTraceFromSourceGradient         = ( iFromSourceDiffractionOrder == iPreviousIterationOrder );
		if( bTraceFromSourceGradient )
		{
			CITAMesh::FaceFaceCCWIter ff_ccwit = pMesh->ff_ccwbegin( hFace );
			while( ff_ccwit != pMesh->ff_ccwend( hFace ) )
			{
				// Look for adjacent faces that are connected to this face and have currently infinite diffraction order
				CITAMesh::FaceHandle hAdjacentFace( *ff_ccwit++ );
				if( pMesh->property( tDiffractionFaceProp, hAdjacentFace ).iFromSourceDiffractionOrder == DIFFRACTION_ORDER_INF )
				{
					pMesh->property( tDiffractionFaceProp, hAdjacentFace ).iFromSourceDiffractionOrder = iCurrentRecursionIterationOrder;
				}
			}
		}

		// Target
		int iFromReceiverDiffractionOrder = pMesh->property( tDiffractionFaceProp, hFace ).iFromReceiverDiffractionOrder;
		bool bTraceFromReceiverGradient   = ( iFromReceiverDiffractionOrder == iPreviousIterationOrder );
		if( bTraceFromReceiverGradient )
		{
			CITAMesh::FaceFaceCCWIter ff_ccwit = pMesh->ff_ccwbegin( hFace );
			while( ff_ccwit != pMesh->ff_ccwend( hFace ) )
			{
				CITAMesh::FaceHandle hAdjacentFace( *ff_ccwit++ );
				if( pMesh->property( tDiffractionFaceProp, hAdjacentFace ).iFromReceiverDiffractionOrder == DIFFRACTION_ORDER_INF )
				{
					pMesh->property( tDiffractionFaceProp, hAdjacentFace ).iFromReceiverDiffractionOrder = iCurrentRecursionIterationOrder;
				}
			}
		}

		if( pMesh->property( tDiffractionFaceProp, hFace ).iFromSourceDiffractionOrder == DIFFRACTION_ORDER_INF &&
		    pMesh->property( tDiffractionFaceProp, hFace ).iFromReceiverDiffractionOrder == DIFFRACTION_ORDER_INF )
			bWholeMeshCovered = false;
	}

	// Recursion intervention
	if( iCurrentRecursionIterationOrder >= oAbortionCriteria.iDiffractionPreparationOrder )
		return bWholeMeshCovered;

	return RecursiveDiffractionGradientSearch( iCurrentRecursionIterationOrder + 1 );
}

bool CEngineImpl::ConstructPropagationPathFromCandidate( const std::vector<CITAMesh::HalfedgeHandle>& vhCandidate, CPropagationPath& oPath ) const
{
	ITAMESH_MACRO;

	size_t N = vhCandidate.size( );

	// Special case: first order diffraction, no rojection required.
	// We know the target position in advance, so calculation becomes trivial. It's a wedge.
	if( N == 1 )
	{
		CITAMesh::HalfedgeHandle hCurrentHalfedge( vhCandidate[0] );

		CITAMesh::FaceHandle hSourceFace = pMesh->face_handle( hCurrentHalfedge );

		CITAMesh::FaceHandle hTargetFace = pMesh->opposite_face_handle( vhCandidate[0] );
		VistaVector3D v3SourceFaceNormal( pMesh->calc_face_normal( hSourceFace ).data( ) );
		VistaVector3D v3TargetFaceNormal( pMesh->calc_face_normal( hTargetFace ).data( ) );

		CITAMesh::VertexHandle hFromVertexStart = pMesh->from_vertex_handle( vhCandidate[0] );
		CITAMesh::VertexHandle hFromVertexEnd   = pMesh->to_vertex_handle( vhCandidate[0] );
		VistaVector3D v3FromVertexStart( CITAMesh::Point( pMesh->point( hFromVertexStart ) ).data( ) );
		VistaVector3D v3FromVertexEnd( CITAMesh::Point( pMesh->point( hFromVertexEnd ) ).data( ) );

		VistaVector3D v3AperturePoint;
		if( ITAGeoUtils::CalculateDiffractionAperturePoint( v3FromVertexStart, v3FromVertexEnd, m_pSourceAnchor->v3InteractionPoint, m_pTargetAnchor->v3InteractionPoint,
		                                                    v3AperturePoint ) )
		{
			// @todo jst: check if source interaction point is outside wedge?

			auto pWedge                       = std::make_shared<CITADiffractionOuterWedgeAperture>( );
			pWedge->v3AperturePoint           = v3AperturePoint;
			pWedge->v3MainWedgeFaceNormal     = v3SourceFaceNormal;
			pWedge->v3OppositeWedgeFaceNormal = v3TargetFaceNormal;
			pWedge->v3VertextEnd              = v3FromVertexEnd;
			pWedge->v3VertextStart            = v3FromVertexStart;
			oPath.push_back( m_pSourceAnchor );
			oPath.push_back( pWedge );
			oPath.push_back( m_pTargetAnchor );

			return true;
		}
		else
		{
			return false;
		}
	}

	assert( N > 1 );

	// Rotational projection and line-of-sight test in reduced dimension.
	// Rotates successively the source position into the next-hop plane and
	// performes a perspective angle range test on next edge range.

	// Forward rojection algorithm
	std::vector<VistaVector3D> vv3SuccessivelyRojectedPoints( N );

	// First anchor (source) is handled individually
	{
		CITAMesh::HalfedgeHandle hSource     = vhCandidate[0];
		CITAMesh::HalfedgeHandle hTarget     = pMesh->opposite_halfedge_handle( hSource );
		CITAMesh::FaceHandle hNextTargetFace = pMesh->face_handle( hTarget );
		VistaVector3D v3TargetFaceNormal( pMesh->calc_face_normal( hNextTargetFace ).data( ) );

		CITAMesh::VertexHandle hVertexStart = pMesh->from_vertex_handle( hSource );
		CITAMesh::VertexHandle hVertexEnd   = pMesh->to_vertex_handle( hSource );

		VistaVector3D v3VertexStart( CITAMesh::Point( pMesh->point( hVertexStart ) ).data( ) );
		VistaVector3D v3VertexEnd( CITAMesh::Point( pMesh->point( hVertexEnd ) ).data( ) );

		// @todo jst: what to do if this point can not be rojected?
		if( !ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3TargetFaceNormal, m_pSourceAnchor->v3InteractionPoint, vv3SuccessivelyRojectedPoints[0] ) )
			return false;
	}

	// Now roject successively ( N > 1 )
	for( size_t i = 1; i < N; i++ )
	{
		CITAMesh::HalfedgeHandle hSource = vhCandidate[i];
		CITAMesh::HalfedgeHandle hTarget = pMesh->opposite_halfedge_handle( hSource );

		CITAMesh::FaceHandle hSourceFace     = pMesh->face_handle( hSource );
		CITAMesh::FaceHandle hNextTargetFace = pMesh->face_handle( hTarget );

		VistaVector3D v3SourceFaceNormal( pMesh->calc_face_normal( hSourceFace ).data( ) );
		VistaVector3D v3TargetFaceNormal( pMesh->calc_face_normal( hNextTargetFace ).data( ) );

		CITAMesh::VertexHandle hVertexStart = pMesh->from_vertex_handle( hSource );
		CITAMesh::VertexHandle hVertexEnd   = pMesh->to_vertex_handle( hSource );

		VistaVector3D v3VertexStart( CITAMesh::Point( pMesh->point( hVertexStart ) ).data( ) );
		VistaVector3D v3VertexEnd( CITAMesh::Point( pMesh->point( hVertexEnd ) ).data( ) );

		// @todo jst: what to do if this point can not be rojected?
		if( !ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3TargetFaceNormal, vv3SuccessivelyRojectedPoints[i - 1], vv3SuccessivelyRojectedPoints[i] ) )
			return false;
	}


	// Backwards edge intersection aperture calc and line-of-sight test
	std::vector<VistaVector3D> vv3InverseAperturePoints( N );
	for( size_t i = 0; i < N; i++ )
	{
		CITAMesh::HalfedgeHandle hSource = vhCandidate[N - 1 - i];
		CITAMesh::HalfedgeHandle hTarget = pMesh->opposite_halfedge_handle( hSource );

		CITAMesh::FaceHandle hSourceFace = pMesh->face_handle( hSource );
		CITAMesh::FaceHandle hTargetFace = pMesh->face_handle( hTarget );

		VistaVector3D v3SourceFaceNormal( pMesh->calc_face_normal( hSourceFace ).data( ) );
		VistaVector3D v3TargetFaceNormal( pMesh->calc_face_normal( hTargetFace ).data( ) );

		CITAMesh::VertexHandle hVertexStart = pMesh->from_vertex_handle( hSource );
		CITAMesh::VertexHandle hVertexEnd   = pMesh->to_vertex_handle( hSource );

		VistaVector3D v3VertexStart( CITAMesh::Point( pMesh->point( hVertexStart ) ).data( ) );
		VistaVector3D v3VertexEnd( CITAMesh::Point( pMesh->point( hVertexEnd ) ).data( ) );

		const VistaVector3D v3SourcePos = vv3SuccessivelyRojectedPoints[N - 1 - i];
		VistaVector3D v3TargetPos;
		if( i == 0 )
		{
			v3TargetPos = m_pTargetAnchor->v3InteractionPoint;
		}
		else
		{
			v3TargetPos = vv3InverseAperturePoints[i - 1];
		}
		VistaVector3D& v3AperturePoint = vv3InverseAperturePoints[i];
		if( !ITAGeoUtils::CalculateDiffractionAperturePoint( v3VertexStart, v3VertexEnd, v3SourcePos, v3TargetPos, v3AperturePoint ) )
			return false; // Not in line-of-sights
	}

	// Begin with source anchor
	oPath.push_back( m_pSourceAnchor );

	// Add diffraction anchors (from inverse aperture list)
	for( size_t i = 0; i < N; i++ )
	{
		auto pDiffractionAnchor             = std::make_shared<ITAGeo::CITADiffractionOuterWedgeAperture>( );
		pDiffractionAnchor->v3AperturePoint = vv3InverseAperturePoints[N - i - 1];
		// @todo: define anchor params properly
		oPath.push_back( pDiffractionAnchor );
	}

	// End with target anchor
	oPath.push_back( m_pTargetAnchor );

	return true;
}

void CEngineImpl::FilterValidPropagationPaths( std::vector<CITAGeoPropagationPathTree>& voCandidateTrees, std::vector<CPropagationPath>& voValidPropagationPaths ) const
{
	for( size_t t = 0; t < voCandidateTrees.size( ); t++ )
	{
		CITAGeoPropagationPathTree& oCandidate( voCandidateTrees[t] );
		for( size_t n = 0; n < oCandidate.vvhCandidates.size( ); n++ )
		{
			std::vector<CITAMesh::HalfedgeHandle>& vhDiffractionEdgeList( oCandidate.vvhCandidates[n] );
			if( ValidateAbortionAngleRanges( vhDiffractionEdgeList ) )
			{
				CPropagationPath oValidPath;
				if( ConstructPropagationPathFromCandidate( vhDiffractionEdgeList, oValidPath ) )
				{
					voValidPropagationPaths.push_back( oValidPath );
				}
			}
		}
	}
}

void CEngineImpl::GetCombinedDiffractionGradientVisualization( ITAGeo::Halfedge::CMeshModel* pVisModel ) const
{
	pVisModel->CopyFrom( *m_pGeoModel );

	CITAMesh* pVisMesh = pVisModel->GetMesh( );
	pVisMesh->add_property( tDiffractionFaceProp );

	CITAMesh::FaceIter f_it = pVisMesh->faces_begin( );
	while( f_it != pVisMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );
		pVisMesh->property( tDiffractionFaceProp, hFace ) = m_pGeoModel->GetMesh( )->property( tDiffractionFaceProp, hFace );
		int iFromSourceOrder                              = pVisMesh->property( tDiffractionFaceProp, hFace ).iFromSourceDiffractionOrder;
		int iFromReceiverOrder                            = pVisMesh->property( tDiffractionFaceProp, hFace ).iFromReceiverDiffractionOrder;
		int iCombinedOrder                                = iFromSourceOrder + iFromReceiverOrder;

		auto pVisMaterial = std::make_shared<ITAGeo::Material::CVisualizationMaterial>( );
		if( iFromSourceOrder == DIFFRACTION_ORDER_INF || iFromReceiverOrder == DIFFRACTION_ORDER_INF )
		{
			pVisMaterial->SetBlack( ); // dark side
			pVisMaterial->dAlpha = .0f;
		}
		else if( iCombinedOrder < 0 || iCombinedOrder > oAbortionCriteria.iNumMaxDiffractions )
		{
			pVisMaterial->SetWhite( ); // reacheable, but not within abortion criterium
			pVisMaterial->dAlpha = .0f;
		}
		else
		{
			int iN = oAbortionCriteria.iNumMaxDiffractions + 1;
			assert( iFromSourceOrder < iN );
			assert( iFromReceiverOrder < iN );
			pVisMaterial->dRed  = ( iN - iFromSourceOrder ) / double( iN );
			pVisMaterial->dBlue = ( iN - iFromReceiverOrder ) / double( iN );
		}

		int iFaceID = hFace.idx( );
		pVisModel->SetFaceAcousticMaterial( iFaceID, pVisMaterial );
	}

	return;
}

void CEngineImpl::GetFromSourceDiffractionGradientVisualization( Halfedge::CMeshModel* pVisModel ) const
{
	pVisModel->CopyFrom( *m_pGeoModel );

	CITAMesh* pVisMesh = pVisModel->GetMesh( );
	pVisMesh->add_property( tDiffractionFaceProp );

	CITAMesh::FaceIter f_it = pVisMesh->faces_begin( );
	while( f_it != pVisMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );
		pVisMesh->property( tDiffractionFaceProp, hFace ) = m_pGeoModel->GetMesh( )->property( tDiffractionFaceProp, hFace );
		int iFromSourceOrder                              = pVisMesh->property( tDiffractionFaceProp, hFace ).iFromSourceDiffractionOrder;

		auto pVisMaterial = std::make_shared<ITAGeo::Material::CVisualizationMaterial>( );
		if( iFromSourceOrder == DIFFRACTION_ORDER_INF )
		{
			pVisMaterial->SetWhite( );
			pVisMaterial->dAlpha = 0.0f;
		}
		else
		{
			int iN = oAbortionCriteria.iDiffractionPreparationOrder + 1;
			assert( iN >= iFromSourceOrder );
			pVisMaterial->dRed = ( iN - iFromSourceOrder ) / double( iN );
		}

		int iFaceID = hFace.idx( );
		pVisModel->SetFaceAcousticMaterial( iFaceID, pVisMaterial );
	}

	return;
}

void CEngineImpl::GetFromTargetDiffractionGradientVisualization( Halfedge::CMeshModel* pVisModel ) const
{
	pVisModel->CopyFrom( *m_pGeoModel );

	CITAMesh* pVisMesh = pVisModel->GetMesh( );
	pVisMesh->add_property( tDiffractionFaceProp );

	CITAMesh::FaceIter f_it = pVisMesh->faces_sbegin( );
	while( f_it != pVisMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );
		pVisMesh->property( tDiffractionFaceProp, hFace ) = m_pGeoModel->GetMesh( )->property( tDiffractionFaceProp, hFace );
		int iFromReceiverOrder                            = pVisMesh->property( tDiffractionFaceProp, hFace ).iFromReceiverDiffractionOrder;

		auto pVisMaterial = std::make_shared<ITAGeo::Material::CVisualizationMaterial>( );
		if( iFromReceiverOrder == DIFFRACTION_ORDER_INF )
		{
			pVisMaterial->SetWhite( );
			pVisMaterial->dAlpha = 0.0f;
		}
		else
		{
			int iN = oAbortionCriteria.iDiffractionPreparationOrder + 1;
			assert( iN >= iFromReceiverOrder );
			pVisMaterial->dBlue = ( iN - iFromReceiverOrder ) / double( iN );
		}

		int iFaceID = hFace.idx( );
		pVisModel->SetFaceAcousticMaterial( iFaceID, pVisMaterial );
	}

	return;
}

void CEngineImpl::GetPropagationPathCandidatesVisualization( CPropagationPathList& lPropPathsCandidates )
{
	ITAMESH_MACRO;

	std::vector<CITAGeoPropagationPathTree> voPropCandidates;
	DerivePropagationPathTreeFromIlluminationArea( voPropCandidates );

	for( size_t n = 0; n < voPropCandidates.size( ); n++ )
	{
		const CITAGeoPropagationPathTree& oCandidate( voPropCandidates[n] );

		for( size_t n = 0; n < oCandidate.vvhCandidates.size( ); n++ )
		{
			const std::vector<CITAMesh::HalfedgeHandle>& vhEdges( oCandidate.vvhCandidates[n] );

			CPropagationPath oPath;
			oPath.push_back( m_pSourceAnchor );

			size_t N = vhEdges.size( );
			for( size_t i = N - 1; i > 0; i-- )
			{
				CITAMesh::HalfedgeHandle hNextEdge = vhEdges[i]; // Reverse order

				CITAMesh::VertexHandle hVertexStart = pMesh->from_vertex_handle( hNextEdge );
				CITAMesh::VertexHandle hVertexEnd   = pMesh->to_vertex_handle( hNextEdge );

				VistaVector3D v3VertexStart( CITAMesh::Point( pMesh->point( hVertexStart ) ).data( ) );
				VistaVector3D v3VertexEnd( CITAMesh::Point( pMesh->point( hVertexEnd ) ).data( ) );
				VistaVector3D v3HalfedgeDir( v3VertexEnd - v3VertexStart );

				auto pAnchor                = std::make_shared<CPropagationAnchor>( );
				pAnchor->v3InteractionPoint = v3VertexStart + .5f * v3HalfedgeDir;
				oPath.push_back( pAnchor );
			}

			assert( oPath.size( ) + 1 > vhEdges.size( ) );
			if( oCandidate.iTrunkFaceCombinedContourOrder == oAbortionCriteria.iNumMaxDiffractions - 1 )
				lPropPathsCandidates.push_back( oPath );
		}
	}
}
