#include <ITAPropagationPathSim/ConvexDiffraction/Engine.h>
#include <ITAPropagationPathSim/ConvexDiffraction/EngineImpl.h>

using namespace ITAGeo;
using namespace ITAPropagationPathSim;

ConvexDiffraction::CEngine* ConvexDiffraction::CEngine::Create( Halfedge::CMeshModel* pModel, std::shared_ptr<CPropagationAnchor> pSource,
                                                                std::shared_ptr<CPropagationAnchor> pTarget )
{
	return new CEngineImpl( pModel, pSource, pTarget );
}

void ConvexDiffraction::CEngine::SetDefaultAbortionCriteria( )
{
	oAbortionCriteria.SetDefaults( );
}
