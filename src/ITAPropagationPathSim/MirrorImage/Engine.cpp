#include <ITAPropagationPathSim/MirrorImage/Engine.h>

// ITA includes
#include <ITAConstants.h>
#include <ITAException.h>

// Vista include
#include <VistaMath/VistaGeometries.h>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <list>

using namespace ITAPropagationPathSim;
using namespace MirrorImage;


CEngine::CEngine( const ITAGeo::Halfedge::CMeshModel& oModel, const int iImageOrder /*= 2*/ )
{
	m_oMeshModel.CopyFrom( oModel );

	// Calculate mesh face normals
	CITAMesh* pMesh = m_oMeshModel.GetMesh( );
	pMesh->request_face_normals( );
	pMesh->update_face_normals( );

	m_pImagesRoot = new CImageNode( ITAGeo::ORDER_0, iImageOrder, GetNumberOfFaces( ) );
}

CEngine::~CEngine( )
{
	if( m_pImagesRoot != nullptr )
		delete m_pImagesRoot;
}

int CEngine::GetImageOrder( ) const
{
	// Deep tree traversal
	CImageNode* pNode = m_pImagesRoot;
	int nImageOrder   = ITAGeo::ORDER_0;
	while( pNode->HasSiblings( ) )
	{
		nImageOrder++;
		pNode = pNode->vpImageChildren[0];
	}
	return nImageOrder;
}

unsigned long int CEngine::GetNumberOfFaces( ) const
{
	return (int)m_oMeshModel.ConstGetMesh( )->n_faces( );
}

unsigned long int CEngine::GetNumberOfImages( ) const
{
	return ITAGeoUtils::CalculateNumberOfImages( GetNumberOfFaces( ), GetImageOrder( ) );
}

void CEngine::ConstructImages( std::shared_ptr<const ITAGeo::CPropagationAnchor> pEmitter )
{
	// Zero-order "image" is equivalent to emitter anchor
	m_pImagesRoot->pImage->CopyFromAnchor( pEmitter );

	// Next-order (first order) requires recursive construction of images (aborts if image order is set to zero)
	ConstructImagesRecursive( m_pImagesRoot, GetImageOrder( ), ITAGeo::ORDER_1 );
}

void CEngine::ConstructImagesRecursive( CImageNode* pParent, const int iMaxOrder, const int iCurrentOrder /*= 0 */ )
{
	// Recursion abortion criteria
	if( iCurrentOrder > iMaxOrder )
		return;

	CITAMesh* pMesh = m_oMeshModel.GetMesh( );

	/*
	 * Construct one image per face and traverse recursively until max order reached
	 */

	int n                         = 0;
	CITAMesh::ConstFaceIter cf_it = pMesh->faces_begin( );
	while( cf_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *cf_it++ );


		if( pParent->pParent == nullptr )
		{
			CImageNode* pSibling = pParent->vpImageChildren[n++];
			CalculateImage( pMesh, pParent->pImage, pSibling->pImage, hFace ); // Calculates next-order images from parent image based on mirror approach

			ConstructImagesRecursive( pSibling, iMaxOrder, iCurrentOrder + 1 ); // Recursive construction until max order is reached
		}
		else
		{
			if( hFace.idx( ) != pParent->pImage->iPolygonIndex )
			{
				CImageNode* pSibling = pParent->vpImageChildren[n++];
				CalculateImage( pMesh, pParent->pImage, pSibling->pImage, hFace ); // Calculates next-order images from parent image based on mirror approach

				ConstructImagesRecursive( pSibling, iMaxOrder, iCurrentOrder + 1 ); // Recursive construction until max order is reached
			}
		}
	}
}

void CEngine::CalculateImage( CITAMesh* pMesh, std::shared_ptr<const ITAGeo::CMirrorImage> pOrigin, std::shared_ptr<ITAGeo::CMirrorImage> pImage,
                              CITAMesh::FaceHandle hFace )
{
	CITAMesh::FaceVertexIter fv_begin = pMesh->fv_begin( hFace );

	const VistaVector3D v3MirrorOrigin( pMesh->point( *fv_begin ).data( ) ); // Use first vertex point as origin

	assert( pMesh->has_face_normals( ) );
	CITAMesh::Normal normal( pMesh->normal( hFace ) );
	const VistaVector3D v3FaceNormal( normal.data( ) );

	VistaPlane oMirrorPlane;
	oMirrorPlane.SetOrigin( v3MirrorOrigin );
	oMirrorPlane.SetNormVector( v3FaceNormal.GetNormalized( ) );

	ITAGeoUtils::MirrorPointOverPlane( pOrigin->v3InteractionPoint, oMirrorPlane, pImage->v3InteractionPoint );
	pImage->v3MirrorNormal = oMirrorPlane.GetNormVector( );
	pImage->iPolygonIndex  = hFace.idx( );
}

void CEngine::ConstructReflectionPaths( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                        const ITAGeo::ECulling eCulling /* = NONE*/, const int iMaxOrder /* = -1 */ )
{
	if( iMaxOrder == -1 )
		ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, GetImageOrder( ), eCulling );
	else
		ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, iMaxOrder, eCulling );
}

void CEngine::ConstructReflectionPathsOutside( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                               const int iMaxOrder )
{
	ConstructReflectionPaths( pDestinationAnchor, oPathList, ITAGeo::ECulling::BACKFACE, iMaxOrder );
}

void CEngine::ConstructReflectionPathsInside( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                              const int iMaxOrder )
{
	ConstructReflectionPaths( pDestinationAnchor, oPathList, ITAGeo::ECulling::FRONTFACE, iMaxOrder );
}

void CEngine::ConstructReflectionPathsForOrder( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList, const int iOrder,
                                                const ITAGeo::ECulling eCulling /* = NONE*/ )
{
	ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, iOrder, eCulling, iOrder );
}

void CEngine::ConstructReflectionPathsOutsideForOrder( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                                       const int iOrder )
{
	ConstructReflectionPathsForOrder( pDestinationAnchor, oPathList, iOrder, ITAGeo::ECulling::BACKFACE );
}

void CEngine::ConstructReflectionPathsInsideForOrder( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                                      const int iOrder )
{
	ConstructReflectionPathsForOrder( pDestinationAnchor, oPathList, iOrder, ITAGeo::ECulling::FRONTFACE );
}

void CEngine::ConstructReflectionPathList( CImageNode* pRoot, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPathList& oPathList,
                                           const int iMaxOrder, const ITAGeo::ECulling eCulling, const int iOrder /* = 0 */ )
{
	// Construct path between current node and the destination
	ITAGeo::CPropagationPath oPath;
	oPath.resize( pRoot->pImage->iOrder + 2 );

	if( ( iOrder == -1 ) || ( pRoot->pImage->iOrder == iOrder ) )
	{
		// Add start and end anchor of path
		oPath[0]                 = this->m_pImagesRoot->pImage;
		oPath[oPath.size( ) - 1] = pDestination;

		// Add reflection points to path
		ConstructReflectionsPath( pRoot, pDestination, oPath, eCulling );

		// Add last anchor of path if path is not empty(and thus not a valid path)
		if( !oPath.empty( ) )
		{
			oPathList.push_back( oPath );
		}
	}


	// Consider children of node if current order is not iOrder and is smaller than the max order
	auto iCurrentOrder = pRoot->pImage->iOrder;
	if( ( iOrder != iCurrentOrder ) && ( iCurrentOrder < iMaxOrder ) )
	{
		for( auto pChild: pRoot->vpImageChildren )
		{
			ConstructReflectionPathList( pChild, pDestination, oPathList, iMaxOrder, eCulling, iOrder );
		}
	}
}

void CEngine::ConstructReflectionsPath( CImageNode* pNode, std::shared_ptr<const ITAGeo::CPropagationAnchor> pStartAnchor, ITAGeo::CPropagationPath& oPath,
                                        const ITAGeo::ECulling eCulling )
{
	auto pMesh = m_oMeshModel.GetMesh( );

	int iFaceIndex = pNode->pImage->iPolygonIndex;

	// current node is root and thus is not reflected alongside a face
	if( iFaceIndex == -1 )
		return;

	auto hFace = pMesh->face_handle( iFaceIndex );

	VistaVector3D v3CurrentOrigin      = pStartAnchor->v3InteractionPoint;
	VistaVector3D v3CurrentDestination = pNode->pImage->v3InteractionPoint;

	VistaRay currentRay( v3CurrentOrigin, v3CurrentDestination - v3CurrentOrigin );

	VistaVector3D v3IntersectionPoint;
	bool bIntersects = ITAGeoUtils::RayFaceIntersectionTest( currentRay, pMesh, hFace, v3IntersectionPoint, eCulling );

	if( bIntersects )
	{
		// Set reflection anchor at intersection point
		std::shared_ptr<ITAGeo::CSpecularReflection> pReflectionAnchor = std::make_shared<ITAGeo::CSpecularReflection>( );
		pReflectionAnchor->v3InteractionPoint                          = v3IntersectionPoint;
		pReflectionAnchor->v3FaceNormal                                = pNode->pImage->v3MirrorNormal;
		pReflectionAnchor->iPolygonID                                  = pNode->pImage->iPolygonIndex;

		// Add reflection to path
		oPath[pNode->pImage->iOrder] = pReflectionAnchor;

		// Add further propagation anchors to path
		if( pNode->HasParent( ) )
		{
			ConstructReflectionsPath( pNode->pParent, pReflectionAnchor, oPath, eCulling );
		}
	}
	else
	{
		// No valid path
		oPath.clear( );
	}
}

void CEngine::GetAudiblePaths( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPathList& lPaths, const int iMaxOrder /* = -1 */,
                               const int iCulling /* = ITAGeo::ECulling::NONE */ ) const
{
	GetAudiblePathsRecursive( m_pImagesRoot, pDestination, lPaths, iCulling, iMaxOrder );
}

void CEngine::GetAudiblePathsRecursive( CImageNode* pSource, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPathList& lPaths,
                                        const int iCulling, const int iMaxOrder ) const
{
	ITAGeo::CPropagationPath oDirectSoundPath;
	if( AudibilityTest( pSource, pDestination, oDirectSoundPath, iCulling ) )
		lPaths.push_back( oDirectSoundPath );

	if( pSource->HasSiblings( ) && pSource->pImage->iOrder < iMaxOrder )
	{
		for( auto pSourceSibling: pSource->vpImageChildren )
			GetAudiblePathsRecursive( pSourceSibling, pDestination, lPaths, iCulling, iMaxOrder );
	}
}

void CEngine::GetAudiblePathsForOrder( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPathList& lFoldedImagePaths, const int iMaxOrder,
                                       const int iCulling /*= ITAGeo::ECulling::NONE */ ) const
{
	ITAGeo::CPropagationPathList lAllFoldedImagePaths;
	GetAudiblePaths( pDestination, lAllFoldedImagePaths, iMaxOrder, iCulling );

	for( auto p: lAllFoldedImagePaths )
		if( p.size( ) == iMaxOrder + 2 )
			lFoldedImagePaths.push_back( p );
}

void CEngine::GetImagesHedgehogVisualization( ITAGeo::CPropagationPathList& lPaths, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, const int iOrder )
{
	std::vector<CImageNode*> vpLeafNodes;
	m_pImagesRoot->GetLeafNodes( vpLeafNodes, ( iOrder < 0 ? GetImageOrder( ) : iOrder ) );

	for( auto n: vpLeafNodes )
	{
		ITAGeo::CPropagationPath oPath;
		oPath.resize( 2 );
		oPath[0] = pDestination;
		oPath[1] = n->pImage;
		lPaths.push_back( oPath );
	}
}

void CEngine::GetImageVisualizationPaths( ITAGeo::CPropagationPathList& lPaths, const int iOrder )
{
	std::vector<CImageNode*> vpLeafNodes;
	m_pImagesRoot->GetLeafNodes( vpLeafNodes, ( iOrder < 0 ? GetImageOrder( ) : iOrder ) );

	for( auto n: vpLeafNodes )
		lPaths.push_back( GetImageVisualizationPath( n ) );
}

bool CEngine::AudibilityTest( CImageNode* pSource, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPath& oPath,
                              const int /* iCulling */ ) const
{
	// Destination positions is fixed
	const VistaVector3D& v3Dest = pDestination->v3InteractionPoint;

	oPath.resize( pSource->pImage->iOrder + 2 );
	oPath[0]                 = m_pImagesRoot->pImage; // All paths start at emitter
	oPath[oPath.size( ) - 1] = pDestination;          // All paths end at sensor

	// Backwards audibility test from source image to source
	size_t iAnchorIdx             = oPath.size( ) - 2;
	CImageNode* pCurrentImageNode = pSource;
	VistaVector3D v3CurrentSpecularReflectionPoint;
	while( pCurrentImageNode->HasParent( ) )
	{
		CITAMesh::FaceHandle hFace = m_oMeshModel.ConstGetMesh( )->face_handle( pCurrentImageNode->pImage->iPolygonIndex );
		VistaPolygon oPolygon;
		ITAGeoUtils::VistaPolygonFromOpenMeshFace( m_oMeshModel.ConstGetMesh( ), hFace, oPolygon );

		const VistaVector3D& v3Pos = pCurrentImageNode->pImage->v3InteractionPoint;

		const VistaVector3D& v3PathOrigin( v3Pos );
		const VistaVector3D v3PathDirection( v3Dest - v3Pos );

		VistaRay rPathSegment( v3PathOrigin, v3PathDirection );
		if( !ITAGeoUtils::RayConvexPolygonIntersectionTest( rPathSegment, oPolygon, v3CurrentSpecularReflectionPoint ) )
		{
			oPath.clear( );
			return false;
		}

		auto pR             = std::make_shared<ITAGeo::CSpecularReflection>( v3CurrentSpecularReflectionPoint );
		pR->pMaterial       = nullptr; // @todo jst: get from Mesh
		oPath[iAnchorIdx--] = pR;

		pCurrentImageNode = pCurrentImageNode->pParent;
	}

	assert( iAnchorIdx == 0 );

	return true;
}

ITAGeo::CPropagationPath CEngine::GetImageVisualizationPath( CImageNode* pLeafNode )
{
	ITAGeo::CPropagationPath oPath;
	oPath.resize( pLeafNode->pImage->iOrder + 1 );
	CImageNode* pNode = pLeafNode;
	for( size_t i = oPath.size( ) - 1; i >= 0; i-- )
	{
		oPath[i] = pNode->pImage;

		if( pNode->HasParent( ) )
			pNode = pNode->pParent;
		else
			return oPath;
	}
	return oPath;
}
