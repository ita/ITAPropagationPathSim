// #include "RayGrid.h"
#include <ITAPropagationPathSim/AtmosphericRayTracing/RayGrid.h>

// ITA includes
#include <ITAException.h>

// STD
#include <algorithm>
#include <cmath>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;

// --- CONSTRUCTORS ---
// --------------------
#pragma region Constructors

CRayGrid::CRayGrid( const RayMatrix& rayMatrix, const std::vector<double>& thetaDeg, const std::vector<double>& phiDeg, const bool circularPhi /*= false*/ )
    : m_vvpRayMatrix( rayMatrix )
    , m_vdThetaDeg( thetaDeg )
    , m_vdPhiDeg( phiDeg )
    , m_bCircularPhi( circularPhi )
{
	UpdateDependentRayContainers( );

	if( !m_vpRays.empty( ) )
		m_v3SourcePos = m_vpRays.front( )->SourcePoint( );
}

CRayGrid::CRayGrid( const VistaVector3D& sourcePos, const std::vector<double>& thetaDeg, const std::vector<double>& phiDeg, const bool circularPhi /*= false*/,
                    const bool sortAngleVectors /*= true*/ )
    : m_v3SourcePos( sourcePos )
    , m_vdThetaDeg( thetaDeg )
    , m_vdPhiDeg( phiDeg )
    , m_bCircularPhi( circularPhi )
{
	if( sortAngleVectors )
	{
		std::sort( m_vdPhiDeg.begin( ), m_vdPhiDeg.end( ) );
		std::sort( m_vdThetaDeg.begin( ), m_vdThetaDeg.end( ) );
	}

	if( m_vdThetaDeg.front( ) < 0.0 || m_vdThetaDeg.back( ) > 180.0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Values for elevation angle theta are out of bounds [0, 180]" );
	if( m_vdPhiDeg.front( ) <= -360.0 || m_vdPhiDeg.back( ) >= 360.0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Values for azimuth angle phi are out of bounds (-360, 360)" );
	if( ( m_vdPhiDeg.back( ) - m_vdPhiDeg.front( ) ) >= 360.0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Delta for azimuth angle phi is >= 360" );

	InitRays( );
}


CEquiangularRayDistribution::CEquiangularRayDistribution( const VistaVector3D& sourcePos, const int nAngles ) : CEquiangularRayDistribution( sourcePos, nAngles, nAngles )
{
}

CEquiangularRayDistribution::CEquiangularRayDistribution( const VistaVector3D& sourcePos, const int nTheta, const int nPhi ) : CRayGrid( true )
{
	m_v3SourcePos = sourcePos;

	m_vdThetaDeg.reserve( nTheta );
	const double deltaTheta = 180.0 / ( nTheta - 1 );
	for( int idx = 0; idx < nTheta - 1; idx++ )
		m_vdThetaDeg.push_back( idx * deltaTheta );
	m_vdThetaDeg.push_back( 180.0 );

	m_vdPhiDeg.reserve( nPhi );
	const double deltaPhi = 360.0 / nPhi;
	for( int idx = 0; idx < nPhi; idx++ )
		m_vdPhiDeg.push_back( idx * deltaPhi );

	InitRays( );
}
#pragma endregion


// --- INITIALIZATION ---
// ----------------------
#pragma region Initialization

void CRayGrid::UpdateDependentRayContainers( )
{
	m_vpRays.clear( );
	m_vpRays.reserve( NTheta( ) * NPhi( ) );
	for( RayVector rayVector: m_vvpRayMatrix )
		for( RayPtr pRay: rayVector )
			m_vpRays.push_back( pRay );

	m_vpUniqueRays = std::set<RayPtr>( m_vpRays.begin( ), m_vpRays.end( ) );
}
void CRayGrid::InitRays( )
{
	m_vvpRayMatrix.resize( NTheta( ), RayVector( NPhi( ) ) );
	for( int idxTheta = 0; idxTheta < NTheta( ); idxTheta++ )
	{
		const bool isPoleDirection = IsPoleDirection( m_vdThetaDeg[idxTheta] );
		for( int idxPhi = 0; idxPhi < NPhi( ); idxPhi++ )
		{
			if( isPoleDirection && idxPhi > 0 ) // At poles: Insert same ray multiple times
				m_vvpRayMatrix[idxTheta][idxPhi] = m_vvpRayMatrix[idxTheta].front( );
			else
				m_vvpRayMatrix[idxTheta][idxPhi] = std::make_shared<CRay>( m_v3SourcePos, m_vdThetaDeg[idxTheta], m_vdPhiDeg[idxPhi] );
		}
	}
	UpdateDependentRayContainers( );
}
#pragma endregion


// --- PROTECTED ---
// -----------------
#pragma region PROTECTED
void CRayGrid::SetRayMatrix( const RayMatrix& newRayMatrix )
{
	m_vvpRayMatrix = newRayMatrix;
	UpdateDependentRayContainers( );
}

void CRayGrid::FilterDirections( const std::vector<int>& thetaIdxVec, const std::vector<int>& phiIdxVec, const bool circularPhi /*= false*/ )
{
	auto newThetaDeg       = std::vector<double>( thetaIdxVec.size( ) );
	auto newPhiDeg         = std::vector<double>( phiIdxVec.size( ) );
	RayMatrix newRayMatrix = RayMatrix( thetaIdxVec.size( ), RayVector( phiIdxVec.size( ) ) );

	for( int idxNewPhi = 0; idxNewPhi < phiIdxVec.size( ); idxNewPhi++ )
		newPhiDeg[idxNewPhi] = m_vdPhiDeg[phiIdxVec[idxNewPhi]];

	for( int idxNewTheta = 0; idxNewTheta < thetaIdxVec.size( ); idxNewTheta++ )
	{
		newThetaDeg[idxNewTheta] = m_vdThetaDeg[thetaIdxVec[idxNewTheta]];
		for( int idxPhi = 0; idxPhi < phiIdxVec.size( ); idxPhi++ )
			newRayMatrix[idxNewTheta][idxPhi] = m_vvpRayMatrix[thetaIdxVec[idxNewTheta]][phiIdxVec[idxPhi]];
	}

	m_bCircularPhi = m_bCircularPhi & circularPhi;
	m_vdThetaDeg   = newThetaDeg;
	m_vdPhiDeg     = newPhiDeg;
	m_vvpRayMatrix = newRayMatrix;
	UpdateDependentRayContainers( );
}

const CRayGrid::RayVector& CRayGrid::GetRaysWithSameTheta( const std::shared_ptr<CRay> pRay )
{
	const int idxTheta = IndexToThetaIndex( GetIndex( pRay ) );
	if( idxTheta < 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray is not part of this ray grid." );

	return m_vvpRayMatrix[idxTheta];
}
CRayGrid::RayVector CRayGrid::GetRaysWithSamePhi( const std::shared_ptr<CRay> pRay )
{
	const int idxPhi = IndexToPhiIndex( GetIndex( pRay ) );
	if( idxPhi < 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray is not part of this ray grid." );

	RayVector thetaRays( NTheta( ) );
	for( int idx = 0; idx < NTheta( ); idx++ )
		thetaRays[idx] = m_vvpRayMatrix[idx][idxPhi];

	return thetaRays;
}
#pragma endregion


#pragma region WAVEFRONTNORMAL SURFACE
//---HELPERS ---
//--------------
inline float DeterminantCofactor( const float matrix[][3], const int idx1, const int idx2 )
{
	float cofactor;
	if( ( idx1 + idx2 ) % 2 == 0 )
		cofactor = matrix[( idx1 + 1 ) % 3][( idx2 + 1 ) % 3] * matrix[( idx1 + 2 ) % 3][( idx2 + 2 ) % 3] -
		           matrix[( idx1 + 1 ) % 3][( idx2 + 2 ) % 3] * matrix[( idx1 + 2 ) % 3][( idx2 + 1 ) % 3];
	else
		cofactor = -( -matrix[( idx1 + 1 ) % 3][( idx2 + 1 ) % 3] * matrix[( idx1 + 2 ) % 3][( idx2 + 2 ) % 3] +
		              matrix[( idx1 + 1 ) % 3][( idx2 + 2 ) % 3] * matrix[( idx1 + 2 ) % 3][( idx2 + 1 ) % 3] );
	return cofactor;
};
inline float Determinant( const float matrix[][3] )
{
	const float x = matrix[0][0] * DeterminantCofactor( matrix, 0, 0 );
	const float y = matrix[0][1] * DeterminantCofactor( matrix, 0, 1 );
	const float z = matrix[0][2] * DeterminantCofactor( matrix, 0, 2 );
	return x + y + z;
};
inline double SurfaceAreaOfRayTriangle( const std::vector<std::shared_ptr<CRay>>& rays, const double& time )
{
	VistaVector3D p0 = rays[0]->AtTime( time ).position;
	VistaVector3D p1 = rays[1]->AtTime( time ).position;
	VistaVector3D p2 = rays[2]->AtTime( time ).position;

	const float matrixXY[3][3] = { { p0[Vista::X], p1[Vista::X], p2[Vista::X] }, { p0[Vista::Y], p1[Vista::Y], p2[Vista::Y] }, { 1, 1, 1 } };
	const float matrixYZ[3][3] = { { p0[Vista::Y], p1[Vista::Y], p2[Vista::Y] }, { p0[Vista::Z], p1[Vista::Z], p2[Vista::Z] }, { 1, 1, 1 } };
	const float matrixZX[3][3] = { { p0[Vista::Z], p1[Vista::Z], p2[Vista::Z] }, { p0[Vista::X], p1[Vista::X], p2[Vista::X] }, { 1, 1, 1 } };

	const float detXY = Determinant( matrixXY );
	const float detYZ = Determinant( matrixYZ );
	const float detZX = Determinant( matrixZX );

	return 0.5 * std::sqrtf( detXY * detXY + detYZ * detYZ + detZX * detZX );
};

//---CLASS METHODS ---
//--------------------
double CRayGrid::WavefrontSurface( const double& time ) const
{
	if( !Is2D( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Can only calculate the wavefront surface for 2D grids." );

	double surfaceArea = 0.0;
	const int nPhi     = NPhi( ) + m_bCircularPhi;
	for( int idxTheta = 0; idxTheta < NTheta( ) - 1; idxTheta++ )
		for( int idxPhi = 0; idxPhi < nPhi - 1; idxPhi++ )
		{
			const int idxNextPhi = ( idxPhi + 1 ) % NPhi( );
			if( IsPoleDirection( m_vdThetaDeg[idxTheta] ) )
				surfaceArea += SurfaceAreaOfRayTriangle(
				    { m_vvpRayMatrix[idxTheta][idxPhi], m_vvpRayMatrix[idxTheta + 1][idxPhi], m_vvpRayMatrix[idxTheta + 1][idxNextPhi] }, time );
			else if( IsPoleDirection( m_vdThetaDeg[idxTheta + 1] ) )
				surfaceArea +=
				    SurfaceAreaOfRayTriangle( { m_vvpRayMatrix[idxTheta][idxPhi], m_vvpRayMatrix[idxTheta][idxNextPhi], m_vvpRayMatrix[idxTheta + 1][idxPhi] }, time );
			else
			{
				const bool diagonalTopLeftToBottomRight = ( idxTheta + idxPhi ) % 2 == 0;
				RayVector raysTriangle1, raysTriangle2;
				if( diagonalTopLeftToBottomRight )
				{
					raysTriangle1 = { m_vvpRayMatrix[idxTheta][idxPhi], m_vvpRayMatrix[idxTheta][idxNextPhi], m_vvpRayMatrix[idxTheta + 1][idxNextPhi] };
					raysTriangle2 = { m_vvpRayMatrix[idxTheta][idxPhi], m_vvpRayMatrix[idxTheta + 1][idxPhi], m_vvpRayMatrix[idxTheta + 1][idxNextPhi] };
				}
				else
				{
					raysTriangle1 = { m_vvpRayMatrix[idxTheta][idxPhi], m_vvpRayMatrix[idxTheta][idxNextPhi], m_vvpRayMatrix[idxTheta + 1][idxPhi] };
					raysTriangle2 = { m_vvpRayMatrix[idxTheta][idxNextPhi], m_vvpRayMatrix[idxTheta + 1][idxPhi], m_vvpRayMatrix[idxTheta + 1][idxNextPhi] };
				}
				surfaceArea += SurfaceAreaOfRayTriangle( raysTriangle1, time );
				surfaceArea += SurfaceAreaOfRayTriangle( raysTriangle2, time );
			}
		}

	return surfaceArea;
}

double CRayGrid::WavefrontSurfaceReference( ) const
{
	if( !Is2D( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Can only calculate the wavefront surface reference for 2D grids." );

	const double theta1 = m_vdThetaDeg.front( ) * M_PI / 180.0;
	const double theta2 = m_vdThetaDeg.back( ) * M_PI / 180.0;
	double deltaPhi;
	if( m_bCircularPhi )
		deltaPhi = 2 * M_PI;
	else
	{
		deltaPhi = m_vdPhiDeg.back( ) - m_vdPhiDeg.front( );
		if( deltaPhi < 0 )
			deltaPhi += 360;
		deltaPhi *= M_PI / 180.0;
	}

	return deltaPhi * ( std::cos( theta1 ) - std::cos( theta2 ) );
}


#pragma endregion


// --- NEIGHBORING RAYS ---
// ------------------------
#pragma region NEIGHBORING RAYS - public
CRayGrid CRayGrid::SurroundingGrid( const std::shared_ptr<CRay> pRay ) const
{
	if( !Contains( pRay ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray is not part of this ray grid." );

	std::vector<int> thetaFilterIndices;
	std::vector<int> phiFilterIndices;
	GetNeighboringAngleIndices( pRay, thetaFilterIndices, phiFilterIndices );

	auto newThetaDeg       = std::vector<double>( thetaFilterIndices.size( ) );
	auto newPhiDeg         = std::vector<double>( phiFilterIndices.size( ) );
	RayMatrix newRayMatrix = RayMatrix( thetaFilterIndices.size( ), RayVector( phiFilterIndices.size( ) ) );

	for( int idxPhi = 0; idxPhi < phiFilterIndices.size( ); idxPhi++ )
		newPhiDeg[idxPhi] = m_vdPhiDeg[phiFilterIndices[idxPhi]];

	for( int idxTheta = 0; idxTheta < thetaFilterIndices.size( ); idxTheta++ )
	{
		const int oldIdxTheta = thetaFilterIndices[idxTheta];
		newThetaDeg[idxTheta] = m_vdThetaDeg[oldIdxTheta];
		for( int idxPhi = 0; idxPhi < phiFilterIndices.size( ); idxPhi++ )
			newRayMatrix[idxTheta][idxPhi] = m_vvpRayMatrix[oldIdxTheta][phiFilterIndices[idxPhi]];
	}

	const bool newGridHasCircularPhi = m_bCircularPhi && HasPoleDirection( pRay );
	return CRayGrid( newRayMatrix, newThetaDeg, newPhiDeg, newGridHasCircularPhi );
}

void CRayGrid::SetToSurroundingGrid( const std::shared_ptr<CRay> pRay )
{
	*this = SurroundingGrid( pRay );
}
#pragma endregion

#pragma region COPY - public
CRayGrid CRayGrid::CopyWithNewRays( ) const
{
	return CRayGrid( m_v3SourcePos, m_vdThetaDeg, m_vdPhiDeg, m_bCircularPhi, false );
}
#pragma endregion


// --- PRIVATE HELPERS ---
// -------------------------
#pragma region INDICING - protected

int CRayGrid::GetIndex( const std::shared_ptr<CRay> pRay ) const
{
	auto it = std::find( m_vpRays.cbegin( ), m_vpRays.cend( ), pRay );
	if( it == m_vpRays.cend( ) )
		return -1;

	return (int)std::distance( m_vpRays.cbegin( ), it );
}
int CRayGrid::IndexToThetaIndex( const int idx ) const
{
	if( idx < 0 || idx >= m_vpRays.size( ) )
		return -1;

	return idx / NPhi( ); // Integer-division (floor-function)
}
int CRayGrid::IndexToPhiIndex( const int idx ) const
{
	if( idx < 0 || idx >= m_vpRays.size( ) )
		return -1;

	return idx % NPhi( );
}

void CRayGrid::GetNeighboringAngleIndices( const std::shared_ptr<CRay> pRay, std::vector<int>& thetaIdxVec, std::vector<int>& phiIdxVec ) const
{
	thetaIdxVec.clear( );
	phiIdxVec.clear( );

	const int idxRay = GetIndex( pRay );
	if( idxRay < 0 )
		return;
	const int idxTheta = IndexToThetaIndex( idxRay );
	const int idxPhi   = IndexToPhiIndex( idxRay );


	thetaIdxVec = { idxTheta - 1, idxTheta, idxTheta + 1 };
	if( thetaIdxVec.front( ) < 0 )
		thetaIdxVec.erase( thetaIdxVec.begin( ) );
	if( thetaIdxVec.back( ) >= NTheta( ) )
		thetaIdxVec.pop_back( );


	if( HasPoleDirection( pRay ) )
		for( int idx = 0; idx < NPhi( ); idx++ )
			phiIdxVec.push_back( idx );
	else if( m_bCircularPhi )
		phiIdxVec = { ( idxPhi - 1 + NPhi( ) ) % NPhi( ), idxPhi, ( idxPhi + 1 ) % NPhi( ) }; // Circular indexing;
	else
	{
		phiIdxVec = { idxPhi - 1, idxPhi, idxPhi + 1 };
		if( phiIdxVec.front( ) < 0 )
			phiIdxVec.erase( phiIdxVec.begin( ) );
		if( phiIdxVec.back( ) >= NPhi( ) )
			phiIdxVec.pop_back( );
	}
}
#pragma endregion


#pragma region BOOLEANS
bool CRayGrid::IsPoleDirection( const double& thetaDeg ) const
{
	return std::abs( thetaDeg ) < DBL_EPSILON || std::abs( thetaDeg - 180.0 ) < DBL_EPSILON;
}

bool CRayGrid::HasPoleDirection( const std::shared_ptr<CRay> pRay ) const
{
	const VistaVector3D& n0 = pRay->InitialDirection( );
	return std::abs( n0[Vista::X] ) < FLT_EPSILON && std::abs( n0[Vista::Y] ) < FLT_EPSILON; // x- and y- component are zero
}

bool CRayGrid::IsEmpty( ) const
{
	return NTheta( ) == 0 || NPhi( ) == 0;
}

bool CRayGrid::Is2D( ) const
{
	return NTheta( ) > 1 && NPhi( ) > 1;
}

bool CRayGrid::Contains( const std::shared_ptr<CRay> pRay ) const
{
	return std::find( m_vpRays.cbegin( ), m_vpRays.cend( ), pRay ) != m_vpRays.cend( );
}
#pragma endregion
