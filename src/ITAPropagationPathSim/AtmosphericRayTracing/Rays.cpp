#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>

// ITA includes
#include <ITAException.h>

// STD
// #include <cmath>
#include <algorithm>
#include <math.h>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;


CRayElement CRayElement::Interpolate( const CRayElement& target, const float fFraction ) const
{
	const double newTime = timeStamp + ( target.timeStamp - timeStamp ) * fFraction;
	return CRayElement( position.Interpolate( target.position, fFraction ), wavefrontNormal.Interpolate( target.wavefrontNormal, fFraction ), newTime );
}


CRay::CRay( const VistaVector3D& v3SourcePos, const double& thetaDeg, const double& phiDeg )
{
	const double thetaRad = thetaDeg * M_PI / 180;
	const double phiRad   = phiDeg * M_PI / 180;
	const double sinTheta = std::sin( thetaRad );
	auto v3Direction      = VistaVector3D( sinTheta * std::cos( phiRad ), sinTheta * std::sin( phiRad ), std::cos( thetaRad ) );
	Append( v3SourcePos, v3Direction.GetNormalized( ), 0.0 );
}
CRay::CRay( const VistaVector3D& v3SourcePos, const VistaVector3D& v3Direction )
{
	Append( v3SourcePos, v3Direction.GetNormalized( ), 0.0 );
}

// int CRay::ReflectionOrder(const int idx) const
//{
//	if (idx < 0 || idx >= NumPoints())
//		return -1;
//
//	auto reflOrderIt = std::lower_bound(iReflectionIndices.cbegin(), iReflectionIndices.cend(), idx);
//	if (reflOrderIt == iReflectionIndices.cend())
//		return NumPoints() - 1;
//	return *reflOrderIt;
//}


void ITAPropagationPathSim::AtmosphericRayTracing::CRay::MarkAsEigenray( bool bAccuracyReached, int iRayZoomingIterations )
{
	m_bEigenray             = true;
	m_bReceiverHit          = bAccuracyReached;
	m_iRayZoomingIterations = iRayZoomingIterations;
}

void CRay::Append( const VistaVector3D& position, const VistaVector3D& wavefrontNormal, const double& timeStamp )
{
	push_back( CRayElement( position, wavefrontNormal, timeStamp ) );
}
void CRay::AppendReflection( const VistaVector3D& position, const VistaVector3D& wavefrontNormal, const double& timeStamp )
{
	Append( position, wavefrontNormal, timeStamp );
	m_viReflectionIndices.push_back( NumPoints( ) - 1 );
}
void CRay::AddLastPointToReflectionList( )
{
	m_viReflectionIndices.push_back( NumPoints( ) - 1 );
}


bool CRay::SameDirection( const CRay& other ) const
{
	return InitialDirection( ) == other.InitialDirection( );
}

bool CRay::IsReflectionIdx( const int idx ) const
{
	return std::find( m_viReflectionIndices.begin( ), m_viReflectionIndices.end( ), idx ) != m_viReflectionIndices.end( );
}


CRayElement CRay::AtTime( const double& time ) const
{
	CRay::const_iterator itAfterTime = IteratorAfterTime( time );

	if( itAfterTime == end( ) )
		return back( );
	if( itAfterTime == begin( ) )
		return front( );
	if( itAfterTime->timeStamp == time )
		return *itAfterTime;

	// Linear interpolation
	CRay::const_iterator itBeforeTime = itAfterTime - 1;
	float fraction                    = ( time - itBeforeTime->timeStamp ) / ( itAfterTime->timeStamp - itBeforeTime->timeStamp );
	return itBeforeTime->Interpolate( *itAfterTime, fraction );
}

const CRayElement& CRay::Nearest( const double& time ) const
{
	CRay::const_iterator itAfterTime = IteratorAfterTime( time );

	if( itAfterTime == end( ) )
		return back( );
	if( itAfterTime == begin( ) )
		return front( );
	if( itAfterTime->timeStamp == time )
		return *itAfterTime;

	// Nearest neighbor
	CRay::const_iterator itBeforeTime = itAfterTime - 1;
	if( std::abs( itBeforeTime->timeStamp - time ) <= std::abs( itAfterTime->timeStamp - time ) )
		return *itBeforeTime;
	return *itAfterTime;
}

CRay::const_iterator CRay::IteratorAfterTime( const double& time ) const
{
	if( time < 0.0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Only positive values for time are allowed." );

	CRayElement compare;
	compare.timeStamp = time;
	return std::lower_bound( cbegin( ), cend( ), compare );
}


// ----RECEIVER DISTANCE----
// -------------------------
#pragma region Receiver distance
const CRayReceiverData* CRay::ReceiverDistanceData( const VistaVector3D& receiverPos ) const
{
	ReceiverDistanceMap::const_iterator receiverDataIt = m_mReceiverDistanceMap.find( &receiverPos );
	if( receiverDataIt == m_mReceiverDistanceMap.cend( ) ) // Receiver not yet in map
		return nullptr;

	return &receiverDataIt->second;
}


void CRay::UpdateMinimumReceiverDistance( const VistaVector3D& receiverPos )
{
	float distance = ( LastPoint( ) - receiverPos ).GetLength( );
	if( m_mReceiverDistanceMap.find( &receiverPos ) == m_mReceiverDistanceMap.cend( ) ) // Receiver not yet in map
	{
		m_mReceiverDistanceMap[&receiverPos] = CRayReceiverData( NumPoints( ) - 1, distance, ReflectionOrder( ) );
		return;
	}

	CRayReceiverData& receiverDistanceData = m_mReceiverDistanceMap[&receiverPos];
	if( distance >= receiverDistanceData.distance )
	{
		receiverDistanceData.bDistanceUpdatedInLastIt = false;
	}
	else
	{
		receiverDistanceData.distance                 = distance;
		receiverDistanceData.idxMinDist               = NumPoints( ) - 1;
		receiverDistanceData.reflectionOrder          = ReflectionOrder( );
		receiverDistanceData.bDistanceUpdatedInLastIt = true;
	}
}

bool CRay::ReceiverDistanceUpdatedOnLastTimeStep( )
{
	for( auto const& element: m_mReceiverDistanceMap )
	{
		const CRayReceiverData receiverData = element.second;
		if( receiverData.bDistanceUpdatedInLastIt )
			return true;
	}
	return false;
}

void CRay::FinalizeMinimumReceiverDistances( )
{
	for( auto const& element: m_mReceiverDistanceMap )
		InterpolateToRealMinimumPosition( *element.first );
}

inline VistaVector3D ClosestPointOnLineSegmentToReceiver( const VistaVector3D& segmentP1, const VistaVector3D& segmentP2, const VistaVector3D& receiverPoint )
{
	const VistaVector3D normal      = ( segmentP2 - segmentP1 ).GetNormalized( );
	VistaVector3D intersectionPoint = segmentP1 + normal.Dot( receiverPoint - segmentP1 ) * normal; // intersection with line (not necessarily segment)

	if( ( segmentP1 - intersectionPoint ).Dot( segmentP2 - intersectionPoint ) < 0 ) // Point inside segment
		return intersectionPoint;

	const float dP1R = ( segmentP1 - receiverPoint ).GetLength( );
	const float dP2R = ( segmentP2 - receiverPoint ).GetLength( );
	return dP1R <= dP2R ? segmentP1 : segmentP2; // Return point of segment with minimum distance
};
void CRay::InterpolateToRealMinimumPosition( const VistaVector3D& receiverPos )
{
	int& iMinReceiverDistance = m_mReceiverDistanceMap[&receiverPos].idxMinDist;
	float& dMin               = m_mReceiverDistanceMap[&receiverPos].distance;

	const VistaVector3D& rTmpMin = at( iMinReceiverDistance ).position;

	VistaVector3D rAfter = rTmpMin;
	if( iMinReceiverDistance < size( ) - 1 )
		rAfter = ClosestPointOnLineSegmentToReceiver( rTmpMin, at( iMinReceiverDistance + 1 ).position, receiverPos );

	VistaVector3D rBefore = rTmpMin;
	if( iMinReceiverDistance > 0 )
		rBefore = ClosestPointOnLineSegmentToReceiver( rTmpMin, at( iMinReceiverDistance - 1 ).position, receiverPos );


	dMin                   = ( rAfter - receiverPos ).GetLength( );
	const float dMinBefore = ( rBefore - receiverPos ).GetLength( );
	if( dMinBefore < dMin )
	{
		iMinReceiverDistance--;
		dMin                                            = dMinBefore;
		m_mReceiverDistanceMap[&receiverPos].posMinDist = rBefore;
	}
	else
		m_mReceiverDistanceMap[&receiverPos].posMinDist = rAfter;
}
#pragma endregion