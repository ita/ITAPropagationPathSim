#include <ITAPropagationPathSim/AtmosphericRayTracing/ODESolver/ODESolver.h>
#include <cmath>
#include <vector>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;

VistaVector3D ODESolver::NormalToSlowness( const VistaVector3D& n, const double& rz, const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	const double c        = atmosphere.SpeedOfSound( rz );
	const VistaVector3D v = atmosphere.WindVector( rz );
	return n / ( c + n.Dot( v ) );
}
VistaVector3D ODESolver::SlownessToNormal( const VistaVector3D& s /*, const double& rz, const ITAGeo::CStratifiedAtmosphere& atmosphere*/ )
{
	// const double c = atmosphere.SpeedOfSound(rz);
	// const double omega = 1.0 - atmosphere.WindVector(rz).Dot(s);
	// return (c / omega * s);
	return s.GetNormalized( );
}


//! Calculates the derivative of the wavefront position according to the ODEs for an inhomogeneous medium
VistaVector3D WFPositionDerivative( const VistaVector3D& s, const double& omega, const double& c, const VistaVector3D& v )
{
	return ( c * c / omega ) * s + v;
}

//! Calculates the derivative of the slowness vector z-component according to the ODEs for a stratified medium
double SlownessZDerivativeStratified( const VistaVector3D& s, const double& omega, const double& c, const double& dc, const VistaVector3D& dv )
{
	return -( omega / c * dc + s[Vista::X] * dv[Vista::X] + s[Vista::Y] * dv[Vista::Y] );
}

//! Calculates the derivatives of the wavefront position and slowness vector according to the ODEs for a stratified medium
/**
 * @param[in] rz Altitude of current wavefront position
 * @param[in] s Current slowness vector
 * @param[in] atmosphere Stratified atmosphere
 * @param[out] dr New delta of wavefront position (ray movement)
 * @param[out] dsz New delta of slowness vector z-component
 *
 * The differential equations are taken from the slowness vector approach described in:
 * A. D. Pierce. Acoustics: An Introduction to Its Physical Principles and Applications, volume 20. McGraw-Hill New York, 1981.
 *
 * More details can be found in the Master thesis:
 * MA2017 - Sch�fer - Atmospheric Ray Tracing basen on altitude-dependent weather data
 */
void ODEDerivatives( const double& rz, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& dr, double& dsz )
{
	const int signRZ   = copysign( 1, rz );
	const double absRZ = abs( rz );

	const double c         = atmosphere.SpeedOfSound( absRZ );
	const double dc        = atmosphere.SpeedOfSoundGradient( absRZ );
	const VistaVector3D v  = atmosphere.WindVector( absRZ );
	const VistaVector3D dv = atmosphere.WindVectorGradient( absRZ );

	const double omega = 1 - v.Dot( s );

	dr  = WFPositionDerivative( s, omega, c, v );
	dsz = SlownessZDerivativeStratified( s, omega, c, dc, dv );

	dsz = signRZ * dsz; // Invert derivative in negative altitude
}


void ODESolver::Euler( VistaVector3D& r, VistaVector3D& s, const double& dt, const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	VistaVector3D rNew, sNew;
	Euler( r, s, dt, atmosphere, rNew, sNew );
	r = rNew;
	s = sNew;
}

void ODESolver::Euler( const VistaVector3D& r, const VistaVector3D& s, const double& dt, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew,
                       VistaVector3D& sNew )
{
	VistaVector3D dr;
	double dsz = 0;
	ODEDerivatives( r[Vista::Z], s, atmosphere, dr, dsz );

	sNew           = s;
	sNew[Vista::Z] = s[Vista::Z] + dt * dsz;
	rNew           = r + dt * dr;
}

void ODESolver::RungeKutta( VistaVector3D& r, VistaVector3D& s, const double& dt, const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	VistaVector3D rNew, sNew;
	RungeKutta( r, s, dt, atmosphere, rNew, sNew );
	r = rNew;
	s = sNew;
}

void ODESolver::RungeKutta( const VistaVector3D& r, const VistaVector3D& s, const double& dt, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew,
                            VistaVector3D& sNew )
{
	const double sz = s[Vista::Z];
	sNew            = s;

	auto dr  = std::vector<VistaVector3D>( 4 );
	auto dsz = std::vector<double>( 4 );

	ODEDerivatives( r[Vista::Z], s, atmosphere, dr[0], dsz[0] );

	rNew           = r + dt / 2 * dr[0];
	sNew[Vista::Z] = sz + dt / 2 * dsz[0];
	ODEDerivatives( rNew[Vista::Z], sNew, atmosphere, dr[1], dsz[1] );

	rNew           = r + dt / 2 * dr[1];
	sNew[Vista::Z] = sz + dt / 2 * dsz[1];
	ODEDerivatives( rNew[Vista::Z], sNew, atmosphere, dr[2], dsz[2] );

	rNew           = r + dt * dr[2];
	sNew[Vista::Z] = sz + dt * dsz[2];
	ODEDerivatives( rNew[Vista::Z], sNew, atmosphere, dr[3], dsz[3] );

	sNew[Vista::Z] = sz + dt * ( dsz[0] + 2 * dsz[1] + 2 * dsz[2] + dsz[3] ) / 6;
	rNew           = r + dt * ( dr[0] + 2 * dr[1] + 2 * dr[2] + dr[3] ) / 6;
}
