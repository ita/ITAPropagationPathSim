/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_PROPAGATIONPATHSIM_ART_SIMULATION_ADAPTIVESOLVER
#define IW_ITA_PROPAGATIONPATHSIM_ART_SIMULATION_ADAPTIVESOLVER

#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTSettings.h>

// ITA includes
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>


namespace ITAPropagationPathSim
{
	namespace AtmosphericRayTracing
	{
		namespace Simulation
		{
			class CAdaptiveSolver
			{
			public:
				static const unsigned int MaxAllowedAdaptationLevel =
				    31; //! Maximum allowed value for Simulation::AdaptiveIntegrationSettings.iMaxAdaptationLevel. TODO: Move to Simulation::Settings?
			private:
				const Settings m_settings;
				double m_dCurrentDt               = m_settings.dIntegrationTimeStep;
				unsigned int m_iDtAdaptationLevel = 0;

				const unsigned long int m_iMaxDtPortion;       //! = 2 ^ iMaxAdaptionLevel
				unsigned long int m_iTimeFramePortion = 0;     //! Portion of processed time of an unadapted time step. Integer between 0 and maxDtPortion.
				bool m_bIncreaseDtInNextStep          = false; //! Indicates whether the error is small enough that the timestep can be increased during next step.

			public:
				CAdaptiveSolver( const Settings& settings );

			public:
				double CurrentStepSize( ) const { return m_dCurrentDt; }
				//! Performs a single integration step based on the given simulation settings.
				void Process( const VistaVector3D& r, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew, VistaVector3D& sNew );

			private:
				//! Performs a single integration step without adapting the step size
				void IntegrationStep( const VistaVector3D& r, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew,
				                      VistaVector3D& sNew ) const;
				//! Performs a single integration step and adapting the step size if necessary
				void AdaptiveIntegrationStep( const VistaVector3D& r, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew,
				                              VistaVector3D& sNew );

				//! Returns an estimation of the integration error which is used for the adaptation of the step size
				double EstimateIntegrationError( const double& rz1, const double& rz2, const VistaVector3D& n2, const ITAGeo::CStratifiedAtmosphere& atmosphere ) const;

				//! Increases (doubles) time step if requirements are matched and returns true on success
				bool TryIncreaseTimeStep( );
				//! Decreases (halves) time step if requirements are matched and returns true on success
				bool TryDecreaseTimeStep( );

				//! Updates iTimeFramePortion after an adaptive integration step
				void UpdateTimeFramePortion( );
				//! Returns true if increasing (doubling) the step size still allows to stay within the original resolution
				bool IncreasedDtFitsInRestOfTimeFrame( ) const;
			};
		} // namespace Simulation
	} // namespace AtmosphericRayTracing
} // namespace ITAPropagationPathSim

#endif // IW_ITA_PROPAGATIONPATHSIM_ART_SIMULATION_ADAPTIVESOLVER