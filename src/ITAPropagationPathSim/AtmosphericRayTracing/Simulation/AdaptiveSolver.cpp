#include "AdaptiveSolver.h"

// ITA includes
#include <ITAException.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/ODESolver/ODESolver.h>

// STD
#include <algorithm>
#include <cmath>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;

Simulation::CAdaptiveSolver::CAdaptiveSolver( const Settings& settings )
    : m_settings( settings )
    , m_iMaxDtPortion( (unsigned long int)1 << std::min( m_settings.adaptiveIntegration.iMaxAdaptationLevel, MaxAllowedAdaptationLevel ) )
{
}

void Simulation::CAdaptiveSolver::Process( const VistaVector3D& r, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew,
                                           VistaVector3D& sNew )
{
	if( m_settings.adaptiveIntegration.bActive )
		AdaptiveIntegrationStep( r, s, atmosphere, rNew, sNew );
	else
		IntegrationStep( r, s, atmosphere, rNew, sNew );
}


void Simulation::CAdaptiveSolver::IntegrationStep( const VistaVector3D& r, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere, VistaVector3D& rNew,
                                                   VistaVector3D& sNew ) const
{
	switch( m_settings.solverMethod )
	{
		case SolverMethod::EULER:
			ODESolver::Euler( r, s, m_dCurrentDt, atmosphere, rNew, sNew );
			break;
		case SolverMethod::RUNGE_KUTTA:
			ODESolver::RungeKutta( r, s, m_dCurrentDt, atmosphere, rNew, sNew );
			break;
		default:
			ITA_EXCEPT_INVALID_PARAMETER( "Unknown solver method." );
	}
}


void Simulation::CAdaptiveSolver::AdaptiveIntegrationStep( const VistaVector3D& r, const VistaVector3D& s, const ITAGeo::CStratifiedAtmosphere& atmosphere,
                                                           VistaVector3D& rNew, VistaVector3D& sNew )
{
	if( m_bIncreaseDtInNextStep )
		TryIncreaseTimeStep( );

	while( 1 )
	{
		IntegrationStep( r, s, atmosphere, rNew, sNew );

		const double rz          = abs( r[Vista::Z] );
		const double rzNew       = abs( rNew[Vista::Z] );
		const VistaVector3D nNew = ODESolver::SlownessToNormal( sNew );

		const double error = EstimateIntegrationError( rz, rzNew, nNew, atmosphere );
		if( error <= m_settings.adaptiveIntegration.dMaxError )
		{
			m_bIncreaseDtInNextStep = error < m_settings.adaptiveIntegration.dUncriticalError;
			break;
		}

		if( !TryDecreaseTimeStep( ) )
			break;
	}

	UpdateTimeFramePortion( );
}

double Simulation::CAdaptiveSolver::EstimateIntegrationError( const double& rz1, const double& rz2, const VistaVector3D& n2,
                                                              const ITAGeo::CStratifiedAtmosphere& atmosphere ) const
{
	const VistaVector3D windVectorError = atmosphere.WindVector( rz2 ) - atmosphere.WindVector( rz1 ) - atmosphere.WindVectorGradient( rz1 ) * (float)( rz2 - rz1 );
	const double error                  = windVectorError[Vista::X] * n2[Vista::X] + windVectorError[Vista::Y] * n2[Vista::Y]; // Same as .Dot since z-component is zero
	return std::abs( error );
}

bool Simulation::CAdaptiveSolver::TryIncreaseTimeStep( )
{
	if( m_iDtAdaptationLevel <= 0 || !IncreasedDtFitsInRestOfTimeFrame( ) )
		return false;

	m_dCurrentDt *= 2;
	m_iDtAdaptationLevel--;
	return true;
}
bool Simulation::CAdaptiveSolver::TryDecreaseTimeStep( )
{
	if( m_iDtAdaptationLevel >= m_settings.adaptiveIntegration.iMaxAdaptationLevel )
		return false;

	m_dCurrentDt /= 2;
	m_iDtAdaptationLevel++;
	return true;
}

void Simulation::CAdaptiveSolver::UpdateTimeFramePortion( )
{
	m_iTimeFramePortion += (unsigned long int)1 << ( m_settings.adaptiveIntegration.iMaxAdaptationLevel - m_iDtAdaptationLevel );
	if( m_iTimeFramePortion >= m_iMaxDtPortion )
		m_iTimeFramePortion = 0;
}
bool Simulation::CAdaptiveSolver::IncreasedDtFitsInRestOfTimeFrame( ) const
{
	const unsigned long int newDtPortion    = (unsigned long int)1 << ( m_settings.adaptiveIntegration.iMaxAdaptationLevel - ( m_iDtAdaptationLevel - 1 ) );
	const unsigned long int leftDtPortion   = ( m_iMaxDtPortion - m_iTimeFramePortion );
	const unsigned long int moduloDtPortion = leftDtPortion % newDtPortion;
	return moduloDtPortion == 0;
	// return ((maxDtPortion - iTimeFramePortion) % newDtPortion) == 0;
}