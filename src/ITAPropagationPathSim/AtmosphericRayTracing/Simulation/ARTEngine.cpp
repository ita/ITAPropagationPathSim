#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTEngine.h>

// ITA includes
#include "AdaptiveSolver.h"

#include <ART_instrumentation.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/ODESolver/ODESolver.h>

// STD
#include <algorithm>
#include <cmath>

// OMP
#ifdef _OPENMP
#	include <omp.h>
#endif

using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::Simulation;

namespace ITAPropagationPathSim
{
	namespace AtmosphericRayTracing
	{
		namespace Simulation
		{
			class CWorker
			{
			private:
				std::shared_ptr<CRay> pRay;
				const IExternalWatcher& rExternalWatcher; //!< Reference to externally defined abort criterion.
				CAdaptiveSolver solver;

			public:
				inline CWorker( std::shared_ptr<CRay> ray, const Settings& simSettings, const IExternalWatcher& externalWatcher )
				    : pRay( ray )
				    , rExternalWatcher( externalWatcher )
				    , solver( CAdaptiveSolver( simSettings ) ) {};

			private:
				inline bool GroundReflectionOccured( const double& rz1, const double& rz2 ) const { return signbit( rz1 ) != signbit( rz2 ); };
				inline void InterpolateToReflectionPoint( const VistaVector3D& r1, const VistaVector3D& r2, const double& dt, VistaVector3D& rReflection,
				                                          double& dtReflection )
				{
					const VistaVector3D dr = r2 - r1;
					if( dr[Vista::Z] == 0 )
						ITA_EXCEPT_INVALID_PARAMETER( "Both points are already at same altitude." );
					const double portion = -r1[Vista::Z] / dr[Vista::Z];

					rReflection  = r1 + dr * (float)portion;
					dtReflection = dt * portion;
				};
				inline void ExtendRayByOnePeriod( )
				{
					const std::vector<int>& iReflectionIndices = pRay->ReflectionIndices( );
					const int reflectionOrder                  = pRay->ReflectionOrder( );

					if( reflectionOrder < 2 )
						ITA_EXCEPT_INVALID_PARAMETER( "Cannot extend a ray with a reflection order below 2." );

					const int idxStartReflection = iReflectionIndices[reflectionOrder - 2];
					const int idxEndReflection   = iReflectionIndices[reflectionOrder - 1];

					double tOffset          = pRay->at( idxEndReflection ).timeStamp - pRay->at( idxStartReflection ).timeStamp;
					VistaVector3D rXYOffset = pRay->at( idxEndReflection ).position - pRay->at( idxStartReflection ).position;

					for( int idx = idxStartReflection + 1; idx < idxEndReflection; idx++ )
					{
						const double t  = pRay->at( idx ).timeStamp + tOffset;
						VistaVector3D r = pRay->at( idx ).position + rXYOffset;
						VistaVector3D n = pRay->at( idx ).wavefrontNormal;
						r[Vista::Z]     = -r[Vista::Z];
						n[Vista::Z]     = -n[Vista::Z];

						pRay->Append( r, n, t );
						rExternalWatcher.ProcessRay( pRay );

						if( rExternalWatcher.AbortRequested( pRay ) )
							return;
					}

					const double t  = pRay->at( idxEndReflection ).timeStamp + tOffset;
					VistaVector3D r = pRay->at( idxEndReflection ).position + rXYOffset;
					VistaVector3D n = pRay->at( idxEndReflection ).wavefrontNormal;
					r[Vista::Z]     = -r[Vista::Z];
					n[Vista::Z]     = -n[Vista::Z];
					pRay->AppendReflection( r, n, t );
					rExternalWatcher.ProcessRay( pRay );
				};
				inline void ExtendRayPeriodically( )
				{
					while( !rExternalWatcher.AbortRequested( pRay ) )
						ExtendRayByOnePeriod( );
				};
				// Calculates the path using given atmosphere for initialized ray and returns its maximum altitude
				inline double CalculateRay( const ITAGeo::CStratifiedAtmosphere& atmosphere )
				{
					VistaVector3D r = pRay->LastPoint( );
					double rz       = r[Vista::Z];
					VistaVector3D n = pRay->LastWavefrontNormal( );
					double time     = pRay->LastTimeStamp( );

					VistaVector3D s = ODESolver::NormalToSlowness( n, rz, atmosphere );

					VistaVector3D rNew, sNew;
					double rzMax = rz;
					while( !rExternalWatcher.AbortRequested( pRay ) )
					{
						solver.Process( r, s, atmosphere, rNew, sNew );
						n               = ODESolver::SlownessToNormal( s );
						const double dt = solver.CurrentStepSize( );

						if( GroundReflectionOccured( r[Vista::Z], rNew[Vista::Z] ) )
						{
							if( std::abs( r[Vista::Z] ) < DBL_EPSILON ) // Last point was excactly at z = 0: => Interpolation would lead to a second point at ground
								pRay->AddLastPointToReflectionList( );
							else
							{
								VistaVector3D rGround;
								double dtGround;
								InterpolateToReflectionPoint( r, rNew, dt, rGround, dtGround );
								rGround[Vista::Z] = 0; // Making sure z-component is truely zero
								pRay->AppendReflection( rGround, n, time + dtGround );
								rExternalWatcher.ProcessRay( pRay );
							}

							if( rExternalWatcher.AbortRequested( pRay ) )
								return rzMax;

							if( pRay->ReflectionOrder( ) >= 2 ) // Ray is periodic
							{
								ExtendRayPeriodically( );
								return rzMax;
							}
						}

						r = rNew;
						s = sNew;
						time += dt;
						pRay->Append( r, n, time );
						rExternalWatcher.ProcessRay( pRay );

						rzMax = std::max( rzMax, (double)r[Vista::Z] );
					}
					return rzMax;
				};

			public:
				inline double TraceRay( const ITAGeo::CStratifiedAtmosphere& atmosphere )
				{
					const double rzMax = CalculateRay( atmosphere );
					rExternalWatcher.FinalizeRay( pRay );
					return rzMax;
				};
			};
		} // namespace Simulation
	} // namespace AtmosphericRayTracing
} // namespace ITAPropagationPathSim


CEngine::CEngine( bool bSuppressWarnings ) : m_bSuppressWarnings( bSuppressWarnings )
{
	pExternalWatcher = std::make_shared<CAbortAtMaxTime>( );
}

CEngine::CEngine( std::shared_ptr<IExternalWatcher> pWatcher, bool bSuppressWarnings ) : pExternalWatcher( pWatcher ), m_bSuppressWarnings( bSuppressWarnings ) {}

std::vector<std::shared_ptr<CRay>> CEngine::Run( const ITAGeo::CStratifiedAtmosphere& atmosphere, const VistaVector3D& m_v3SourcePosition,
                                                 const std::vector<VistaVector3D>& v3RayDirections ) const
{
	std::vector<std::shared_ptr<CRay>> rays;
	rays.reserve( v3RayDirections.size( ) );
	for( const VistaVector3D& v3Direction: v3RayDirections )
		rays.push_back( std::make_shared<CRay>( m_v3SourcePosition, v3Direction ) );

	TraceRays( atmosphere, rays );
	return rays;
}
bool CEngine::Run( const ITAGeo::CStratifiedAtmosphere& atmosphere, const std::set<std::shared_ptr<CRay>>& rays ) const
{
	return TraceRays( atmosphere, std::vector<std::shared_ptr<CRay>>( rays.begin( ), rays.end( ) ) );
}

bool CEngine::TraceRays( const ITAGeo::CStratifiedAtmosphere& atmosphere, const std::vector<std::shared_ptr<CRay>>& rays ) const
{
	std::shared_ptr<IExternalWatcher> simulationWatcher = pExternalWatcher;
	if( !simulationWatcher )
		ITA_EXCEPT_INVALID_PARAMETER( "Simulation-Engine: Interface to external watcher is not set. Cannot run simulation." );

	std::vector<double> vdMaxRayAltitude( rays.size( ) );
	if( settings.bMultiThreading )
	{
#pragma omp parallel for schedule( static )
		for( int idx = 0; idx < rays.size( ); idx++ )
		{
			if( rays[idx] == nullptr )
				continue;

			auto worker           = CWorker( rays[idx], settings, *simulationWatcher );
			vdMaxRayAltitude[idx] = worker.TraceRay( atmosphere );
		}
	}
	else
	{
		for( int idx = 0; idx < rays.size( ); idx++ )
		{
			if( rays[idx] == nullptr )
				continue;

			auto worker           = CWorker( rays[idx], settings, *simulationWatcher );
			vdMaxRayAltitude[idx] = worker.TraceRay( atmosphere );
		}
	}
	const double dMaxRayAltitude = *std::max_element( vdMaxRayAltitude.begin( ), vdMaxRayAltitude.end( ) );
	const bool bInsideBounds     = atmosphere.InsideBounds( dMaxRayAltitude );
	if( !m_bSuppressWarnings && !bInsideBounds )
		ART_WARN( "Simulation::Engine: Maximum altitude of atmosphere exceeded for at least one ray. Results should be treated with care." );
	return bInsideBounds;
}
