/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_PROPAGATIONPATHSIM_ART_EIGENRAYSEARCH_WORKER
#define IW_ITA_PROPAGATIONPATHSIM_ART_EIGENRAYSEARCH_WORKER

#include <ITAPropagationPathSim/Definitions.h>

// Vista includes
#include <VistaBase/VistaVector3D.h>

// ITA includes
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/AdaptiveRayGrid.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenraySettings.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/RayGrid.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Rays.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTEngine.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/Simulation/ARTSettings.h>

// STD
#include <memory>
#include <set>
#include <vector>


namespace ITAPropagationPathSim
{
	namespace AtmosphericRayTracing
	{
		namespace EigenraySearch
		{
			typedef std::shared_ptr<CRay> RayPtr;
			typedef std::vector<RayPtr> RayVector;

			class CReceiverData
			{
			public:
				const VistaVector3D& v3Position;
				const int iReflectionOrder;
				inline CReceiverData( const VistaVector3D& pos, const int reflOrder ) : v3Position( pos ), iReflectionOrder( reflOrder ) {};
				inline CReceiverData& operator=( const CReceiverData& other )
				{
					if( this != &other )
						*this = CReceiverData( other.v3Position, other.iReflectionOrder );
					return *this;
				};
			};
			typedef std::vector<CReceiverData> ReceiverDataVector;

			class CEigenraySearchWatcher : public Simulation::IExternalWatcher
			{
			private:
				ReceiverDataVector m_vReceiverData;
				double m_dTMax;
				int m_iMaxReflectionOrderRTAbort;
				bool m_bAbortOnReceiverDistIncrease;

			public:
				CEigenraySearchWatcher( const ReceiverDataVector& vReceiverData, const double& tMax, const bool bAbortOnReceiverDistIncrease );

			public:
				void InitRayReceiverDistances( const std::set<RayPtr>& rays ) const;

				virtual bool AbortRequested( const std::shared_ptr<CRay> pRay ) const;
				//! Interface function to Simulation::Engine: Updates the minimum receiver distance of the ray after each processing step
				virtual void ProcessRay( std::shared_ptr<CRay> ) const;
				//! Interface function to Simulation::Engine: Called after tracing a ray is finished.
				virtual void FinalizeRay( std::shared_ptr<CRay> ) const;
			};

			//! Basis for a worker which tracks the distance between rays and a receiver to find eigenrays
			class CWorkerBase
			{
			protected:
				std::shared_ptr<CEigenraySearchWatcher> m_pSimulationWatcher;
				Simulation::CEngine m_simulationEngine;
				RayTracingAbortSettings m_rayTracingAbortSettings;
				VistaVector3D m_v3SourcePosition;
				int m_iTotalNumRaysTraced          = 0;
				bool m_bRaysInsideAtmosphereBounds = true;

			private:
				VistaVector3D m_v3ReceiverPosition;
				VistaVector3D m_v3MirroredReceiverPosition;

			public:
				CWorkerBase( const VistaVector3D& sourcePosition, const VistaVector3D& receiverPosition, const Simulation::Settings& simSettings,
				             const RayTracingAbortSettings& abortSettings );
				// Returns the total number of rays traced by this worker
				inline int TotalNumRaysTraced( ) { return m_iTotalNumRaysTraced; };
				inline bool RaysInsideAtmosphereBounds( ) { return m_bRaysInsideAtmosphereBounds; };

			protected:
				inline const VistaVector3D& ReceiverPosition( ) const { return m_v3ReceiverPosition; };
				inline const VistaVector3D& MirroredReceiverPosition( ) const { return m_v3MirroredReceiverPosition; };
				const VistaVector3D& VirtualReceiverPosition( const int reflectionOrder ) const;

				void RunRayTracing( const ITAGeo::CStratifiedAtmosphere& atmosphere, const std::set<RayPtr>& rays );
				RayPtr FindMinimumDistanceRay( const std::set<RayPtr>& rays, const int reflectionOrder ) const;
			};

			//! Does a rough search for potential eigenrays directions for multiple reflection orders
			class CInitialWorker : public CWorkerBase
			{
			private:
				RayVector m_vpMinDistanceRays; //!< Vector containing the current ray of minimum receiver distance for each reflection order

			public:
				CInitialWorker( const VistaVector3D& sourcePosition, const VistaVector3D& receiverPosition, const Simulation::Settings& simSettings,
				                const RayTracingAbortSettings& abortSettings );

				std::vector<CRayGrid> Run( const ITAGeo::CStratifiedAtmosphere& atmosphere );

			private:
				CRayGrid InitRayGrid( const ITAGeo::CStratifiedAtmosphere& atmosphere );
				void FindMinimumDistanceRays( const std::set<RayPtr>& rays );
				std::vector<CRayGrid> FinalizeResult( const CRayGrid& initialRayGrid );
			};

			//! Based on a result of CInitialWorker, this worker adaptively reduces the directional limits while increasing the resolution to find an eigenray of a
			//! specific reflection order
			class CAdaptiveWorker : public CWorkerBase
			{
			private:
				CAdaptiveRayGrid m_adaptiveRayGrid;
				const int m_iActiveReflexionOrder;
				RayAdaptationSettings m_rayAdaptationSettings;
				float m_fReceiverRadius;

				RayPtr m_pMinDistanceRay;
				int m_iRayZoomingIterations = 0;

			public:
				CAdaptiveWorker( const CRayGrid& rayGrid, const VistaVector3D& receiverPosition, const Simulation::Settings& simSettings,
				                 const Settings& eigenraySettings, const int activeReflectionOrder );

				RayPtr Run( const ITAGeo::CStratifiedAtmosphere& atmosphere );

			private:
				inline const VistaVector3D& VirtualReceiverPosition( ) const { return CWorkerBase::VirtualReceiverPosition( m_iActiveReflexionOrder ); };

				bool EigenrayAccuracyReached( );
				bool AbortCriterionReached( );
				void PostProcessEigenray( const ITAGeo::CStratifiedAtmosphere& atmosphere );
				void SetEigenrayStatus( );
				void SetEigenrayEndPoint( );
				void CalculateEigenraySpreadingLoss( const ITAGeo::CStratifiedAtmosphere& atmosphere );
				double CalculateEnergyProportion( const double& A, const double& c, const VistaVector3D& vecN, const VistaVector3D& vecV );
			};
		} // namespace EigenraySearch
	} // namespace AtmosphericRayTracing
} // namespace ITAPropagationPathSim

#endif // IW_ITA_PROPAGATIONPATHSIM_ART_EIGENRAYSEARCH_WORKER