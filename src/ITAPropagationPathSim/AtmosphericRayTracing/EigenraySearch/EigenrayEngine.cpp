#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/EigenrayEngine.h>

// ITA includes
#include "Worker.h"

#include <ART_instrumentation.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/RayGrid.h>


using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch;


std::vector<std::shared_ptr<CRay>> CEngine::Run( const ITAGeo::CStratifiedAtmosphere& atmosphere, const VistaVector3D& sourcePosition,
                                                 const VistaVector3D& receiverPosition ) const
{
	std::vector<int> viTotalNumRaysTraced;
	return Run( atmosphere, sourcePosition, receiverPosition, viTotalNumRaysTraced );
}

std::vector<std::shared_ptr<CRay>> ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch::CEngine::Run( const ITAGeo::CStratifiedAtmosphere& atmosphere,
                                                                                                               const VistaVector3D& sourcePosition,
                                                                                                               const VistaVector3D& receiverPosition,
                                                                                                               std::vector<int>& viTotalNumRaysTraced ) const
{
	if( sourcePosition[Vista::Z] < 0.0 || receiverPosition[Vista::Z] < 0.0 )
		ART_ERROR( "EigenraySearch::Engine: Source and/or receiver position are below ground (z < 0). Resulting rays will be unreliable." );

	viTotalNumRaysTraced.clear( );
	Simulation::Settings initialSettings = simulationSettings;
	initialSettings.dIntegrationTimeStep *= 10;
	CInitialWorker initialRayTracing( sourcePosition, receiverPosition, initialSettings, eigenraySettings.rayTracing );
	std::vector<CRayGrid> initialRayGrids = initialRayTracing.Run( atmosphere );

	RayVector eigenrays;
	bool bAccuracyReached            = true;
	bool bRaysInsideAtmosphereBounds = true;
	for( int reflectionOrder = 0; reflectionOrder < initialRayGrids.size( ); reflectionOrder++ )
	{
		CAdaptiveWorker worker( initialRayGrids[reflectionOrder], receiverPosition, simulationSettings, eigenraySettings, reflectionOrder );
		eigenrays.push_back( worker.Run( atmosphere ) );
		viTotalNumRaysTraced.push_back( worker.TotalNumRaysTraced( ) );
		bAccuracyReached &= eigenrays.back( )->ReceiverHit( );
		bRaysInsideAtmosphereBounds &= worker.RaysInsideAtmosphereBounds( );
	}

	if( !bAccuracyReached )
		ART_WARN( "EigenraySearch::Engine: Could not find eigenray(s) with proper accuracy. Abort criterion reached." );
	if( !bRaysInsideAtmosphereBounds )
		ART_WARN( "EigenraySearch::Engine: Maximum altitude of atmosphere exceeded for at least one ray during eigenray search. Results should be treated with care." );

	return eigenrays;
}
