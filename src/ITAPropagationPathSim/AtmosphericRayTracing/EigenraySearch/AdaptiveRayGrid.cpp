#include <ITAException.h>
#include <ITAPropagationPathSim/AtmosphericRayTracing/EigenraySearch/AdaptiveRayGrid.h>
#include <math.h>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch;

#pragma region CONSTRUCTOR / INITIATION
EigenraySearch::CAdaptiveRayGrid::CAdaptiveRayGrid( const CRayGrid& rayGrid ) : CRayGrid( rayGrid )
{
	if( rayGrid.UniqueRays( ).size( ) <= 1 )
		ITA_EXCEPT_INVALID_PARAMETER( "Ray Grid must atleast contain 2 unique rays" );
	Init( );
}

void EigenraySearch::CAdaptiveRayGrid::Init( )
{
	m_vpNewRaysOfLastAdaptation.clear( );
	for( int idx = 1; idx < m_vdThetaDeg.size( ); idx++ )
	{
		double deltaTheta = m_vdThetaDeg[idx] - m_vdThetaDeg[idx - 1];
		if( m_dMaxDeltaTheta < deltaTheta )
			m_dMaxDeltaTheta = deltaTheta;
	}

	for( int idx = 1; idx < m_vdPhiDeg.size( ); idx++ )
	{
		double deltaPhi = m_vdPhiDeg[idx] - m_vdPhiDeg[idx - 1];
		if( deltaPhi < 0 )
			deltaPhi += 360;
		if( m_dMaxDeltaPhi < deltaPhi )
			m_dMaxDeltaPhi = deltaPhi;
	}
}

void CAdaptiveRayGrid::Reset( const CRayGrid& rayGrid )
{
	if( rayGrid.UniqueRays( ).size( ) <= 1 )
		ITA_EXCEPT_INVALID_PARAMETER( "Ray Grid must atleast contain 2 unique rays" );

	*this = rayGrid;
	Init( );
}
#pragma endregion


#pragma region RAY ZOOMING - public
void CAdaptiveRayGrid::ZoomIntoRay( const std::shared_ptr<CRay> pRay )
{
	if( !Contains( pRay ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray is not part of the adaptive ray grid." );

	SetToSurroundingGrid( pRay );
	DoubleRayResolution( );
}

void CAdaptiveRayGrid::ZoomIntoRay( const std::shared_ptr<CRay> pRay, const VistaVector3D& v3ReceiverPosition, const double& dThreshold )
{
	if( !Contains( pRay ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray is not part of the adapted grid." );

	const CRayReceiverData* receiverData = pRay->ReceiverDistanceData( v3ReceiverPosition );
	if( !receiverData )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray has no data about given receiver." );

	SetToSurroundingGrid( pRay );
	SetAdvancedRayGridLimits( pRay, receiverData->idxMinDist, v3ReceiverPosition, dThreshold );
	DoubleRayResolution( );
}

void CAdaptiveRayGrid::ZoomIntoRay( const std::shared_ptr<CRay> pZoomRay, const double& deltaTheta, const double& deltaPhi )
{
	const int idx = GetIndex( pZoomRay );
	if( idx < 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Given ray is not part of the adapted grid." );
	if( deltaTheta <= 0 || deltaPhi <= 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Delta angles must be positive numbers > 0." );

	const double thetaDeg = m_vdThetaDeg[IndexToThetaIndex( idx )];
	const double phiDeg   = m_vdPhiDeg[IndexToPhiIndex( idx )];
	const bool bIsPole    = IsPoleDirection( thetaDeg );

	int idxRayTheta = 1;
	int idxRayPhi   = 1;
	m_vdThetaDeg    = { thetaDeg - deltaTheta, thetaDeg, thetaDeg + deltaTheta };
	m_vdPhiDeg      = { phiDeg - deltaPhi, phiDeg, phiDeg + deltaPhi };
	if( m_vdThetaDeg.front( ) < 0 )
	{
		m_vdThetaDeg.erase( m_vdThetaDeg.begin( ) );
		idxRayTheta = 0;
	}
	if( m_vdThetaDeg.back( ) > 180 )
		m_vdThetaDeg.pop_back( );

	if( bIsPole )
	{
		idxRayPhi      = 0;
		const int nPhi = (int)std::ceil( 360.0 / deltaPhi );
		m_vdPhiDeg.resize( nPhi );
		// const double deltaPhi = 360.0 / nPhi;
		for( int idx = 0; idx < nPhi; idx++ )
			m_vdPhiDeg[idx] = idx * deltaPhi;
	}
	else
		m_vdPhiDeg = { phiDeg - deltaPhi, phiDeg, phiDeg + deltaPhi };

	RayMatrix newRayMatrix( NTheta( ), RayVector( NPhi( ) ) );
	m_vpNewRaysOfLastAdaptation.clear( );
	for( int idxTheta = 0; idxTheta < m_vdThetaDeg.size( ); idxTheta++ )
	{
		for( int idxPhi = 0; idxPhi < m_vdPhiDeg.size( ); idxPhi++ )
		{
			RayPtr pCurrentRay;
			if( idxRayTheta == idxTheta && idxRayPhi == idxPhi )
				pCurrentRay = pZoomRay;
			else if( IsPoleDirection( m_vdThetaDeg[idxTheta] ) && ( idxRayTheta == idxTheta || idxPhi > 0 ) )
			{
				if( idxTheta == idxRayTheta )
					pCurrentRay = pZoomRay;
				else
					pCurrentRay = newRayMatrix[idxTheta].front( );
			}
			else
			{
				pCurrentRay = std::make_shared<CRay>( m_v3SourcePos, m_vdThetaDeg[idxTheta], m_vdPhiDeg[idxPhi] );
				m_vpNewRaysOfLastAdaptation.insert( pCurrentRay );
			}

			newRayMatrix[idxTheta][idxPhi] = pCurrentRay;
		}
	}

	m_bCircularPhi = bIsPole;
	SetRayMatrix( newRayMatrix );
}
#pragma endregion

#pragma region NEW RAY LIMITS - private
void CAdaptiveRayGrid::SetAdvancedRayGridLimits( const std::shared_ptr<CRay> pRay, const int idxMinDist, const VistaVector3D& v3ReceiverPosition,
                                                 const double& dThreshold )
{
	if( NPhi( ) > 3 ) // There can be more than three rays if pRay is located at a pole
		return;

	std::vector<int> phiIdxVec   = FindAdvancedRayGridLimits1D( GetRaysWithSameTheta( pRay ), idxMinDist, v3ReceiverPosition, dThreshold );
	std::vector<int> thetaIdxVec = FindAdvancedRayGridLimits1D( GetRaysWithSamePhi( pRay ), idxMinDist, v3ReceiverPosition, dThreshold );
	FilterDirections( thetaIdxVec, phiIdxVec );
}

std::vector<int> CAdaptiveRayGrid::FindAdvancedRayGridLimits1D( const std::vector<std::shared_ptr<CRay>>& vpRayVector, const int idxMinDist,
                                                                const VistaVector3D& v3ReceiverPosition, const double& dThreshold ) const
{
	if( vpRayVector.size( ) == 1 )
		return { 0 };
	if( vpRayVector.size( ) == 2 )
		return { 0, 1 };
	if( vpRayVector.size( ) != 3 )
		ITA_EXCEPT_INVALID_PARAMETER( "Expected a vector with one to three rays." );

	const std::shared_ptr<CRay>& pRayMin = vpRayVector[1];
	const std::shared_ptr<CRay>& pRay1   = vpRayVector[0];
	const std::shared_ptr<CRay>& pRay2   = vpRayVector[2];
	const double tMin                    = pRayMin->at( idxMinDist ).timeStamp;
	const VistaVector3D v1               = ( v3ReceiverPosition - pRay1->AtTime( tMin ).position ).GetNormalized( );
	const VistaVector3D v2               = ( v3ReceiverPosition - pRay2->AtTime( tMin ).position ).GetNormalized( );
	const VistaVector3D vMin             = ( v3ReceiverPosition - pRayMin->at( idxMinDist ).position ).GetNormalized( );

	const float vProd1 = v1.Dot( vMin );
	const float vProd2 = v2.Dot( vMin );

	if( std::abs( vProd1 - vProd2 ) < dThreshold )
		return { 0, 1, 2 };
	if( vProd1 < vProd2 )
		return { 0, 1 };
	return { 1, 2 };
}
#pragma endregion


#pragma region DOUBLING RESOLUTION - private
void CAdaptiveRayGrid::DoubleRayResolution( )
{
	if( NRays( ) <= 1 )
		return;

	DoubleThetaResolution( );
	if( NPhi( ) <= 3 ) // Do not double phi resolution if grid is centered around pole
		DoublePhiResolution( );

	RayMatrix newRayMatrix( NTheta( ), RayVector( NPhi( ) ) );
	m_vpNewRaysOfLastAdaptation.clear( );
	RayVector::const_iterator iteratorOldRays = Rays( ).cbegin( );
	for( int idxTheta = 0; idxTheta < m_vdThetaDeg.size( ); idxTheta++ )
	{
		const bool isNewTheta = ( idxTheta % 2 ) == 1;
		for( int idxPhi = 0; idxPhi < m_vdPhiDeg.size( ); idxPhi++ )
		{
			const bool isNewPhi = ( idxPhi % 2 ) == 1;
			RayPtr pRay;
			if( IsPoleDirection( m_vdThetaDeg[idxTheta] ) )
				pRay = Matrix( )[idxTheta / 2].front( ); // Int-Division to convert to theta index of old matrix
			else if( isNewTheta || isNewPhi )
			{
				pRay = std::make_shared<CRay>( m_v3SourcePos, m_vdThetaDeg[idxTheta], m_vdPhiDeg[idxPhi] );
				m_vpNewRaysOfLastAdaptation.insert( pRay );
			}
			else
				pRay = *iteratorOldRays;

			newRayMatrix[idxTheta][idxPhi] = pRay;

			if( !isNewTheta && !isNewPhi )
				iteratorOldRays++;
		}
	}
	SetRayMatrix( newRayMatrix );
}

void CAdaptiveRayGrid::DoubleThetaResolution( )
{
	if( m_vdThetaDeg.size( ) < 2 )
		return;

	std::vector<double> newAngleVector;
	newAngleVector.reserve( 2 * m_vdThetaDeg.size( ) - 1 );
	newAngleVector.push_back( m_vdThetaDeg.front( ) );
	for( int idxAngle = 1; idxAngle < m_vdThetaDeg.size( ); idxAngle++ )
	{
		const double& angle1 = m_vdThetaDeg[idxAngle - 1];
		const double& angle2 = m_vdThetaDeg[idxAngle];
		newAngleVector.push_back( ( angle1 + angle2 ) / 2 );
		newAngleVector.push_back( angle2 );
	}

	m_vdThetaDeg = newAngleVector;
	m_dMaxDeltaTheta /= 2;
}

void CAdaptiveRayGrid::DoublePhiResolution( )
{
	if( m_vdPhiDeg.size( ) < 2 )
		return;

	std::vector<double> newAngleVector;
	newAngleVector.reserve( 2 * m_vdPhiDeg.size( ) - 1 + IsCircular( ) );
	newAngleVector.push_back( m_vdPhiDeg.front( ) );
	for( int idxAngle = 1; idxAngle < m_vdPhiDeg.size( ); idxAngle++ )
	{
		const double& angle1 = m_vdPhiDeg[idxAngle - 1];
		const double& angle2 = m_vdPhiDeg[idxAngle];
		double newAngle      = ( angle1 + angle2 ) / 2;
		if( angle2 < angle1 )
			newAngle = std::fmod( newAngle + 180, 360 );
		newAngleVector.push_back( newAngle );
		newAngleVector.push_back( angle2 );
	}
	if( IsCircular( ) )
	{
		double newAngle = ( m_vdPhiDeg.front( ) + m_vdPhiDeg.back( ) ) / 2;
		if( m_vdPhiDeg.front( ) < m_vdPhiDeg.back( ) )
			newAngle = std::fmod( newAngle + 180, 360 );
		newAngleVector.push_back( newAngle );
	}

	m_vdPhiDeg = newAngleVector;
	m_dMaxDeltaPhi /= 2;
}
#pragma endregion