#include "Worker.h"

// STD
#include <ART_instrumentation.h>
#include <algorithm>
#include <cmath>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;


#pragma region EigenraySearchWatcher
EigenraySearch::CEigenraySearchWatcher::CEigenraySearchWatcher( const ReceiverDataVector& vReceiverData, const double& tMax, const bool bAbortOnReceiverDistIncreas )
    : m_vReceiverData( vReceiverData )
    , m_dTMax( tMax )
    , m_bAbortOnReceiverDistIncrease( bAbortOnReceiverDistIncreas )
{
	int maxReflOrder = 0;
	for( const CReceiverData& receiverData: m_vReceiverData )
		std::max( maxReflOrder, receiverData.iReflectionOrder );

	m_iMaxReflectionOrderRTAbort = maxReflOrder + 1; // Abort Ray Tracing only if the reflection order is 2 above the maximum eigenray reflection order
}

void EigenraySearch::CEigenraySearchWatcher::InitRayReceiverDistances( const std::set<RayPtr>& vpRays ) const
{
	for( RayPtr pRay: vpRays )
	{
		if( !pRay )
			continue;

		for( const CReceiverData& receiverData: m_vReceiverData )
			pRay->UpdateMinimumReceiverDistance( receiverData.v3Position );
	}
}

bool EigenraySearch::CEigenraySearchWatcher::AbortRequested( const std::shared_ptr<CRay> pRay ) const
{
	return pRay->LastTimeStamp( ) >= m_dTMax || pRay->ReflectionOrder( ) > m_iMaxReflectionOrderRTAbort ||
	       m_bAbortOnReceiverDistIncrease && !pRay->ReceiverDistanceUpdatedOnLastTimeStep( );
}

void EigenraySearch::CEigenraySearchWatcher::ProcessRay( std::shared_ptr<CRay> pRay ) const
{
	for( const CReceiverData& receiverData: m_vReceiverData )
	{
		if( pRay->ReflectionOrder( ) == receiverData.iReflectionOrder || pRay->ReflectionOrder( ) == receiverData.iReflectionOrder - 1 )
			pRay->UpdateMinimumReceiverDistance( receiverData.v3Position );
	}
}

void EigenraySearch::CEigenraySearchWatcher::FinalizeRay( std::shared_ptr<CRay> pRay ) const
{
	pRay->FinalizeMinimumReceiverDistances( );
}
#pragma endregion


#pragma region WORKERBASE
EigenraySearch::CWorkerBase::CWorkerBase( const VistaVector3D& sourcePosition, const VistaVector3D& receiverPosition, const Simulation::Settings& simSettings,
                                          const RayTracingAbortSettings& abortSettings )
    : m_v3SourcePosition( sourcePosition )
    , m_v3ReceiverPosition( receiverPosition )
    , m_rayTracingAbortSettings( abortSettings )
    , m_v3MirroredReceiverPosition( receiverPosition )
    , m_simulationEngine( Simulation::CEngine( true ) ) // Suppress warnings in simulation engine to catch them in this one
{
	m_simulationEngine.settings            = simSettings;
	m_v3MirroredReceiverPosition[Vista::Z] = -m_v3MirroredReceiverPosition[Vista::Z];
}

const VistaVector3D& EigenraySearch::CWorkerBase::VirtualReceiverPosition( const int reflectionOrder ) const
{
	if( reflectionOrder % 2 != 0 ) // On uneven reflection order
		return m_v3MirroredReceiverPosition;
	return m_v3ReceiverPosition;
}
void EigenraySearch::CWorkerBase::RunRayTracing( const ITAGeo::CStratifiedAtmosphere& atmosphere, const std::set<RayPtr>& rays )
{
	m_pSimulationWatcher->InitRayReceiverDistances( rays );
	m_bRaysInsideAtmosphereBounds &= m_simulationEngine.Run( atmosphere, rays );
	m_iTotalNumRaysTraced += int( rays.size( ) );
}
EigenraySearch::RayPtr EigenraySearch::CWorkerBase::FindMinimumDistanceRay( const std::set<RayPtr>& rays, const int reflectionOrder ) const
{
	float dMin                       = FLT_MAX;
	const VistaVector3D& receiverPos = VirtualReceiverPosition( reflectionOrder );
	RayPtr pMinRay                   = nullptr;
	for( const RayPtr& pRay: rays )
	{
		const CRayReceiverData* distanceData = pRay->ReceiverDistanceData( receiverPos );
		if( distanceData && distanceData->distance < dMin )
		{
			dMin    = distanceData->distance;
			pMinRay = pRay;
		}
	}
	return pMinRay;
}
#pragma endregion


#pragma region INITIAL WORKER

EigenraySearch::CInitialWorker::CInitialWorker( const VistaVector3D& sourcePosition, const VistaVector3D& receiverPosition, const Simulation::Settings& simSettings,
                                                const RayTracingAbortSettings& abortSettings )
    : CWorkerBase( sourcePosition, receiverPosition, simSettings, abortSettings )
{
	ReceiverDataVector receiverData = { CReceiverData( ReceiverPosition( ), 0 ), CReceiverData( MirroredReceiverPosition( ), 1 ) };
	m_pSimulationWatcher =
	    std::make_shared<CEigenraySearchWatcher>( receiverData, m_rayTracingAbortSettings.maxTime, m_rayTracingAbortSettings.bAbortOnReceiverDistIncrease );
	m_simulationEngine.pExternalWatcher = m_pSimulationWatcher;
}


std::vector<CRayGrid> EigenraySearch::CInitialWorker::Run( const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	CRayGrid rayGrid = InitRayGrid( atmosphere );
	RunRayTracing( atmosphere, rayGrid.UniqueRays( ) );
	FindMinimumDistanceRays( rayGrid.UniqueRays( ) );
	return FinalizeResult( rayGrid );
}
CRayGrid EigenraySearch::CInitialWorker::InitRayGrid( const ITAGeo::CStratifiedAtmosphere& )
{
	return CEquiangularRayDistribution( m_v3SourcePosition, 7, 10 );
	// TODO: Set limits for additional directions according to atmosphere if possible
}
void EigenraySearch::CInitialWorker::FindMinimumDistanceRays( const std::set<RayPtr>& rays )
{
	// NOTE: For now this Worker can only find eigenrays up to reflection order 1
	// int maxReflOrder = 0;
	// for (const RayPtr & ray : rays)
	//{
	//	const CRayReceiverData* rayReceiverDistanceData = ray->ReceiverDistanceData( ReceiverPosition() );
	//	if (rayReceiverDistanceData)
	//		maxReflOrder = std::max( rayReceiverDistanceData->reflectionOrder, maxReflOrder );

	//	rayReceiverDistanceData = ray->ReceiverDistanceData( MirroredReceiverPosition() );
	//	if (rayReceiverDistanceData)
	//		maxReflOrder = std::max( rayReceiverDistanceData->reflectionOrder, maxReflOrder );
	//}
	// maxReflOrder = std::min(maxReflOrder, rayTracingAbortSettings.maxReflectionOrder);

	int maxReflOrder = std::min( 1, m_rayTracingAbortSettings.maxReflectionOrder );

	m_vpMinDistanceRays.resize( maxReflOrder + 1 );
	for( int reflOrder = 0; reflOrder <= maxReflOrder; reflOrder++ )
		m_vpMinDistanceRays[reflOrder] = FindMinimumDistanceRay( rays, reflOrder );
}
std::vector<CRayGrid> EigenraySearch::CInitialWorker::FinalizeResult( const CRayGrid& initialRayGrid )
{
	std::vector<CRayGrid> rayGridsOfReflectionOrder;
	for( RayPtr pRay: m_vpMinDistanceRays )
		rayGridsOfReflectionOrder.push_back( initialRayGrid.SurroundingGrid( pRay ).CopyWithNewRays( ) );

	return rayGridsOfReflectionOrder;
}
#pragma endregion


#pragma region ADAPTIVE WORKER

EigenraySearch::CAdaptiveWorker::CAdaptiveWorker( const CRayGrid& rayGrid, const VistaVector3D& receiverPosition, const Simulation::Settings& simSettings,
                                                  const Settings& eigenraySettings, const int activeReflectionOrder )
    : CWorkerBase( rayGrid.SourcePosition( ), receiverPosition, simSettings, eigenraySettings.rayTracing )
    , m_adaptiveRayGrid( rayGrid )
    , m_iActiveReflexionOrder( activeReflectionOrder )
    , m_rayAdaptationSettings( eigenraySettings.rayAdaptation )
{
	const float sourceReceiverDistance = ( VirtualReceiverPosition( ) - rayGrid.SourcePosition( ) ).GetLength( );
	m_fReceiverRadius                  = (float)tan( m_rayAdaptationSettings.accuracy.maxSourceReceiverAngle * M_PI / 180.0 ) * sourceReceiverDistance;
	m_fReceiverRadius                  = fmin( (float)m_rayAdaptationSettings.accuracy.maxReceiverRadius, m_fReceiverRadius );

	ReceiverDataVector receiverData = { CReceiverData( VirtualReceiverPosition( ), m_iActiveReflexionOrder ) };
	m_pSimulationWatcher =
	    std::make_shared<CEigenraySearchWatcher>( receiverData, m_rayTracingAbortSettings.maxTime, m_rayTracingAbortSettings.bAbortOnReceiverDistIncrease );
	m_simulationEngine.pExternalWatcher = m_pSimulationWatcher;
}

EigenraySearch::RayPtr EigenraySearch::CAdaptiveWorker::Run( const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	RunRayTracing( atmosphere, m_adaptiveRayGrid.UniqueRays( ) );
	m_pMinDistanceRay = FindMinimumDistanceRay( m_adaptiveRayGrid.UniqueRays( ), m_iActiveReflexionOrder );
	while( !EigenrayAccuracyReached( ) && !AbortCriterionReached( ) )
	{
		if( m_rayAdaptationSettings.advancedRayZooming.bActive )
			m_adaptiveRayGrid.ZoomIntoRay( m_pMinDistanceRay, VirtualReceiverPosition( ), m_rayAdaptationSettings.advancedRayZooming.threshold );
		else
			m_adaptiveRayGrid.ZoomIntoRay( m_pMinDistanceRay );

		RunRayTracing( atmosphere, m_adaptiveRayGrid.NewRaysOfLastAdaptation( ) );
		m_pMinDistanceRay = FindMinimumDistanceRay( m_adaptiveRayGrid.UniqueRays( ), m_iActiveReflexionOrder );
		m_iRayZoomingIterations++;
	}

	PostProcessEigenray( atmosphere );
	return m_pMinDistanceRay;
}


bool EigenraySearch::CAdaptiveWorker::EigenrayAccuracyReached( )
{
	const CRayReceiverData* receiverData = m_pMinDistanceRay->ReceiverDistanceData( VirtualReceiverPosition( ) );
	return ( receiverData && receiverData->distance <= m_fReceiverRadius );
}
bool ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch::CAdaptiveWorker::AbortCriterionReached( )
{
	return m_iRayZoomingIterations > m_rayAdaptationSettings.abort.maxNAdaptations ||
	       m_adaptiveRayGrid.MaxAngularResolution( ) < m_rayAdaptationSettings.abort.minAngleResolutionDeg;
}
void EigenraySearch::CAdaptiveWorker::PostProcessEigenray( const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	SetEigenrayStatus( );
	SetEigenrayEndPoint( );
	CalculateEigenraySpreadingLoss( atmosphere );
}
void ITAPropagationPathSim::AtmosphericRayTracing::EigenraySearch::CAdaptiveWorker::SetEigenrayStatus( )
{
	const bool bAccuracyReached = EigenrayAccuracyReached( );
	m_pMinDistanceRay->MarkAsEigenray( bAccuracyReached, m_iRayZoomingIterations );
}
void EigenraySearch::CAdaptiveWorker::SetEigenrayEndPoint( )
{
	const CRayReceiverData* receiverData = m_pMinDistanceRay->ReceiverDistanceData( VirtualReceiverPosition( ) );
	const int idxBeforeMin               = receiverData->idxMinDist;
	const VistaVector3D& rMin            = receiverData->posMinDist;

	if( m_pMinDistanceRay->NumPoints( ) < idxBeforeMin )
		ITA_EXCEPT_INVALID_PARAMETER( "Index for minimum distance out of bounds!" );

	if( m_pMinDistanceRay->NumPoints( ) == idxBeforeMin + 1 ) // Min dist point is endpoint
	{
		ART_WARN( "EigenraySearch::Engine: Ray end point is closest point to receiver. This usually happens if maximum propagation time is too low." );
		return;
	}

	// Interpolate to new point of minimum
	const VistaVector3D& r1   = m_pMinDistanceRay->at( idxBeforeMin ).position;
	const VistaVector3D& r2   = m_pMinDistanceRay->at( idxBeforeMin + 1 ).position;
	const float pathFraction  = ( rMin - r1 ).GetLength( ) / ( r2 - r1 ).GetLength( );
	CRayElement newRayElement = m_pMinDistanceRay->at( idxBeforeMin ).Interpolate( m_pMinDistanceRay->at( idxBeforeMin + 1 ), pathFraction );

	// Remove all points before and add new point
	m_pMinDistanceRay->resize( idxBeforeMin + 2 );
	m_pMinDistanceRay->back( ) = newRayElement;
}
void EigenraySearch::CAdaptiveWorker::CalculateEigenraySpreadingLoss( const ITAGeo::CStratifiedAtmosphere& atmosphere )
{
	const double zRef         = std::abs( m_pMinDistanceRay->SourcePoint( )[Vista::Z] );
	const VistaVector3D& nRef = m_pMinDistanceRay->InitialDirection( );

	const double& time             = m_pMinDistanceRay->back( ).timeStamp;
	const double zReceiver         = std::abs( m_pMinDistanceRay->back( ).position[Vista::Z] );
	const VistaVector3D& nReceiver = m_pMinDistanceRay->back( ).wavefrontNormal;

	const double dTheta     = 180.0 / M_PI * std::acos( nRef[Vista::Z] );
	const bool bCloseToPole = dTheta < 1.0 || dTheta > 179.0;
	if( bCloseToPole || m_adaptiveRayGrid.MaxDeltaTheta( ) > m_rayAdaptationSettings.accuracy.maxAngleForGeomSpreading || !m_adaptiveRayGrid.Is2D( ) )
	{
		double dDeltaPhi = m_rayAdaptationSettings.accuracy.maxAngleForGeomSpreading;

		// Lower phi resolution if close pole to avoid numerical cancellation
		const bool bIsPole = std::abs( nRef[Vista::X] ) < FLT_EPSILON && std::abs( nRef[Vista::Y] ) < FLT_EPSILON;
		if( bIsPole )
			dDeltaPhi = 5.0;
		else if( bCloseToPole )
			dDeltaPhi *= 10;

		m_adaptiveRayGrid.ZoomIntoRay( m_pMinDistanceRay, m_rayAdaptationSettings.accuracy.maxAngleForGeomSpreading, dDeltaPhi );
		auto tmpSimulationEngine     = Simulation::CEngine( std::make_shared<Simulation::CAbortAtMaxTime>( time ), true );
		tmpSimulationEngine.settings = m_simulationEngine.settings;
		tmpSimulationEngine.Run( atmosphere, m_adaptiveRayGrid.NewRaysOfLastAdaptation( ) );
	}
	else
		m_adaptiveRayGrid.SetToSurroundingGrid( m_pMinDistanceRay );

	const double surfaceReceiver           = m_adaptiveRayGrid.WavefrontSurface( time );
	const double cReceiver                 = atmosphere.SpeedOfSound( zReceiver );
	const VistaVector3D vReceiver          = atmosphere.WindVector( zReceiver );
	const double dEnergyProportionReceiver = CalculateEnergyProportion( surfaceReceiver, cReceiver, nReceiver, vReceiver );

	if( std::isinf( dEnergyProportionReceiver ) )
	{
		ART_WARN( "EigenraySearch::Engine : Numerical cancellation during spreading loss calculation.Spreading loss is unset( = -1 )." );
		return;
	}

	const double surfaceRef           = m_adaptiveRayGrid.WavefrontSurfaceReference( );
	const double cRef                 = atmosphere.SpeedOfSound( zRef );
	const VistaVector3D vRef          = atmosphere.WindVector( zRef );
	const double dEnergyProportionRef = CalculateEnergyProportion( surfaceRef, cRef, nRef, vRef );

	m_pMinDistanceRay->SetSpreadingLoss( std::sqrt( dEnergyProportionReceiver / dEnergyProportionRef ) );
}
double EigenraySearch::CAdaptiveWorker::CalculateEnergyProportion( const double& A, const double& c, const VistaVector3D& vecN, const VistaVector3D& vecV )
{
	const double dRelVToC     = (double)( vecN * vecV ) / c;
	const double vSquared     = vecV.GetLengthSquared( );
	const double dDenominator = A * ( 1 + dRelVToC ) * std::sqrt( 1 + 2 * dRelVToC + vSquared / ( c * c ) );
	return c / dDenominator;
}
#pragma endregion