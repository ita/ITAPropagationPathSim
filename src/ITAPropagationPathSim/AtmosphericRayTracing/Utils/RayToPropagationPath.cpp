#include <ITAPropagationPathSim/AtmosphericRayTracing/Utils/RayToPropagationPath.h>

// Vista includes
#include <VistaBase/VistaVector3D.h>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;

ITAGeo::CPropagationPath Utils::ToPropagationPath( const CRay& ray )
{
	ITAGeo::CPropagationPath propagationPath;
	propagationPath.reserve( ray.size( ) );
	VistaVector3D position, wavefrontNormal;

	propagationPath.push_back( std::make_shared<ITAGeo::CEmitterInhomogeneous>( ray.SourcePoint( ), ray.InitialDirection( ) ) );
	for( int idx = 1; idx < ray.size( ); idx++ )
	{
		position        = ray[idx].position;
		wavefrontNormal = ray[idx].wavefrontNormal;
		if( position[Vista::Z] < 0 )
		{
			position[Vista::Z]        = -position[Vista::Z];
			wavefrontNormal[Vista::Z] = -wavefrontNormal[Vista::Z];
		}

		if( idx == ray.size( ) - 1 )
		{
			auto pReceiver                   = std::make_shared<ITAGeo::CSensorInhomogeneous>( position, wavefrontNormal, ray[idx].timeStamp );
			pReceiver->dSpreadingLoss        = ray.SpreadingLoss( );
			pReceiver->bEigenray             = ray.IsEigenray( );
			pReceiver->bReceiverHit          = ray.ReceiverHit( );
			pReceiver->iRayZoomingIterations = ray.RayZoomingIterations( );
			propagationPath.push_back( pReceiver );
		}
		else if( ray.IsReflectionIdx( idx ) )
		{
			auto anchor          = std::make_shared<ITAGeo::CSpecularReflectionInhomogeneous>( position, wavefrontNormal, ray[idx].timeStamp );
			anchor->v3FaceNormal = VistaVector3D( 0, 0, 1 );
			propagationPath.push_back( anchor );
		}
		else
			propagationPath.push_back( std::make_shared<ITAGeo::CInhomogeneity>( position, wavefrontNormal, ray[idx].timeStamp ) );
	}

	return propagationPath;
}

ITAGeo::CPropagationPathList Utils::ToPropagationPath( const std::vector<std::shared_ptr<CRay>>& vpRays )
{
	ITAGeo::CPropagationPathList propagationPathList;
	propagationPathList.reserve( vpRays.size( ) );
	for( std::shared_ptr<CRay> pRay: vpRays )
	{
		if( !pRay )
			continue;

		propagationPathList.push_back( ToPropagationPath( *pRay ) );
	}

	return propagationPathList;
}
