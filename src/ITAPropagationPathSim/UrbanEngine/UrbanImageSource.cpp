#include <ITAPropagationPathSim\UrbanEngine\UrbanImageSource.h>


// Used Namespaces
using namespace ITAPropagationPathSim::MirrorImage;
using namespace ITAPropagationPathSim::UrbanEngine::MirrorImage;


// Typedefs
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

//---Constructor----------------------------------------------------------------------------------------------------------------------------

CSource::CSource( const ITAGeo::Urban::CModel& oUrbanModel, const int iEmitterImageOrder )
{
	// Copy to buildings vector
	for( int i = 0; i < oUrbanModel.GetNumBuildings( ); i++ )
	{
		auto oCurrentBuilding     = oUrbanModel.GetBuildings( )[i];
		std::string sBuildingName = oUrbanModel.GetBuildingNames( )[i];

		ITAGeo::Halfedge::CMeshModel* pCurrentBuildingCopy = new ITAGeo::Halfedge::CMeshModel( );
		pCurrentBuildingCopy->CopyFrom( *oCurrentBuilding );

		// Calculate mesh face normals
		CITAMesh* pMesh = pCurrentBuildingCopy->GetMesh( );
		pMesh->request_face_normals( );
		pMesh->update_face_normals( );

		// Add current building to member variable
		m_mpBuildings[sBuildingName] = pCurrentBuildingCopy;
	}

	// Initialize image root
	m_pImagesRoot = new CImageNode( ITAGeo::ORDER_0, iEmitterImageOrder, oUrbanModel );

	// Set max order member variable
	m_iMaxOrder = iEmitterImageOrder;

	// Get building name vector
	m_vsBuildingNames = oUrbanModel.GetBuildingNames( );
}

//---Destructor-----------------------------------------------------------------------------------------------------------------------------

CSource::~CSource( )
{
	delete m_pImagesRoot;

	for( std::string buildingName: m_vsBuildingNames )
	{
		delete m_mpBuildings[buildingName];
	}
}

//---Construct functions--------------------------------------------------------------------------------------------------------------------

void CSource::ConstructImages( std::shared_ptr<ITAGeo::CPropagationAnchor> pEmitter )
{
	// Zero-order "image" is equivalent to emitter anchor
	m_pImagesRoot->pImage->CopyFromAnchor( pEmitter );

	// Emitter anchor
	m_pStartAnchor = pEmitter;


	// Next-order (first order) requires recursive construction of images (aborts if image order is set to zero)
	ConstructImagesRecursive( m_pImagesRoot, GetImageOrder( ), ITAGeo::ORDER_1 );
}

void CSource::ConstructImagesRecursive( CImageNode* pParent, const int iMaxOrder, const int iCurrentOrder /*= 0 */ )
{
	// Recursion abortion criteria
	if( iCurrentOrder > iMaxOrder )
		return;

	// Iterate over all buildings
	for( std::string sBuildingName: m_vsBuildingNames )
	{
		CITAMesh* pCurrentMesh = m_mpBuildings[sBuildingName]->GetMesh( );


		// Iterate over all faces of current building mesh
		size_t n_face                 = 0;
		CITAMesh::ConstFaceIter cf_it = pCurrentMesh->faces_begin( );
		while( cf_it != pCurrentMesh->faces_end( ) )
		{
			CITAMesh::FaceHandle hFace( *cf_it++ );

			if( pParent->pParent == nullptr || ( hFace.idx( ) != pParent->pImage->iPolygonIndex ) || ( sBuildingName != pParent->m_sBuildingName ) )
			{
				CImageNode* pSibling = pParent->mpImageChildren[sBuildingName][n_face++];

				// Calculates next-order images from parent image based on mirror approach
				CalculateImage( pCurrentMesh, pParent->pImage, pSibling->pImage, hFace );

				// Recursive construction until max order is reached
				ConstructImagesRecursive( pSibling, iMaxOrder, iCurrentOrder + 1 );
			}
		}
	}
}

void CSource::ConstructReflectionPaths( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                        const ITAGeo::ECulling eCulling /* = NONE*/, const int iMaxOrder /* = -1 */ )
{
	if( iMaxOrder == -1 )
		ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, GetImageOrder( ), eCulling );
	else
		ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, iMaxOrder, eCulling );
}

void CSource::ConstructReflectionPathsOutside( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                               const int iMaxOrder )
{
	ConstructReflectionPaths( pDestinationAnchor, oPathList, ITAGeo::ECulling::BACKFACE, iMaxOrder );
}

void CSource::ConstructReflectionPathsInside( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                              const int iMaxOrder )
{
	ConstructReflectionPaths( pDestinationAnchor, oPathList, ITAGeo::ECulling::FRONTFACE, iMaxOrder );
}

void CSource::ConstructReflectionPathList( CImageNode* pRoot, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPathList& oPathList,
                                           const int iMaxOrder, const ITAGeo::ECulling eCulling, const int iOrder /* = 0 */ )
{
	// Construct path between current node and the destination
	ITAGeo::CPropagationPath oPath;
	oPath.resize( pRoot->pImage->iOrder + 2 );

	if( ( iOrder == -1 ) || ( pRoot->pImage->iOrder == iOrder ) )
	{
		// Add start and end anchor of path
		oPath[0]                 = m_pStartAnchor;
		oPath[oPath.size( ) - 1] = pDestination;

		// Add reflection points to path
		ConstructReflectionsPath( pRoot, pDestination, oPath, eCulling );

		// Add last anchor of path if path is not empty(and thus not a valid path)
		if( !oPath.empty( ) )
		{
			oPathList.push_back( oPath );
		}
	}


	// Consider children of node if current order is not iOrder and is smaller than the max order
	auto iCurrentOrder = pRoot->pImage->iOrder;
	if( ( iOrder != iCurrentOrder ) && ( iCurrentOrder < iMaxOrder ) )
	{
		for( std::string sBuildingName: m_vsBuildingNames )
		{
			for( auto pChild: pRoot->mpImageChildren[sBuildingName] )
			{
				ConstructReflectionPathList( pChild, pDestination, oPathList, iMaxOrder, eCulling, iOrder );
			}
		}
	}
}

void CSource::ConstructReflectionsPath( CImageNode* pNode, std::shared_ptr<const ITAGeo::CPropagationAnchor> pStartAnchor, ITAGeo::CPropagationPath& oPath,
                                        const ITAGeo::ECulling eCulling )
{
	int iFaceIndex = pNode->pImage->iPolygonIndex;

	// current node is root and thus is not reflected alongside a face
	if( iFaceIndex == -1 )
		return;

	// Get current mesh and the corresponding face handle
	auto pCurrentMesh = m_mpBuildings[pNode->m_sBuildingName]->GetMesh( );
	auto hFace        = pCurrentMesh->face_handle( iFaceIndex );

	VistaVector3D v3CurrentOrigin      = pStartAnchor->v3InteractionPoint;
	VistaVector3D v3CurrentDestination = pNode->pImage->v3InteractionPoint;

	VistaRay currentRay( v3CurrentOrigin, v3CurrentDestination - v3CurrentOrigin );

	VistaVector3D v3IntersectionPoint;
	bool bIntersects = ITAGeoUtils::RayFaceIntersectionTest( currentRay, pCurrentMesh, hFace, v3IntersectionPoint, eCulling );

	if( bIntersects )
	{
		// Set reflection anchor at intersection point
		std::shared_ptr<ITAGeo::CSpecularReflection> pReflectionAnchor = std::make_shared<ITAGeo::CSpecularReflection>( );
		pReflectionAnchor->v3InteractionPoint                          = v3IntersectionPoint;
		pReflectionAnchor->v3FaceNormal                                = pNode->pImage->v3MirrorNormal;
		pReflectionAnchor->iPolygonID                                  = pNode->pImage->iPolygonIndex;

		// Add reflection to path
		oPath[pNode->pImage->iOrder] = pReflectionAnchor;

		// Add further propagation anchors to path until root of image sources is reached (no further buildings)
		if( pNode->m_sBuildingName != "" )
		{
			ConstructReflectionsPath( pNode->pParent, pReflectionAnchor, oPath, eCulling );
		}
	}
	else
	{
		// No valid path
		oPath.clear( );
	}
}

//---Get functions--------------------------------------------------------------------------------------------------------------------------

int CSource::GetImageOrder( ) const
{
	return m_iMaxOrder;

	//// Deep tree traversal
	// CImageNode* pNode = m_pImagesRoot;
	// int nImageOrder = ITAGeo::ORDER_0;
	// while (pNode->HasSiblings())
	//{
	//	nImageOrder++;
	//	pNode = pNode->mpImageChildren[0][0][0];
	//}
	// return nImageOrder;
}

//===CImageNode====================================================================================================================================================

CSource::CImageNode::CImageNode( const int iOrder, const int iMaxOrder, const ITAGeo::Urban::CModel& oUrbanModel, CImageNode* pParent_ /* = nullptr */,
                                 const int iBuildingIndex /* = -1 */ )
    // CSourceEngine::CImageNode::CImageNode(const int iOrder, const int iBuildingIndex, CImageNode* pParent_ /* = nullptr */)
    : pParent( pParent_ )
{
	pImage         = std::make_shared<ITAGeo::CMirrorImage>( );
	pImage->iOrder = iOrder;

	// Set building name
	if( iBuildingIndex >= 0 )
		m_sBuildingName = oUrbanModel.GetBuildingNames( ).at( iBuildingIndex );
	else
		m_sBuildingName = ""; // the root of the image sources is linked to no building


	////Construct node tree
	size_t numBuildings = oUrbanModel.GetNumBuildings( );

	for( int it_building = 0; it_building < numBuildings; it_building++ )
	{
		// Current building name
		std::string sCurrentBuildingName = oUrbanModel.GetBuildingNames( ).at( it_building );


		// Get number of faces of current building mesh
		size_t iNumFaces =
		    oUrbanModel.GetBuildings( ).at( it_building )->ConstGetMesh( )->n_faces( ); //@todo: Reason for long intialization. Change call of number of faces

		if( iOrder < iMaxOrder )
		{
			if( iOrder == ITAGeo::ORDER_0 || it_building != iBuildingIndex )
			{
				for( int i = 0; i < iNumFaces; i++ )
					mpImageChildren[sCurrentBuildingName].push_back( new CImageNode( iOrder + 1, iMaxOrder, oUrbanModel, this, it_building ) );
			}
			else
			{
				// After first order we only create one image less because there is no image from the same face as parent
				for( int i = 0; i < iNumFaces - 1; i++ )
					mpImageChildren[sCurrentBuildingName].push_back( new CImageNode( iOrder + 1, iMaxOrder, oUrbanModel, this, it_building ) );
			}
		}
	}
}