#include <ITAPropagationPathSim/BaseEngine/BaseDiffraction.h>


using namespace ITAPropagationPathSim::BaseEngine;
using namespace ITAGeo;


CDiffractionPath::CDiffractionPath( ITAGeo::Halfedge::CMeshModelList pMeshModelList, const int iDiffractionOrder )
{
	m_iMaxDiffractionOrder = iDiffractionOrder;

	// Set mesh model list member variable //TODO: ersetzen durch kopie und Eingangsparameter auf const setzen
	m_vpMeshModelList = pMeshModelList;

	// Copy to mesh model map
	std::vector<Halfedge::CMeshModelShared> pMeshModels;
	for( int i = 0; i < pMeshModelList.GetNumMeshes( ); i++ )
	{
		// Calculate mesh normals
		CITAMesh* pMesh = m_vpMeshModelList[i]->GetMesh( );
		pMesh->request_face_normals( );
		pMesh->update_face_normals( );
		pMesh->request_halfedge_normals( );
		pMesh->request_vertex_normals( );
	}


	// Construct diffraction matrix
	ConstructDiffractionMatrix( );
}

void CDiffractionPath::ConstructDiffractionPaths( std::shared_ptr<CPropagationAnchor> pReceiverAnchor, CPropagationPathList& oPathList )
{
	// Set destination anchor
	m_pDestinationAnchor = pReceiverAnchor;


	// Filter illuminable tree candidates
	std::vector<std::shared_ptr<DiffractionEdge>> vpIllumDiffractionEdges;

	// Get destination point
	auto v3DestinationPoint = m_pDestinationAnchor->v3InteractionPoint;

	// Check every from the source illuminated edge and its children
	for( auto& kmDiffractionEdges: m_mvhIlluminatedEdges )
	{
		for( auto& pDiffractionEdge: kmDiffractionEdges.second )
		{
			RecursiveGetIlluminableDiffractionEdges( vpIllumDiffractionEdges, v3DestinationPoint, pDiffractionEdge );
		}
	}

	// Create path out of each illuminated diffraction edge
	for( auto& pDiffractionEdge: vpIllumDiffractionEdges )
	{
		//@Todo aer: recursive function for edge relations and start aperture relations

		// Because pDiffractionEdge only contains the edge illuminated by the receiver,
		// add the other edges unt�l the edge illuminated by the source is reached
		std::vector<std::shared_ptr<DiffractionEdge>> vpCurrentDiffractionPathEdges( pDiffractionEdge->iDiffractionOrder );
		while( pDiffractionEdge != nullptr )
		{
			// Add diffraction edge, so that the last element will be the edge illuminated by the receiver
			// and the first element will be the edge illuminated by the source
			vpCurrentDiffractionPathEdges[pDiffractionEdge->iDiffractionOrder - 1] = pDiffractionEdge;

			// Repeat loop with parent until no further parent is found(edge illuminated by source)
			pDiffractionEdge = pDiffractionEdge->parentEdge;
		}

		// Get corresponding vectors for calculation of aperture points

		// Path to be calculated
		auto pPath = new ITAGeo::CPropagationPath( );

		if( CalculateDiffractionPath( vpCurrentDiffractionPathEdges, pPath ) )
			oPathList.push_back( *pPath );
	}
}

bool CDiffractionPath::CalculateDiffractionPath( std::vector<std::shared_ptr<DiffractionEdge>>& vpDiffractionPathEdges, ITAGeo::CPropagationPath* pPath )
{
	// Preparation of the parameters for the calculation of the aperture points
	std::vector<float> vfApertureModifierOld;

	// Set initial modification to the half of the length of the edges(midpoints)
	for( int i = 0; i < vpDiffractionPathEdges.size( ); i++ )
	{
		vfApertureModifierOld.push_back( vpDiffractionPathEdges[i]->fLength / 2.0 );
	}


	// Lengths and differences between the current and the previous/next aperture points
	float fPrevToCurrLength, fNextToCurrLength;
	float fPrevToCurrApertureDiff, fNextToCurrApertureDiff;

	// iterate five times
	for( int iteration = 0; iteration < 5; iteration++ )
	{
		// Parameters for the calculation of the aperture points
		std::vector<float> vfEdgeRelations, vfApertureDiffs, vfSumInvPathLengths;

		for( int j = 0; j < vpDiffractionPathEdges.size( ); j++ )
		{
			auto pCurrentDiffractionEdge = vpDiffractionPathEdges[j];

			// Mesh list edge handle of current edge
			MeshListEdgeHandle hCurrentEdge = make_pair( pCurrentDiffractionEdge->sMeshModelName, pCurrentDiffractionEdge->hEdge );

			// Set aperture differences and edge direction relations
			if( j == 0 )
			{
				// Get length of subpath
				fPrevToCurrLength =
				    ( m_pOriginAnchor->v3InteractionPoint - pCurrentDiffractionEdge->v3StartPoint - vfApertureModifierOld[j] * pCurrentDiffractionEdge->v3Direction )
				        .GetLength( );
				// Get corresponding edge start difference
				fPrevToCurrApertureDiff = ( m_pOriginAnchor->v3InteractionPoint - pCurrentDiffractionEdge->v3StartPoint ) * pCurrentDiffractionEdge->v3Direction;
			}
			else
			{
				// Previous edge
				auto pPrevDiffractionEdge    = vpDiffractionPathEdges[j - 1];
				MeshListEdgeHandle hPrevEdge = make_pair( pPrevDiffractionEdge->sMeshModelName, pPrevDiffractionEdge->hEdge );

				// Get length of subpath from previous to current edge
				fPrevToCurrLength = fNextToCurrLength;
				// Get corresponding edge start difference
				fPrevToCurrApertureDiff = m_mdApertureStartDifferenceMap[hPrevEdge][hCurrentEdge];
			}
			if( j < vpDiffractionPathEdges.size( ) - 1 )
			{
				// Next edge
				auto pNextDiffractionEdge    = vpDiffractionPathEdges[j + 1];
				MeshListEdgeHandle hNextEdge = make_pair( pNextDiffractionEdge->sMeshModelName, pNextDiffractionEdge->hEdge );

				// Get length of subpath from current to next edge
				fNextToCurrLength = ( pNextDiffractionEdge->v3StartPoint + vfApertureModifierOld[j + 1] * pNextDiffractionEdge->v3Direction -
				                      pCurrentDiffractionEdge->v3StartPoint - vfApertureModifierOld[j] * pCurrentDiffractionEdge->v3Direction )
				                        .GetLength( );

				// Get corresponding edge start difference
				fNextToCurrApertureDiff = m_mdApertureStartDifferenceMap[hNextEdge][hCurrentEdge];

				vfEdgeRelations.push_back( m_mdEdgeDirMultiplicationMap[hNextEdge][hCurrentEdge] / fNextToCurrLength );
			}
			else
			{
				// Get length of subpath from current edge to destination point
				fNextToCurrLength =
				    ( m_pDestinationAnchor->v3InteractionPoint - pCurrentDiffractionEdge->v3StartPoint - vfApertureModifierOld[j] * pCurrentDiffractionEdge->v3Direction )
				        .GetLength( );

				// Get corresponding edge start and destionation point difference
				fNextToCurrApertureDiff = ( m_pDestinationAnchor->v3InteractionPoint - pCurrentDiffractionEdge->v3StartPoint ) * pCurrentDiffractionEdge->v3Direction;
			}

			vfSumInvPathLengths.push_back( 1 / fPrevToCurrLength + 1 / fNextToCurrLength );
			vfApertureDiffs.push_back( fPrevToCurrApertureDiff / fPrevToCurrLength + fNextToCurrApertureDiff / fNextToCurrLength );
		}

		// Calculation of the diffraction path with the containing aperture points
		std::vector<float> vfC, vfP, vfApertureModifierNew;
		vfC.push_back( vfSumInvPathLengths[0] );
		vfP.push_back( vfApertureDiffs[0] );
		for( int i = 0; i < vfEdgeRelations.size( ); i++ )
		{
			float fLambda = -vfEdgeRelations[i] / vfC[i];
			vfC.push_back( vfSumInvPathLengths[i + 1] + vfEdgeRelations[i] * fLambda );
			vfP.push_back( vfApertureDiffs[i + 1] - vfP[i] * fLambda );
		}

		// Calculate the adjustment factors for the aperture points
		vfApertureModifierNew.resize( vfP.size( ) );
		vfApertureModifierNew[vfApertureModifierNew.size( ) - 1] = vfP.back( ) / vfC.back( );
		// if (vfApertureModifierNew.back() < 0.0 || vfApertureModifierNew.back() > vpDiffractionPathEdges.back()->fLength)
		//	return false;

		for( int i = vfApertureModifierNew.size( ) - 2; i >= 0; i-- )
		{
			vfApertureModifierNew[i] = ( vfP[i] + vfEdgeRelations[i] * vfApertureModifierNew[i + 1] ) / vfC[i];

			// vfApertureModifierNew[j] must be in a valid range to create a valid anchor point
			//(between zero and the length of the corresponding edge)
			// if (vfApertureModifierNew[j] < 0.0 || vfApertureModifierNew[j] > vpDiffractionPathEdges[j]->fLength)
			//	return false;
		}

		vfApertureModifierOld = vfApertureModifierNew;
	}

	// Check if vfApertureModifier is in the range of the edge
	for( int i = 0; i < vfApertureModifierOld.size( ); i++ )
	{
		if( ( vfApertureModifierOld[i] < 0 ) || ( vfApertureModifierOld[i] > vpDiffractionPathEdges[i]->fLength ) )
			return false;
	}

	// Add origin as first anchor
	pPath->push_back( m_pOriginAnchor );

	// Create Diffraction anchor
	for( int i = 0; i < vfApertureModifierOld.size( ); i++ )
	{
		auto v3CurrentStartPoint = vpDiffractionPathEdges[i]->v3StartPoint;
		auto v3CurrentEdgeDir    = vpDiffractionPathEdges[i]->v3Direction;

		std::shared_ptr<ITAGeo::CITADiffractionWedgeApertureBase> pAperturePoint;

		// Todo: aer Choose between CITADiffractionOuterWedgeAperture and CITADiffractionInnerWedgeAperture (cross product of main face normal and main face halfedge,
		// multiplied with opposite face normal))
		bool bISOuterWedge = true;
		if( bISOuterWedge )
			pAperturePoint = std::make_shared<ITAGeo::CITADiffractionOuterWedgeAperture>( );
		else
			pAperturePoint = std::make_shared<ITAGeo::CITADiffractionInnerWedgeAperture>( );

		pAperturePoint->v3AperturePoint           = v3CurrentStartPoint + vfApertureModifierOld[i] * v3CurrentEdgeDir;
		pAperturePoint->v3VertextStart            = vpDiffractionPathEdges[i]->v3StartPoint;
		pAperturePoint->v3VertextEnd              = vpDiffractionPathEdges[i]->v3EndPoint;
		pAperturePoint->v3MainWedgeFaceNormal     = vpDiffractionPathEdges[i]->v3MainFaceNormal;
		pAperturePoint->v3OppositeWedgeFaceNormal = vpDiffractionPathEdges[i]->v3OppositeFaceNormal;
		pAperturePoint->iMainWedgeFaceID          = vpDiffractionPathEdges[i]->iMainWedgeFaceID;
		pAperturePoint->iOppositeWedgeFaceID      = vpDiffractionPathEdges[i]->iOppositeWedgeFaceID;

		pPath->push_back( pAperturePoint );
	}

	// Add destination as last anchor
	pPath->push_back( m_pDestinationAnchor );

	return true;
}

void CDiffractionPath::RecursiveGetIlluminableDiffractionEdges( std::vector<std::shared_ptr<DiffractionEdge>>& vpOutIllumEdges, const VistaVector3D& v3InOrigin,
                                                                std::shared_ptr<DiffractionEdge> pInParentEdge )
{
	// Add current edge to output vector if current edge and origin point can see each other
	auto pEdgeMesh = m_vpMeshModelList.GetMeshModel( pInParentEdge->sMeshModelName )->GetMesh( );
	if( IsEdgeIlluminated( *pEdgeMesh, pInParentEdge->hEdge, v3InOrigin ) )
		vpOutIllumEdges.push_back( pInParentEdge );

	// Further addition of illuminable diffraction edges
	for( auto& kmChildMeshModel: pInParentEdge->mChildren )
	{
		for( auto& pChildEdge: kmChildMeshModel.second )
		{
			RecursiveGetIlluminableDiffractionEdges( vpOutIllumEdges, v3InOrigin, pChildEdge );
		}
	}
}

void CDiffractionPath::ConstructDiffractionMatrix( )
{
	// get all diffraction edges
	std::vector<std::shared_ptr<DiffractionEdge>> vpDiffractionEdges;
	for( auto& pMeshModel: m_vpMeshModelList )
	{
		CITAMesh* pCurrentMesh                             = pMeshModel->GetMesh( );
		CITAMesh::ConstEdgeRangeSkipping hCurrentMeshEdges = pCurrentMesh->edges( );


		// Create diffraction edges for all edges used as keys in matrix
		for( const auto& hCurrentEdge: hCurrentMeshEdges )
		{
			// Get edge properties
			auto hHalfedge     = pCurrentMesh->halfedge_handle( hCurrentEdge, 0 );
			auto hFromVertex   = pCurrentMesh->from_vertex_handle( hHalfedge );
			auto hToVertex     = pCurrentMesh->to_vertex_handle( hHalfedge );
			auto hMainFace     = pCurrentMesh->face_handle( hHalfedge );
			auto hOppositeFace = pCurrentMesh->opposite_face_handle( hHalfedge );

			VistaVector3D v3FromPoint( pCurrentMesh->point( hFromVertex ).data( ) );
			VistaVector3D v3ToPoint( pCurrentMesh->point( hToVertex ).data( ) );

			VistaVector3D v3MainFaceNormal( pCurrentMesh->calc_face_normal( hMainFace ).data( ) );
			VistaVector3D v3OppositeFaceNormal( pCurrentMesh->calc_face_normal( hMainFace ).data( ) );

			VistaVector3D v3Direction = v3ToPoint - v3FromPoint;
			float fLength             = v3Direction.GetLength( );

			// Set diffraction edge
			auto pDiffractionEdge                  = std::make_shared<DiffractionEdge>( );
			pDiffractionEdge->sMeshModelName       = pMeshModel->GetName( );
			pDiffractionEdge->hEdge                = hCurrentEdge;
			pDiffractionEdge->v3StartPoint         = v3FromPoint;
			pDiffractionEdge->v3EndPoint           = v3ToPoint;
			pDiffractionEdge->v3MainFaceNormal     = v3MainFaceNormal;
			pDiffractionEdge->v3OppositeFaceNormal = v3OppositeFaceNormal;
			pDiffractionEdge->v3Direction          = v3Direction / fLength;
			pDiffractionEdge->fLength              = fLength;
			pDiffractionEdge->iMainWedgeFaceID     = hMainFace.idx( );
			pDiffractionEdge->iOppositeWedgeFaceID = hOppositeFace.idx( );

			// Add diffraction edge to vector
			vpDiffractionEdges.push_back( pDiffractionEdge );
		}
	}

	// Compare illumination for each diffraction edge
	for( auto pDiffractionEdgeStart: vpDiffractionEdges )
	{
		std::set<std::shared_ptr<DiffractionEdge>> vpIlluminatedEdges;

		auto oEdgeStart = make_pair( pDiffractionEdgeStart->sMeshModelName, pDiffractionEdgeStart->hEdge );
		for( auto pDiffractionEdgeEnd: vpDiffractionEdges )
		{
			auto oEdgeEnd = make_pair( pDiffractionEdgeEnd->sMeshModelName, pDiffractionEdgeEnd->hEdge );

			if( pDiffractionEdgeStart.get( ) == pDiffractionEdgeEnd.get( ) )
				continue;

			if( IsDiffractionEdgeIlluminated( *pDiffractionEdgeStart, *pDiffractionEdgeEnd ) )
			{
				vpIlluminatedEdges.insert( pDiffractionEdgeEnd );

				// Set relation maps for later calculation of aperture points
				m_mdEdgeDirMultiplicationMap[oEdgeStart][oEdgeEnd] = pDiffractionEdgeStart->v3Direction * pDiffractionEdgeEnd->v3Direction;
				m_mdEdgeDirMultiplicationMap[oEdgeEnd][oEdgeStart] = m_mdEdgeDirMultiplicationMap[oEdgeStart][oEdgeEnd];

				m_mdApertureStartDifferenceMap[oEdgeStart][oEdgeEnd] =
				    ( pDiffractionEdgeStart->v3StartPoint - pDiffractionEdgeEnd->v3StartPoint ) * pDiffractionEdgeEnd->v3Direction;
			}
		}

		// Add illuminable diffraction edges to corresponding key of diffraction matrix
		m_mDiffractionMatrix[oEdgeStart] = vpIlluminatedEdges;
	}
}

void CDiffractionPath::ConstructDiffractionTree( const CPropagationAnchor* pOrigin )
{
	// Set member variable
	m_pOriginAnchor              = std::make_shared<CPropagationAnchor>( pOrigin->v3InteractionPoint );
	m_pOriginAnchor->iAnchorType = pOrigin->iAnchorType;

	// Clear illuminated edges map
	m_mvhIlluminatedEdges.clear( );

	// Set start points for diffraction paths
	SetStartDiffractionEdges( );

	// Add further diffractions
	for( auto& kpDiffractionEdge: m_mvhIlluminatedEdges )
	{
		for( auto pParentDiffractionHalfedge: kpDiffractionEdge.second )
		{
			// Add diffractions from one halfedge to another
			RecursiveAddIlluminatedEdges( pParentDiffractionHalfedge );
		}
	}

	return;
}

void CDiffractionPath::RecursiveAddIlluminatedEdges( std::shared_ptr<DiffractionEdge>& pParentDiffractionEdge )
{
	CITAMesh* pParentMesh                    = m_vpMeshModelList.GetMeshModel( pParentDiffractionEdge->sMeshModelName )->GetMesh( );
	CITAMesh::HalfedgeHandle hParentHalfedge = pParentMesh->halfedge_handle( pParentDiffractionEdge->hEdge, 0 ); // Halfedge is needed for vertex points

	std::string sParentMeshModelName = pParentDiffractionEdge->sMeshModelName;
	CITAMesh::EdgeHandle hParentEdge = pParentDiffractionEdge->hEdge;

	for( auto& pChildDiffractionEdge: m_mDiffractionMatrix[make_pair( sParentMeshModelName, hParentEdge )] )
	{
		auto pChildEdgeCopy = std::make_shared<DiffractionEdge>( );
		AddChildDiffractionEdge( pChildEdgeCopy, pParentDiffractionEdge, pChildDiffractionEdge->hEdge, pChildDiffractionEdge->sMeshModelName );
		pChildEdgeCopy->v3StartPoint = pChildDiffractionEdge->v3StartPoint;
		pChildEdgeCopy->v3Direction  = pChildDiffractionEdge->v3Direction;
		pChildEdgeCopy->fLength      = pChildDiffractionEdge->fLength;

		// Add further diffraction edges
		if( pChildEdgeCopy->iDiffractionOrder < m_iMaxDiffractionOrder )
		{
			RecursiveAddIlluminatedEdges( pChildEdgeCopy );
		}
	}
}

void CDiffractionPath::AddChildDiffractionEdge( std::shared_ptr<DiffractionEdge>& pChildDiffractionEdge, std::shared_ptr<DiffractionEdge>& pParentDiffractionEdge,
                                                const CITAMesh::EdgeHandle& hChildEdge, const std::string& sMeshName )
{
	// Set parameter for child diffraction halfedge
	pChildDiffractionEdge->sMeshModelName    = sMeshName;
	pChildDiffractionEdge->hEdge             = hChildEdge;
	pChildDiffractionEdge->iDiffractionOrder = pParentDiffractionEdge->iDiffractionOrder + 1;
	pChildDiffractionEdge->dAngle            = 0; //@todo: add angle between faces (?)
	pChildDiffractionEdge->dAccumulatedAngle = pParentDiffractionEdge->dAccumulatedAngle + pChildDiffractionEdge->dAngle;

	// Add parent to child and child to parent
	pChildDiffractionEdge->parentEdge = pParentDiffractionEdge;
	pParentDiffractionEdge->mChildren[pChildDiffractionEdge->sMeshModelName].insert( pChildDiffractionEdge );
}

void CDiffractionPath::SetStartDiffractionEdges( )
{
	for( auto& kDiffractionEdge: m_mDiffractionMatrix )
	{
		auto sCurrentMeshModelName = kDiffractionEdge.first.first;
		auto hCurrentEdge          = kDiffractionEdge.first.second;
		auto pCurrentMesh          = m_vpMeshModelList.GetMeshModel( sCurrentMeshModelName )->GetMesh( );

		// Face handles of current edge handle
		auto hFace0 = pCurrentMesh->face_handle( pCurrentMesh->halfedge_handle( hCurrentEdge, 0 ) );
		auto hFace1 = pCurrentMesh->face_handle( pCurrentMesh->halfedge_handle( hCurrentEdge, 1 ) );

		// Face can be illuminated if scalar product of face normal and a vector from the emitter location to the face location is negative
		if( CanFaceBeIlluminated( *pCurrentMesh, hFace0, m_pOriginAnchor->v3InteractionPoint ) ||
		    CanFaceBeIlluminated( *pCurrentMesh, hFace1, m_pOriginAnchor->v3InteractionPoint ) )
		{
			if( IsEdgeIlluminated( *pCurrentMesh, hCurrentEdge, m_pOriginAnchor->v3InteractionPoint ) )
			{
				// Get edge properties
				auto hHalfedge   = pCurrentMesh->halfedge_handle( hCurrentEdge, 0 );
				auto hFromVertex = pCurrentMesh->from_vertex_handle( hHalfedge );
				auto hToVertex   = pCurrentMesh->to_vertex_handle( hHalfedge );
				VistaVector3D v3FromPoint( pCurrentMesh->point( hFromVertex ).data( ) );
				VistaVector3D v3ToPoint( pCurrentMesh->point( hToVertex ).data( ) );
				VistaVector3D v3Direction = v3ToPoint - v3FromPoint;
				float fLength             = v3Direction.GetLength( );

				std::shared_ptr<DiffractionEdge> pDiffractionEdge = std::make_shared<DiffractionEdge>( );
				pDiffractionEdge->sMeshModelName                  = sCurrentMeshModelName;
				pDiffractionEdge->hEdge                           = hCurrentEdge;
				pDiffractionEdge->iDiffractionOrder               = 1;
				pDiffractionEdge->v3StartPoint                    = v3FromPoint;
				pDiffractionEdge->v3Direction                     = v3Direction / fLength;
				pDiffractionEdge->fLength                         = fLength;

				m_mvhIlluminatedEdges[sCurrentMeshModelName].insert( pDiffractionEdge );
			}
		}
	}
}


// todo: move to ITAGeoUtils
bool CDiffractionPath::CanFaceBeIlluminated( const CITAMesh& oMesh, CITAMesh::FaceHandle hFace, const VistaVector3D& v3Origin )
{
	VistaVector3D v3FaceNormal( oMesh.calc_face_normal( hFace ).data( ) );
	VistaVector3D v3FaceCentroid( oMesh.calc_face_centroid( hFace ).data( ) );

	VistaVector3D v3EmitterFaceDirection = ( v3FaceCentroid - v3Origin ).GetNormalized( );

	float fDotProduct = v3FaceNormal.Dot( v3EmitterFaceDirection );

	if( fDotProduct < 0 + Vista::Epsilon )
		return true;
	else
		return false;
}

// todo: move to ITAGeoUtils
bool CDiffractionPath::CanEdgeIlluminateInDirection( const CITAMesh& oMesh, const CITAMesh::EdgeHandle hEdge, const VistaVector3D& v3Direction )
{
	// Corresponding halfedge
	auto hHalfedge = oMesh.halfedge_handle( hEdge, 0 );

	// Check first face for illuminability
	auto hFace = oMesh.face_handle( hHalfedge );
	VistaVector3D v3FaceNormal( oMesh.calc_face_normal( hFace ).data( ) );

	// Direction vector is in illumination range of first face
	if( v3FaceNormal * v3Direction >= 0 - Vista::Epsilon )
		return true;

	// Check second face for illuminability
	hFace = oMesh.opposite_face_handle( hHalfedge );
	v3FaceNormal.SetValues( ( oMesh.calc_face_normal( hFace ).data( ) ) );

	// Direction vector is in illumination range of second face
	if( v3FaceNormal * v3Direction >= 0 - Vista::Epsilon )
		return true;

	// Edge can't be illuminated in the given direction
	return false;
}

bool CDiffractionPath::IsDiffractionEdgeIlluminated( const DiffractionEdge& oStartEdge, const DiffractionEdge& oEndEdge )
{
	// Start edge mesh and end edge mesh
	auto pStartEdgeMesh = m_vpMeshModelList.GetMeshModel( oStartEdge.sMeshModelName )->GetMesh( );
	auto pEndEdgeMesh   = m_vpMeshModelList.GetMeshModel( oEndEdge.sMeshModelName )->GetMesh( );

	// Corresponding Halfedges
	auto pStartHalfedge = pStartEdgeMesh->halfedge_handle( oStartEdge.hEdge, 0 );
	auto pEndHalfedge   = pStartEdgeMesh->halfedge_handle( oEndEdge.hEdge, 0 );


	// Start and end point of edges
	VistaVector3D v3StartEdgeStartVertex( pStartEdgeMesh->point( pStartEdgeMesh->from_vertex_handle( pStartHalfedge ) ).data( ) );
	VistaVector3D v3StartEdgeEndVertex( pStartEdgeMesh->point( pStartEdgeMesh->to_vertex_handle( pStartHalfedge ) ).data( ) );
	VistaVector3D v3EndEdgeStartVertex( pEndEdgeMesh->point( pEndEdgeMesh->from_vertex_handle( pEndHalfedge ) ).data( ) );
	VistaVector3D v3EndEdgeEndVertex( pEndEdgeMesh->point( pEndEdgeMesh->to_vertex_handle( pEndHalfedge ) ).data( ) );
	VistaVector3D v3StartEdgeStartPoint( v3StartEdgeStartVertex + ITAConstants::EPS_F_L * ( v3StartEdgeEndVertex - v3StartEdgeStartVertex ) );
	VistaVector3D v3StartEdgeEndPoint( v3StartEdgeEndVertex + ITAConstants::EPS_F_L * ( v3StartEdgeStartVertex - v3StartEdgeEndVertex ) );
	VistaVector3D v3EndEdgeStartPoint( v3EndEdgeStartVertex + ITAConstants::EPS_F_L * ( v3EndEdgeEndVertex - v3EndEdgeStartVertex ) );
	VistaVector3D v3EndEdgeEndPoint( v3EndEdgeEndVertex + ITAConstants::EPS_F_L * ( v3EndEdgeStartVertex - v3EndEdgeEndVertex ) );

	// Check if edges are in illuminable range of each other
	if( !CanEdgeIlluminateInDirection( *pStartEdgeMesh, oStartEdge.hEdge, v3EndEdgeStartPoint - v3StartEdgeStartPoint ) // From start to end
	    && !CanEdgeIlluminateInDirection( *pStartEdgeMesh, oStartEdge.hEdge, v3EndEdgeEndPoint - v3StartEdgeStartPoint ) )
		return false;
	if( !CanEdgeIlluminateInDirection( *pEndEdgeMesh, oEndEdge.hEdge, v3StartEdgeStartPoint - v3EndEdgeStartPoint ) // From end to start
	    && !CanEdgeIlluminateInDirection( *pEndEdgeMesh, oEndEdge.hEdge, v3StartEdgeEndPoint - v3EndEdgeStartPoint ) )
		return false;

	// Number of steps. The higher the number, the higher the accuracy(minimum step size is one)
	const int numSteps = 3;
	VistaVector3D v3CurrentPoint;

	for( double i = 0; i <= 1; i += 1.0 / ( numSteps - 1 ) )
	{
		v3CurrentPoint = v3StartEdgeStartPoint + i * ( v3StartEdgeEndPoint - v3StartEdgeStartPoint );


		if( IsEdgeIlluminated( *pEndEdgeMesh, oEndEdge.hEdge, v3CurrentPoint ) )
		{
			return true;
		}
	}

	// Diffraction edges do not see each other
	return false;
}

bool CDiffractionPath::IsEdgeIlluminated( const CITAMesh& oEdgeMesh, const CITAMesh::EdgeHandle& hEdge, const VistaVector3D& v3Origin )
{
	// Halfedge handle and corresponding vertices
	auto hHalfedge    = oEdgeMesh.halfedge_handle( hEdge, 0 );
	auto hStartVertex = oEdgeMesh.from_vertex_handle( hHalfedge );
	auto hEndVertex   = oEdgeMesh.to_vertex_handle( hHalfedge );

	// Corresponding faces
	auto hFace0 = oEdgeMesh.face_handle( hHalfedge );
	auto hFace1 = oEdgeMesh.opposite_face_handle( hHalfedge );

	// Test if edge can not be illuminated from the origin point
	if( !CanFaceBeIlluminated( oEdgeMesh, hFace0, v3Origin ) && !CanFaceBeIlluminated( oEdgeMesh, hFace1, v3Origin ) )
		return false;


	// Corresponding points
	VistaVector3D oStartVertex( oEdgeMesh.point( hStartVertex ).data( ) );
	VistaVector3D oEndVertex( oEdgeMesh.point( hEndVertex ).data( ) );
	VistaVector3D oStartPoint( oStartVertex + 0.01 * ( oEndVertex - oStartVertex ) );
	VistaVector3D oEndPoint( oEndVertex + 0.01 * ( oStartVertex - oEndVertex ) );

	// Test if any face is between the edge vertex point and the emitter
	bool isEdgeIlluminated = true;
	for( auto& pMeshModel: m_vpMeshModelList )
	{
		// Current Mesh of current and its face handles
		CITAMesh* pCurrentMesh                  = pMeshModel->GetMesh( );
		CITAMesh::ConstFaceRangeSkipping hFaces = pCurrentMesh->faces( );

		for( CITAMesh::FaceHandle const& hFace: hFaces )
		{
			EIntersecting isIntersecting = ITAGeoUtils::IsLineIntersectingFace( v3Origin, oStartPoint, pCurrentMesh, hFace );

			// Intersecting point lies on the edge(at the end of the line between emitter and edge point). Face is not in the way of the edge, if other intersection point
			// is also on the edge or is not intersecting
			if( isIntersecting == EIntersecting::ATSTART || isIntersecting == EIntersecting::ATEND )
			{
				isIntersecting = ITAGeoUtils::IsLineIntersectingFace( v3Origin, oEndPoint, pCurrentMesh, hFace );
				if( isIntersecting == EIntersecting::BETWEEN )
				{
					isEdgeIlluminated = false;
					break;
				}
			}
			// Intersection point is between the edge and the emitter. There must be a non occuring intersection
			else if( isIntersecting == EIntersecting::BETWEEN )
			{
				isIntersecting = ITAGeoUtils::IsLineIntersectingFace( v3Origin, oEndPoint, pCurrentMesh, hFace );
				if( isIntersecting != EIntersecting::NOINTERSECTION )
				{
					isEdgeIlluminated = false;
					break;
				}
			}
		}
	}

	return isEdgeIlluminated;
}
