#include <ITAPropagationPathSim\BaseEngine\BaseImageSource.h>


// Used Namespaces
using namespace ITAPropagationPathSim::MirrorImage;
using namespace ITAPropagationPathSim::BaseEngine::MirrorImage;


// Typedefs
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

//---Constructor----------------------------------------------------------------------------------------------------------------------------

CSource::CSource( std::shared_ptr<const ITAGeo::Halfedge::CMeshModelList> vpMeshModelList, const int iSourceImageOrder )
{
	m_vpMeshModelList = vpMeshModelList;

	// Copy to mesh model vector
	for( int i = 0; i < vpMeshModelList->GetNumMeshes( ); i++ )
	{
		// Calculate mesh face normals
		CITAMesh* pMesh = m_vpMeshModelList->at( i )->GetMesh( );
		pMesh->request_face_normals( );
		pMesh->update_face_normals( );
	}

	// Initialize image root
	m_pImagesRoot = new CImageNode( ITAGeo::ORDER_0, iSourceImageOrder, *vpMeshModelList );

	// Set max order member variable
	m_iMaxOrder = iSourceImageOrder;
}

//---Destructor-----------------------------------------------------------------------------------------------------------------------------

CSource::~CSource( )
{
	delete m_pImagesRoot;
}

//---Construct functions--------------------------------------------------------------------------------------------------------------------

void CSource::ConstructImages( std::shared_ptr<ITAGeo::CPropagationAnchor> pEmitter )
{
	// Zero-order "image" is equivalent to emitter anchor
	m_pImagesRoot->pImage->CopyFromAnchor( pEmitter );

	// Emitter anchor
	m_pStartAnchor = pEmitter;


	// Next-order (first order) requires recursive construction of images (aborts if image order is set to zero)
	ConstructImagesRecursive( m_pImagesRoot, GetImageOrder( ), ITAGeo::ORDER_1 );
}

void CSource::ConstructImagesRecursive( CImageNode* pParent, const int iMaxOrder, const int iCurrentOrder /*= 0 */ )
{
	// Recursion abortion criteria
	if( iCurrentOrder > iMaxOrder )
		return;

	// Iterate over all mesh models
	for( auto& pMeshModel: *m_vpMeshModelList )
	{
		CITAMesh* pCurrentMesh = pMeshModel->GetMesh( );

		// Iterate over all faces of current mesh
		size_t n_face                 = 0;
		CITAMesh::ConstFaceIter cf_it = pCurrentMesh->faces_begin( );
		while( cf_it != pCurrentMesh->faces_end( ) )
		{
			CITAMesh::FaceHandle hFace( *cf_it++ );

			if( pParent->pParent == nullptr || ( hFace.idx( ) != pParent->pImage->iPolygonIndex ) || ( pMeshModel->GetName( ) != pParent->m_sMeshModelName ) )
			{
				CImageNode* pSibling = pParent->mpImageChildren[pMeshModel->GetName( )][n_face++];

				// Calculates next-order images from parent image based on mirror approach
				CalculateImage( pCurrentMesh, pParent->pImage, pSibling->pImage, hFace );

				// Recursive construction until max order is reached
				ConstructImagesRecursive( pSibling, iMaxOrder, iCurrentOrder + 1 );
			}
		}
	}
}

void CSource::ConstructReflectionPaths( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                        const ITAGeo::ECulling eCulling /* = NONE*/, const int iMaxOrder /* = -1 */ )
{
	if( iMaxOrder == -1 )
		ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, GetImageOrder( ), eCulling );
	else
		ConstructReflectionPathList( m_pImagesRoot, pDestinationAnchor, oPathList, iMaxOrder, eCulling );
}

void CSource::ConstructReflectionPathsOutside( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                               const int iMaxOrder )
{
	ConstructReflectionPaths( pDestinationAnchor, oPathList, ITAGeo::ECulling::BACKFACE, iMaxOrder );
}

void CSource::ConstructReflectionPathsInside( std::shared_ptr<ITAGeo::CPropagationAnchor> pDestinationAnchor, ITAGeo::CPropagationPathList& oPathList,
                                              const int iMaxOrder )
{
	ConstructReflectionPaths( pDestinationAnchor, oPathList, ITAGeo::ECulling::FRONTFACE, iMaxOrder );
}

void CSource::ConstructReflectionPathList( CImageNode* pRoot, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, ITAGeo::CPropagationPathList& oPathList,
                                           const int iMaxOrder, const ITAGeo::ECulling eCulling, const int iOrder /* = 0 */ )
{
	// Construct path between current node and the destination
	ITAGeo::CPropagationPath oPath;
	oPath.resize( pRoot->pImage->iOrder + 2 );

	if( ( iOrder == -1 ) || ( pRoot->pImage->iOrder == iOrder ) )
	{
		// Add start and end anchor of path
		oPath[0]                 = m_pStartAnchor;
		oPath[oPath.size( ) - 1] = pDestination;

		// Add reflection points to path
		ConstructReflectionsPath( pRoot, pDestination, oPath, eCulling );

		// Add last anchor of path if path is not empty(and thus not a valid path)
		if( !oPath.empty( ) )
		{
			oPathList.push_back( oPath );
		}
	}


	// Consider children of node if current order is not iOrder and is smaller than the max order
	auto iCurrentOrder = pRoot->pImage->iOrder;
	if( ( iOrder != iCurrentOrder ) && ( iCurrentOrder < iMaxOrder ) )
	{
		for( auto& pMeshModel: *m_vpMeshModelList )
		{
			for( auto pChild: pRoot->mpImageChildren[pMeshModel->GetName( )] )
			{
				ConstructReflectionPathList( pChild, pDestination, oPathList, iMaxOrder, eCulling, iOrder );
			}
		}
	}
}

void CSource::ConstructReflectionsPath( CImageNode* pNode, std::shared_ptr<const ITAGeo::CPropagationAnchor> pStartAnchor, ITAGeo::CPropagationPath& oPath,
                                        const ITAGeo::ECulling eCulling )
{
	int iFaceIndex = pNode->pImage->iPolygonIndex;

	// current node is root and thus is not reflected alongside a face
	if( iFaceIndex == -1 )
		return;

	// Get current mesh and the corresponding face handle
	auto pCurrentMesh = m_vpMeshModelList->GetMeshModel( pNode->m_sMeshModelName )->GetMesh( );
	auto hFace        = pCurrentMesh->face_handle( iFaceIndex );

	VistaVector3D v3CurrentOrigin      = pStartAnchor->v3InteractionPoint;
	VistaVector3D v3CurrentDestination = pNode->pImage->v3InteractionPoint;

	VistaRay currentRay( v3CurrentOrigin, v3CurrentDestination - v3CurrentOrigin );

	VistaVector3D v3IntersectionPoint;
	bool bIntersects = ITAGeoUtils::RayFaceIntersectionTest( currentRay, pCurrentMesh, hFace, v3IntersectionPoint, eCulling );

	if( bIntersects )
	{
		// Set reflection anchor at intersection point
		std::shared_ptr<ITAGeo::CSpecularReflection> pReflectionAnchor = std::make_shared<ITAGeo::CSpecularReflection>( );
		pReflectionAnchor->v3InteractionPoint                          = v3IntersectionPoint;
		pReflectionAnchor->v3FaceNormal                                = pNode->pImage->v3MirrorNormal;
		pReflectionAnchor->iPolygonID                                  = pNode->pImage->iPolygonIndex;

		// Add reflection to path
		oPath[pNode->pImage->iOrder] = pReflectionAnchor;

		// Add further propagation anchors to path until root of image sources is reached (no further buildings)
		if( pNode->m_sMeshModelName != "" )
		{
			ConstructReflectionsPath( pNode->pParent, pReflectionAnchor, oPath, eCulling );
		}
	}
	else
	{
		// No valid path
		oPath.clear( );
	}
}

//---Get functions--------------------------------------------------------------------------------------------------------------------------

int CSource::GetImageOrder( ) const
{
	return m_iMaxOrder;

	//// Deep tree traversal
	// CImageNode* pNode = m_pImagesRoot;
	// int nImageOrder = ITAGeo::ORDER_0;
	// while (pNode->HasSiblings())
	//{
	//	nImageOrder++;
	//	pNode = pNode->mpImageChildren[0][0][0];
	//}
	// return nImageOrder;
}

//===CImageNode====================================================================================================================================================

CSource::CImageNode::CImageNode( const int iOrder, const int iMaxOrder, const ITAGeo::Halfedge::CMeshModelList& vpMeshModelList, CImageNode* pParent_ /* = nullptr */,
                                 const int iMeshIndex /* = -1 */ )
    // CSourceEngine::CImageNode::CImageNode(const int iOrder, const int iBuildingIndex, CImageNode* pParent_ /* = nullptr */)
    : pParent( pParent_ )
{
	pImage         = std::make_shared<ITAGeo::CMirrorImage>( );
	pImage->iOrder = iOrder;

	// Set building name
	if( iMeshIndex >= 0 )
		m_sMeshModelName = vpMeshModelList[iMeshIndex]->GetName( );
	else
		m_sMeshModelName = ""; // the root of the image sources is linked to no building

	// Construct node tree
	size_t numMeshes = vpMeshModelList.size( );

	for( int it_mesh = 0; it_mesh < numMeshes; it_mesh++ )
	{
		// Current building name
		std::string sCurrentMeshModelName = vpMeshModelList[it_mesh]->GetName( );


		// Get number of faces of current building mesh
		size_t iNumFaces = vpMeshModelList[it_mesh]->GetMesh( )->n_faces( ); //@todo: Reason for long intialization. Change call of number of faces

		if( iOrder < iMaxOrder )
		{
			if( iOrder == ITAGeo::ORDER_0 || it_mesh != iMeshIndex )
			{
				for( int i = 0; i < iNumFaces; i++ )
					mpImageChildren[sCurrentMeshModelName].push_back( new CImageNode( iOrder + 1, iMaxOrder, vpMeshModelList, this, it_mesh ) );
			}
			else
			{
				// After first order we only create one image less because there is no image from the same face as parent
				for( int i = 0; i < iNumFaces - 1; i++ )
					mpImageChildren[sCurrentMeshModelName].push_back( new CImageNode( iOrder + 1, iMaxOrder, vpMeshModelList, this, it_mesh ) );
			}
		}
	}
}