/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2020-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Benchmarks used in the dissertation of J. Stienen
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>


using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

struct CScene
{
	inline CScene( const std::string& sName, shared_ptr<CEmitter> pEmitter, shared_ptr<CSensor> pSensor ) : sName( sName ), pEmitter( pEmitter ), pSensor( pSensor ) {};

	shared_ptr<CSensor> pSensor;
	shared_ptr<CEmitter> pEmitter;
	string sName;
	string sModelPath;
	int N = 1, O = 1, P = 1;
	CombinedModel::CPathEngine::CSimulationConfig oSimConf;
	CombinedModel::CPathEngine::CAbortionCriteria oAbort;
};

void run( CScene& );

int main( int, char** )
{
	cout << " - - - - -  Benchmarks used in the dissertation of J. Stienen  - - - - - " << endl << endl;

	// Scenes
	auto pVehicle01  = make_shared<CEmitter>( VistaVector3D( 2.487f, 2.985f, 1.235f ) );
	auto pReceiver01 = make_shared<CSensor>( VistaVector3D( 8.512f, 2.985f, 1.235f ) );

	CScene oScene1( "T-junction test", pVehicle01, pReceiver01 );
	oScene1.sModelPath           = "t_junction_test.skp";
	oScene1.O                    = 1e2;
	oScene1.P                    = 1e2;
	oScene1.oAbort.fDynamicRange = 94.0f;

	run( oScene1 );

	return 0;
}

void run( CScene& oScene )
{
	cout << " --- " << oScene.sName << " --- " << endl;

	// Geo input

	auto pMeshModelList = make_shared<ITAGeo::Halfedge::CMeshModelList>( );

	if( pMeshModelList->Load( oScene.sModelPath ) )
	{
		cout << "Successfully loaded geometry mesh from input file '" << oScene.sModelPath << "'" << endl;
		for( int i = 0; i < pMeshModelList->GetNumMeshes( ); i++ )
			cout << pMeshModelList->GetMeshModel( i )->GetName( ) << ": " << pMeshModelList->GetMeshModel( i )->GetNumFaces( ) << " faces" << endl;
		cout << endl;
	}
	else
	{
		cerr << "Could not load " << oScene.sModelPath << endl;
	}

	// Benchmark

	// Path finding

	ITAStopWatch sw;
	auto pPathEngine = shared_ptr<CombinedModel::CPathEngine>( );
	for( int n = 0; n < oScene.N; n++ )
	{
		pPathEngine = make_shared<CombinedModel::CPathEngine>( );
		sw.start( );
		pPathEngine->InitializePathEnvironment( pMeshModelList );
		sw.stop( );
	}
	cout << "Geometry pre-processing statistics: " << sw << endl;
	sw.reset( );

	for( int o = 0; o < oScene.O; o++ )
	{
		sw.start( );
		pPathEngine->UpdateTargetEntity( oScene.pSensor );
		pPathEngine->UpdateOriginEntity( oScene.pEmitter );
		sw.stop( );
	}
	cout << "Propagation tree (target entity location): " << sw << endl;
	sw.reset( );

	CPropagationPathList oPathList;
	for( int p = 0; p < oScene.P; p++ )
	{
		sw.start( );
		pPathEngine->ConstructPropagationPaths( oPathList );
		sw.stop( );
	}
	cout << "Pathfinder (origin entity location): " << sw << endl;
	sw.reset( );


	cout << endl;
}
