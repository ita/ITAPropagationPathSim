/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2020-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Benchmarks used in the dissertation of J. Stienen
 *
 */

#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <ITAPropagationPathSim/CombinedModel/PropagationEngine.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>


using namespace std;
using namespace ITAGeo;
using namespace ITAPropagationPathSim;

void preproc_stats( string );

int main( int, char** )
{
	cout << " - - - - -  Benchmarks used in the dissertation of J. Stienen  - - - - - " << endl << endl;

	preproc_stats( "1000_generated_buildings.skp" );
	preproc_stats( "t_junction_three_buildings.skp" );
	preproc_stats( "auralisation_test_square.skp" );
	preproc_stats( "residential_quarter.skp" );

	return 0;
}

void preproc_stats( string sInFile )
{
	// Geo input

	auto pMeshModelList = make_shared<ITAGeo::Halfedge::CMeshModelList>( );

	if( pMeshModelList->Load( sInFile ) )
	{
		cout << "Successfully loaded geometry mesh from input file '" << sInFile << "'" << endl;
		for( int i = 0; i < pMeshModelList->GetNumMeshes( ); i++ )
			cout << "\t+ " << pMeshModelList->GetMeshModel( i )->GetName( ) << ": " << pMeshModelList->GetMeshModel( i )->GetNumFaces( ) << " faces" << endl;
		cout << endl;
	}
	else
	{
		cerr << "Could not load " << sInFile << endl;
	}

	// Scenes

	struct CScene
	{
		inline CScene( const std::string& sName, shared_ptr<CEmitter> pEmitter, shared_ptr<CSensor> pSensor )
		    : sName( sName )
		    , pEmitter( pEmitter )
		    , pSensor( pSensor ) {};

		shared_ptr<CSensor> pSensor;
		shared_ptr<CEmitter> pEmitter;
		string sName;
	};

	auto pVehicle01   = make_shared<CEmitter>( VistaVector3D( 2.487f, 2.985f, 1.235f ) );
	pVehicle01->sName = "Vehicle location 01";

	auto pReceiver01   = make_shared<CSensor>( VistaVector3D( 8.512f, 2.985f, 1.235f ) );
	pReceiver01->sName = "Receiver location 01";

	std::vector<CScene> voScenes = {
		CScene( "SceneV01R01", pVehicle01, pReceiver01 ),
	};

	// Benchmark
	int N = 1000;

	int i = 1;
	for( auto& scene: voScenes )
	{
		cout << "* Starting " << scene.sName << " (" << i++ << "/" << voScenes.size( ) << ")" << endl;

		// Path finding

		ITAStopWatch sw;
		for( int n = 0; n < N; n++ )
		{
			auto pPathEngine = make_shared<CombinedModel::CPathEngine>( );
			sw.start( );
			pPathEngine->InitializePathEnvironment( pMeshModelList );
			sw.stop( );
		}
		cout << "Geometry pre-processing statistics: " << sw << endl;
	}
}
